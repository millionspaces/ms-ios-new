//
//  ManualPaymentInfoViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class ManualPaymentInfoViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var timerBarButton: UIBarButtonItem!
    @IBOutlet weak var instructionsMessageLabel: UILabel!
    @IBOutlet weak var accountHolderNameLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var branchNameLabel: UILabel!
    @IBOutlet weak var swiftCodeLabel: UILabel!
    
    
    // MARK: - Class variables
    private let MANUAL_PAYMENT_RESPONSE_STATUSES = (Success : "make your payment within 24 hours", HasPendingPayment : "pending payment", Failure : "failure")
    
    var chargeAmountStr: String!
    var orderId: Int!
    var bookingId: String!
    
    var alertController: UIAlertController?
    var timerAlertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addPaymentSessionTimerLabelToNavBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePaymentSessionTimerInProgress(_:)), name: NSNotification.Name(rawValue: Constants.PAYMEMT_SESSION_IN_PROGRESS), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePaymentSessionTimerExpiry), name: NSNotification.Name(rawValue: Constants.PAYMEMT_SESSION_IS_UP), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(popToHomeWhenAppGoesForeground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //PaymentSessionTimer.sharedTimer.resumePaymentSessionTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.removePaymentSessionTimerLabelFromNavBar()
        //PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func popToHomeWhenAppGoesForeground() {
        alertController?.dismiss(animated: false, completion: nil)
        timerAlertController?.dismiss(animated: false, completion: nil)
        self.popToRootViewController()
    }
    
    func configureView() {
        print("ManualPaymentInfoVC configureView")
        
        self.navigationItem.title = "Tentative Reservation"
        
        let instructionMessageAttributed = NSMutableAttributedString(string: "Please deposit payment of ")
        
        let fontColorAttribute = [NSAttributedString.Key.foregroundColor : ColorUtils.BOOKING_RESERVATION_STATUS_COLORS.CANCELLED]
        let paymentAmountAttributedStr = NSAttributedString(string: chargeAmountStr, attributes: fontColorAttribute)
        instructionMessageAttributed.append(paymentAmountAttributedStr)
        
        let fontBoldAttribute = [NSAttributedString.Key.font : UIFont.init(name: "HelveticaNeue-Medium", size: 14.0)]
        instructionMessageAttributed.addAttributes(fontBoldAttribute, range: NSRange.init(location: 26, length: chargeAmountStr.characters.count))
        
        instructionMessageAttributed.append(NSAttributedString.init(string: " to the below bank account. You have a 24-hour window to complete the transaction. Payment instructions will also be sent to your email address. Once the payment is made, please respond to the booking instructions email with proof of payment. It is only thereafter that your booking will be confirmed."))
        
        self.instructionsMessageLabel.attributedText = instructionMessageAttributed
        
        self.accountHolderNameLabel.text = Strings.MS_BANK_ACCOUNT_HOLDER_NAME
        self.accountNumberLabel.text = Strings.MS_BANK_ACCOUNT_NUMBER
        self.bankNameLabel.text = Strings.MS_BANK_NAME
        self.branchNameLabel.text = Strings.MS_BANK_BRANCH_NAME
        self.swiftCodeLabel.text = Strings.MS_BANK_ACCOUNT_SWIFT_CODE
    }
    
    func viewBookingDetails() {
        print("ManualPaymentInfoVC viewBookingDetails")
        self.performSegue(segueID: "BookingDetailsSegue")
    }
    
    @objc func handlePaymentSessionTimerInProgress(_ notification: Notification) {
        if let timerValue = notification.userInfo?["timer"] {
            print("ManualPaymentInfoVC handlePaymentSessionTimerInProgress | timerValue : \(timerValue)")
            self.setPaymentSessionTimerLabelText(text: timerValue as! String)
        }
    }
    
    @objc func handlePaymentSessionTimerExpiry() {
        alertController?.dismiss(animated: false, completion: nil)
        
        timerAlertController = UIAlertController(title: AlertMessages.ALERT_TITLE, message: AlertMessages.PAYMENT_SESSION_TIME_UP, preferredStyle: .alert)
        let returnHomeAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
            self.popToRootViewController()
        }
        timerAlertController!.addAction(returnHomeAction)
        self.present(timerAlertController!, animated: true, completion: nil)
    }
    
    func presentConfirmationAlert() {
        alertController = UIAlertController(title: AlertMessages.CONFIRM_TITLE, message: AlertMessages.TENTATIVE_RESERVATION_CONFIRMATION_MESSAGE, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertMessages.YES_ACTION, style: .default) { (alert: UIAlertAction) in
            self.performTentativeReservation()
        }
        let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
        alertController!.addAction(noAction)
        alertController!.addAction(yesAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func presentResponseAlert(status: String) {
        
        if status == MANUAL_PAYMENT_RESPONSE_STATUSES.Success {
            
            PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
            self.setPaymentSessionTimerLabelText(text: "00:00")
            timerAlertController?.dismiss(animated: false, completion: nil)
            
            alertController = UIAlertController(title: AlertMessages.SUCCESS_TITLE, message: AlertMessages.TENTATIVE_RESERVATION_SUCCESS_MESSAGE, preferredStyle: .alert)
            
            let returnToHomeAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
                self.popToRootViewController()
            }
            
            let viewHomeAction = UIAlertAction(title: "View Booking", style: .default) { (alert: UIAlertAction) in
                self.viewBookingDetails()
            }
            
            alertController!.addAction(returnToHomeAction)
            alertController!.addAction(viewHomeAction)
            
        }
        else if status == MANUAL_PAYMENT_RESPONSE_STATUSES.HasPendingPayment {
            
            timerAlertController?.dismiss(animated: false, completion: nil)
            
            alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.TENTATIVE_RESERVATION_EXISTING_PAYMENT_ERROR_MESSAGE, preferredStyle: .alert)
            
            let yesAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
                self.popToRootViewController()
            }
            alertController!.addAction(yesAction)
        }
        else {
            alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.TENTATIVE_RESERVATION_FAILURE_MESSAGE, preferredStyle: .alert)
            
            let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
            alertController!.addAction(noAction)
            
            let retryAction = UIAlertAction(title: AlertMessages.RETRY_ACTION, style: .default) { (alert: UIAlertAction) in
                self.performTentativeReservation()
            }
            alertController!.addAction(retryAction)
        }
        
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    func performTentativeReservation() {
        self.showActivityIndicator()
        
        WebServiceCall.makeTentativeReservation(withBookingID: orderId!, status: 1, successBlock: { (results: Any) in
            print("ManualPaymentInfoVC performTentativeReservation | results : \(results)")
            
            if let responseDic = results as? [String : Any] {
                
                if (responseDic["message"] as! String).contains(self.MANUAL_PAYMENT_RESPONSE_STATUSES.Success) {
                    self.presentResponseAlert(status: self.MANUAL_PAYMENT_RESPONSE_STATUSES.Success)
                }
                else if (responseDic["message"] as! String).contains(self.MANUAL_PAYMENT_RESPONSE_STATUSES.HasPendingPayment) {
                    self.presentResponseAlert(status: self.MANUAL_PAYMENT_RESPONSE_STATUSES.HasPendingPayment)
                }
                else {
                    self.presentResponseAlert(status: self.MANUAL_PAYMENT_RESPONSE_STATUSES.Failure)
                }
            }
            
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("ManualPaymentInfoVC performTentativeReservation | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    
    // MARK: - IBActions
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func returnToHomeButtonPressed(_ sender: UIButton) {
        presentConfirmationAlert()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookingDetailsSegue" {
            let bookingDetailsVC = segue.destination as! BookingDetailsViewController
            bookingDetailsVC.bookingId = self.orderId!
            bookingDetailsVC.isManualBooking = false
            bookingDetailsVC.isViewedAsGuest = true
            bookingDetailsVC.shouldReturnToPreviousScreen = false
        }
    }
    
    
}
