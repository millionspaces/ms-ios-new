//
//  CollectionViewEmbeddedCollectionViewCell.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class CollectionViewEmbeddedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    func setEmbeddedCollectionViewDatasourceDelegate <D: UICollectionViewDataSource & UICollectionViewDelegate> (dataSourceDelegate: D, identifier id: Int) {
        self.collectionView.dataSource = dataSourceDelegate
        self.collectionView.delegate = dataSourceDelegate
        self.collectionView.tag = id
        self.collectionView.allowsMultipleSelection = false
        self.collectionView.reloadData()
    }
}
