//
//  SpaceViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var eventSpaceTableView: UITableView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filtersButton: UIButton!
    @IBOutlet weak var optionsView: UIView! // bottom view for sort and filter buttons
    @IBOutlet weak var sortOptionsView: UIView! // background view with sort options subview
    @IBOutlet weak var constSortOptionsSubviewBottom: NSLayoutConstraint!
    
    // MARK: - Constants
    let SPACE_CELL_ID = "SpaceCell"
    let SORT_OPTIONS_VIEW_TAGS = (DISTANCE: 1, PRICE_LOW_TO_HIGH: 2, PRICE_HIGH_TO_LOW: 3)
    let SPACES_TABLE_CELL_TAGS = (IMAGE: 101, NAME: 200, ADDRESS_2: 201,  PARTICIPANT_COUNT: 202, STARTING_PRICE: 203, RATE_VIEW: 301, PARENT_VIEW: 401)
    
    // MARK: - Class properties / variables
    var filteredSpaces: [Space] = []
    var totalFilteredSpacesCount: Int = 0
    var currentPage: Int = 0
    var searchBar: UISearchBar?
    var alertController: UIAlertController?
    var selectedSortOption: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // NOTE - to avoid nav/status bars getting hidden in details screen effecting back to the screen on popViewController
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        UIApplication.shared.isStatusBarHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToSpacesFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        //self.constOptionViewBottom.constant = 0.0
        
        initSearchBar()
        
        if FilterManager.spaceFilter.shouldRefreshSpacesListWithFilters {
            self.currentPage = 0
            self.filteredSpaces.removeAll()
            loadFilteredSpaces()
        }
        self.configureSortOptionsViewSelection()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        if let search = self.searchBar {
            search.removeFromSuperview()
            self.searchBar = nil
        }
    }
    
    /*
     func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
     return true
     }*/
    
    @objc func returnedToSpacesFromBackground() {
        if Utils.didPassDuration(durationInHours: 0.5) {
            self.alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = Strings.MS_TITLE
        self.sortOptionsView.isHidden = true
        self.constSortOptionsSubviewBottom.constant = -160.0
    }
    
    func initSearchBar() {
        self.searchBar = UISearchBar()
        self.searchBar?.frame = CGRect.init(x: 10.0, y: 0.0, width: Utils.SCREEN_WIDTH - 10.0, height: 44.0)
        self.searchBar?.searchBarStyle = UISearchBar.Style.default
        self.searchBar?.backgroundColor = ColorUtils.MS_DARK_BLUE_COLOR
        self.searchBar?.tintColor = UIColor.lightGray
        self.searchBar?.keyboardAppearance = .dark
        self.searchBar?.showsCancelButton = true
        self.searchBar?.placeholder = Strings.SEARCH
        self.searchBar?.enablesReturnKeyAutomatically = false
        self.searchBar?.delegate = self
    }
    
    func loadFilteredSpaces() {
        print("SpaceViewController loadFilteredSpaces | filters: \(FilterManager.retrieveFiltersForWebRequest())")
        
        showActivityIndicator()
        WebServiceCall.retrieveSpaces(withPageNumber: currentPage, successBlock: { (results: Any) in
            if let _results = results as? [String : Any] {
                print("SpaceViewController loadFilteredSpaces | _results: \(_results)")
                
                self.totalFilteredSpacesCount = _results["count"] as! Int
                let arrSpaceResults = _results["spaces"] as! [[String : Any]]
                
                for dic in arrSpaceResults {
                    let space = Space()
                    space.initSpace(withDictionary: dic)
                    self.filteredSpaces.append(space)
                }
                
                DispatchQueue.main.sync(execute: {
                    let isCurrentPage_0 = (self.currentPage == 0) ? true : false
                    if self.totalFilteredSpacesCount != self.filteredSpaces.count {
                        self.currentPage += 1
                    }
                    self.eventSpaceTableView.reloadData()
                    
                    if isCurrentPage_0 && self.totalFilteredSpacesCount > 0 {
                        self.eventSpaceTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
                    }
                    
                    if self.totalFilteredSpacesCount > 0 {
                        self.navigationItem.title = self.totalFilteredSpacesCount == 1 ? "1 Space" : "\(String(self.totalFilteredSpacesCount)) Spaces"
                        self.eventSpaceTableView.isHidden = false
                        self.sortButton.isUserInteractionEnabled = true
                    }
                    else {
                        self.navigationItem.title = Strings.MS_TITLE
                        self.eventSpaceTableView.isHidden = true
                        self.sortButton.isUserInteractionEnabled = false
                    }
                    
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    self.addSpacesReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    self.eventSpaceTableView.reloadData()
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("SpaceViewController loadFilteredSpaces | failure | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.eventSpaceTableView.reloadData()
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpacesReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpacesReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpacesReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func testSpaces() { // NOTE: - Used for testing purposes
        print("")
        for space in self.filteredSpaces {
            print("SpaceVC testSpaces | space --> name : \(space.name!) | eventTypes count : \(space.eventTypes.count) | seatingArrangements count : \(space.seatingArrangements.count)")
        }
        print("")
    }
    
    func addSpacesReloadAlert(message: String) {
        self.alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.loadFilteredSpaces()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        self.alertController!.addAction(dismissAction)
        self.alertController!.addAction(reloadAction)
        self.present(self.alertController!, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if self.sortOptionsView.frame.contains(location) {
            hideSortOptionsView()
        }
    }
    
    
    // MARK: - IBActions
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if let search = self.searchBar {
            self.navigationController?.navigationBar.addSubview(search)
            search.becomeFirstResponder()
        }
    }
    
    @IBAction func filtersButtonPressed(_ sender: UIButton) {
        performSegue(segueID: Constants.SEGUE_TO_FILTER_SCREEN)
    }
    
    @IBAction func sortButtonPressed(_ sender: UIButton) {
        showSortOptionsView()
    }
    
    @IBAction func sortOptionButtonPressed(_ sender: UIButton) {
        if sender.tag == SORT_OPTIONS_VIEW_TAGS.DISTANCE {
            if FilterManager.spaceFilter.location != nil {
                FilterManager.spaceFilter.sortBy = nil
            }
            else {
                self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.LOCATION_SORT_WARNING_MESSAGE)
                return
            }
        }
        else if sender.tag == SORT_OPTIONS_VIEW_TAGS.PRICE_LOW_TO_HIGH {
            FilterManager.spaceFilter.sortBy = StaticData.FILTER_SORT_OPTIONS.PLf
        }
        else if sender.tag == SORT_OPTIONS_VIEW_TAGS.PRICE_HIGH_TO_LOW {
            FilterManager.spaceFilter.sortBy = StaticData.FILTER_SORT_OPTIONS.PHf
        }
        
        configureSortOptionsViewSelection()
        hideSortOptionsView()
        
        if FilterManager.spaceFilter.location != nil || FilterManager.spaceFilter.sortBy != nil {
            currentPage = 0
            self.filteredSpaces.removeAll()
            loadFilteredSpaces()
        }
    }
    
    
    // MARK: - Handling sort options view visibility
    
    func configureSortOptionsViewSelection() {
        if let sortBy = FilterManager.spaceFilter.sortBy {
            if sortBy == StaticData.FILTER_SORT_OPTIONS.PLf {
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.PRICE_LOW_TO_HIGH) as! UIButton).isSelected = true
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.DISTANCE) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.PRICE_HIGH_TO_LOW) as! UIButton).isSelected = false
            }
            else {
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.PRICE_HIGH_TO_LOW) as! UIButton).isSelected = true
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.DISTANCE) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.PRICE_LOW_TO_HIGH) as! UIButton).isSelected = false
            }
        }
        else {
            if FilterManager.spaceFilter.location != nil {
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.DISTANCE) as! UIButton).isSelected = true
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.PRICE_LOW_TO_HIGH) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.PRICE_HIGH_TO_LOW) as! UIButton).isSelected = false
            }
            else {
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.DISTANCE) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.PRICE_LOW_TO_HIGH) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.PRICE_HIGH_TO_LOW) as! UIButton).isSelected = false
            }
        }
    }
    
    func showSortOptionsView() {
        self.sortOptionsView.isHidden = false
        self.constSortOptionsSubviewBottom.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideSortOptionsView() {
        self.constSortOptionsSubviewBottom.constant = -160.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        delay(0.5) {
            self.sortOptionsView.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


// MARK: - UIScrollViewDelegate
extension SpaceViewController {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        if let search = self.searchBar, search.isFirstResponder {
//            self.searchBar!.endEditing(true)
//            self.searchBar!.removeFromSuperview()
//        }
//
//        self.constOptionViewBottom.constant = -50.0
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        self.constOptionViewBottom.constant = 0.0
//        UIView.animate(withDuration: 0.5, delay: 0.7, options: UIView.AnimationOptions.allowUserInteraction, animations: {
//            self.view.layoutIfNeeded()
//        }, completion: nil)
    }
}


// MARK: - UISearchBarDelegate
extension SpaceViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.runSearch(keyword: searchBar.text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar!.endEditing(true)
        self.searchBar!.removeFromSuperview()
    }
    
    func runSearch(keyword: String?) {
        self.searchBar!.endEditing(true)
        self.searchBar!.removeFromSuperview()
        FilterManager.spaceFilter.searchKeyword = (keyword != nil) ? "\(keyword!)" : nil
        currentPage = 0
        self.filteredSpaces.removeAll()
        loadFilteredSpaces()
    }
}


// MARK: - UITableViewDataSource
extension SpaceViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredSpaces.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SPACE_CELL_ID, for: indexPath)
        let spaceAtIndex  = self.filteredSpaces[indexPath.row]
        
        if let parentView = cell.viewWithTag(SPACES_TABLE_CELL_TAGS.PARENT_VIEW) {
            parentView.layer.shadowColor = UIColor.black.cgColor
            parentView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
            parentView.layer.shadowRadius = 2
            parentView.layer.shadowOpacity = 0.5
        }
        
        (cell.viewWithTag(SPACES_TABLE_CELL_TAGS.NAME) as? UILabel)?.text = spaceAtIndex.name!
        (cell.viewWithTag(SPACES_TABLE_CELL_TAGS.ADDRESS_2) as? UILabel)?.text = spaceAtIndex.addressLine2 ?? spaceAtIndex.name!
        (cell.viewWithTag(SPACES_TABLE_CELL_TAGS.PARTICIPANT_COUNT) as? UILabel)?.text = String(describing: spaceAtIndex.participantCount!)
        let formattedNumber = Utils.formatPriceValues(price: spaceAtIndex.ratePerHour!)
        (cell.viewWithTag(SPACES_TABLE_CELL_TAGS.STARTING_PRICE) as? UILabel)?.text = "LKR \(formattedNumber)"
        
        // ImageView
        let imageView = cell.viewWithTag(SPACES_TABLE_CELL_TAGS.IMAGE) as! UIImageView
        imageView.sd_setImage(with: URL(string: (NetworkConfiguration.IMAGE_BASE_URL + spaceAtIndex.thumbnailImage)), completed: nil)
        
        //public static final String IMAGE_PLACEHOLDER_FACTOR = "c_scale,e_blur:1500,q_10,w_200/";
        //public static final String IMAGE_PLACEHOLDER_FACTOR_WINDOW = "c_scale,w_";
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SpaceViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if AuthenticationManager.isUserAuthenticated {
            FilterManager.spaceFilter.shouldRefreshSpacesListWithFilters = false
            let spaceAtIndex = self.filteredSpaces[indexPath.row]
            openSpaceDetailsViewController(forSpaceBySpaceID: spaceAtIndex.id)
        }
        else {
            //openSignInViewController()
            presentGuestCheckoutAlert()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.filteredSpaces.count - 1) && (self.totalFilteredSpacesCount != self.filteredSpaces.count) {
            self.loadFilteredSpaces()
        }
    }
}
