//
//  BaseTableViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
    
    // MARK: - Base Class Properties
    private var backgroundViewForActivityIndicator: UIView?
    private var baseActivityIndicator: UIActivityIndicatorView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initBaseActivityIndicator()
        addKeyBoardDismissOnTap()
    }
    
    
    // MARK: - Base Activity Indicator
    private func initBaseActivityIndicator() {
        // Background UIView
        self.backgroundViewForActivityIndicator = UIView.init(frame: self.view.bounds)
        self.backgroundViewForActivityIndicator?.backgroundColor = UIColor.clear
        // UIActivityIndicatorView
        self.baseActivityIndicator = UIActivityIndicatorView()
        self.baseActivityIndicator!.frame = CGRect(x: self.view.frame.size.width/2 - 25.0, y: self.view.frame.size.height/2 - 25.0, width: 50.0, height: 50.0)
        self.baseActivityIndicator!.style = .whiteLarge
        self.baseActivityIndicator!.color = UIColor.init(red: 0.0, green: 142.0/255.0, blue: 218.0/255.0, alpha: 1.0)
        self.baseActivityIndicator!.hidesWhenStopped = true
        self.backgroundViewForActivityIndicator?.addSubview(self.baseActivityIndicator!)
    }
    
    func showActivityIndicator() {
        self.baseActivityIndicator?.startAnimating()
        self.view.addSubview(self.backgroundViewForActivityIndicator!)
    }
    
    func hideActivityIndicator() {
        self.backgroundViewForActivityIndicator?.removeFromSuperview()
        self.baseActivityIndicator?.stopAnimating()
    }
    
    
    // MARK: Keyboard Dismiss on tap
    func addKeyBoardDismissOnTap() {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    // MARK: - Present Modal View Controllers
    func openSignInViewController() {
        let storyboard = UIStoryboard.init(name: Constants.MAIN_STORYBOARD, bundle: nil)
        let signInVC = storyboard.instantiateViewController(withIdentifier: Constants.VC_ID_SIGN_IN_SCREEN) as! SignInViewController
        let navController = UINavigationController.init(rootViewController: signInVC)
        navController.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.present(navController, animated: true, completion: nil)
    }
    
    func openNotificationViewController() {
        let storyboard = UIStoryboard.init(name: Constants.MAIN_STORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constants.VC_ID_NOTIFICATION_SCREEN)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    func performSegue(segueID: String) {
        self.performSegue(withIdentifier: segueID, sender: self)
    }
    
    func dismissViewController() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func popToRootViewController() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func popToViewController(viewController: Swift.AnyClass) {
        self.navigationController?.backToViewController(viewController: viewController.self)
    }
    
    
    // MARK: - Add done button for number pad style keyboard
    func addNumberPadDoneButton(textField: UITextField) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.tintColor = UIColor.white
        keyboardToolbar.barTintColor = UIColor.init(red: 63.0/255.0, green: 63.0/255.0, blue: 63.0/255.0, alpha: 1.0)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
    
    
    // MARK: - UIAlertControllers
    func presentServerErrorAlert() {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE_2, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentNetworkUnavailabilityErrorAlert() {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE_2, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentDismissableAlertWithMessage(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    
}
