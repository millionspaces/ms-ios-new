//
//  CollectionViewEmbeddedTableViewCell.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class CollectionViewEmbeddedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    func setCollectionViewDatasourceDelegate <D: UITableViewDataSource & UITableViewDelegate> (dataSourceDelegate: D, identifier id: Int) {
        self.collectionView.dataSource = dataSourceDelegate as? UICollectionViewDataSource
        self.collectionView.delegate = dataSourceDelegate as? UICollectionViewDelegate
        self.collectionView.tag = id
        self.collectionView.allowsMultipleSelection = true
        self.collectionView.reloadData()
        if self.pageController != nil {
            self.pageController.tag = id
            self.pageController.numberOfPages = (dataSourceDelegate as! UICollectionViewDataSource).collectionView(self.collectionView, numberOfItemsInSection: 0)
        }
    }
}
