//
//  User.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class User: NSObject {
    
    // MARK: - Non-optional values
    var id: Int!
    var name: String!
    var email: String!
    var role: String?
    
    // MARK: - Optional values
    var profileImageUrl: URL?
    var mobileNumber: String?
    var companyName: String?
    var companyAddress: String?
    var jobTitle: String?
    var companyPhone: String?
    
    
    
    func initUser(withUserDetailsDictionary dic: [String : Any]) {
        
        self.id = (dic["id"] as! Int)
        self.email = (dic["email"] as! String)
        
        if let name = dic["name"] as? String {
            self.name = name
        }
        
        if let role = dic["role"] as? String {
            self.role = role
        }
        
        if let imageUrlStr = dic["imageUrl"] as? String {
            if let imgUrl = URL(string: imageUrlStr) {
                self.profileImageUrl = imgUrl
            }
        }
        
        if let mobileNum = dic["mobileNumber"] as? String {
            self.mobileNumber = mobileNum
        }
        
        if let compName = dic["companyName"] as? String {
            self.companyName = compName
        }
        
        if let compAddress = dic["address"] as? String {
            self.companyAddress = compAddress
        }
        
        if let job = dic["jobTitle"] as? String {
            self.jobTitle = job
        }
        
        if let compPhone = dic["companyPhone"] as? String {
            self.companyPhone = compPhone
        }
    }
    
    
}
