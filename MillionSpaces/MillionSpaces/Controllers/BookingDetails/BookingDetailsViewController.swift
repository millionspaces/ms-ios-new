//
//  BookingDetailsViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

struct BookingDataStructureForTableViewSection {
    var section: Int!
    var sectionSpecialIdentifier: Int!
    var numOfRows: Int!
}

class BookingDetailsViewController: BaseViewController, UITextViewDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var bookingDetailsTableView: UITableView!
    @IBOutlet weak var bookingCancelUpdateButton: UIButton!
    @IBOutlet weak var constBookingCancelUpdateButtonBottom: NSLayoutConstraint!
    
    
    var bookingId: Int!
    var isManualBooking: Bool!
    var isViewedAsGuest: Bool!
    var shouldReturnToPreviousScreen: Bool!
    
    var booking: Booking!
    var bookingDataStructure: [String : BookingDataStructureForTableViewSection] = [:]
    var remarkByHost: String?
    
    var complementaryAmenitiesEmbeddedCVCellHight: CGFloat = 0.0
    var footer: UIView!
    
    let testLongString = "Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String Test Long String"
    
    var extrasSectionRowCount = 1
    
    var alertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Booking Details"
        
        footer = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:10))
        footer.backgroundColor = UIColor.groupTableViewBackground //init(red: 235.0/255.0, green: 235.0/255.0, blue: 241.0/255.0, alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToBookingDetailsFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        
        //self.constBookingCancelUpdateButtonBottom.constant = -50.0
        self.bookingCancelUpdateButton.isHidden = true
        self.bookingCancelUpdateButton.isUserInteractionEnabled = false
        
        self.retrieveBookingDetails()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func returnedToBookingDetailsFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) { // ~ 10s
            alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }
    
    func retrieveBookingDetails() {
        
        self.showActivityIndicator()
        
        WebServiceCall.retrieveBookingDetails(withBookingID: bookingId, isManual: isManualBooking, successBlock: { (results: Any) in
            print("BookingDetailsVC retrieveBookingDetails | results : \(results)")
            
            if let result = results as? [String : Any] {
                self.booking = Booking()
                self.booking.initBooking(withDictionary: result)
            }
            
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if self.isManualBooking {
                    self.configureViewForManualBooking()
                }
                else {
                    self.configureViewForMSBooking()
                }
            })
        })
        { (errorCode: Int, error: String) in
            print("BookingDetailsVC retrieveBookingDetails | failure errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addBookingDetailsReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addBookingDetailsReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addBookingDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func addBookingDetailsReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retrieveBookingDetails()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    func configureViewForMSBooking() {
        if self.booking.isUpcomingBooking && self.isViewedAsGuest && StaticData.CANCELABLE_BOOKING_RESERVATION_STATUS_RANGE.contains(self.booking.bookingReservationStatusId) {
            //self.constBookingCancelUpdateButtonBottom.constant = 0.0
            self.bookingCancelUpdateButton.isHidden = false
            self.bookingCancelUpdateButton.isUserInteractionEnabled = true
        }
        
        var section = 0
        
        // Main Details
        var b0 = BookingDataStructureForTableViewSection()
        b0.section = section
        b0.sectionSpecialIdentifier = 0
        b0.numOfRows = 1
        bookingDataStructure["\(section)"] = b0
        
        // Primary Booking Details
        section += 1
        var b1 = BookingDataStructureForTableViewSection()
        b1.section = section
        b1.sectionSpecialIdentifier = 10
        b1.numOfRows = 1
        bookingDataStructure["\(section)"] = b1
        
        // Secondary Booking Details
        section += 1
        var b2 = BookingDataStructureForTableViewSection()
        b2.section = section
        b2.sectionSpecialIdentifier = 20
        b2.numOfRows =  (self.booking.menu != nil) ? 2 : 1
        bookingDataStructure["\(section)"] = b2
        
        // Review
        if self.booking.isReviewed || StaticData.REVIEWABLE_BOOKING_RESERVATION_STATUS_RANGE.contains(self.booking.bookingReservationStatusId) && !self.booking.isUpcomingBooking && self.isViewedAsGuest {
            
            section += 1
            var b3 = BookingDataStructureForTableViewSection()
            b3.section = section
            b3.sectionSpecialIdentifier = 30
            b3.numOfRows = 2
            bookingDataStructure["\(section)"] = b3
        }
        
        // Amenities
        if self.booking.spaceAmenities.count > 0 || self.booking.spaceBookingExtraAmenities.count > 0 {
            section += 1
            var b4 = BookingDataStructureForTableViewSection()
            b4.section = section
            b4.sectionSpecialIdentifier = 40
            b4.numOfRows = 1
            if self.booking.spaceAmenities.count > 0 {
                b4.numOfRows = b4.numOfRows + 1 // Occured compiler error for += in Swift 3.0
            }
            if self.booking.spaceBookingExtraAmenities.count > 0 {
                b4.numOfRows = b4.numOfRows + self.booking.spaceBookingExtraAmenities.count + 1
            }
            bookingDataStructure["\(section)"] = b4
        }
        
        self.complementaryAmenitiesEmbeddedCVCellHight = Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.booking.spaceAmenities.count, cvCellHeight: 50, itemsPerRow: 2) + 20
        
        // Cancellation Policy
        section += 1
        var b5 = BookingDataStructureForTableViewSection()
        b5.section = section
        b5.sectionSpecialIdentifier = 50
        b5.numOfRows = 2
        bookingDataStructure["\(section)"] = b5
        
        // TODO
        //Cancel Button
        
        self.bookingDetailsTableView.reloadData()
    }
    
    func configureViewForManualBooking() {
        
        // TODO Make button available
        
        var section = 0
        
        // Main Details
        var b0 = BookingDataStructureForTableViewSection()
        b0.section = section
        b0.sectionSpecialIdentifier = 0
        b0.numOfRows = 1
        bookingDataStructure["\(section)"] = b0
        
        // Primary Booking Details
        section += 1
        var b1 = BookingDataStructureForTableViewSection()
        b1.section = section
        b1.sectionSpecialIdentifier = 10
        b1.numOfRows = 1
        bookingDataStructure["\(section)"] = b1
        
        // Secondary Booking Details
        section += 1
        var b2 = BookingDataStructureForTableViewSection()
        b2.section = section
        b2.sectionSpecialIdentifier = 20
        b2.numOfRows =  (self.booking.menu != nil) ? 2 : 1
        bookingDataStructure["\(section)"] = b2
        
        // Review
        if self.booking.isReviewed {
            section += 1
            var b3 = BookingDataStructureForTableViewSection()
            b3.section = section
            b3.sectionSpecialIdentifier = 30
            b3.numOfRows = 2
            bookingDataStructure["\(section)"] = b3
        }
        
        // Amenities
        if self.booking.spaceAmenities.count > 0 || self.booking.spaceBookingExtraAmenities.count > 0 {
            section += 1
            var b4 = BookingDataStructureForTableViewSection()
            b4.section = section
            b4.sectionSpecialIdentifier = 40
            b4.numOfRows = 1
            if self.booking.spaceAmenities.count > 0 {
                b4.numOfRows = b4.numOfRows + 1 // Occured compiler error for += in Swift 3.0
            }
            if self.booking.spaceBookingExtraAmenities.count > 0 {
                b4.numOfRows = b4.numOfRows + self.booking.spaceBookingExtraAmenities.count + 1
            }
            bookingDataStructure["\(section)"] = b4
        }
        
        self.complementaryAmenitiesEmbeddedCVCellHight = Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.booking.spaceAmenities.count, cvCellHeight: 50, itemsPerRow: 2) + 20
        
        // Cancellation Policy
        section += 1
        var b5 = BookingDataStructureForTableViewSection()
        b5.section = section
        b5.sectionSpecialIdentifier = 50
        b5.numOfRows = 2
        bookingDataStructure["\(section)"] = b5
        
        /* TEMP
         // Remarks
         section += 1
         var b6 = BookingDataStructureForTableViewSection()
         b6.section = section
         b6.sectionSpecialIdentifier = 60
         b6.numOfRows = 2
         bookingDataStructure["\(section)"] = b6
         */
        
        // TODO
        // Cancel and Save buttons
        
        self.bookingDetailsTableView.reloadData()
    }
    
    
    // MARK: - UITextViewDelegate
    func textViewDidEndEditing(_ textView: UITextView) {
        print("BookingVC textViewDidEndEditing")
    }
    
    // MARK: - IBActions
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if self.shouldReturnToPreviousScreen {
            self.popViewController()
        }
        else {
            self.popToRootViewController()
        }
    }
    
    
    @IBAction func bookingCancelUpdateButtonPressed(_ sender: UIButton) {
        //print("BookingVC bookingCancelUpdateButtonPressed")
        
        // TODO: Check if cancelling or updating
        
        if self.booking!.bookingReservationStatusId != 1 && self.booking!.bookingReservationStatusId != 2 {
            self.performSegue(segueID: "CancelBookingSegue")
        }
        else {
            self.presentConfirmationAlertForCancellation()
        }
    }
    
    @objc func addBookingReview() {
        //print("BookingVC addBookingReview button pressed")
        self.performSegue(segueID: "ReviewBookingSegue")
    }
    
    @objc func viewMenu() {
        //print("BookingVC viewMenu button pressed")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CancelBookingSegue" {
            let cancelBookingVC = segue.destination as! CancelBookingViewController
            cancelBookingVC.booking = self.booking!
        }
        else if segue.identifier == "ReviewBookingSegue" {
            let reviewBookingVC = segue.destination as! ReviewViewController
            reviewBookingVC.booking = self.booking!
        }
    }
    
    
}


/*
 
 MainCell 200
 status       1
 space name   2
 address      3
 guest name   4
 guest mobile 5
 guest email  6
 m logo       7
 
 PrimaryBookingDetailsCell auto
 field_1       1
 datetime_1    2
 field_2       3
 datetime_2    4
 bookingmade   5
 charge        6
 
 SecondaryBookingDetailsCell 120
 eventType      1
 seating        2
 guest count    3
 
 MenuCell    40
 menu           1
 
 HeaderCell  50
 header         1
 
 LeaveReviewCell  50
 button         1
 
 ReviewCell      auto
 title          1
 r services     2
 r cleanliness  3
 r value        4
 description    5
 
 ComplementaryExtrasCVECell - calculate size + 20
 ecv            1 screenW-50
 
 ComplementaryExtraCVCell 50
 image          1
 extra          2
 
 ChargableExtrasHeaderCell 20
 header         1
 
 ChargableExtraCell 50
 image          1
 extra          2
 no of units    3
 
 cPolicyCell     40
 title          1
 description    2
 
 RemarksCell     100
 textview       1
 */



// MARK: - Helper Functions for
extension BookingDetailsViewController {
    
    func calculateNumberOfRows(forSection section: Int) -> Int {
        let dataStructureForRow = self.bookingDataStructure[section.description]
        return dataStructureForRow!.numOfRows
    }
    
    func calculateHeight(forRow row: Int, inSection section: Int) -> CGFloat {
        let dataStructureForRow = self.bookingDataStructure[section.description]
        
        switch dataStructureForRow!.sectionSpecialIdentifier {
        case 0: // MainCell
            return UITableView.automaticDimension
            
        case 10: // PrimaryBookingDetailsCell
            return UITableView.automaticDimension
            
        case 20: // SecondaryBookingDetailsCell
            return row == 0 ? 120 : 40
            
        case 30: // Review
            if self.booking.isReviewed {
                return UITableView.automaticDimension
            }
            return 40
            
        case 40: // Amenities
            if row == 0 {
                return 50
            }
            else {
                if self.booking.spaceAmenities.count > 0 && self.booking.spaceBookingExtraAmenities.count > 0 {
                    if row == 1 {
                        return self.complementaryAmenitiesEmbeddedCVCellHight
                    }
                    else if row == 2 {
                        return 20
                    }
                    else {
                        return 50
                    }
                }
                else {
                    if self.booking.spaceAmenities.count > 0 {
                        return self.complementaryAmenitiesEmbeddedCVCellHight
                    }
                    else if self.booking.spaceBookingExtraAmenities.count > 0 {
                        if row == 1 {
                            return 20
                        }
                        else {
                            return 50
                        }
                    }
                    else {
                        return 0
                    }
                }
            }
            
        case 50: // cPolicyCell
            return 50
            
        case 60: // Remark
            return 100
            
        default:
            return 0
        }
    }
    
    func populateTableViewCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        let dataStructureForRow = self.bookingDataStructure[indexPath.section.description]
        
        switch dataStructureForRow!.sectionSpecialIdentifier {
            
        case 0: // MainCell
            let mainCell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath)
            
            (mainCell.viewWithTag(1) as! UILabel).text = self.booking.bookingReservationStatusDisplayName
            (mainCell.viewWithTag(1) as! UILabel).backgroundColor = ColorUtils.BOOKING_RESERVATION_STATUS_COLORS_BY_CODE[self.booking.bookingReservationStatusId]
            
            (mainCell.viewWithTag(2) as! UILabel).text = self.booking.spaceName
            (mainCell.viewWithTag(3) as! UILabel).text = self.booking.spaceFullAddress
            
            (mainCell.viewWithTag(4) as! UILabel).text = self.booking.bookingByUserName
            (mainCell.viewWithTag(5) as! UILabel).text = self.booking.bookingByUserMobile
            (mainCell.viewWithTag(6) as! UILabel).text = self.booking.bookingByUserEmail
            
            (mainCell.viewWithTag(7) as! UIImageView).isHidden = (self.isViewedAsGuest == true) ? true : self.booking.isManual
            
            return mainCell
            
        case 10: // PrimaryBookingDetailsCell
            let primaryBookingDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PrimaryBookingDetailsCell", for: indexPath)
            
            let field_1_label = primaryBookingDetailsCell.viewWithTag(1) as! UILabel
            let field_1_value_label = primaryBookingDetailsCell.viewWithTag(2) as! UILabel
            let field_2_label = primaryBookingDetailsCell.viewWithTag(3) as! UILabel
            let field_2_value_label = primaryBookingDetailsCell.viewWithTag(4) as! UILabel
            
            if self.booking.bookingDates.count > 0 {
                if self.booking.isMultipleDayManualBooking {
                    field_1_label.text = "From"
                    field_2_label.text = "To"
                    
                    field_1_value_label.text = "\(CalendarUtils.retrieveSimpleDateString_2(fromDateStr: self.booking.bookingDates[0]["fromDate"]![0])) \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: self.booking.bookingDates[0]["fromDate"]![1], isWithSeconds: false))"
                    
                    field_2_value_label.text = "\(CalendarUtils.retrieveSimpleDateString_2(fromDateStr: self.booking.bookingDates[0]["toDate"]![0])) \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: self.booking.bookingDates[0]["toDate"]![1], isWithSeconds: false))"
                }
                else {
                    field_1_label.text = "Date"
                    field_2_label.text = "Reservation Time"
                    
                    field_1_value_label.text = "\(CalendarUtils.retrieveSimpleDateString_2(fromDateStr: self.booking.bookingDates[0]["fromDate"]![0]))"
                    
                    
                    if !self.booking.hasMultipleBlocks {
                        field_2_value_label.text = "\(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: self.booking.bookingDates[0]["fromDate"]![1], isWithSeconds: false)) - \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: self.booking.bookingDates[0]["toDate"]![1], isWithSeconds: false))"
                    }
                    else {
                        var blockTimesStr = ""
                        for i in self.booking.bookingDates {
                            blockTimesStr.append("\(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: i["fromDate"]![1], isWithSeconds: false)) - \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: i["toDate"]![1], isWithSeconds: false))\n")
                        }
                        field_2_value_label.text = String(blockTimesStr.dropLast(1))
                    }
                }
            }
            else {
                field_1_label.text = "Date"
                field_2_label.text = "Reservation Time"
                field_1_value_label.text = "N/A"
                field_2_value_label.text = "N/A"
            }
            
            // TODO Format Booking Made Date
            
            (primaryBookingDetailsCell.viewWithTag(5) as! UILabel).text = "\(self.booking.bookingMadeDate.replacingOccurrences(of: "T", with: " "))"
            (primaryBookingDetailsCell.viewWithTag(6) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: self.booking.bookingCharge))"
            
            
            return primaryBookingDetailsCell
            
        case 20: // SecondaryBookingDetailsCell // TEMP // TODO Menu Cell to be added when necessary
            if indexPath.row == 0 {
                let secondaryBookingDetailsCell = tableView.dequeueReusableCell(withIdentifier: "SecondaryBookingDetailsCell", for: indexPath)
                (secondaryBookingDetailsCell.viewWithTag(1) as! UILabel).text = self.booking.eventType?.name!
                (secondaryBookingDetailsCell.viewWithTag(2) as! UILabel).text = self.booking.seatingArrangement?.name!
                (secondaryBookingDetailsCell.viewWithTag(3) as! UILabel).text = "\(self.booking.expectedGuestCount)"
                return secondaryBookingDetailsCell
            }
            else {
                let menu = self.booking.menu!
                let menuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath)
                let menuButton = menuCell.viewWithTag(1) as! UIButton
                menuButton.setTitle("\(menu.menuName!)", for: .normal)
                menuButton.addTarget(self, action: #selector(viewMenu), for: .touchUpInside)
                return menuCell
            }
            
        case 30: // Review
            if indexPath.row ==  0 {
                let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                (headerCell.viewWithTag(1) as! UILabel).text = "Review"
                return headerCell
            }
            else {
                if self.booking.isReviewed {
                    let reviewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath)
                    (reviewCell.viewWithTag(1) as! UILabel).text = self.booking.review!.rTitle
                    (reviewCell.viewWithTag(2) as! StarRatingView).setRatingValue(withInt: self.booking.review!.serviceScore)
                    (reviewCell.viewWithTag(3) as! StarRatingView).setRatingValue(withInt: self.booking.review!.cleanlinessScore)
                    (reviewCell.viewWithTag(4) as! StarRatingView).setRatingValue(withInt: self.booking.review!.valueScore)
                    (reviewCell.viewWithTag(5) as! UILabel).text = self.booking.review!.rDescription
                    return reviewCell
                }
                else {
                    let leaveReviewCell = tableView.dequeueReusableCell(withIdentifier: "LeaveReviewCell", for: indexPath)
                    (leaveReviewCell.viewWithTag(1) as! UIButton).addTarget(self, action: #selector(addBookingReview), for: .touchUpInside)
                    return leaveReviewCell
                }
            }
            
        case 40: // Amenities
            // TODO
            if indexPath.row ==  0 {
                let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                (headerCell.viewWithTag(1) as! UILabel).text = "Amenities"
                return headerCell
            }
            else {
                if self.booking.spaceAmenities.count > 0 && self.booking.spaceBookingExtraAmenities.count > 0 {
                    if indexPath.row == 1 {
                        let complementaryExtrasCVECell = tableView.dequeueReusableCell(withIdentifier: "ComplementaryExtrasCVECell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
                        complementaryExtrasCVECell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: dataStructureForRow!.sectionSpecialIdentifier)
                        return complementaryExtrasCVECell
                    }
                    else if indexPath.row == 2 {
                        let chargableExtrasHeaderCell = tableView.dequeueReusableCell(withIdentifier: "ChargableExtrasHeaderCell", for: indexPath)
                        return chargableExtrasHeaderCell
                    }
                    else {
                        let extraAtIndex = self.booking.spaceBookingExtraAmenities[indexPath.row - 3]
                        
                        let chargableExtraCell = tableView.dequeueReusableCell(withIdentifier: "ChargableExtraCell", for: indexPath)
                        (chargableExtraCell.viewWithTag(1) as! UIImageView).sd_setImage(with: URL(string: extraAtIndex.extraAmenity.extraAmenity.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
                        (chargableExtraCell.viewWithTag(2) as! UILabel).text = extraAtIndex.extraAmenity.extraAmenity.name!
                        let extraUnitName = extraAtIndex.extraAmenity.extraAmenityUnit.name!.replacingOccurrences(of: "per ", with: "")
                        (chargableExtraCell.viewWithTag(3) as! UILabel).text = "\(extraAtIndex.amount!) \(extraUnitName)(s)"
                        return chargableExtraCell
                    }
                }
                else {
                    if self.booking.spaceAmenities.count > 0 {
                        if indexPath.row == 1 {
                            let complementaryExtrasCVECell = tableView.dequeueReusableCell(withIdentifier: "ComplementaryExtrasCVECell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
                            complementaryExtrasCVECell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: dataStructureForRow!.sectionSpecialIdentifier)
                            return complementaryExtrasCVECell
                        }
                        else if indexPath.row == 2 {
                            let chargableExtrasHeaderCell = tableView.dequeueReusableCell(withIdentifier: "ChargableExtrasHeaderCell", for: indexPath)
                            return chargableExtrasHeaderCell
                        }
                    }
                    else if self.booking.spaceBookingExtraAmenities.count > 0 {
                        if indexPath.row == 1 {
                            let chargableExtrasHeaderCell = tableView.dequeueReusableCell(withIdentifier: "ChargableExtrasHeaderCell", for: indexPath)
                            return chargableExtrasHeaderCell
                        }
                        else {
                            let extraAtIndex = self.booking.spaceBookingExtraAmenities[indexPath.row - 2]
                            
                            let chargableExtraCell = tableView.dequeueReusableCell(withIdentifier: "ChargableExtraCell", for: indexPath)
                            (chargableExtraCell.viewWithTag(1) as! UIImageView).sd_setImage(with: URL(string: extraAtIndex.extraAmenity.extraAmenity.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
                            (chargableExtraCell.viewWithTag(2) as! UILabel).text = extraAtIndex.extraAmenity.extraAmenity.name!
                            let extraUnitName = extraAtIndex.extraAmenity.extraAmenityUnit.name!.replacingOccurrences(of: "per ", with: "")
                            (chargableExtraCell.viewWithTag(3) as! UILabel).text = "\(extraAtIndex.amount!) \(extraUnitName)(s)"
                            return chargableExtraCell
                        }
                    }
                }
            }
            return UITableViewCell()
            
        case 50: // cPolicyCell
            if indexPath.row ==  0 {
                let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                (headerCell.viewWithTag(1) as! UILabel).text = "Cancellation Policy"
                return headerCell
            }
            else {
                let cPolicyCell = tableView.dequeueReusableCell(withIdentifier: "cPolicyCell", for: indexPath)
                (cPolicyCell.viewWithTag(1) as! UILabel).text = self.booking.spaceCancellationPolicy.name!
                (cPolicyCell.viewWithTag(2) as! UILabel).text = self.booking.spaceCancellationPolicy.policyDescription!
                return cPolicyCell
            }
            
        case 60: // Remarks
            if indexPath.row ==  0 {
                let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                (headerCell.viewWithTag(1) as! UILabel).text = "Remarks"
                return headerCell
            }
            else {
                let remarksCell = tableView.dequeueReusableCell(withIdentifier: "RemarksCell", for: indexPath)
                (remarksCell.viewWithTag(1) as! UITextView).delegate = self
                return remarksCell
            }
        default:
            return UITableViewCell()
        }
    }
}


extension BookingDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.bookingDataStructure.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //if section == self.bookingDataStructure.count - 1 {
        //  return 60
        //}
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footer
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if booking != nil {
            return calculateNumberOfRows(forSection: section)
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if booking != nil {
            return calculateHeight(forRow: indexPath.row, inSection: indexPath.section)
        }
        else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if booking != nil {
            return calculateHeight(forRow: indexPath.row, inSection: indexPath.section)
        }
        else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if booking != nil {
            return populateTableViewCell(tableView: tableView, indexPath: indexPath)
        }
        else {
            return UITableViewCell()
        }
    }
}


extension BookingDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if booking != nil {
            return self.booking.spaceAmenities.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if booking != nil {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComplementaryExtraCVCell", for: indexPath)
            let amenityAtIndex = booking.spaceAmenities[indexPath.item]
            (cell.viewWithTag(1) as! UIImageView).sd_setImage(with: URL(string: amenityAtIndex.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            (cell.viewWithTag(2) as! UILabel).text = amenityAtIndex.name!
            return cell
        }
        else {
            return UICollectionViewCell()
        }
    }
}


extension BookingDetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if booking != nil {
            let cellWidth = (UIScreen.main.bounds.width - 50) / 2
            return CGSize(width: cellWidth, height: 50)
        }
        else {
            return CGSize()
        }
    }
}



// NOTE / TODO : Code repetition / To be removed
extension BookingDetailsViewController {
    
    func cancelBooking() {
        self.showActivityIndicator()
        
        WebServiceCall.cancelGuestMSBooking(forBookingID: self.booking!.id!, withRefundableAmount: 0, successBlock: { (result: Any) in
            print("CancelBookingVC cancelBooking | result : \(result)")
            
            if let response = result as? [String : Any], response["new_state"] as? String == "CANCELLED" {
                self.presentResponseAlert(isSuccessful: true)
            }
            else {
                self.presentResponseAlert(isSuccessful: false)
            }
            
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("CancelBookingVC cancelBooking | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func presentConfirmationAlertForCancellation() {
        alertController = UIAlertController(title: AlertMessages.CONFIRM_TITLE, message: AlertMessages.CANCEL_BOOKING_CONFIRMATION_MESSAGE, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertMessages.YES_ACTION, style: .default) { (alert: UIAlertAction) in
            self.cancelBooking()
        }
        let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
        alertController!.addAction(noAction)
        alertController!.addAction(yesAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func presentResponseAlert(isSuccessful: Bool) {
        if isSuccessful {
            alertController = UIAlertController(title: AlertMessages.SUCCESS_TITLE, message: AlertMessages.CANCEL_BOOKING_SUCCESS_MESSAGE, preferredStyle: .alert)
            let dissmissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default) { (alert: UIAlertAction) in
                //self.constBookingCancelUpdateButtonBottom.constant = -50.0
                self.bookingCancelUpdateButton.isHidden = true
                self.bookingCancelUpdateButton.isUserInteractionEnabled = false
                self.retrieveBookingDetails()
            }
            alertController!.addAction(dissmissAction)
        }
        else {
            alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.CANCEL_BOOKING_ERROR_MESSAGE, preferredStyle: .alert)
            let dissmissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
            alertController!.addAction(dissmissAction)
        }
        
        self.present(alertController!, animated: true, completion: nil)
    }
}
