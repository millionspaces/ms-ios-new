//
//  ManualBookingViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class ManualBookingViewController: BaseViewController, UITextFieldDelegate, UITextViewDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet var screenTableView: UITableView!
    @IBOutlet weak var parentContainerView: UIView!
    
    //Date Time Section
    @IBOutlet weak var bookingStartDateTImeLabel: UILabel!
    @IBOutlet weak var bookingEndDateTImeLabel: UILabel!
    
    // Guest Details Section
    @IBOutlet weak var guestNameText: UITextField!
    @IBOutlet weak var guestContactNoText: UITextField!
    @IBOutlet weak var guestEmailText: UITextField!
    @IBOutlet weak var spaceReservationFeeText: UITextField!
    
    // CVs View
    @IBOutlet weak var constCvParentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constEventTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constSeatingViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var eventTypeCollectionView: UICollectionView!
    @IBOutlet weak var seatingCollectionView: UICollectionView!
    
    // Other View
    @IBOutlet weak var expectedGuestCountText: UITextField!
    @IBOutlet weak var bookingStatusLabel: UILabel!
    @IBOutlet weak var amenitiesTextView: UITextView!
    @IBOutlet weak var remarksTextView: UITextView!
    
    // Date Time Picker Views
    @IBOutlet weak var viewForDateTimePickers: UIView!
    @IBOutlet weak var pickersViewCancelButton: UIButton!
    @IBOutlet weak var pickersViewSetButton: UIButton!
    @IBOutlet weak var constViewForDateTimePickersSubviewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var hourPicker: UIPickerView!
    @IBOutlet weak var amPmPicker: UIPickerView!
    
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var saveUpdateButton: UIButton!
    @IBOutlet weak var constSaveUpdateButtonLeading: NSLayoutConstraint!
    @IBOutlet weak var constSaveUpdateButtonBottom: NSLayoutConstraint!
    
    
    
    // MARK: - Class Variables
    
    //From previousScreen
    var mySpaceID: Int!
    var manualBookingID: Int?
    var spaceCalendarEndDate: SpaceCalendarDate!
    var calendarSetDate: SpaceCalendarDate?
    var calendarSetStartingTimeHourInt: Int?
    var calendarSetEndingTimeHourInt: Int?
    
    
    //var bookingSpace: Space! // TODO: Will have to get Space by ID
    var eventTypes: [EventType] = []
    var seatingArrangements: [SeatingArrangement] = []
    
    var manualBookingEventTypeID: Int?
    var manualBookingSeatingArrangementID: Int?
    
    var manualBookingStartDate: Date?
    var manualBookingStartTimeHourInt: Int?
    var manualBookingStartTimeAmPmStatus: String?
    
    var manualBookingEndDate: Date?
    var manualBookingEndTimeHourInt: Int?
    var manualBookingEndTimeAmPmStatus: String?
    
    var guestName: String?
    var guestContactNumber: String?
    var guestEmail: String?
    var spaceReservationFee: Int?
    var expectedGuestCount: Int?
    var reservationStatusIDStr: String?
    var amenitiesRequested: String?
    var remarks: String?
    
    var manualBookingMade: String?
    
    var eventTypesCVHeight: CGFloat = 100.0
    var seatingCVHeight: CGFloat = 100.0
    var ecvCellWidth: CGFloat!
    let ecvCellHeight: CGFloat = 100.0
    
    var isSettingStartDateTime = true
    var hasValidStartEndDateTime = false
    
    
    
    var alertController: UIAlertController?
    var reservationStatusActionSheet: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToManualBookingFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToManualBookingFromBackground() {
        if Utils.didPassDuration(durationInHours: 0.5) { // ~ 10s
            alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            reservationStatusActionSheet?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = "Manual Booking"
        
        //self.addKeyBoardDismissOnTap()
        
        if manualBookingID != nil {
            self.constSaveUpdateButtonLeading.constant = (Utils.SCREEN_WIDTH / 2.0) - 10.0
            self.saveUpdateButton.setTitle("UPDATE", for: .normal)
        }
        else {
            self.constSaveUpdateButtonLeading.constant = 10.0
        }
        
        self.eventTypes = MetaData.retrieveEventTypes() as! [EventType]
        self.seatingArrangements = MetaData.retrieveSeatingArrangements() as! [SeatingArrangement]
        
        eventTypesCVHeight = Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.eventTypes.count, cvCellHeight: Int(ecvCellHeight), itemsPerRow: 3)
        seatingCVHeight = Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.seatingArrangements.count, cvCellHeight: Int(ecvCellHeight), itemsPerRow: 3)
        
        ecvCellWidth = (Utils.SCREEN_WIDTH - 20.0) / 3.0
        
        
        self.constEventTypeViewHeight.constant = eventTypesCVHeight + 50.0
        self.constSeatingViewHeight.constant = seatingCVHeight + 50.0
        self.constCvParentViewHeight.constant = eventTypesCVHeight + seatingCVHeight + 110.0
        
        let parentContentViewHeightToBe = 1650.0 - 310.0 + (eventTypesCVHeight + seatingCVHeight + 110.0)
        self.parentContainerView.frame = CGRect.init(x: 0.0, y: 0.0, width: Utils.SCREEN_WIDTH, height: parentContentViewHeightToBe)
        
        /// 50 + content height for one cv
        // 310, 150, 150
        // 1560
        
        self.viewForDateTimePickers.isHidden = true
        self.constViewForDateTimePickersSubviewBottom.constant = -250.0
        
        self.datePicker.minimumDate = CalendarUtils.currentDeviceDate()
        self.datePicker.timeZone = TimeZone(secondsFromGMT: 0)
        self.hourPicker.delegate = self
        self.amPmPicker.delegate = self
        
        self.addNumberPadDoneButton(textField: self.guestContactNoText)
        self.addNumberPadDoneButton(textField: self.spaceReservationFeeText)
        self.addNumberPadDoneButton(textField: self.expectedGuestCountText)
        self.guestContactNoText.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        
        configureReservationStatusActionSheet()
        
        configureForDateTimesSetAtHostCalendar()
    }
    
    
    func configureForDateTimesSetAtHostCalendar() {
        if let setDate = self.calendarSetDate, let setStartHour = self.calendarSetStartingTimeHourInt, let setEndHour = self.calendarSetEndingTimeHourInt {
            //print("ManualBookingVC configureForDateTimesSetAtHostCalendar | StartDate : \(setDate.date!)")
            //print("ManualBookingVC configureForDateTimesSetAtHostCalendar | setStartHour : \(setStartHour)")
            //print("ManualBookingVC configureForDateTimesSetAtHostCalendar | setEndHour : \(setEndHour)")
            
            let startTimeComponents = self.hourIn12HAndAmPMComponentsForCalendarSetTime(hour: setStartHour)
            self.setStartEndDateTimeValues(date: setDate.date!, hourInt: startTimeComponents.0, amPm: startTimeComponents.1, startTime: true)
            
            let endTimeComponents = self.hourIn12HAndAmPMComponentsForCalendarSetTime(hour: setEndHour)
            let endDate = setEndHour == 24 ? CalendarUtils.retrieveDate(thatComesAfterDays: 1, fromDate: setDate.date!) : setDate.date!
            //print("ManualBookingVC configureForDateTimesSetAtHostCalendar | EndDate : \(endDate)")
            
            self.setStartEndDateTimeValues(date: endDate, hourInt: endTimeComponents.0, amPm: endTimeComponents.1, startTime: false)
        }
        else {
            validateStartEndTime()
        }
    }
    
    private func hourIn12HAndAmPMComponentsForCalendarSetTime(hour: Int) -> (Int, Int) {
        if hour == 0 || hour == 24 { return (12, 0) }
        else if hour < 12 { return (hour, 0) }
        else if hour > 12 { return (hour % 12, 1) }
        else { return (12, 1) }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if self.viewForDateTimePickers.frame.contains(location) && !self.viewForDateTimePickers.viewWithTag(3)!.frame.contains(location) {
            hideViewForDateTimePickers()
        }
    }
    
    
    // MARK: - Handle Date Tike Picker View Visibility
    
    func showViewForDateTimePickers() {
        self.viewForDateTimePickers.isHidden = false
        self.constViewForDateTimePickersSubviewBottom.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideViewForDateTimePickers() {
        self.constViewForDateTimePickersSubviewBottom.constant = -250.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        perform(#selector(hidePickersView), with: nil, afterDelay: 0.5)
    }
    
    @objc func hidePickersView() {
        self.viewForDateTimePickers.isHidden = true
    }
    
    
    // MARK: - Populate Reservation Status Action Sheet
    
    func configureReservationStatusActionSheet() {
        
        reservationStatusActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let CANCEL_ACTION_BUTTON = UIAlertAction(title: AlertMessages.CANCEL_ACTION, style: .cancel, handler: nil)
        
        let PENDING_PAYMENT_STATUS = StaticData.MANUAL_BOOKING_RESERVATION_STATUS[2]!
        let CONFIRMED_STATUS = StaticData.MANUAL_BOOKING_RESERVATION_STATUS[5]!
        
        let PENDING_PAYMENT_STATUS_ACTION_BUTTON = UIAlertAction(title: PENDING_PAYMENT_STATUS["displayName"], style: .default) { action -> Void in
            self.bookingStatusLabel.text = PENDING_PAYMENT_STATUS["displayName"]
            self.reservationStatusIDStr = PENDING_PAYMENT_STATUS["id"]
        }
        
        let CONFIRMED_STATUS_ACTION_BUTTON = UIAlertAction(title: CONFIRMED_STATUS["displayName"], style: .default) { action -> Void in
            self.bookingStatusLabel.text = CONFIRMED_STATUS["displayName"]
            self.reservationStatusIDStr = CONFIRMED_STATUS["id"]
        }
        
        reservationStatusActionSheet!.addAction(CANCEL_ACTION_BUTTON)
        reservationStatusActionSheet!.addAction(PENDING_PAYMENT_STATUS_ACTION_BUTTON)
        reservationStatusActionSheet!.addAction(CONFIRMED_STATUS_ACTION_BUTTON)
    }
    
    
    
    // MARK: UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.textColor = UIColor.black
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.guestNameText.isFirstResponder {
            self.guestContactNoText.becomeFirstResponder()
        }
        else if self.guestEmailText.isFirstResponder {
            self.spaceReservationFeeText.becomeFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.guestNameText && self.guestNameText.text!.count > 0 {
            if Utils.containsOnlyLetters(input: self.guestNameText.text!) {
                self.guestName = self.guestNameText.text!
            }
            else {
                self.guestName = nil
                self.guestNameText.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            }
        }
        else if textField == self.guestContactNoText && self.guestContactNoText.text!.count > 0 {
            if Utils.isStringNumeric(input: self.guestContactNoText.text!) {
                self.guestContactNumber = self.guestContactNoText.text!.count <= 10 ? self.guestContactNoText.text! : nil
            }
            else {
                self.guestContactNoText.text = ""
            }
        }
        else if textField == self.guestEmailText && self.guestEmailText.text!.count > 0 {
            if Utils.isValidEmailAddress(emailEntry: self.guestEmailText.text!) {
                self.guestEmail = self.guestEmailText.text!
            }
            else {
                self.guestEmail = nil
                self.guestEmailText.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            }
        }
        else if textField == self.spaceReservationFeeText && self.spaceReservationFeeText.text!.count > 0 {
            if let fee = Int(self.spaceReservationFeeText.text!) {
                print("test Int")
                self.spaceReservationFee = fee
            }
            else if let fee = Double(self.spaceReservationFeeText.text!) {
                print("test double")
                self.spaceReservationFee = Int(fee)
            }
            else {
                self.spaceReservationFee = nil
                self.spaceReservationFeeText.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            }
        }
        else if textField == self.expectedGuestCountText && self.expectedGuestCountText.text!.count > 0 {
            if let guestCount = Int(self.expectedGuestCountText.text!) {
                self.expectedGuestCount = guestCount
            }
            else {
                self.expectedGuestCount = nil
                self.expectedGuestCountText.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            }
        }
    }
    
    @objc func textChanged(_ textField: UITextField) {
        if textField.text!.count > 10 {
            textField.deleteBackward()
        }
    }
    
    
    // MARK: UITextViewDelegate
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == self.amenitiesTextView {
            self.amenitiesRequested = self.amenitiesTextView.text!.count > 0 ? self.amenitiesTextView.text! : nil
        }
        else if textView == self.remarksTextView {
            self.remarks = self.remarksTextView.text!.count > 0 ? self.remarksTextView.text! : nil
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func bookingStartDateTimeSelectionButtonPressed(_ sender: UIButton) {
        self.isSettingStartDateTime = true
        configureDatePickerDates()
        showViewForDateTimePickers()
    }
    
    @IBAction func bookingEndDateTimeSelectionButtonPressed(_ sender: UIButton) {
        self.isSettingStartDateTime = false
        configureDatePickerDates()
        showViewForDateTimePickers()
    }
    
    @IBAction func bookingStatusSelectionButtonPressed(_ sender: UIButton) {
        self.present(reservationStatusActionSheet!, animated: true, completion: nil)
    }
    
    @IBAction func removeButtonPressed(_ sender: UIButton) {
        print("ManualBookingVC removeButtonPressed")
    }
    
    @IBAction func saveUpdateButtonPressed(_ sender: UIButton) {
        print("ManualBookingVC saveUpdateButtonPressed")
        if self.hasValidStartEndDateTime {
            self.manualBookingConfirmationAlert()
        }
    }
    
    @IBAction func pickerViewCancelButtonPressed(_ sender: UIButton) {
        hideViewForDateTimePickers()
    }
    
    @IBAction func pickerViewSaveButtonPressed(_ sender: UIButton) {
        let setDate = self.datePicker.date
        print("ManualBookingVC pickerViewSaveButtonPressed | setDate : \(setDate)")
        let setTimeHour = self.hourPicker.selectedRow(inComponent: 0) + 1
        let setAmPm = self.amPmPicker.selectedRow(inComponent: 0)
        self.setStartEndDateTimeValues(date: setDate, hourInt: setTimeHour, amPm: setAmPm, startTime: isSettingStartDateTime)
        hideViewForDateTimePickers()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }`
     */
    
}



extension ManualBookingViewController {
    
    func configureDatePickerDates() {
        
        self.datePicker.date = CalendarUtils.currentDeviceDate()
        self.datePicker.minimumDate = CalendarUtils.currentDeviceDate()
        
        if isSettingStartDateTime {
            // Setting Date Picker Date
            if let date = self.manualBookingStartDate {
                self.datePicker.date = date
            }
            
            // Setting Date Picker Date maximum date
            if let setEndDate = self.manualBookingEndDate {
                self.datePicker.maximumDate = setEndDate
            }
            
            // Setting Time Picker Hour
            if let setStartHour = self.manualBookingStartTimeHourInt {
                self.hourPicker.selectRow(setStartHour - 1, inComponent: 0, animated: true)
            }
            
            // Setting AM PM Picker Value
            if let setAmPm = self.manualBookingStartTimeAmPmStatus {
                self.amPmPicker.selectRow((setAmPm == "AM") ? 0 : 1, inComponent: 0, animated: true)
            }
        }
        else {
            // Setting Date Picker Date
            if let date = self.manualBookingEndDate {
                self.datePicker.date = date
            }
            
            // Setting Date Picker Maximum Date for Ending DateTime Picker
            self.datePicker.maximumDate = self.spaceCalendarEndDate.date!
            
            // Setting Date Picker Minimum date
            if let setStartDate = self.manualBookingStartDate {
                self.datePicker.minimumDate = setStartDate
            }
            
            // Setting Time Picker Hour
            if let setEndHour = self.manualBookingEndTimeHourInt {
                self.hourPicker.selectRow(setEndHour - 1, inComponent: 0, animated: true)
            }
            
            // Setting AM PM Picker Value
            if let setAmPm = self.manualBookingEndTimeAmPmStatus {
                self.amPmPicker.selectRow((setAmPm == "AM") ? 0 : 1, inComponent: 0, animated: true)
            }
        }
    }
    
    func setStartEndDateTimeValues(date: Date, hourInt: Int, amPm: Int, startTime: Bool) {
        
        //let dateValue = CalendarUtils.retrieveLocaleDate_2(fromSimpleDate: date)
        let dateDisplayValue =  CalendarUtils.retrieveSimpleDateString_2(fromDate: date)
        
        let timeHourValue = hourInt
        let ampmValue = (amPm == 0) ? "AM" : "PM"
        
        if startTime {
            //print("ManualBookingVC setStartDateTimeValues | StartDate : \(date)")
            //print("ManualBookingVC setStartDateTimeValues | StartDate DisplayValue : \(dateDisplayValue)")
            //print("ManualBookingVC setStartDateTimeValues | timeHourValue : \(timeHourValue)")
            self.manualBookingStartDate = date
            self.manualBookingStartTimeHourInt = timeHourValue
            self.manualBookingStartTimeAmPmStatus = ampmValue
            
            self.bookingStartDateTImeLabel.text = "\(dateDisplayValue),  \(timeHourValue) \(ampmValue)"
        }
        else {
            //print("ManualBookingVC setEndDateTimeValues | EndDate : \(date)")
            //print("ManualBookingVC setEndDateTimeValues | EndDate DisplayValue : \(dateDisplayValue)")
            //print("ManualBookingVC setEndDateTimeValues | timeHourValue : \(timeHourValue)")
            self.manualBookingEndDate = date
            self.manualBookingEndTimeHourInt = timeHourValue
            self.manualBookingEndTimeAmPmStatus = ampmValue
            
            self.bookingEndDateTImeLabel.text = "\(dateDisplayValue),  \(timeHourValue) \(ampmValue)"
        }
        
        validateStartEndTime()
    }
    
    func validateStartEndTime() {
        self.saveUpdateButton.backgroundColor = UIColor.lightGray
        self.hasValidStartEndDateTime = false
        
        if self.manualBookingStartDate != nil && self.manualBookingEndDate != nil {
            
            let manualBookingSetStartDate = self.prepareDateTimeForDateValidation(date: self.manualBookingStartDate!, hour: self.manualBookingStartTimeHourInt!, amPM: self.manualBookingStartTimeAmPmStatus!)
            let manualBookingSetEndDate = self.prepareDateTimeForDateValidation(date: self.manualBookingEndDate!, hour: self.manualBookingEndTimeHourInt!, amPM: self.manualBookingEndTimeAmPmStatus!)
            
            //print("ManualBookingVC validateStartEndTime | manualBookingSetStartDate : \(manualBookingSetStartDate)")
            //print("ManualBookingVC validateStartEndTime | manualBookingSetEndDate : \(manualBookingSetEndDate)")
            
            self.hasValidStartEndDateTime = manualBookingSetStartDate < manualBookingSetEndDate
            
            //print("ManualBookingVC validateStartEndTime | hasValidStartEndDateTime : \(self.hasValidStartEndDateTime)")
            
            if self.hasValidStartEndDateTime {
                self.saveUpdateButton.backgroundColor = ColorUtils.MS_BLUE_COLOR
                self.hasValidStartEndDateTime = true
                self.bookingStartDateTImeLabel.textColor = UIColor.black
                self.bookingEndDateTImeLabel.textColor = UIColor.black
            }
            else {
                self.bookingStartDateTImeLabel.textColor = ColorUtils.MS_CANCEL_RED
                self.bookingEndDateTImeLabel.textColor = ColorUtils.MS_CANCEL_RED
            }
        }
    }
    
    func prepareDateTimeForDateValidation(date: Date, hour: Int, amPM: String) -> Date {
        //print("ManualBookingVC prepareDateTimeForDateValidation | hour : \(hour)")
        
        let hour24 = (amPM == "AM") ? ((hour == 12) ? 0 : hour) : ((hour == 12) ? hour : hour + 12)
        //print("ManualBookingVC prepareDateTimeForDateValidation | hour24 : \(hour24)")
        
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        //print("ManualBookingVC prepareDateTimeForDateValidation | day : \(day)")
        
        var dateComponents = DateComponents()
        dateComponents.timeZone = TimeZone(secondsFromGMT: 0) // This line should be here
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour24
        dateComponents.minute = 0
        dateComponents.second = 0
        
        return calendar.date(from: dateComponents)!
    }
    
    
    func prepareManualBooking() {
        let startDateSimpleStr = CalendarUtils.retrieveSimpleDateString(fromDate: self.manualBookingStartDate!)
        let startTimeHour12 = self.manualBookingStartTimeHourInt!
        let startTimehour24 = (self.manualBookingStartTimeAmPmStatus == "AM") ? ((startTimeHour12 == 12) ? 0 : startTimeHour12) : ((startTimeHour12 == 12) ? startTimeHour12 : startTimeHour12 + 12)
        
        let endDateSimpleStr = CalendarUtils.retrieveSimpleDateString(fromDate: self.manualBookingEndDate!)
        let endTimeHour12 = self.manualBookingEndTimeHourInt!
        let endTimehour24 = (self.manualBookingEndTimeAmPmStatus == "AM") ? ((endTimeHour12 == 12) ? 0 : endTimeHour12) : ((endTimeHour12 == 12) ? endTimeHour12 : endTimeHour12 + 12)
        
        let manualBookingStartDateTimeStr = "\(startDateSimpleStr)T\(startTimehour24):00Z"
        let manualBookingEndDateTimeStr = "\(endDateSimpleStr)T\(endTimehour24):00Z"
        let currentDateTime = "\(CalendarUtils.currentDeviceDateSimpleString())T\(CalendarUtils.currentDeviceTimeShort())Z"
        
        //print("ManualBookingVC prepareManualBooking | manualBookingStartDateTimeStr = \(manualBookingStartDateTimeStr)")
        //print("ManualBookingVC prepareManualBooking | manualBookingEndDateTimeStr = \(manualBookingEndDateTimeStr)")
        //print("ManualBookingVC prepareManualBooking | currentDateTime = \(currentDateTime)")
        
        var manualBookingDetails: [String : Any] = ["space" : self.mySpaceID!]
        manualBookingDetails["fromDate"] = "\(manualBookingStartDateTimeStr)"
        manualBookingDetails["toDate"] = "\(manualBookingEndDateTimeStr)"
        manualBookingDetails["dateBookingMade"] = "\(currentDateTime)"
        
        if let gName = self.guestName {
            manualBookingDetails["guestName"] = gName
        }
        
        if let gContactNum = self.guestContactNumber {
            manualBookingDetails["guestContactNumber"] = gContactNum
        }
        
        if let gEmail = self.guestEmail {
            manualBookingDetails["guestEmail"] = gEmail
        }
        
        if let fee = self.spaceReservationFee {
            manualBookingDetails["cost"] = "\(fee)"
        }
        
        if let eTypeID = self.manualBookingEventTypeID {
            manualBookingDetails["eventTypeId"] = "\(eTypeID)"
        }
        
        if let seatingID = self.manualBookingSeatingArrangementID {
            manualBookingDetails["seatingArrangementId"] = "\(seatingID)"
        }
        
        if let guestCount = self.expectedGuestCount {
            manualBookingDetails["noOfGuests"] = "\(guestCount)"
        }
        
        if let statusIDStr = self.reservationStatusIDStr {
            manualBookingDetails["reservationStatus"] = ["id" : statusIDStr]
        }
        
        if let amenities = self.amenitiesRequested {
            manualBookingDetails["extrasRequested"] = amenities
        }
        
        if let remarksStr = self.remarks {
            manualBookingDetails["remark"] = remarksStr
        }
        
        // Update or Add new manual boooking
        if let bookingId = self.manualBookingID {
            print("Updating Manual Booking with BookingID : \(bookingId)")
        }
        else {
            print("ManualBookingVC performManualBooking | manualBookingDetails : \(manualBookingDetails)")
            
            self.performManualBooking(bookingDetails: manualBookingDetails)
        }
    }
    
    
    func performManualBooking(bookingDetails: [String : Any]) {
        self.showActivityIndicator()
        
        WebServiceCall.addManualBooking(withBookingDetails: bookingDetails, successBlock: { (result: Any) in
            //print("ManualBookingVC performManualBooking | WebServiceCall.addManualBooking success | result : \(result)")
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
                
                if let responseBookingID = result as? String {
                    if responseBookingID != "0" {
                        self.addManualBookingResponseAlert(message: AlertMessages.MANUAL_BOOKING_SUCCESS_MESSAGE, isSuccess: true)
                    }
                    else {
                        self.addManualBookingResponseAlert(message: AlertMessages.MANUAL_BOOKING_ERROR_MESSAGE, isSuccess: false)
                    }
                }
            })
        })
        { (errorCode: Int, error: String) in
            print("ManualBookingVC performManualBooking | WebServiceCall.addManualBooking failed | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addManualBookingResponseAlert(message: AlertMessages.SERVER_ERROR_MESSAGE, isSuccess: false)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addManualBookingResponseAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE, isSuccess: false)
                }
                else {
                    self.addManualBookingResponseAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE, isSuccess: false)
                }
            })
        }
    }
    
    
    
    
    
    func addManualBookingResponseAlert(message: String, isSuccess: Bool) {
        //var alertController: UIAlertController!
        if isSuccess {
            alertController = UIAlertController(title: AlertMessages.SUCCESS_TITLE, message: message, preferredStyle: .alert)
            let goBackAction = UIAlertAction(title: AlertMessages.GO_BACK_ACTION, style: .default) { (alert: UIAlertAction) in
                self.popViewController()
            }
            alertController!.addAction(goBackAction)
        }
        else {
            alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
            alertController!.addAction(dismissAction)
        }
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func manualBookingConfirmationAlert() {
        
        let alertMessage = (self.manualBookingID != nil) ? AlertMessages.MANUAL_BOOKING_UPDATE_CONFIRMATION_MESSAGE : AlertMessages.MANUAL_BOOKING_NEW_CONFIRMATION_MESSAGE + " from \(self.bookingStartDateTImeLabel.text!) to \(self.bookingEndDateTImeLabel.text!) ?"
        
        alertController = UIAlertController(title: AlertMessages.CONFIRM_TITLE, message: alertMessage, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertMessages.YES_ACTION, style: .default) { (alert: UIAlertAction) in
            self.prepareManualBooking()
        }
        
        let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
        alertController!.addAction(noAction)
        alertController!.addAction(yesAction)
        
        self.present(alertController!, animated: true, completion: nil)
    }
    
}




// MARK: - UIPickerView DataSource and Delegate
extension ManualBookingViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == hourPicker {
            return StaticData.HOUR_ARRAY.count
        }
        return StaticData.AMPM_ARRAY.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == hourPicker {
            return StaticData.HOUR_ARRAY[row]
        }
        return StaticData.AMPM_ARRAY[row]
    }
}




// MARK: - UICollectionViewDataSource and Delegate
extension ManualBookingViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.eventTypeCollectionView {
            return self.eventTypes.count
        }
        else if collectionView == self.seatingCollectionView  {
            return self.seatingArrangements.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return populateCollectionViewCell(forCollectionView: collectionView, forIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.eventTypeCollectionView {
            self.manualBookingEventTypeID = Int(self.eventTypes[indexPath.item].id)
            let selectedEventTypeCell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = selectedEventTypeCell.viewWithTag(1)
            backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
        }
        else if collectionView == self.seatingCollectionView  {
            self.manualBookingSeatingArrangementID = Int(self.seatingArrangements[indexPath.item].id)
            let selectedSeatingACell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = selectedSeatingACell.viewWithTag(1)
            backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == self.eventTypeCollectionView {
            let deselectedEventTypeCell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = deselectedEventTypeCell.viewWithTag(1)
            backgroundView?.layer.borderColor = UIColor.clear.cgColor
            
        }
        else if collectionView == self.seatingCollectionView  {
            let deselectedSeatingACell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = deselectedSeatingACell.viewWithTag(1)
            backgroundView?.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    
    func populateCollectionViewCell(forCollectionView collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectableItemCell", for: indexPath)
        
        let backgroundView = collectionViewCell.viewWithTag(1)
        
        let itemLabel = collectionViewCell.viewWithTag(2) as! UILabel
        let itemIconImageView = collectionViewCell.viewWithTag(3) as! UIImageView
        
        if collectionView == self.eventTypeCollectionView  {
            let eventTypeAtIndex = self.eventTypes[indexPath.item]
            itemLabel.text = eventTypeAtIndex.name!
            itemIconImageView.sd_setImage(with: URL(string: eventTypeAtIndex.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            if manualBookingEventTypeID == Int(eventTypeAtIndex.id) {
                backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
            else {
                backgroundView?.layer.borderColor = UIColor.clear.cgColor
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        else if collectionView == self.seatingCollectionView {
            let seatingAtIndex = self.seatingArrangements[indexPath.item]
            itemLabel.text = seatingAtIndex.name!
            itemIconImageView.sd_setImage(with: URL(string: seatingAtIndex.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            if manualBookingSeatingArrangementID == Int(seatingAtIndex.id) {
                backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
            else {
                backgroundView?.layer.borderColor = UIColor.clear.cgColor
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        
        return collectionViewCell
    }
}


// MARK: - UICollectionViewDelegateFlowLayout
extension ManualBookingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: ecvCellWidth, height: ecvCellHeight)
    }
}
