//
//  MySpacesViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class MySpacesViewController: BaseViewController {
    
    @IBOutlet weak var mySpacesTableView: UITableView!
    @IBOutlet weak var noSpacesMessageLabel: UILabel!
    
    var mySpaces: [Space] = []
    var selectedMySpaceID: Int?
    
    let noSpacesMessage = "You have not listed any spaces. \nGo to MillionSpaces.com on web to list your space now."
    
    var alertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToMySpacesFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToMySpacesFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) { // ~ 10s
            alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = "My Spaces"
        
        self.noSpacesMessageLabel.text = noSpacesMessage
        
        self.retrieveMySpaces()
    }
    
    func retrieveMySpaces() {
        self.showActivityIndicator()
        
        self.mySpaces.removeAll()
        
        WebServiceCall.retrieveMySpaces(successBlock: { (results: Any) in
            //print("MySpacesViewController retrieveMySpaces | success | results : \(results)")
            
            if let arrMySpaces = results as? [[String : Any]] {
                
                for i in arrMySpaces {
                    let mySpace = Space()
                    mySpace.initSpace(withDictionary: i)
                    self.mySpaces.append(mySpace)
                }
                
                DispatchQueue.main.sync(execute: {
                    if self.mySpaces.count > 0 {
                        self.mySpacesTableView.isHidden = false
                        self.noSpacesMessageLabel.isHidden = true
                        self.mySpacesTableView.reloadData()
                    }
                    else {
                        self.mySpacesTableView.isHidden = true
                        self.noSpacesMessageLabel.isHidden = false
                    }
                    
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    //self.addSpacesReloadAlert(message: AlertMessages.INTERNAL_ERROR)
                    self.mySpacesTableView.isHidden = true
                    self.noSpacesMessageLabel.isHidden = false
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("MySpacesViewController retrieveMySpaces | failure | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.mySpacesTableView.reloadData()
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpacesReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpacesReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpacesReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func addSpacesReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retrieveMySpaces()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func presentContactMSAlert() {
        alertController = UIAlertController(title: AlertMessages.HELLO_TITLE, message: AlertMessages.EDIT_MYSPACE_MESSAGE, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
            let callAction = UIAlertAction(title: AlertMessages.CALL_ACTION, style: .default) { (alert: UIAlertAction) in
                if let phoneNumUrl = URL(string: "tel://\(Constants.MS_PHONE_NUMBER)") {
                    UIApplication.shared.open(phoneNumUrl)
                }
            }
            alertController!.addAction(callAction)
        }
        
        self.present(alertController!, animated: true, completion: nil)
    }
    
    @objc func mySpaceAvailabilityButtonPressed(_ sender: UIButton) {
        //print("MySpacesVC mySpaceAvailabilityButtonPressed | MySpaceID : \(sender.accessibilityIdentifier!)")
        self.selectedMySpaceID = Int(sender.accessibilityIdentifier!)
        self.performSegue(segueID: "HostCalendarSegue")
    }
    
    @objc func mySpaceBookingsButtonPressed(_ sender: UIButton) {
        //print("MySpacesVC mySpaceAvailabilityButtonPressed | MySpaceID : \(sender.accessibilityIdentifier!)")
        self.selectedMySpaceID = Int(sender.accessibilityIdentifier!)
        self.performSegue(segueID: "MySpaceBookingsSegue")
    }
    
    @objc func mySpaceEditButtonPressed(_ sender: UIButton) {
        self.presentContactMSAlert()
    }
    
    //MARK: - IBActions
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MySpaceBookingsSegue" {
            let mySpaceBookingsVC = segue.destination as! MySpaceBookingsViewController
            mySpaceBookingsVC.mySpaceID = self.selectedMySpaceID
        }
        else if segue.identifier == "HostCalendarSegue" {
            let hostCalendarVC = segue.destination as! HostCalendarViewController
            hostCalendarVC.mySpaceID = self.selectedMySpaceID
        }
    }
    
    
}



/*
 main view - 1
 image - 2
 space name - 3
 address - 4
 availability b - 5
 bookings b - 6
 edit b - 7
 */

extension MySpacesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mySpaces.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let mySpacesCell = tableView.dequeueReusableCell(withIdentifier: "MySpacesCell", for: indexPath)
        
        if let parentView = mySpacesCell.viewWithTag(1) {
            parentView.layer.shadowColor = UIColor.black.cgColor
            parentView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
            parentView.layer.shadowRadius = 2
            parentView.layer.shadowOpacity = 0.5
        }
        
        let mySpaceAtIndex = self.mySpaces[indexPath.row]
        
        (mySpacesCell.viewWithTag(2) as! UIImageView).sd_setImage(with: URL(string: NetworkConfiguration.IMAGE_BASE_URL + mySpaceAtIndex.thumbnailImage!), completed: nil)
        (mySpacesCell.viewWithTag(3) as! UILabel).text = mySpaceAtIndex.name!
        (mySpacesCell.viewWithTag(4) as! UILabel).text = mySpaceAtIndex.fullAdrress!
        
        (mySpacesCell.viewWithTag(5) as! UIButton).accessibilityIdentifier = mySpaceAtIndex.id.description
        (mySpacesCell.viewWithTag(5) as! UIButton).addTarget(self, action: #selector(self.mySpaceAvailabilityButtonPressed(_:)), for: .touchUpInside)
        
        (mySpacesCell.viewWithTag(6) as! UIButton).accessibilityIdentifier = mySpaceAtIndex.id.description
        (mySpacesCell.viewWithTag(6) as! UIButton).addTarget(self, action: #selector(self.mySpaceBookingsButtonPressed(_:)), for: .touchUpInside)
        
        (mySpacesCell.viewWithTag(7) as! UIButton).addTarget(self, action: #selector(self.mySpaceEditButtonPressed(_:)), for: .touchUpInside)
        
        return mySpacesCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mySpaceAtIndex = self.mySpaces[indexPath.row]
        openSpaceDetailsViewController(forSpaceBySpaceID: mySpaceAtIndex.id)
    }
    
}
