//
//  SignUpViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var constSignUpSubViewLeading: NSLayoutConstraint!
    @IBOutlet weak var constSignUpSubViewTrailing: NSLayoutConstraint!
    @IBOutlet weak var closeButtonLabel: UILabel!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var fullNameValidationStatusLabel: UILabel!
    @IBOutlet weak var emailValidationStatusLabel: UILabel!
    @IBOutlet weak var passwordValidationStatusLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signUpSuccessView: UIView!
    @IBOutlet weak var signUpSuccessMessageLabel: UILabel!
    
    // MARK: - Local variables
    var isValidName: Bool!
    var isValidEmail: Bool!
    var isValidPassword: Bool!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        //addKeyBoardDismissOnTap()
    }
    
    
    // TODO: Handle APPLICATION_IN_FOREGROUND
    
    
    override func viewWillAppear(_ animated: Bool) {
        configureView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        handleSignUpSuccessViewVisibility(isHidden: true)
    }
    
    func returnedToSignUpFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) {
            popToRootViewController()
        }
    }
    
    private func configureView() {
        self.signUpSuccessView.isHidden = true
        self.closeButtonLabel.layer.borderWidth = 1.0
        self.closeButtonLabel.layer.borderColor = UIColor.darkGray.cgColor
        self.closeButtonLabel.layer.cornerRadius = self.closeButtonLabel.frame.size.width/2
        self.fullNameValidationStatusLabel.text = ""
        self.emailValidationStatusLabel.text = ""
        self.passwordValidationStatusLabel.text = ""
        self.fullNameTextField.text = ""
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        self.isValidName = false
        self.isValidEmail = false
        self.isValidPassword = false
    }
    
    func handleSignUpSuccessViewVisibility(isHidden: Bool) {
        UIView.transition(with: self.signUpSuccessView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.signUpSuccessView.isHidden = isHidden
        }, completion: nil)
    }
    
    func openTermsOfUseAndPrivacyPolicy() {
        if let url = URL(string: NetworkConfiguration.TERMS_PRIVACY_URL) {
            UIApplication.shared.open(url)
        }
    }
    
    // MARK: - IBActions
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        validateUserSignUp()
    }
    
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        popViewController()
    }
    
    @IBAction func signUpSuccessGoToSignInButtonPressed(_ sender: UIButton) {
        handleSignUpSuccessViewVisibility(isHidden: true)
        popViewController()
    }
    
    @IBAction func termsOfServiceButtonPressed(_ sender: UIButton) {
        openTermsOfUseAndPrivacyPolicy()
    }
    
    @IBAction func privacyPolicyButtonPressed(_ sender: UIButton) {
        openTermsOfUseAndPrivacyPolicy()
    }
    
    @IBAction func clostButtonPressed(_ sender: UIButton) {
        popViewController()
    }
    
    // MARK: - UIWebViewDelegate
    func webViewDidStartLoad(_ webView: UIWebView) {
        showActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        hideActivityIndicator()
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.fullNameTextField {
            self.fullNameValidationStatusLabel.text = ""
            self.isValidName = false
        }
        else if textField == self.emailTextField {
            self.emailValidationStatusLabel.text = ""
            self.isValidEmail = false
        }
        else {
            self.passwordValidationStatusLabel.text = ""
            self.isValidPassword = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text!.trimmingCharacters(in: .whitespaces)
        
        if textField == self.fullNameTextField {
            if textField.text!.count > 0 {
                if Utils.containsOnlyLetters(input: textField.text!) {
                    self.fullNameValidationStatusLabel.text = ""
                    self.isValidName = true
                }
                else {
                    self.fullNameValidationStatusLabel.text = AlertMessages.SIGN_UP_ENTER_VALID_NAME_WARNING
                    self.isValidName = false
                }
            }
            else {
                self.fullNameValidationStatusLabel.text = AlertMessages.SIGN_UP_NAME_REQUIRED_WARNING
                self.isValidName = false
            }
        }
        else if textField == self.emailTextField {
            textField.text = textField.text!.replacingOccurrences(of: " ", with: "")
            
            if textField.text!.count > 0 {
                if Utils.isValidEmailAddress(emailEntry: textField.text!) {
                    self.emailValidationStatusLabel.text = ""
                    self.isValidEmail = false
                    validateUserEmail(email: textField.text!)
                }
                else {
                    self.emailValidationStatusLabel.text = AlertMessages.SIGN_UP_ENTER_VALID_EMAIL_WARNING
                    self.isValidEmail = false
                }
            }
            else {
                self.emailValidationStatusLabel.text = AlertMessages.SIGN_UP_EMAIL_REQUIRED_WARNING
                self.isValidEmail = false
            }
        }
        else {
            textField.text = textField.text!.replacingOccurrences(of: " ", with: "")
            if textField.text!.count > 0 {
                if textField.text!.count >= 8 {
                    self.passwordValidationStatusLabel.text = ""
                    self.isValidPassword = true
                }
                else {
                    self.passwordValidationStatusLabel.text = AlertMessages.SIGN_UP_ENTER_VALID_PASSWORD_WARNING
                    self.isValidPassword = false
                }
            }
            else {
                self.passwordValidationStatusLabel.text = AlertMessages.SIGN_UP_PASSWORD_REQUIRED_WARNING
                self.isValidPassword = false
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.fullNameTextField.isFirstResponder {
            self.emailTextField.becomeFirstResponder()
        }
        else if self.emailTextField.isFirstResponder {
            self.passwordTextField.becomeFirstResponder()
        }
        else {
            self.passwordTextField.resignFirstResponder()
            validateUserSignUp()
        }
        
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension SignUpViewController {
    
    func validateUserEmail(email: String) {
        WebServiceCall.checkEmail(email: email, successBlock: { (results: Any) in
            // print("SignUpViewController validateUserEmail | success results : \(results)")
            DispatchQueue.main.sync(execute: {
                if results as? String == "false" {
                    self.emailValidationStatusLabel.text = ""
                    self.isValidEmail = true
                }
                else {
                    self.emailValidationStatusLabel.text = AlertMessages.SIGN_UP_ACCOUNT_WITH_EMAIL_EXISTS_ERROR_MESSAGE
                    self.isValidEmail = false
                }
            })
        })
        { (errorCode: Int, error: String) in
            //print("SignUpViewController validateUserEmail | failure error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.emailValidationStatusLabel.text = AlertMessages.SIGN_UP_EMAIL_VALIDATION_FAILED_ERROR_MESSAGE
                self.isValidEmail = false
                self.view.endEditing(true)
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentServerErrorAlert()
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentNetworkUnavailabilityErrorAlert()
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func validateUserSignUp() {
        if isValidName! && isValidEmail && isValidPassword {
            let name = self.fullNameTextField.text!
            let email = self.emailTextField.text!
            let passwordEncrypted = self.passwordTextField.text!.sha256()
            //print("SignUpVC validateUserSignUp | password : \(self.passwordTextField.text!)")
            //print("SignUpVC validateUserSignUp | passwordEncrypted : \(passwordEncrypted)")
            let userDetails = ["name": name, "email": email, "password": passwordEncrypted]
            signUpUser(userDetails: userDetails)
        }
    }
    
    func signUpUser(userDetails: [String : String]) {
        showActivityIndicator()
        WebServiceCall.registerUser(userDetails: userDetails, successBlock: { (results: Any) in
            //print("SignUpViewController registerUser | success results : \(results)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if (results as! String).contains("User creation success") {
                    self.signUpSuccessMessageLabel.text = "Your account creation was successful. Please verify your account creation from the link that was sent to the email address : \(self.emailTextField.text!) for the account activation process."
                    self.handleSignUpSuccessViewVisibility(isHidden: false)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SIGN_UP_FAILED_ERROR_MESSAGE)
                }
            })
        },
                                    failureBlock: { (errorCode: Int, error: String) in
                                        //print("SignUpViewController registerUser | failure error : \(error)")
                                        DispatchQueue.main.sync(execute: {
                                            self.hideActivityIndicator()
                                            if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                                                self.presentServerErrorAlert()
                                            }
                                            else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                                                self.presentNetworkUnavailabilityErrorAlert()
                                            }
                                            else {
                                                self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                                            }
                                        })
        })
    }
}

