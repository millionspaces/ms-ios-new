//
//  PaymentWebViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/15/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
//import WebKit
// , WKUIDelegate

class PaymentWebViewController: BaseViewController {
    
    // MARK: - IBOutlets
    //@IBOutlet var webView: WKWebView!
    @IBOutlet weak var viewForWebView: UIView!
    @IBOutlet weak var webViewOld: UIWebView!
    
    
    // MARK: - Class constants/variables
    //var webView: WKWebView!
    var bookingSpace: Space!
    var bookingRequest: SpaceBookingRequest!
    var bookingResponse: [String : Any]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("PaymentWebViewVC viewDidLoad")
        
        self.navigationItem.title = "Payment"
        
        self.webViewOld.delegate = self
        

        //let myURL = URL(string:"https://www.apple.com")
        ///let myRequest = URLRequest(url: myURL!)
        //self.webView.load(myRequest)
    }
    
    
//    override func loadView() {
//        print("PaymentWebViewVC loadView")
//
//        let webConfiguration = WKWebViewConfiguration()
//        self.webView = WKWebView(frame: .zero, configuration: webConfiguration)
//        self.webView.uiDelegate = self
//        self.view = self.webView
//    }
//
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.popToHomeWhenAppGoesForeground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        configurePaymentWebview()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func configurePaymentWebview() {
    
        let paramData = NSMutableData(data: "bookingId=\(bookingResponse["id"] as! Int)".data(using: String.Encoding.utf8)!)
        paramData.append("&payLaterEnabled=\(Bool(truncating: (bookingResponse["isPayLaterEnabled"] as! Int) as NSNumber))".data(using: String.Encoding.utf8)!)
        
        paramData.append("&manualPaymentAllowed=\(Bool(truncating: (bookingResponse["isBookingTimeEligibleForManualPayment"] as! Int) as NSNumber))".data(using: String.Encoding.utf8)!)
        paramData.append("&referenceId=\(bookingResponse["referenceId"] as! String)".data(using: String.Encoding.utf8)!)
        paramData.append("&spaceName=\(bookingResponse["spaceName"] as! String)".data(using: String.Encoding.utf8)!)
        paramData.append("&organization=\(bookingResponse["organization"] as! String)".data(using: String.Encoding.utf8)!)
        paramData.append("&address=\(bookingResponse["address"] as! String)".data(using: String.Encoding.utf8)!)
        paramData.append("&reservationDate=\(bookingResponse["reservationDate"] as! String)".data(using: String.Encoding.utf8)!)
        
        paramData.append("&reservationTime=2:00PM - 3:00PM".data(using: String.Encoding.utf8)!)
        //paramData.append("&reservationTime=\(bookingResponse["reservationTime"] as! [[String : Any]])".data(using: String.Encoding.utf8)!)
        
        paramData.append("&total=\(Double(bookingResponse["bookingCharge"] as! Int))".data(using: String.Encoding.utf8)!)
        paramData.append("&isiOSDevice=true)".data(using: String.Encoding.utf8)!)
        
        
        
        self.showActivityIndicator()
        
        WebServiceCall.loadPaymentWebView(paramData: paramData, successBlock: { (results: Any) in
            
            print("PaymentWebViewVC configurePaymentWebview | results : \(results)")
            
            DispatchQueue.main.sync(execute: {
                self.webViewOld.loadHTMLString(results as! String, baseURL: nil)
                //self.webView.loadHTMLString(results as! String, baseURL: URL(string: NetworkConfiguration.PAYMENT_WEB_URL))
                self.hideActivityIndicator()
            })

        }) { (errorCode, error) in
            print("PaymentWebViewVC configurePaymentWebview | failed | errorCode : \(errorCode), error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
        
        /*
         BookingVC proceedButtonPressed | WebServiceCall.bookSpace : results --> {
         address = "Maitland Cres, Colombo 00700, Sri Lanka";
         advanceOnly = 0;
         bookingCharge = 1;
         discount = "<null>";
         id = 5640;
         isBookingTimeEligibleForManualPayment = 1;
         isPayLaterEnabled = 1;
         isUserEligibleForManualPayment = 1;
         orderId = MQ05640;
         organization = "Four Seasons";
         promotion =     {
         };
         referenceId = MQ05640;
         reservationDate = "2018-11-27";
         reservationTime =     (
         {
         fromDate = 6PM;
         toDate = 10PM;
         }
         );
         spaceName = "Alphine Room";
         }
         */
        
        /*
         String urlParameters = "bookingId="+reserveObject.getId()+
         "&payLaterEnabled=" + reserveObject.isPayLaterEnabled() +
         "&manualPaymentAllowed=" + reserveObject.isUserEligibleForManualPayment() +
         "&referenceId=" + reserveObject.getReferenceId() +
         "&spaceName=" + reserveObject.getSpaceName() +
         "&organization=" + reserveObject.getOrganization() +
         "&address=" + reserveObject.getAddress() +
         "&reservationDate=" + reserveObject.getReservationDate() +
         "&reservationTime=" + getReservationTime(reserveObject.getReservationTime()) +
         "&total=" + reserveObject.getAmount() ;
         */
    }
    
    @objc func popToHomeWhenAppGoesForeground() {
        //alertController?.dismiss(animated: false, completion: nil)
        self.popToRootViewController()
    }
    
    @IBAction func returnToHomeButtonPressed(_ sender: UIButton) {
        self.popToRootViewController()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PaymentWebViewController: UIWebViewDelegate {
 
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.showActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hideActivityIndicator()
    }
}


extension PaymentWebViewController {
    
    /*
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        // Allow navigation for requests loading external web content resources.
        guard navigationAction.targetFrame?.isMainFrame != false else {
            decisionHandler(.allow)
            return
        }
    }*/
}
