//
//  BookingInitialViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class BookingInitialViewController: BaseViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var BookingInitialTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    // MARK: - Class properties / variables
    var bookingSpace: Space!
    var eventTypesECVHeight: CGFloat = 0.0
    var seatingECVHeight: CGFloat = 0.0
    var ecvCellWidth: CGFloat!
    let ecvCellHeight: CGFloat = 100.0
    var selectedEventType: EventType?
    var selectedSpaceSeatingArrangement: SpaceSeatingArrangement?
    var selectedGuestCount: Int?
    var currentMaximumParticipantCount: Int!
    var hasValidDetails = false
    
    // MARK: - Constants
    let GUEST_COUNT_TABLE_VIEW_CELL_ID = "GuestCountCell"
    let GUEST_COUNT_TABLE_VIEW_CELL_TAGS = (TEXT_FIELD: 1, MESSAGE_LABEL: 2, MIN_MAX_LABEL: 3)
    let COLLECTION_VIEW_EMBEDDED_TABLE_VIEW_CELL_ID = "CollectionViewEmbeddedCell"
    let COLLECTION_VIEW_CELL_ID = "SelectableItemCell"
    let INITIAL_PLACEHOLDER_COLOR = UIColor.init(red: 199.0/255.0, green: 199.0/255.0, blue: 205.0/255.0, alpha: 1.0)
    let INVALID_PLACEHOLDER_COLOR = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
    let TEXT_FIELD_VALID_FONT_COLOR = ColorUtils.TEXT_VALIDATION_VALID_FONT_COLOR_BLACK
    let TEXT_FIELD_INVALID_FONT_COLOR = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
//    let GUEST_COUNT_MAX_ALERT_MESSAGE = "Your expected guest count exceeds the maximum capacity. The host may or may not fulfill your request."
//    let GUEST_COUNT_MIN_ALERT_MESSAGE = "Your expected guest count is below the minimum capacity. You will be charged for the minimum defined by the host."
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToBookingInitialScreenFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToBookingInitialScreenFromBackground() {
        if Utils.didPassDuration(durationInHours: 0.5) {
            popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = Strings.BOOKING_ENGINE
        self.eventTypesECVHeight = 60 + Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.bookingSpace.eventTypes.count, cvCellHeight: Int(ecvCellHeight), itemsPerRow: 3)
        self.seatingECVHeight = 60 + Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.bookingSpace.seatingArrangements.count, cvCellHeight: Int(ecvCellHeight), itemsPerRow: 3)
        self.ecvCellWidth = (Utils.SCREEN_WIDTH - 20.0) / 3.0
        //self.nextButton.backgroundColor = UIColor.lightGray
        selectDefaultValues()
        validateSelection()
        updateCurrentMaximumParticipantCount()
    }
    
    func updateCurrentMaximumParticipantCount() {
        if let selectedSpaceSeatingA = self.selectedSpaceSeatingArrangement {
            self.currentMaximumParticipantCount = selectedSpaceSeatingA.participantCount!
        }
        else {
            self.currentMaximumParticipantCount = self.bookingSpace!.participantCount!
        }
        
        self.BookingInitialTableView.reloadData()
    }
    
    func selectDefaultValues() {
        self.selectedEventType = (self.bookingSpace!.eventTypes[0] as! EventType)
        self.selectedSpaceSeatingArrangement = (self.bookingSpace!.seatingArrangements[0] as! SpaceSeatingArrangement)
        self.selectedGuestCount = self.selectedSpaceSeatingArrangement!.participantCount
    }
    
    func prepareExpectedGuestCountPlaceholderText() -> String {
        if self.bookingSpace!.blockChargeType == 2 {
            return "Min. - \(self.bookingSpace!.minimumParticipantCount)  |  Max. - \(self.currentMaximumParticipantCount!)"
        }
        else {
            return "Max. - \(self.currentMaximumParticipantCount!)"
        }
    }
    
    func prepareLimitWarningMessage(forExpectedGuestCount count: Int) -> String {
        if bookingSpace!.blockChargeType == 2 {
            if count < self.bookingSpace!.minimumParticipantCount {
                return "Your expected guest count is below the minimum capacity of \(self.bookingSpace!.minimumParticipantCount). You will be charged for the minimum defined by the host."
            }
        }
        
        if count > self.currentMaximumParticipantCount {
            return "Your expected guest count exceeds the maximum capacity of \(self.currentMaximumParticipantCount!). The host may or may not fulfill your request."
        }
        else {
            return ""
        }
    }
    
    func openBookingCalendarScreen() {
        let storyboard = UIStoryboard.init(name: Constants.MAIN_STORYBOARD, bundle: nil)
        let bookingCalendarVC = storyboard.instantiateViewController(withIdentifier: Constants.VC_ID_BOOKING_CALENDAR_SCREEN) as! BookingCalendarViewController
        bookingCalendarVC.bookingSpace = bookingSpace!
        bookingCalendarVC.selectedEventType = selectedEventType!
        bookingCalendarVC.selectedSeatingArrangement = selectedSpaceSeatingArrangement!.arrangement
        bookingCalendarVC.selectedGuestCount = selectedGuestCount!
        self.navigationController?.pushViewController(bookingCalendarVC, animated: true)
    }
    
    
    // MARK: - IBActions
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if self.hasValidDetails {
            openBookingCalendarScreen()
        }
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        openNotificationViewController()
    }
    
    
    // MARK: - UITextFieldDelegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: self.INITIAL_PLACEHOLDER_COLOR)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.count)! > 0, let numberFromText = Int(textField.text!) {
            if numberFromText != 0 {
                textField.text = String(numberFromText)
                self.selectedGuestCount = (textField.text!.count <= 6) ? numberFromText : nil
                textField.textColor = (textField.text!.count <= 6) ? self.TEXT_FIELD_VALID_FONT_COLOR : self.TEXT_FIELD_INVALID_FONT_COLOR
            }
            else {
                self.selectedGuestCount = nil
                textField.text = nil
                textField.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: self.INVALID_PLACEHOLDER_COLOR)
            }
        }
        else {
            self.selectedGuestCount = nil
            textField.text = nil
            textField.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: self.INVALID_PLACEHOLDER_COLOR)
        }
        
        validateSelection()
        self.BookingInitialTableView.reloadData()
    }
    
    @objc func textChanged(_ textField: UITextField) {
        if textField.text!.count > 6 {
            textField.deleteBackward()
        }
        else {
            textField.textColor = self.TEXT_FIELD_VALID_FONT_COLOR
        }
    }
    
    
    func validateSelection() {
        self.hasValidDetails = false
        self.nextButton.backgroundColor = UIColor.lightGray
        
        guard self.selectedEventType != nil else {
            return
        }
        
        guard self.selectedSpaceSeatingArrangement != nil else {
            return
        }
        
        guard self.selectedGuestCount != nil else {
            let guestCountCell = self.BookingInitialTableView.cellForRow(at: IndexPath.init(row: 0, section: 0))
            if let guestCountText = guestCountCell?.viewWithTag(1) as? UITextField {
                guestCountText.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: self.INVALID_PLACEHOLDER_COLOR)
            }
            return
        }
        
        self.hasValidDetails = true
        self.nextButton.backgroundColor = ColorUtils.MS_BLUE_COLOR
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


// MARK: - UITableView DataSource & Delegate
extension BookingInitialViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableView.automaticDimension
        }
        else if indexPath.row == 1 {
            return self.eventTypesECVHeight
        }
        else {
            return self.seatingECVHeight
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableView.automaticDimension
        }
        else if indexPath.row == 1 {
            return self.eventTypesECVHeight
        }
        else {
            return self.seatingECVHeight
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let guestCountCell = tableView.dequeueReusableCell(withIdentifier: self.GUEST_COUNT_TABLE_VIEW_CELL_ID, for: indexPath)
            if let guestCountText = guestCountCell.viewWithTag(self.GUEST_COUNT_TABLE_VIEW_CELL_TAGS.TEXT_FIELD) as? UITextField {
                guestCountText.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: .lightGray)
                self.addNumberPadDoneButton(textField: guestCountText)
                guestCountText.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
                guestCountText.text = self.selectedGuestCount?.description
                
                let alertLabel = guestCountCell.viewWithTag(self.GUEST_COUNT_TABLE_VIEW_CELL_TAGS.MESSAGE_LABEL) as! UILabel
                if guestCountText.text!.count > 0, let numberFromText = Int(guestCountText.text!) {
                    alertLabel.text = self.prepareLimitWarningMessage(forExpectedGuestCount: numberFromText)
                    alertLabel.backgroundColor = ColorUtils.BOOKING_RESERVATION_STATUS_COLORS.CANCELLED
                }
                
                if let minMaxLabel = guestCountCell.viewWithTag(self.GUEST_COUNT_TABLE_VIEW_CELL_TAGS.MIN_MAX_LABEL) as? UILabel {
                    minMaxLabel.text = "(\(self.prepareExpectedGuestCountPlaceholderText()))"
                }
            }
            
            return guestCountCell
        }
        else {
            let collectionViewEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: self.COLLECTION_VIEW_EMBEDDED_TABLE_VIEW_CELL_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
            collectionViewEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.row)
            collectionViewEmbeddedCell.collectionView.allowsMultipleSelection = false
            if indexPath.row == 1 {
                (collectionViewEmbeddedCell.viewWithTag(1) as! UILabel).text = Strings.SELECT_EVENT_TYPE
            }
            else {
                (collectionViewEmbeddedCell.viewWithTag(1) as! UILabel).text = Strings.SELECT_SEATING_ARRANGEMENTS
            }
            
            return collectionViewEmbeddedCell
        }
    }
}


// MARK: - UICollectionView DataSource and Delegate
extension BookingInitialViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            return self.bookingSpace.eventTypes.count
        }
        else {
            return self.bookingSpace.seatingArrangements.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return populateCollectionViewCell(forCollectionView: collectionView, forIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            self.selectedEventType = bookingSpace.eventTypes[indexPath.item] as? EventType
            let selectedEventTypeCell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = selectedEventTypeCell.viewWithTag(1)
            backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
        }
        else {
            self.selectedSpaceSeatingArrangement = (bookingSpace.seatingArrangements[indexPath.item] as! SpaceSeatingArrangement)
            let selectedSeatingACell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = selectedSeatingACell.viewWithTag(1)
            backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
            updateCurrentMaximumParticipantCount()
        }
        
        validateSelection()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            let deselectedEventTypeCell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = deselectedEventTypeCell.viewWithTag(1)
            backgroundView?.layer.borderColor = UIColor.clear.cgColor
        }
        else {
            let deselectedSeatingACell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = deselectedSeatingACell.viewWithTag(1)
            backgroundView?.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func populateCollectionViewCell(forCollectionView collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: self.COLLECTION_VIEW_CELL_ID, for: indexPath)
        let backgroundView = collectionViewCell.viewWithTag(1)
        let itemLabel = collectionViewCell.viewWithTag(2) as! UILabel
        let itemIconImageView = collectionViewCell.viewWithTag(3) as! UIImageView
        
        if collectionView.tag == 1 {
            let eventTypeAtIndex = bookingSpace.eventTypes[indexPath.item] as! EventType
            itemLabel.text = eventTypeAtIndex.name!
            itemIconImageView.sd_setImage(with: URL(string: eventTypeAtIndex.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            if selectedEventType == eventTypeAtIndex {
                backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
            else {
                backgroundView?.layer.borderColor = UIColor.clear.cgColor
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        else {
            let seatingAtIndex = self.bookingSpace.seatingArrangements[indexPath.item] as! SpaceSeatingArrangement
            itemLabel.text = seatingAtIndex.arrangement.name!
            itemIconImageView.sd_setImage(with: URL(string: seatingAtIndex.arrangement.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            if let selectedSpaceSeatingA = selectedSpaceSeatingArrangement?.arrangement, selectedSpaceSeatingA.id == Int16(seatingAtIndex.id) {
                backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
            else {
                backgroundView?.layer.borderColor = UIColor.clear.cgColor
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        
        return collectionViewCell
    }
}


// MARK: - UICollectionViewDelegateFlowLayout
extension BookingInitialViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: ecvCellWidth, height: ecvCellHeight)
    }
}
