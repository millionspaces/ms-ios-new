//
//  BaseViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import SDWebImage

class BaseViewController: UIViewController {
    
    // MARK: - Base Class Properties
    private var backgroundViewForActivityIndicator: UIView?
    private var baseActivityIndicator: UIActivityIndicatorView?
    var baseCommonAlertController: UIAlertController?
    var paymentSessionTimerLabel: UILabel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initBaseActivityIndicator()
        addKeyBoardDismissOnTap()
    }
    
    
    // MARK: - Base Activity Indicator
    private func initBaseActivityIndicator() {
        // Background UIView
        self.backgroundViewForActivityIndicator = UIView.init(frame: self.view.bounds)
        self.backgroundViewForActivityIndicator?.backgroundColor = UIColor.clear
        // UIActivityIndicatorView
        self.baseActivityIndicator = UIActivityIndicatorView()
        self.baseActivityIndicator!.frame = CGRect(x: self.view.frame.size.width/2 - 25.0, y: self.view.frame.size.height/2 - 25.0, width: 50.0, height: 50.0)
        self.baseActivityIndicator!.style = .whiteLarge
        self.baseActivityIndicator!.color = UIColor.init(red: 0.0, green: 142.0/255.0, blue: 218.0/255.0, alpha: 1.0)
        self.baseActivityIndicator!.hidesWhenStopped = true
        self.backgroundViewForActivityIndicator?.addSubview(self.baseActivityIndicator!)
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            self.baseActivityIndicator?.startAnimating()
        }
        self.view.addSubview(self.backgroundViewForActivityIndicator!)
    }
    
    func hideActivityIndicator() {
        self.backgroundViewForActivityIndicator?.removeFromSuperview()
        self.baseActivityIndicator?.stopAnimating()
    }
    
    
    // MARK: - Delay functions with duration
    func delay(_ delay: Double, closure: @escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    
    // MARK: - Set Window Orientation Restriction
    func setWindowOrientationRestriction(restriction: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.deviceOrientationRestriction = restriction ? "Portrait" : "All"
    }
    
    
    // MARK: - Keyboard Dismiss on tap
    func addKeyBoardDismissOnTap() {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    // MARK: - Present Commonly Navigated View Controllers
    func openSignInViewController() {
        let storyboard = UIStoryboard.init(name: Constants.MAIN_STORYBOARD, bundle: nil)
        let signInVC = storyboard.instantiateViewController(withIdentifier: Constants.VC_ID_SIGN_IN_SCREEN) as! SignInViewController
        let navController = UINavigationController.init(rootViewController: signInVC)
        navController.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.present(navController, animated: true, completion: nil)
    }
    
    func openNotificationViewController() {
        let storyboard = UIStoryboard.init(name: Constants.MAIN_STORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constants.VC_ID_NOTIFICATION_SCREEN)
        self.present(vc, animated: true, completion: nil)
    }
    
    func openSpaceDetailsViewController(forSpaceBySpaceID spaceID: Int) {
        let spaceDetailsController = self.storyboard?.instantiateViewController(withIdentifier: Constants.VC_ID_SPACE_DETAILS_SCREEN) as! SpaceDetailsViewController
        spaceDetailsController.displayingSpaceId = spaceID
        self.navigationController?.pushViewController(spaceDetailsController, animated: true)
    }
    
    
    // MARK: - Navigation
    func performSegue(segueID: String) {
        self.performSegue(withIdentifier: segueID, sender: self)
    }
    
    func dismissViewController() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func popToRootViewController() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func popToViewController(viewController: Swift.AnyClass) {
        self.navigationController?.backToViewController(viewController: viewController.self)
    }
    
    func popToSplashViewController() {
        self.popToViewController(viewController: SplashViewController.classForCoder())
    }
    
    
    // MARK: - Add done button for number pad style keyboard
    func addNumberPadDoneButton(textField: UITextField) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.tintColor = UIColor.white
        keyboardToolbar.barTintColor = UIColor.init(red: 63.0/255.0, green: 63.0/255.0, blue: 63.0/255.0, alpha: 1.0)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
    
    
    // MARK: - UIAlertControllers
    func presentServerErrorAlert() {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE_2, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentNetworkUnavailabilityErrorAlert() {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE_2, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentDismissableAlertWithMessage(title: String, message: String) {
        self.baseCommonAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        self.baseCommonAlertController!.addAction(dismissAction)
        self.present(self.baseCommonAlertController!, animated: true, completion: nil)
    }
    
    
    // MARK: Payment Session Timer Label
    func addPaymentSessionTimerLabelToNavBar() {
        if let navigationBar = self.navigationController?.navigationBar {
            self.paymentSessionTimerLabel = UILabel.init(frame: CGRect(x: Utils.SCREEN_WIDTH - 60.0, y: 10.0, width: 60.0, height: 24.0))
            self.paymentSessionTimerLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 18.0)
            self.paymentSessionTimerLabel?.textColor = .white
            self.paymentSessionTimerLabel?.layer.masksToBounds = true
            navigationBar.addSubview(self.paymentSessionTimerLabel!)
        }
    }
    
    func setPaymentSessionTimerLabelText(text: String) {
        self.paymentSessionTimerLabel?.text = text
    }
    
    func removePaymentSessionTimerLabelFromNavBar() {
        self.paymentSessionTimerLabel?.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



// MARK: - Handle Guest Check out
extension BaseViewController {
    
    func presentGuestCheckoutAlert() {
        self.baseCommonAlertController = UIAlertController(title: "To continue", message: "", preferredStyle: .alert)
        
        let guestAction = UIAlertAction(title: AlertMessages.PROCEED_AS_GUEST_ACTION, style: .cancel) { (action: UIAlertAction) in
            self.presentEmailSubmissionAlertInGuestMode()
        }
        
        let signInAction = UIAlertAction(title: AlertMessages.SIGN_IN_ACTION, style: .default) { (action: UIAlertAction) in
            self.openSignInViewController()
        }
        
        self.baseCommonAlertController!.addAction(guestAction)
        self.baseCommonAlertController!.addAction(signInAction)
        
        self.present(self.baseCommonAlertController!, animated: true, completion: nil)
    }
    
    
    func presentEmailSubmissionAlertInGuestMode() {
        self.baseCommonAlertController = UIAlertController(title: AlertMessages.PROCEED_AS_GUEST_TITLE, message: AlertMessages.PROCEED_AS_GUEST_EMAIL_MESSAGE, preferredStyle: .alert)
        
        self.baseCommonAlertController?.addTextField(configurationHandler: { (textField: UITextField) in
            textField.placeholder = Strings.EMAIL
            textField.text = nil
            textField.keyboardAppearance = .dark
            textField.addTarget(self, action: #selector(self.alertViewTextFieldChanged(_:)), for: .editingChanged)
        })
        

        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        self.baseCommonAlertController!.addAction(dismissAction)
        
        let submitAction = UIAlertAction(title: AlertMessages.SUBMIT_ACTION, style: .default) { (action: UIAlertAction) in
            if let textField = self.baseCommonAlertController?.textFields?[0], textField.text!.count > 0 {
                let email = textField.text!
                print("SessionVC | Entered File Name : \(email)")
                // NOTE: - Do email submission for Guest
                self.signInUserForGuest(email: email)
            }
        }
        submitAction.isEnabled = false
        self.baseCommonAlertController!.addAction(submitAction)
        self.present(self.baseCommonAlertController!, animated: true, completion: nil)
    }
    
    @objc func alertViewTextFieldChanged(_ sender: Any) {
        let textField = sender as! UITextField
        var responder : UIResponder! = textField
        while !(responder is UIAlertController) { responder = responder.next }
        let alert = responder as! UIAlertController
        let submitAction = alert.actions[1]
        
        if textField.text?.count == 0 {
            submitAction.isEnabled = false
        }
        else {
            submitAction.isEnabled = Utils.isValidEmailAddress(emailEntry: textField.text!)
        }
    }
    
    func signInUserForGuest(email: String) {
        self.showActivityIndicator()
        WebServiceCall.authenticateUser(username: email, password: nil, successBlock: { (results: Any) in
            print("SignInVC signInUser | Success results : \(results)")
            DispatchQueue.main.sync(execute: {
                AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("SignInVC signInUser | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SIGN_IN_WRONG_CREDENTIALS_ERROR_MESSAGE)
                }
                else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentServerErrorAlert()
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentNetworkUnavailabilityErrorAlert()
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
}


extension UINavigationController {
    
    func backToViewController(viewController: Swift.AnyClass) {
        for element in viewControllers {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

