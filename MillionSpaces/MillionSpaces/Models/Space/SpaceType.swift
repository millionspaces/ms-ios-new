//
//  SpaceType.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceType: NSObject {
    
    var spaceTypeID: Int?
    var spaceTypeName: String?
    var spaceTypeIconUrlStr: String?
    
    func initSpaceType(withDictionary dic: [String : Any]) {
        if let id = dic["id"] as? Int {
            self.spaceTypeID = id
        }
        
        if let name = dic["name"] as? String {
            self.spaceTypeName = name
        }
        
        if let mobileIcon = dic["mobileIcon"] as? String {
            self.spaceTypeIconUrlStr = mobileIcon
        }
    }
}
