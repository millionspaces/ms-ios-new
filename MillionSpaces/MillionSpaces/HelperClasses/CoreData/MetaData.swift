//
//  MetaData.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import CoreData

typealias CompletionBlock = (_ completion: Bool, _ statusCode: Int) -> Void

class MetaData: NSObject {
    
    // MARK: - Instance Variables
    private static let ENTITIES = (EventType: "EventType",
                                   Amenity: "Amenity",
                                   AmenityUnit: "AmenityUnit",
                                   CPolicy: "CancellationPolicy",
                                   SpaceRule: "Rule",
                                   SeatingArrangement: "SeatingArrangement",
                                   MeasurementUnit: "MeasurementUnit")
    
    
    
    // Async function sequence for saving meta data before proceeding to the app from separate APIs
    static func saveAllMetaData(completionBlock: @escaping CompletionBlock) {
        print("MetaData saveAllMetaData")
        
        // 1
        self.saveEventTypes { (completion: Bool, statusCode: Int) in
            if completion {
                print("MetaData saveAllMetaData | saveEventTypesTemp complete true")
                
                // 2
                self.saveAmenities (completionBlock: { (completion: Bool, statusCode: Int) in
                    if completion {
                        print("MetaData saveAllMetaData | saveAmenitiesTemp complete true")
                        
                        // 3
                        self.saveAmenityUnits (completionBlock: { (completion: Bool, statusCode: Int) in
                            if completion {
                                print("MetaData saveAllMetaData | saveAmenityUnitsTemp complete true")
                                
                                // 4
                                self.saveCancellationPolicies (completionBlock: { (completion: Bool, statusCode: Int) in
                                    if completion {
                                        print("MetaData saveAllMetaData | saveCancellationPoliciesTemp complete true")
                                        
                                        // 5
                                        self.saveSpaceRules (completionBlock: { (completion: Bool, statusCode: Int) in
                                            if completion {
                                                print("MetaData saveAllMetaData | saveSpaceRulesTemp complete true")
                                                
                                                // 6
                                                self.saveSeatingArrangements (completionBlock: { (completion: Bool, statusCode: Int) in
                                                    if completion {
                                                        print("MetaData saveAllMetaData | saveSeatingArrangementsTemp complete true")
                                                        
                                                        // 7
                                                        self.saveMeasurementUnits(completionBlock: { (completion: Bool, statusCode: Int) in
                                                            if completion {
                                                                print("MetaData saveAllMetaData | saveMeasurementUnitsTemp complete true")
                                                                // Completion
                                                                completionBlock(true, Constants.STATUS_CODE_REQUEST_SUCCESS)
                                                            }
                                                            else {
                                                                print("MetaData saveAllMetaData | saveMeasurementUnitsTemp complete false")
                                                                completionBlock(false, statusCode)
                                                            }
                                                        })
                                                        
                                                    }
                                                    else {
                                                        print("MetaData saveAllMetaData | saveSeatingArrangementsTemp complete false")
                                                        completionBlock(false, statusCode)
                                                    }
                                                })
                                                
                                            }
                                            else {
                                                print("MetaData saveAllMetaData | saveSpaceRulesTemp complete false")
                                                completionBlock(false, statusCode)
                                            }
                                        })
                                        
                                    }
                                    else {
                                        print("MetaData saveAllMetaData | saveCancellationPoliciesTemp complete false")
                                        completionBlock(false, statusCode)
                                    }
                                })
                                
                                
                            }
                            else {
                                print("MetaData saveAllMetaData | saveAmenityUnitsTemp complete false")
                                completionBlock(false, statusCode)
                            }
                        })
                        
                    }
                    else {
                        print("MetaData saveAllMetaData | saveAmenitiesTemp complete false")
                        completionBlock(false, statusCode)
                    }
                })
                
            }
            else {
                print("MetaData saveAllMetaData | saveEventTypesTemp complete false")
                completionBlock(false, statusCode)
            }
        }
    }
    
    
    // MARK: - Temp functions for Saving of Meta Data
    
    static func saveEventTypes(completionBlock: @escaping CompletionBlock) {
        WebServiceCall.allEventTypes(successBlock: { (results: Any) in
            print("MetaData saveEventTyes | results : \(results)")
            let arrResults = results as! [[String : Any]]
            self.saveDataTo(entity: ENTITIES.EventType, data: arrResults)
            completionBlock(true, Constants.STATUS_CODE_REQUEST_SUCCESS)
        })
        { (errorCode: Int, error: String) in
            print("MetaData saveEventTyes | error : \(error)")
            completionBlock(false, errorCode)
        }
    }
    
    
    static func saveAmenities(completionBlock: @escaping CompletionBlock) {
        WebServiceCall.allAmenities(successBlock: { (results: Any) in
            print("MetaData saveAmenitiesTemp | results : \(results)")
            let arrResults = results as! [[String : Any]]
            self.saveDataTo(entity: ENTITIES.Amenity, data: arrResults)
            completionBlock(true, Constants.STATUS_CODE_REQUEST_SUCCESS)
        })
        { (errorCode: Int, error: String) in
            print("MetaData saveAmenities | error : \(error)")
            completionBlock(false, errorCode)
        }
    }
    
    
    static func saveAmenityUnits(completionBlock: @escaping CompletionBlock) {
        WebServiceCall.amenityUnits(successBlock: { (results: Any) in
            print("MetaData saveAmenityUnitsTemp | results : \(results)")
            let arrResults = results as! [[String : Any]]
            self.saveDataTo(entity: ENTITIES.AmenityUnit, data: arrResults)
            completionBlock(true, Constants.STATUS_CODE_REQUEST_SUCCESS)
        }) { (errorCode: Int, error: String) in
            print("MetaData saveAmenityUnits | error : \(error)")
            completionBlock(false, errorCode)
        }
    }
    
    
    static func saveCancellationPolicies(completionBlock: @escaping CompletionBlock) {
        WebServiceCall.allCancellationPolicies(successBlock: { (results: Any) in
            print("MetaData saveCancellationPoliciesTemp | results : \(results)")
            let arrResults = results as! [[String : Any]]
            self.saveDataTo(entity: ENTITIES.CPolicy, data: arrResults)
            completionBlock(true, Constants.STATUS_CODE_REQUEST_SUCCESS)
        }) { (errorCode: Int, error: String) in
            print("MetaData | saveCancellationPolicies - error : \(error)")
            completionBlock(false, errorCode)
        }
    }
    
    
    static func saveSpaceRules(completionBlock: @escaping CompletionBlock) {
        WebServiceCall.allSpaceRules(successBlock: { (results: Any) in
            print("MetaData saveSpaceRulesTemp | results : \(results)")
            let arrResults = results as! [[String : Any]]
            self.saveDataTo(entity: ENTITIES.SpaceRule, data: arrResults)
            completionBlock(true, Constants.STATUS_CODE_REQUEST_SUCCESS)
        }) { (errorCode: Int, error: String) in
            print("MetaData | saveSpaceRules - error : \(error)")
            completionBlock(false, errorCode)
        }
    }
    
    
    static func saveSeatingArrangements(completionBlock: @escaping CompletionBlock) {
        WebServiceCall.allSeatingArrangements(successBlock: { (results: Any) in
            print("MetaData saveSeatingArrangementsTemp | results : \(results)")
            let arrResults = results as! [[String : Any]]
            self.saveDataTo(entity: ENTITIES.SeatingArrangement, data: arrResults)
            completionBlock(true, Constants.STATUS_CODE_REQUEST_SUCCESS)
        }) { (errorCode: Int, error: String) in
            print("MetaData | saveSeatingArrangements - error : \(error)")
            completionBlock(false, errorCode)
        }
    }
    
    
    static func saveMeasurementUnits(completionBlock: @escaping CompletionBlock) {
        WebServiceCall.measurementUnits(successBlock: { (results: Any) in
            print("MetaData saveMeasurementUnitsTemp | results : \(results)")
            let arrResults = results as! [[String : Any]]
            self.saveDataTo(entity: ENTITIES.MeasurementUnit, data: arrResults)
            completionBlock(true, Constants.STATUS_CODE_REQUEST_SUCCESS)
        }) { (errorCode: Int, error: String) in
            print("MetaData | saveMeasurementUnits - error : \(error)")
            completionBlock(false, errorCode)
        }
    }
    
    
    
    
    // MARK: - Delete data currently in Entities
    static func deleteEventTypes() {
        self.deleteData(entity: ENTITIES.EventType)
    }
    
    static func deleteAmenities() {
        self.deleteData(entity: ENTITIES.Amenity)
    }
    
    static func deleteAmenityUnits() {
        self.deleteData(entity: ENTITIES.AmenityUnit)
    }
    
    static func deleteCancellationPolicies() {
        self.deleteData(entity: ENTITIES.CPolicy)
    }
    
    static func deleteSpaceRules() {
        self.deleteData(entity: ENTITIES.SpaceRule)
    }
    
    static func deleteSeatingArrangements() {
        self.deleteData(entity: ENTITIES.SeatingArrangement)
    }
    
    static func deleteMeasurementUnits() {
        self.deleteData(entity: ENTITIES.MeasurementUnit)
    }
    
    static func deleteAll() {
        deleteAmenities()
        deleteAmenityUnits()
        deleteEventTypes()
        deleteCancellationPolicies()
        deleteSpaceRules()
        deleteSeatingArrangements()
        deleteMeasurementUnits()
    }
    
    
    // MARK: - Retrieve all entity meta data
    static func retrieveEventTypes() -> NSMutableArray {
        return self.metaData(entity: ENTITIES.EventType)
    }
    
    static func retrieveAmenities() -> NSMutableArray {
        return self.metaData(entity: ENTITIES.Amenity)
    }
    
    static func retrieveAmenityUnits() -> NSMutableArray {
        return self.metaData(entity: ENTITIES.AmenityUnit)
    }
    
    static func retrieveCancellationPolicies() -> NSMutableArray {
        return self.metaData(entity: ENTITIES.CPolicy)
    }
    
    static func retrieveSpaceRules() -> NSMutableArray {
        return self.metaData(entity: ENTITIES.SpaceRule)
    }
    
    static func retrieveSeatingArrangements() -> NSMutableArray {
        return self.metaData(entity: ENTITIES.SeatingArrangement)
    }
    
    static func retrieveMeasurementUnits() -> NSMutableArray {
        return self.metaData(entity: ENTITIES.MeasurementUnit)
    }
    
    
    
    // MARK: - Retrieve meta data by ID
    static func retrieveEventType(with id: Int) -> EventType? {
        return self.metaData(with: id, entity: ENTITIES.EventType) as? EventType
    }
    
    static func retrieveAmenity(with id: Int) -> Amenity? {
        return self.metaData(with: id, entity: ENTITIES.Amenity) as? Amenity
    }
    
    static func retrieveAmenityUnit(with id: Int) -> AmenityUnit? {
        return self.metaData(with: id, entity: ENTITIES.AmenityUnit) as? AmenityUnit
    }
    
    static func retrieveCancellationPolicies(with id: Int) -> CancellationPolicy? {
        return self.metaData(with: id, entity: ENTITIES.CPolicy) as? CancellationPolicy
    }
    
    static func retrieveSpaceRule(with id: Int) -> Rule? {
        return self.metaData(with: id, entity: ENTITIES.SpaceRule) as? Rule
    }
    
    static func retrieveSeatingArrangement(with id: Int) -> SeatingArrangement? {
        return self.metaData(with: id, entity: ENTITIES.SeatingArrangement) as? SeatingArrangement
    }
    
    static func retrieveMeasurementUnit(with id: Int) -> MeasurementUnit? {
        return self.metaData(with: id, entity: ENTITIES.MeasurementUnit) as? MeasurementUnit
    }
    
    
    
    // MARK: - Init NSManagedObjectContext instance
    private static func managedContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    // MARK: - Creation or updating of data upon Saving of data
    private static func saveDataTo(entity: String, data: [[String : Any]]) {
        
        let container = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        container.performBackgroundTask { (context: NSManagedObjectContext) in
            let _entity = NSEntityDescription.entity(forEntityName: entity, in: context)
            
            for item in data {
                createOrUpdate(item: item as NSDictionary, entity: _entity!, context: context)
            }
            
            do {
                try context.save()
                print("MetaData | saveDataTo : \(entity) - Data saved/updated")
            } catch {
                print("MetaData | saveDataTo : \(entity) - Error saving data")
            }
        }
    }
    
    private static func createOrUpdate(item:NSDictionary, entity:NSEntityDescription, context: NSManagedObjectContext) {
        if entity.name == self.ENTITIES.EventType {  // Saving Event Type entity data
            // Creating new data
            let objectToSave = NSManagedObject(entity: entity, insertInto: context)
            objectToSave.setValue(item["id"] as? Int, forKey: "id")
            objectToSave.setValue(item["name"] as? String, forKey: "name")
            objectToSave.setValue(item["icon"] as? String, forKey: "icon")
            objectToSave.setValue(item["mobileIcon"] as? String, forKey: "iconUrl")
            objectToSave.setValue(item["timeStamp"] as? Int, forKey: "timestamp")
        }
        else if entity.name == ENTITIES.Amenity {    // Saving Amenity entity data
            let objectToSave = NSManagedObject(entity: entity, insertInto: context)
            objectToSave.setValue(item["id"] as? Int, forKey: "id")
            objectToSave.setValue(item["name"] as? String, forKey: "name")
            objectToSave.setValue(item["icon"] as? String, forKey: "icon")
            objectToSave.setValue(item["mobileIcon"] as? String, forKey: "iconUrl")
            objectToSave.setValue(item["amenityUnit"] as? Int, forKey: "amenityUnit")
            objectToSave.setValue(item["timeStamp"] as? Int, forKey: "timestamp")
        }
        else if entity.name == ENTITIES.AmenityUnit {    // Saving Amenity Unit entity data
            let objectToSave = NSManagedObject(entity: entity, insertInto: context)
            objectToSave.setValue(item["id"] as? Int, forKey: "id")
            objectToSave.setValue((item["name"] as? String)?.lowercased(), forKey: "name")
        }
        else if entity.name == ENTITIES.CPolicy {    // Saving Cancellation Policy entity data
            let objectToSave = NSManagedObject(entity: entity, insertInto: context)
            objectToSave.setValue(item["id"] as? Int, forKey: "id")
            objectToSave.setValue(item["name"] as? String, forKey: "name")
            objectToSave.setValue(item["description"] as? String, forKey: "policyDescription")
            objectToSave.setValue(item["refundRate"] as? Float, forKey: "refundRate")
            objectToSave.setValue(item["timeStamp"] as? Int, forKey: "timestamp")
        }
        else if entity.name == ENTITIES.SpaceRule {    // Saving Space Rule entity data
            let objectToSave = NSManagedObject(entity: entity, insertInto: context)
            objectToSave.setValue(item["id"] as? Int, forKey: "id")
            objectToSave.setValue(item["name"] as? String, forKey: "name")
            objectToSave.setValue(item["displayName"] as? String, forKey: "displayName")
            objectToSave.setValue("ico-" + (item["icon"] as! String), forKey: "icon")
            //objectToSave.setValue(item["mobileIcon"] as? String, forKey: "iconUrl")
            objectToSave.setValue(item["timeStamp"] as? Int, forKey: "timestamp")
        }
        else if entity.name == ENTITIES.SeatingArrangement {    // Saving Seating Arrangement entity data
            let objectToSave = NSManagedObject(entity: entity, insertInto: context)
            objectToSave.setValue(item["id"] as? Int, forKey: "id")
            objectToSave.setValue(item["name"] as? String, forKey: "name")
            objectToSave.setValue(item["icon"] as? String, forKey: "icon")
            objectToSave.setValue(item["mobileIcon"] as? String, forKey: "iconUrl")
            objectToSave.setValue(item["timeStamp"] as? Int, forKey: "timestamp")
        }
        else if entity.name == ENTITIES.MeasurementUnit {     // Saving Measurement Unit entity data
            let objectToSave = NSManagedObject(entity: entity, insertInto: context)
            objectToSave.setValue(item["id"] as? Int, forKey: "id")
            objectToSave.setValue((item["name"] as? String)?.lowercased(), forKey: "name")
        }
    }
    
    
    // MARK: - Get data item by ID
    private static func metaData(with id: Int, entity: String) -> Any? {
        var returnObject: AnyObject?
        let managedContext = self.managedContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        request.predicate = NSPredicate(format: "id = %d", id)
        
        do {
            let results = try managedContext.fetch(request)
            if results.count == 1 {
                returnObject = results[0] as AnyObject
                return returnObject!
            }
            else{
                print("MetaData | metaDataWithId : \(entity) - Warning - No existing result")
                return nil
            }
        } catch {
            print ("MetaData | metaDataWithId : \(entity) - Error occured")
            // TODO: Handle error
            return nil
        }
    }
    
    
    // MARK: - Get all data
    private static func metaData(entity: String) -> NSMutableArray {
        let returnArray = NSMutableArray()
        let managedContext = self.managedContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        
        do {
            let results = try managedContext.fetch(request)
            returnArray.addObjects(from: results)
            if results.count > 0 {
                return returnArray
            }
            else{
                print("MetaData | metaData : \(entity) - Warning - No existing results")
                return returnArray
            }
        } catch {
            print ("MetaData | metaData : \(entity) - Error occured")
            // TODO: Handle error
            return returnArray
        }
    }
    
    
    // MARK: - Delete data
    private static func deleteData(entity: String) {
        let managedContext = self.managedContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try managedContext.execute(deleteRequest)
        } catch {
            print("MetaData | deleteData : \(entity) -  Error")
            // TODO: Handle error
        }
    }
}
