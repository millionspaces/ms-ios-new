//
//  SpaceCalendarInfo.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceCalendarInfo: NSObject {
    
    var spaceID: Int!
    
    var availabilityMethod: String! //m     availabilityMethod HOUR_BASE BLOCK_BASE
    var availabilityModules: [SpaceAvailabilityModule] = []
    
    var blockChargeType: Int = 0 //     if availabilityMethod == BLOCK_BASE ---> 1 = Space Only Blocks | 2 = Per Guest Based Blocks
    
    var noticePeriod = 0
    var bufferTime = 0    // bufferTime
    var calendarStartDate: String! // m calendarStart
    var calendarEndDate: String!  // m calendarEnd
    
    //var futureBookings: [SpaceFutureBooking] = [] //futureBookingDates
    var calendarBookings: [SpaceCalendarBooking] = [] // NEW
    
    
    
    func initSpaceCalendarInfo(withSpaceID spaceID: Int, andCalendarInfoDic info: [String : Any], forGuestCalendar: Bool) {
        
        self.spaceID = spaceID
        
        self.availabilityMethod = info["availabilityMethod"] as! String
        
        let availabilityArr = info["availability"] as! [[String : Any]]
        
        for d in availabilityArr {
            let am = SpaceAvailabilityModule()
            am.initSpaceAvailabilityModule(withDictionary: d)
            self.availabilityModules.append(am)
        }
        
        if self.availabilityMethod == "BLOCK_BASE" {
            if let blkChargeType = info["blockChargeType"] as? Int {
                self.blockChargeType = blkChargeType
            }
        }
        
        if let notice = info["noticePeriod"] as? Int {
            self.noticePeriod = notice
        }
        
        if let buffer = info["bufferTime"] as? Int {
            self.bufferTime = buffer
        }
        
        self.calendarStartDate = info["calendarStart"] as! String
        self.calendarEndDate = info["calendarEnd"] as! String
        
        
        if forGuestCalendar {
            
            if let futureBookingDates = info["futureBookingDates"] as? [[String : Any]] {
                
                for fb in futureBookingDates {
                    print("SpaceCalendarInfo initSpaceCalendarInfo | futureBookingDate at i | from : \(fb["from"] as! String) | to : \(fb["to"] as! String)")
                }
                
                self.addGuestCalendarBookings(withCalendarFutureBookingsArray: futureBookingDates, bufferTime: self.bufferTime)
            }
        }
        else {
            if let calendarBookingDates = info["bookingDates"] as? [[String : Any]] {
                
                for cb in calendarBookingDates {
                    print("SpaceCalendarInfo initSpaceCalendarInfo | calendarBookingDate at i | from : \(cb["from"] as! String) | to : \(cb["to"] as! String)")
                }
                
                self.addHostCalendarBookings(withCalendarBookingsArray: calendarBookingDates)
            }
        }
    }
    
    
    // Calendar Future Bookings
    func addGuestCalendarBookings(withCalendarFutureBookingsArray arr: [[String : Any]], bufferTime: Int) {
        for dic in arr {
            if let fbid = dic["id"] as? Int,
                let fbFrom = dic["from"] as? String,
                let fbTo = dic["to"] as? String,
                let isManual = dic["isManual"] as? Int {
                
                // From Date
                let fbFromDateTimeComponents = CalendarUtils.retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr: fbFrom)
                let fbFromDateTime = CalendarUtils.retrieveDateTimeFromComponents(dateStr: fbFromDateTimeComponents[0], timeStr: fbFromDateTimeComponents[1]+":00")
                //print("SpaceCalendarInfo addGuestCalendarFutureBookings | fbFromDateTime :- \(fbFromDateTime)")
                
                let fbFromDateTimeWithBufferTime = (bufferTime == 0) ? fbFromDateTime : CalendarUtils.retrieveDate(thatComesAfterHours: -bufferTime, fromDate: fbFromDateTime)
                //print("SpaceCalendarInfo addGuestCalendarFutureBookings | fbFromDateTimeWithBufferTime :- \(fbFromDateTimeWithBufferTime)")
                
                let fbFromDate = CalendarUtils.retrieveLocaleDate_2(fromSimpleDate: fbFromDateTimeWithBufferTime)
                //print("SpaceCalendarInfo addGuestCalendarFutureBookings | fbFromDate :- \(fbFromDate)")
                
                let fbFromHourInt = CalendarUtils.retrieveTimeHourIn24Hour(date: fbFromDateTimeWithBufferTime)
                //print("SpaceCalendarInfo addGuestCalendarFutureBookings | fbFromHourInt :- \(fbFromHourInt) \n|")
                
                
                
                // To Date
                let fbToDateTimeComponents = CalendarUtils.retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr: fbTo)
                let fbToDateTime = CalendarUtils.retrieveDateTimeFromComponents(dateStr: fbToDateTimeComponents[0], timeStr: fbToDateTimeComponents[1]+":00")
                //print("SpaceCalendarInfo addGuestCalendarFutureBookings | fbToDateTime :- \(fbToDateTime)")
                
                let fbToDateTimeWithBufferTime = (bufferTime == 0) ? fbToDateTime : CalendarUtils.retrieveDate(thatComesAfterHours: bufferTime, fromDate: fbToDateTime)
                //print("SpaceCalendarInfo addGuestCalendarFutureBookings | fbToDateTimeWithBufferTime :- \(fbToDateTimeWithBufferTime)")
                
                let fbToDate = CalendarUtils.retrieveLocaleDate_2(fromSimpleDate: fbToDateTimeWithBufferTime)
                //print("SpaceCalendarInfo addGuestCalendarFutureBookings | fbToDate :- \(fbToDate)")
                
                var fbToHourInt = CalendarUtils.retrieveTimeHourIn24Hour(date: fbToDateTimeWithBufferTime)
                //print("SpaceCalendarInfo addGuestCalendarFutureBookings | fbToHourInt :- \(fbToHourInt) \n \n")
                
                
                
                if fbFromDate == fbToDate && fbFromHourInt >= fbToHourInt {
                    // TEMP / NOTE
                    // To skip wrongly validated futurebookings where start time > end time
                }
                else if fbFromDate == fbToDate {
                    fbToHourInt = (fbFromHourInt > fbToHourInt - 1) ? fbToHourInt : fbToHourInt - 1
                    
                    // For Guest Calendar
                    let fBooking_G = SpaceCalendarBooking()
                    fBooking_G.initSpaceCalendarBooking(bookingId: fbid, fromDate: fbFromDate, fromTimeHourInt: fbFromHourInt, toDate: fbToDate, toTimeHourInt: fbToHourInt, isManual: isManual)
                    self.calendarBookings.append(fBooking_G)
                    
                }
                else {
                    let multipleDates = CalendarUtils.retrieveDates(fromDate: fbFromDate, toDate: fbToDate)
                    let datesCount = multipleDates.count
                    
                    for i in 0..<datesCount {
                        let date_i = multipleDates[i]
                        
                        if i == 0 {
                            // For Guest Calendar
                            let fBooking_G = SpaceCalendarBooking()
                            fBooking_G.initSpaceCalendarBooking(bookingId: fbid, fromDate: date_i, fromTimeHourInt: fbFromHourInt, toDate: date_i, toTimeHourInt: 23, isManual: isManual)
                            self.calendarBookings.append(fBooking_G)
                            
                        }
                        else if i == datesCount - 1 {
                            if !(fbToHourInt == 0 && bufferTime == 0) {
                                let toHour = fbToHourInt != 0 ? fbToHourInt - 1 : fbToHourInt
                                
                                // For Guest Calendar
                                let fBooking_G = SpaceCalendarBooking()
                                fBooking_G.initSpaceCalendarBooking(bookingId: fbid, fromDate: date_i, fromTimeHourInt: 0, toDate: date_i, toTimeHourInt: toHour, isManual: isManual)
                                self.calendarBookings.append(fBooking_G)
                            }
                        }
                        else {
                            // For Guest Calendar
                            let fBooking_G = SpaceCalendarBooking()
                            fBooking_G.initSpaceCalendarBooking(bookingId: fbid, fromDate: date_i, fromTimeHourInt: 0, toDate: date_i, toTimeHourInt: 23, isManual: isManual)
                            self.calendarBookings.append(fBooking_G)
                        }
                    }
                }
            }
        }
    }
    
    
    // Host Calendar Bookings
    func addHostCalendarBookings(withCalendarBookingsArray arr: [[String : Any]]) {
        
        for dic in arr {
            
            if let fbid = dic["id"] as? Int,
                let fbFrom = dic["from"] as? String,
                let fbTo = dic["to"] as? String,
                let isManual = dic["isManual"] as? Int {
                
                // From Date
                let fbFromDateTimeComponents = CalendarUtils.retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr: fbFrom)
                let bookingFromDate = CalendarUtils.retrieveLocaleDate(fromSimpleDateString: fbFromDateTimeComponents[0])
                //print("SpaceCalendarInfo addHostCalendarFutureBookings | bookingFromDate :- \(bookingFromDate!)")
                
                // From Time
                let fbFromHourInt = CalendarUtils.retrieve24HourIntComponent(fromTimeStr: fbFromDateTimeComponents[1])
                //print("SpaceCalendarInfo addHostCalendarFutureBookings | bookingFromDate :- \(fbFromHourInt) \n|")
                
                // To Date
                let fbToDateTimeComponents = CalendarUtils.retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr: fbTo)
                let bookingToDate = CalendarUtils.retrieveLocaleDate(fromSimpleDateString: fbToDateTimeComponents[0])
                //print("SpaceCalendarInfo addHostCalendarFutureBookings | bookingToDate :- \(bookingToDate!)")
                
                // To Time
                var fbToHourInt = CalendarUtils.retrieve24HourIntComponent(fromTimeStr: fbToDateTimeComponents[1])
                //print("SpaceCalendarInfo addHostCalendarFutureBookings | fbToHourInt :- \(fbToHourInt) \n \n")
                
                if bookingFromDate == bookingToDate && fbFromHourInt >= fbToHourInt {
                    // TEMP / NOTE
                    // To skip wrongly validated futurebookings where start time > end time
                }
                else if bookingFromDate == bookingToDate {
                    
                    fbToHourInt = (fbFromHourInt > fbToHourInt - 1) ? fbToHourInt : fbToHourInt - 1
                    
                    // For Host Calendar
                    let cBooking_H = SpaceCalendarBooking()
                    cBooking_H.initSpaceCalendarBooking(bookingId: fbid, fromDate: bookingFromDate!, fromTimeHourInt: fbFromHourInt, toDate: bookingToDate!, toTimeHourInt: fbToHourInt, isManual: isManual)
                    self.calendarBookings.append(cBooking_H)
                }
                else {
                    
                    let multipleDates = CalendarUtils.retrieveDates(fromDate: bookingFromDate!, toDate: bookingToDate!)
                    let datesCount = multipleDates.count
                    
                    for i in 0..<datesCount {
                        let date_i = multipleDates[i]
                        
                        if i == 0 {
                            // For Host Calendar
                            let cBooking_H = SpaceCalendarBooking()
                            cBooking_H.initSpaceCalendarBooking(bookingId: fbid, fromDate: date_i, fromTimeHourInt: fbFromHourInt, toDate: date_i, toTimeHourInt: 23, isManual: isManual)
                            self.calendarBookings.append(cBooking_H)
                        }
                        else if i == datesCount - 1 {
                            if fbToHourInt != 0 {
                                fbToHourInt -= 1
                                
                                // For Host Calendar
                                let cBooking_H = SpaceCalendarBooking()
                                cBooking_H.initSpaceCalendarBooking(bookingId: fbid, fromDate: date_i, fromTimeHourInt: 0, toDate: date_i, toTimeHourInt: fbToHourInt, isManual: isManual)
                                self.calendarBookings.append(cBooking_H)
                            }
                        }
                        else {
                            // For Host Calendar
                            let cBooking_H = SpaceCalendarBooking()
                            cBooking_H.initSpaceCalendarBooking(bookingId: fbid, fromDate: date_i, fromTimeHourInt: 0, toDate: date_i, toTimeHourInt: 23, isManual: isManual)
                            self.calendarBookings.append(cBooking_H)
                        }
                    }
                }
            }
        }
    }
    
}
