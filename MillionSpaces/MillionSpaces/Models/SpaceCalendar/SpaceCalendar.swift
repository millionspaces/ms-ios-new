//
//  SpaceCalendar.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceCalendar: NSObject {
    
    var todayDate: SpaceCalendarDate!
    var currentMonthKey: String!
    
    var spaceCalendarStartingDate: SpaceCalendarDate?    // From Space Object
    var spaceCalendarEndingDate: SpaceCalendarDate!      // From Space Object
    
    var startingMonthFirstDate: SpaceCalendarDate?       // First Date of the Starting Month based on Starting Date
    var currentMonthFirstDate: SpaceCalendarDate!        // First Date of the Current Month based on Today's Date
    var endingMonthLastDate: SpaceCalendarDate!          // Last Date of the Ending Month based on Ending Date
    
    var calendarDates: [SpaceCalendarDate]!              // Dates of the Space Calendar object
    var calendarDatesDictionary: NSMutableDictionary!
    
    
    // For Guest Calendar
    func initSpaceCalendar(withSpaceCalendarInfo calInfo: SpaceCalendarInfo) {
        
        // Today
        //let todayD = CalendarUtils.currentDateTime()[1] as! Date
        let todayDateObj = SpaceCalendarDate()
        todayDateObj.initSpaceCalendarDateForCurrentDate()
        self.todayDate = todayDateObj
        //print("SpaceCalendar initSpaceCalendar | 2) todayDate : \(todayDateObj.date!) | \(todayDateObj.simpleDate!)")
        
        self.currentMonthKey = self.generateUniqueMonthValue(forCalendarDate: todayDateObj)
        //print("SpaceCalendar initSpaceCalendar | 3) currentMonthKey : \(self.currentMonthKey!)")
        
        let eDate = calInfo.calendarEndDate!
        //print("SpaceCalendar initSpaceCalendar | 4) Cal-e-Date : \(eDate)")
        
        // Ending Date
        let endingDate = CalendarUtils.retrieveLocaleDate(fromSimpleDateString: eDate)!
        let endingDateObj = SpaceCalendarDate()
        endingDateObj.initSpaceCalendarDate(fromDate: endingDate)
        self.spaceCalendarEndingDate = endingDateObj
        //print("SpaceCalendar initSpaceCalendar | 5) endingDate : \(endingDateObj.date!) | \(endingDateObj.simpleDate!)")
        
        // First Date of Current Month
        let firstDateOfCurrentMonth = CalendarUtils.retrieveFirstDateOfMonth(fromDate: todayDateObj.date)
        let firstDateObj = SpaceCalendarDate()
        firstDateObj.initSpaceCalendarDate(fromDate: firstDateOfCurrentMonth)
        self.currentMonthFirstDate = firstDateObj
        //print("SpaceCalendar initSpaceCalendar | 6) firstDate : \(firstDateObj.date!) | \(firstDateObj.simpleDate!)")
        
        // Last Date of Ending Month
        let firstDateOfEndingMonth = CalendarUtils.retrieveFirstDateOfMonth(fromDate: endingDate)
        let lastDateOfEndingMonth = CalendarUtils.retrieveLastDateOfMonth(fromFirstDateOfMonth: firstDateOfEndingMonth)
        let lastDateObj = SpaceCalendarDate()
        lastDateObj.initSpaceCalendarDate(fromDate: lastDateOfEndingMonth)
        self.endingMonthLastDate = lastDateObj
        //print("SpaceCalendar initSpaceCalendar | 7) lastDate : \(lastDateObj.date!) | \(lastDateObj.simpleDate!) \n")
        
        let calDates = CalendarUtils.retrieveDates(fromDate: firstDateOfCurrentMonth, toDate: lastDateOfEndingMonth,  calendarStartDate: todayDateObj.date!, calendarEndDate: endingDate, spaceCalendarInfo: calInfo, forGuestCalendar: true)
        self.calendarDates = calDates
        self.calendarDatesDictionary = generateCalendarDatesByMonth(fromDates: calDates)
        
    }
    
    
    // For Host Calendar
    func initHostCalendar(withSpaceCalendarInfo calInfo: SpaceCalendarInfo) {
        
        // Today
        //let todayD = CalendarUtils.currentDateTime()[1] as! Date
        let todayDateObj = SpaceCalendarDate()
        todayDateObj.initSpaceCalendarDateForCurrentDate()
        self.todayDate = todayDateObj
        
        self.currentMonthKey = self.generateUniqueMonthValue(forCalendarDate: todayDateObj)
        
        // First Date of Current Month
        let firstDateOfCurrentMonth = CalendarUtils.retrieveFirstDateOfMonth(fromDate: todayDateObj.date)
        let firstDateCurrentMonthObj = SpaceCalendarDate()
        firstDateCurrentMonthObj.initSpaceCalendarDate(fromDate: firstDateOfCurrentMonth)
        self.currentMonthFirstDate = firstDateCurrentMonthObj
        
        let sDateStr = calInfo.calendarStartDate! // If calendar starts from published date
        //let sDateStr = todayDateObj.simpleDate! // If calendar starts from current month / today
        
        // Space Calendar Start Date
        let startingDate = CalendarUtils.retrieveLocaleDate(fromSimpleDateString: sDateStr)!
        let startingDateObj = SpaceCalendarDate()
        startingDateObj.initSpaceCalendarDate(fromDate: startingDate)
        self.spaceCalendarStartingDate = startingDateObj
        
        // Space Calendar Ending Date
        //let endingDate = CalendarUtils.retrieveLocaleDate(fromSimpleDateString: eDateStr)!
        let endingDate = CalendarUtils.retrieveDate(thatComesAfterYears: 2, fromDate: todayDateObj.date!)
        let endingDateObj = SpaceCalendarDate()
        endingDateObj.initSpaceCalendarDate(fromDate: endingDate)
        self.spaceCalendarEndingDate = endingDateObj
        
        
        // First Date of First Month
        let firstDateOfStartingMonth = CalendarUtils.retrieveFirstDateOfMonth(fromDate: startingDateObj.date)
        let firstDateObj = SpaceCalendarDate()
        firstDateObj.initSpaceCalendarDate(fromDate: firstDateOfStartingMonth)
        self.startingMonthFirstDate = firstDateObj
        
        // Last Date of Ending Month
        let firstDateOfEndingMonth = CalendarUtils.retrieveFirstDateOfMonth(fromDate: endingDate)
        let lastDateOfEndingMonth = CalendarUtils.retrieveLastDateOfMonth(fromFirstDateOfMonth: firstDateOfEndingMonth)
        let lastDateObj = SpaceCalendarDate()
        lastDateObj.initSpaceCalendarDate(fromDate: lastDateOfEndingMonth)
        self.endingMonthLastDate = lastDateObj
        
        
        let calDates = CalendarUtils.retrieveDates(fromDate: firstDateOfStartingMonth, toDate: lastDateOfEndingMonth,  calendarStartDate: startingDate, calendarEndDate: endingDate, spaceCalendarInfo: calInfo, forGuestCalendar: false)
        self.calendarDates = calDates
        self.calendarDatesDictionary = generateCalendarDatesByMonth(fromDates: calDates)
    }
    
    
    private func generateUniqueMonthValue(forCalendarDate calDate: SpaceCalendarDate) -> String {
        let calDateMonth = (calDate.month! >= 10) ? "\(calDate.month!)" : "0\(calDate.month!)"
        return "\(calDate.year!)\(calDateMonth)-\(calDate.monthDisplayName!)-\(calDate.year!)"
    }
    
    
    private func generateCalendarDatesByMonth(fromDates dates: [SpaceCalendarDate]) -> NSMutableDictionary {
        
        let returnMutableDictionary: NSMutableDictionary = [:]
        
        for calDate in dates {
            let dictionaryKeyForValueArray = self.generateUniqueMonthValue(forCalendarDate: calDate)
            
            if let valueArrayInDic = returnMutableDictionary[dictionaryKeyForValueArray] {
                (valueArrayInDic as! NSMutableArray).add(calDate)
            }
            else {
                let valueArray: NSMutableArray = []
                valueArray.add(calDate)
                returnMutableDictionary.setValue(valueArray, forKey: dictionaryKeyForValueArray)
            }
        }
        
        return returnMutableDictionary
    }
}
