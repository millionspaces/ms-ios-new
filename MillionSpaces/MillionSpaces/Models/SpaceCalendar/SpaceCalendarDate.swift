//
//  SpaceCalendarDate.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceCalendarDate: NSObject {
    
    var date: Date!
    var simpleDate: String!
    var simpleDate_2: String!
    
    var day: Int!
    var dayDisplayName: String!
    var dayOfWeek: Int!     // Will change depending on device locale --> Setting
    var dayOfWeekMS: Int!   // Standard used in a
    var month: Int!
    var monthDisplayName: String!
    var year: Int!
    
    var dateStatus: String! // 1 = Unavailable, 2 = Partially Available, 3 = Available
    
    var isCalendarDateToday: Bool!
    
    var availability: [SpaceAvailabilityModule] = []
    
    var calendarTime: SpaceCalendarTime?
    
    
    func initSpaceCalendarDateForCurrentDate() {
        let currentDate = CalendarUtils.currentDateTime()[1] as! Date
        
        self.date = currentDate
        
        let dayInt = CalendarUtils.retrieveDayFromDate(date: currentDate)
        self.day = dayInt
        let dayName = CalendarUtils.retrieveDayDisplayNameFromDate(date: currentDate)
        self.dayDisplayName = dayName
        self.dayOfWeek = CalendarUtils.retrieveDayofWeekFromDate(date: currentDate)
        self.dayOfWeekMS = CalendarUtils.retrieveMSDayofWeek(dayDisplayName: dayName)
        
        let monthInt = CalendarUtils.retrieveMonthFromDate(date: currentDate)
        self.month = monthInt
        self.monthDisplayName = CalendarUtils.retrieveMonthDisplayNameFromDate(date: currentDate)
        
        let yearInt = CalendarUtils.retrieveYearFromDate(date: currentDate)
        self.year = yearInt
        
        self.simpleDate = "\(yearInt)-\(monthInt)-\(dayInt)"
        
        let monthShortDisplayName = CalendarUtils.retrieveMonthShortDisplayNameFromDate(date: currentDate)
        self.simpleDate_2 = "\(dayInt) \(monthShortDisplayName) \(yearInt)"
        
        self.isCalendarDateToday = true
        
        // TEMP
        self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[3]
        
        // TODO: Validation agianst time / futurebookings and current time
    }
    
    
    func initSpaceCalendarDate(fromDate date: Date) {
        
        self.date = date
        
        let dayInt = CalendarUtils.retrieveDayFromDate(date: date)
        self.day = dayInt
        let dayName = CalendarUtils.retrieveDayDisplayNameFromDate(date: date)
        self.dayDisplayName = dayName
        self.dayOfWeek = CalendarUtils.retrieveDayofWeekFromDate(date: date)
        self.dayOfWeekMS = CalendarUtils.retrieveMSDayofWeek(dayDisplayName: dayName)
        
        let monthInt = CalendarUtils.retrieveMonthFromDate(date: date)
        self.month = monthInt
        self.monthDisplayName = CalendarUtils.retrieveMonthDisplayNameFromDate(date: date)
        
        let yearInt = CalendarUtils.retrieveYearFromDate(date: date)
        self.year = yearInt
        
        self.simpleDate = "\(yearInt)-\(monthInt)-\(dayInt)"
        
        let monthShortDisplayName = CalendarUtils.retrieveMonthShortDisplayNameFromDate(date: date)
        self.simpleDate_2 = "\(dayInt) \(monthShortDisplayName) \(yearInt)"
        
        let currentDate = CalendarUtils.currentDateTime()[1] as! Date
        self.isCalendarDateToday = (date == currentDate)
        
        if date < currentDate {
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[1]
        }
        else {
            // TEMP
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[3]
        }
    }
    
    
    //================================================================== Dates for Calendars ======================================================================
    
    func initSpaceCalendarDateForGuestCalendar(fromDate date: Date, calendarEndingDate eDate: Date, spaceCalendarInfo: SpaceCalendarInfo) {
        
        let availabilityMethod = (spaceCalendarInfo.availabilityMethod == "HOUR_BASE") ? 1 : 2
        let availability = spaceCalendarInfo.availabilityModules
        let futureBookings = spaceCalendarInfo.calendarBookings
        
        self.date = date
        
        let dayInt = CalendarUtils.retrieveDayFromDate(date: date)
        self.day = dayInt
        let dayName = CalendarUtils.retrieveDayDisplayNameFromDate(date: date)
        self.dayDisplayName = dayName
        self.dayOfWeek = CalendarUtils.retrieveDayofWeekFromDate(date: date)
        let msDay  = CalendarUtils.retrieveMSDayofWeek(dayDisplayName: dayName)
        self.dayOfWeekMS = msDay
        
        let monthInt = CalendarUtils.retrieveMonthFromDate(date: date)
        self.month = monthInt
        self.monthDisplayName = CalendarUtils.retrieveMonthDisplayNameFromDate(date: date)
        
        let yearInt = CalendarUtils.retrieveYearFromDate(date: date)
        self.year = yearInt
        
        self.simpleDate = "\(yearInt)-\(monthInt)-\(dayInt)"
        
        let monthShortDisplayName = CalendarUtils.retrieveMonthShortDisplayNameFromDate(date: date)
        self.simpleDate_2 = "\(dayInt) \(monthShortDisplayName) \(yearInt)"
        
        let currentDate = CalendarUtils.currentDateTime()[1] as! Date
        self.isCalendarDateToday = (date == currentDate)
        
        
        let availabilityForDate = StaticSpaceFunctions.retrieveSpaceAvailabilityModules(forSpaceCalendarDateMSDayOfWeek: msDay, availabilityMethod: availabilityMethod, availability: availability)
        self.availability = availabilityForDate
        
        
        if date < currentDate || date > eDate {
            // If date is a past date or the date comes after the calendar ending date, date is unavailable
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[1]
        }
        else {
            if availabilityForDate.count > 0 {
                if availabilityMethod == 1 { // Per Hour
                    let timeByHour = SpaceCalendarTime()
                    timeByHour.initSpaceCalendarTimeByPerHourBasis(withCalendarDateAvailability: availabilityForDate)
                    
                    // Today Validation
                    if self.isCalendarDateToday {
                        self.todayValidaionHourlyBased(todayTimeByHour: timeByHour)
                    }
                    
                    // Notice Period Validation
                    if spaceCalendarInfo.noticePeriod != 0 {
                        self.noticePeriodValidation(noticePeriod: spaceCalendarInfo.noticePeriod, date: date, calendarTime: timeByHour, perHourBasis: true)
                    }
                    
                    // Future Booking Validation
                    self.calendarTime = self.validateSpaceCalendarDate_GuestCalendar(date: date, withSpaceFutureBookings: futureBookings, forPerHourBasisSpaceCalendarTime: timeByHour)
                }
                else { // Per Block
                    let timeByBlock = SpaceCalendarTime()
                    timeByBlock.initSpaceCalendarTimeByBlockBasis(withCalendarDateAvailability: availabilityForDate)
                    
                    // Today Validation
                    if self.isCalendarDateToday {
                        self.todayValidaionBlockBased(todayTimeByBlock: timeByBlock)
                    }
                    
                    // Notice Period Validation
                    if spaceCalendarInfo.noticePeriod != 0 {
                        self.noticePeriodValidation(noticePeriod: spaceCalendarInfo.noticePeriod, date: date, calendarTime: timeByBlock, perHourBasis: false)
                    }
                    
                    // Future Booking Validation
                    self.calendarTime = self.validateSpaceCalendarDate_GuestCalendar(date: date, withSpaceFutureBookings: futureBookings, forBlockBasisSpaceCalendarTime: timeByBlock)
                }
            }
            else {
                // Date has no availability modules --> date is unavailable
                self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[1]
            }
        }
    }
    
    func initSpaceCalendarDateForHostCalendar(fromDate date: Date, calendarStartingDate sDate: Date, calendarEndingDate eDate: Date, spaceCalendarInfo: SpaceCalendarInfo) {
        
        let availabilityMethod = (spaceCalendarInfo.availabilityMethod == "HOUR_BASE") ? 1 : 2
        let availability = spaceCalendarInfo.availabilityModules
        let calendarBookings = spaceCalendarInfo.calendarBookings
        
        self.date = date
        
        let dayInt = CalendarUtils.retrieveDayFromDate(date: date)
        self.day = dayInt
        let dayName = CalendarUtils.retrieveDayDisplayNameFromDate(date: date)
        self.dayDisplayName = dayName
        self.dayOfWeek = CalendarUtils.retrieveDayofWeekFromDate(date: date)
        let msDay  = CalendarUtils.retrieveMSDayofWeek(dayDisplayName: dayName)
        self.dayOfWeekMS = msDay
        
        let monthInt = CalendarUtils.retrieveMonthFromDate(date: date)
        self.month = monthInt
        self.monthDisplayName = CalendarUtils.retrieveMonthDisplayNameFromDate(date: date)
        
        let yearInt = CalendarUtils.retrieveYearFromDate(date: date)
        self.year = yearInt
        
        self.simpleDate = "\(yearInt)-\(monthInt)-\(dayInt)"
        
        let monthShortDisplayName = CalendarUtils.retrieveMonthShortDisplayNameFromDate(date: date)
        self.simpleDate_2 = "\(dayInt) \(monthShortDisplayName) \(yearInt)"
        
        let currentDate = CalendarUtils.currentDateTime()[1] as! Date
        self.isCalendarDateToday = (date == currentDate)
        
        
        let availabilityForDate = StaticSpaceFunctions.retrieveSpaceAvailabilityModules(forSpaceCalendarDateMSDayOfWeek: msDay, availabilityMethod: availabilityMethod, availability: availability)
        self.availability = availabilityForDate
        
        
        if date < sDate || date > eDate {
            // If date is a past date or the date comes after the calendar ending date, date is unavailable
            //self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[1]
            self.dateStatus = StaticData.HOST_CALENDAR_DATE_STATUS[4]
        }
        else {
            if availabilityMethod == 1 { // Per Hour
                let timeByHour = SpaceCalendarTime()
                timeByHour.initHostCalendarTime()
                self.calendarTime = self.validateSpaceCalendarDate_HostCalendar(date: date, withSpaceCalendarBookings: calendarBookings, forPerHourBasisSpaceCalendarTime: timeByHour)
            }
            else { // Per Block
                let timeByBlock = SpaceCalendarTime()
                timeByBlock.initHostCalendarTime()
                self.calendarTime = self.validateSpaceCalendarDate_HostCalendar(date: date, withSpaceCalendarBookings: calendarBookings, forBlockBasisSpaceCalendarTime: timeByBlock)
            }
        }
    }
    
    
    //================================================================== Validation for Today ======================================================================
    
    // Today validation for Hourly based --> Guest Calendar
    private func todayValidaionHourlyBased(todayTimeByHour: SpaceCalendarTime) {
        var nextAvailableHour = CalendarUtils.retrieveNextAvailableHourOfToday()
        if nextAvailableHour == 0 { nextAvailableHour = 24 }
        for hourInTime in todayTimeByHour.hoursForCalendarDate {
            if hourInTime.timeHour24H < nextAvailableHour {
                hourInTime.isHourAvailable = false
            }
        }
    }
    
    
    // Today validation for Block based --> Guest Calendar
    private func todayValidaionBlockBased(todayTimeByBlock: SpaceCalendarTime) {
        var nextAvailableHour = CalendarUtils.retrieveNextAvailableHourOfToday()
        if nextAvailableHour == 0 { nextAvailableHour = 24 }
        for block in todayTimeByBlock.blocksForCalendarDate {
            if block.endTimeHour24H <= nextAvailableHour || block.hoursInBlock.contains(nextAvailableHour) {
                block.isBlockAvailable = false
            }
        }
    }
    
    //=========================== Validation with Future Bookings - Guest Calendar ========================
    
    // Validate time for Hourly Based Spaces --> For both Guest Calendare
    private func validateSpaceCalendarDate_GuestCalendar(date: Date, withSpaceFutureBookings futureBookings: [SpaceCalendarBooking], forPerHourBasisSpaceCalendarTime calTime: SpaceCalendarTime) -> SpaceCalendarTime {
        
        // Blocking time with Future Bookings
        for fBooking in futureBookings {
            if date == fBooking.bookingFromDate {
                for hourInTime in calTime.hoursForCalendarDate {
                    if fBooking.bookingFromTimeHourInt <= hourInTime.timeHour24H && fBooking.bookingToTimeHourInt >= hourInTime.timeHour24H {
                        hourInTime.isHourAvailable = false
                    }
                }
            }
        }
        
        // Validating if fully booked, partially booked or available
        var availableHoursCount = 0
        var unavailableHoursCount = 0
        let totalHoursCount = calTime.hoursForCalendarDate.count
        
        for hourInTime in calTime.hoursForCalendarDate {
            if hourInTime.isHourAvailable {
                availableHoursCount += 1
            }
            else {
                unavailableHoursCount += 1
            }
        }
        
        if availableHoursCount == totalHoursCount - 1 { // Date is available
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[3]
        }
        else if unavailableHoursCount == totalHoursCount { // Date is unavailable
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[1]
        }
        else if totalHoursCount - 1 != availableHoursCount && totalHoursCount != unavailableHoursCount {
            // date is partially available
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[2]
        }
        
        return calTime
    }
    
    
    // Validate time for Block Based Spaces --> For Guest Calendar
    private func validateSpaceCalendarDate_GuestCalendar(date: Date, withSpaceFutureBookings futureBookings: [SpaceCalendarBooking], forBlockBasisSpaceCalendarTime calTime: SpaceCalendarTime) -> SpaceCalendarTime {
        
        // Blocking time with Future Bookings
        for fBooking in futureBookings {
            if date == fBooking.bookingFromDate {
                for blockInCalTime in calTime.blocksForCalendarDate {
                    if blockInCalTime.hoursInBlock.contains(fBooking.bookingFromTimeHourInt) || blockInCalTime.hoursInBlock.contains(fBooking.bookingToTimeHourInt) || (fBooking.bookingFromTimeHourInt <= blockInCalTime.startTimeHour24H && fBooking.bookingToTimeHourInt >= blockInCalTime.endTimeHour24H) {
                        blockInCalTime.isBlockAvailable = false
                    }
                }
            }
        }
        
        // Validating if fully booked, partially booked or available
        var availableBlocksCount = 0
        var unavailableBlocksCount = 0
        let totalBlocksCount = calTime.blocksForCalendarDate.count
        
        for blockInCalTime in calTime.blocksForCalendarDate {
            if blockInCalTime.isBlockAvailable {
                availableBlocksCount += 1
            }
            else {
                unavailableBlocksCount += 1
            }
        }
        
        if availableBlocksCount == totalBlocksCount { // Date is available
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[3]
        }
        else if unavailableBlocksCount == totalBlocksCount { // Date is unavailable
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[1]
        }
        else if totalBlocksCount != availableBlocksCount && totalBlocksCount != unavailableBlocksCount {
            // date is partially available
            self.dateStatus = StaticData.SPACE_CALENDAR_DATE_STATUS[2]
        }
        
        return calTime
    }
    
    
    //=========================== Validation with Future Bookings - Host Calendar ========================
    
    // Validate time for Hourly Based Spaces --> For both Guest Calendare
    private func validateSpaceCalendarDate_HostCalendar(date: Date, withSpaceCalendarBookings calendarBookings: [SpaceCalendarBooking], forPerHourBasisSpaceCalendarTime calTime: SpaceCalendarTime) -> SpaceCalendarTime {
        
        // Blocking time with Future Bookings
        for cBooking in calendarBookings {
            if date == cBooking.bookingFromDate {
                for hourInTime in calTime.hoursForCalendarDate {
                    if cBooking.bookingFromTimeHourInt <= hourInTime.timeHour24H && cBooking.bookingToTimeHourInt >= hourInTime.timeHour24H {
                        hourInTime.isHourAvailable = false
                    }
                }
            }
        }
        
        // Validating if fully booked, partially booked or available
        var availableHoursCount = 0
        var unavailableHoursCount = 0
        let totalHoursCount = calTime.hoursForCalendarDate.count
        
        for hourInTime in calTime.hoursForCalendarDate {
            if hourInTime.isHourAvailable {
                availableHoursCount += 1
            }
            else {
                unavailableHoursCount += 1
            }
        }
        
        if availableHoursCount == totalHoursCount - 1 { // Date is available
            self.dateStatus = StaticData.HOST_CALENDAR_DATE_STATUS[3]
        }
        else if unavailableHoursCount == totalHoursCount { // Date is unavailable
            self.dateStatus = StaticData.HOST_CALENDAR_DATE_STATUS[1]
        }
        else if totalHoursCount - 1 != availableHoursCount && totalHoursCount != unavailableHoursCount {
            // date is partially available
            self.dateStatus = StaticData.HOST_CALENDAR_DATE_STATUS[2]
        }
        
        return calTime
    }
    
    // Validate time for Block Based Spaces --> For Host Calendar
    private func validateSpaceCalendarDate_HostCalendar(date: Date, withSpaceCalendarBookings calendarBookings: [SpaceCalendarBooking], forBlockBasisSpaceCalendarTime calTime: SpaceCalendarTime) -> SpaceCalendarTime{
        
        // Blocking time with Future Bookings
        for cBooking in calendarBookings {
            if date == cBooking.bookingFromDate {
                for hourInTime in calTime.hoursForCalendarDate {
                    if cBooking.bookingFromTimeHourInt == hourInTime.timeHour24H || cBooking.bookingToTimeHourInt == hourInTime.timeHour24H || (cBooking.bookingFromTimeHourInt < hourInTime.timeHour24H && hourInTime.timeHour24H < cBooking.bookingToTimeHourInt) {
                        hourInTime.isHourAvailable = false
                    }
                }
            }
        }
        
        
        // Validating if fully booked, partially booked or available
        var availableHoursCount = 0
        var unavailableHoursCount = 0
        let totalHoursCount = calTime.hoursForCalendarDate.count
        
        for hourInTime in calTime.hoursForCalendarDate {
            if hourInTime.isHourAvailable {
                availableHoursCount += 1
            }
            else {
                unavailableHoursCount += 1
            }
        }
        
        if availableHoursCount == totalHoursCount - 1 { // Date is available
            self.dateStatus = StaticData.HOST_CALENDAR_DATE_STATUS[3]
        }
        else if unavailableHoursCount == totalHoursCount { // Date is unavailable
            self.dateStatus = StaticData.HOST_CALENDAR_DATE_STATUS[1]
        }
        else if totalHoursCount - 1 != availableHoursCount && totalHoursCount != unavailableHoursCount {
            // date is partially available
            self.dateStatus = StaticData.HOST_CALENDAR_DATE_STATUS[2]
        }
        
        return calTime
    }
    
    
    // ================================================================== Validation for Notice Period ======================================================================
    
    // For notice period validation - Guest Calendar
    private func noticePeriodValidation(noticePeriod: Int, date: Date, calendarTime: SpaceCalendarTime, perHourBasis: Bool) {
        let currentDeviceDateTimeToNextHour = CalendarUtils.retrieveDate(thatComesAfterHours: 1, fromDate: CalendarUtils.currentDeviceDateTime())
        let noticePeriodEndDateTime = CalendarUtils.retrieveDate(thatComesAfterHours: noticePeriod - 1, fromDate: currentDeviceDateTimeToNextHour)
        
        //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | noticePeriod : \(noticePeriod)")
        //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | currentDeviceDateTimeToNextHour : \(currentDeviceDateTimeToNextHour)")
        //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | noticePeriodEndDateTime : \(noticePeriodEndDateTime) \n")
        
        let currentDate = CalendarUtils.retrieveLocaleDate_2(fromSimpleDate: currentDeviceDateTimeToNextHour)
        let currentDateAvailableHour = CalendarUtils.retrieveTimeHourIn24Hour(date: currentDeviceDateTimeToNextHour)
        let noticePeriodEndDate = CalendarUtils.retrieveLocaleDate_2(fromSimpleDate: noticePeriodEndDateTime)
        let noticePeriodEndHour = CalendarUtils.retrieveTimeHourIn24Hour(date: noticePeriodEndDateTime)
        
        //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | currentDate : \(currentDate)")
        //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | currentDateAvailableHour : \(currentDateAvailableHour)")
        //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | noticePeriodEndDate : \(noticePeriodEndDate)")
        //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | noticePeriodEndHour : \(noticePeriodEndHour) \n")
        
        //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | date : \(date) \n")
        
        if currentDate == noticePeriodEndDate && currentDate == date {
            //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | currentDate == noticePeriodEndDate \n")
            
            if perHourBasis {
                for hourInTime in calendarTime.hoursForCalendarDate {
                    if hourInTime.timeHour24H == currentDateAvailableHour || hourInTime.timeHour24H == noticePeriodEndHour || (hourInTime.timeHour24H > currentDateAvailableHour && hourInTime.timeHour24H < noticePeriodEndHour) {
                        hourInTime.isHourInNoticePeriod = true
                    }
                }
            }
            else {
                for block in calendarTime.blocksForCalendarDate {
                    if block.startTimeHour24H < noticePeriodEndHour {
                        block.isBlockInNoticePeriod = true
                    }
                }
            }
        }
        else {
            let multipleDates = CalendarUtils.retrieveDates(fromDate: currentDate, toDate: noticePeriodEndDate)
            let datesCount = multipleDates.count
            
            for i in 0..<datesCount {
                let date_i = multipleDates[i]
                
                if date_i == date {
                    //print("SpaceCalendarDate Guest Calendar noticePeriodValidation | currentDate != noticePeriodEndDate \n")
                    
                    if i == 0 {
                        if perHourBasis {
                            for hourInTime in calendarTime.hoursForCalendarDate {
                                if hourInTime.timeHour24H >= currentDateAvailableHour {
                                    hourInTime.isHourInNoticePeriod = true
                                }
                            }
                        }
                        else {
                            for block in calendarTime.blocksForCalendarDate {
                                if block.hoursInBlock.contains(currentDateAvailableHour) || block.endTimeHour24H >= currentDateAvailableHour {
                                    block.isBlockInNoticePeriod = true
                                }
                            }
                        }
                    }
                    else if i == datesCount - 1 {
                        if perHourBasis {
                            for hourInTime in calendarTime.hoursForCalendarDate {
                                if hourInTime.timeHour24H < noticePeriodEndHour {
                                    hourInTime.isHourInNoticePeriod = true
                                }
                            }
                        }
                        else {
                            for block in calendarTime.blocksForCalendarDate {
                                if block.endTimeHour24H <= noticePeriodEndHour || block.hoursInBlock.contains(noticePeriodEndHour) {
                                    block.isBlockInNoticePeriod = true
                                }
                            }
                        }
                    }
                    else {
                        // All Hours / Blocks of day isInNoticePeriod
                        
                        if perHourBasis {
                            for hourInTime in calendarTime.hoursForCalendarDate {
                                hourInTime.isHourInNoticePeriod = true
                            }
                        }
                        else {
                            for block in calendarTime.blocksForCalendarDate {
                                block.isBlockInNoticePeriod = true
                            }
                        }
                    }
                }
            }
        }
    }
}
