//
//  ColorUtils.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/2/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class ColorUtils: NSObject {
    
    // MARK: - MS Colors
    static let MS_BLUE_COLOR = UIColor.init(red: 0.0, green: 142.0/255.0, blue: 218.0/255.0, alpha: 1.0)
    static let MS_DARK_BLUE_COLOR = UIColor.init(red: 13.0/255.0, green: 89.0/255.0, blue: 143.0/255.0, alpha: 1.0)
    static let MS_CANCEL_RED = UIColor.init(red: 150.0/255.0, green: 0.0, blue: 0.0, alpha: 1.0)
    
    // MARK: - Booking Reservation Status Colors
    private static let CONFIRMED_COLOR = UIColor.init(red: 34.0/255.0, green: 164.0/255.0, blue: 100.0/255.0, alpha: 1.0)
    private static let PENDING_PAYMENT_COLOR = UIColor.init(red: 254.0/255.0, green: 142.0/255.0, blue: 2.0/255.0, alpha: 1.0)
    private static let CANCELLED_COLOR = UIColor.init(red: 225.0/255.0, green: 84.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    private static let EXPIRED_COLOR = UIColor.init(red: 119.0/255.0, green: 92.0/255.0, blue: 207.0/255.0, alpha: 1.0)
    
    static let BOOKING_RESERVATION_STATUS_COLORS = (CONFIRMED: ColorUtils.CONFIRMED_COLOR, PENDING_PAYMENT: ColorUtils.PENDING_PAYMENT_COLOR, CANCELLED: ColorUtils.CANCELLED_COLOR, EXPIRED: ColorUtils.EXPIRED_COLOR)
    static let BOOKING_RESERVATION_STATUS_COLORS_BY_CODE = [1 : ColorUtils.PENDING_PAYMENT_COLOR, 2 : ColorUtils.PENDING_PAYMENT_COLOR, 3 : ColorUtils.CONFIRMED_COLOR, 4 : ColorUtils.CANCELLED_COLOR, 5 : ColorUtils.CONFIRMED_COLOR, 6 : ColorUtils.EXPIRED_COLOR, 7 : ColorUtils.EXPIRED_COLOR, 0 : UIColor.lightGray]
    
    // MARK: - Text Validation Colors
    static let TEXT_VALIDATION_VALID_PLACEHOLDER_COLOR_WHITE = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.8)
    static let TEXT_VALIDATION_VALID_PLACEHOLDER_COLOR_GRAY = UIColor.init(red: 199.0/255.0, green: 199.0/255.0, blue: 205.0/255.0, alpha: 1.0)
    static let TEXT_VALIDATION_INVALID_PLACEHOLDER_COLOR_RED = UIColor.init(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.8)
    static let TEXT_VALIDATION_VALID_FONT_COLOR_WHITE = UIColor.white
    static let TEXT_VALIDATION_VALID_FONT_COLOR_BLACK = UIColor.black
    static let TEXT_VALIDATION_INVALID_FONT_COLOR_RED = UIColor.red
    
    
    // MARK: - Cancel Booking Screen
    static let CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR = UIColor.init(red: 203.0/255.0, green: 0.0, blue: 0.0, alpha: 1.0)
    static let CANCEL_BOOKING_REFUNDABLE_GREEN_COLOR = UIColor.init(red: 0.0, green: 194.0/255.0, blue: 0.0, alpha: 1.0)
}
