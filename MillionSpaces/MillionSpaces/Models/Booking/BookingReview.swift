//
//  BookingReview.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class BookingReview: NSObject {
    
    var rTitle = "N/A"
    var overallScore = 0
    var serviceScore = 0
    var cleanlinessScore = 0
    var valueScore = 0
    var rDescription = "N/A"
    
    
    func initBookingReview(withDictionary dic: [String : Any]) {
        
        if let title = dic["title"] as? String {
            self.rTitle = title
        }
        
        if let desc = dic["description"] as? String {
            self.rDescription = desc
        }
        
        if let rate = dic["rate"] as? String {
            let ratings = rate.components(separatedBy: "|")
            
            self.overallScore = Int(ratings[0])!
            self.serviceScore = Int(ratings[1])!
            self.cleanlinessScore = Int(ratings[2])!
            self.valueScore = Int(ratings[3])!
        }
    }
}
