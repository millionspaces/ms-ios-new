//
//  Booking.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class Booking: NSObject {
    
    var id: Int!
    var orderId: String!
    var isManual: Bool!
    
    // User
    var bookingByUserId: Int?
    var bookingByUserName: String = "N/A"
    var bookingByUserEmail: String = "N/A"
    var bookingByUserMobile: String = "N/A"
    
    var eventType: EventType?
    var seatingArrangement: SeatingArrangement?
    var expectedGuestCount: Int = 0
    var bookingMadeDate: String = "N/A"
    var bookingCancelledDate: String = "N/A"
    
    var refund: String = "N/A"
    var menu: SpaceMenu?
    
    // Reservation Status
    var bookingReservationStatusId: Int = 0
    var bookingReservationStatusName: String = "N/A"
    var bookingReservationStatusLabel: String = "N/A"
    var bookingReservationStatusDisplayName: String = "N/A"
    
    // Space
    var spaceId: Int!
    var spaceName: String!
    var spaceAddressLine_1: String!
    var spaceAddressLine_2: String?
    var spaceFullAddress: String!
    var spaceImageName: String!
    var spaceCancellationPolicy: CancellationPolicy!
    var spaceAmenities: [Amenity] = []
    var spaceBookingExtraAmenities: [BookingExtraAmenity] = []
    
    var isReviewed: Bool!
    var review: BookingReview?
    var remarks: String = "N/A"
    
    // Charge
    var bookingCharge: Int = 0
    
    // Dates
    var bookingDates: [[String : [String]]] = []
    
    // Custom Variables
    var firstBookingDate: [String : [String]]?
    var hasMultipleBlocks: Bool = false
    var isUpcomingBooking: Bool = false
    var isMultipleDayManualBooking: Bool = false
    
    
    
    func initBooking(withDictionary dic: [String : Any]) {
        
        self.id = dic["id"] as! Int
        self.orderId = dic["orderId"] as! String
        self.isManual = dic["isManual"] as! Bool
        
        // User
        if let user = dic["user"] as? [String : Any] {
            if let userId = user["id"] as? Int {
                self.bookingByUserId = userId
            }
            
            if let userName = user["name"] as? String {
                self.bookingByUserName = userName
            }
            
            if let userEmail = user["email"] as? String {
                self.bookingByUserEmail = userEmail
            }
            
            if let userMobile = user["mobile"] as? String {
                self.bookingByUserMobile = userMobile
            }
        }
        
        // Event Type
        if let eventTypeDetails = dic["eventTypedetail"] as? [String : Any] {
            if let eventTypeId = eventTypeDetails["id"] as? Int {
                let evType = MetaData.retrieveEventType(with: eventTypeId)
                self.eventType = evType
            }
        }
        
        // Seating Arrangement
        if let seatingArrangementDetails = dic["seatingArrangement"] as? [String : Any] {
            if let seatingArrangementId = seatingArrangementDetails["id"] as? Int {
                let seating = MetaData.retrieveSeatingArrangement(with: seatingArrangementId)
                self.seatingArrangement = seating
            }
        }
        
        // Expected Guest Count
        if let guestCount = dic["guestCount"] as? Int {
            self.expectedGuestCount = guestCount
        }
        
        // Booking Made Date
        if let bookedDate = dic["bookedDate"] as? String {
            self.bookingMadeDate = bookedDate
        }
        
        // Booking Cancelled Date
        if let cancelledDate = dic["cancelledDate"] as? String {
            self.bookingCancelledDate = cancelledDate
        }
        
        // Refund
        if !(dic["refund"] is NSNull) {
            //print("Booking initBooking | refund is NOT NSNull")
        }
        else {
            //print("Booking initBooking | refund is NSNull")
        }
        
        // Menu
        if let menuDetails = dic["menu"] as? [[String : Any]] {
            if menuDetails.count > 0 {
                let m = SpaceMenu()
                m.initSpaceMenu(withDictionary: menuDetails[0])
                self.menu = m
            }
        }
        
        // Reservation Status
        if let reservationStatus = dic["reservationStatus"] as? [String : Any] {
            if let statusID = reservationStatus["id"] as? Int {
                self.bookingReservationStatusId = statusID
                self.bookingReservationStatusDisplayName = StaticData.RESERVATION_STATUS[statusID]!
            }
            
            if let statusName = reservationStatus["name"] as? String {
                self.bookingReservationStatusName = statusName
            }
            
            if let statusLabel = reservationStatus["label"] as? String {
                self.bookingReservationStatusLabel = statusLabel
            }
        }
        
        
        // Space
        if let space = dic["space"] as? [String : Any] {
            self.spaceId = space["id"] as? Int
            self.spaceName = (space["name"] as! String).replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
            self.spaceAddressLine_1 = (space["addressLine1"] as! String).replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
            
            if let address2 = space["addressLine2"] as? String {
                self.spaceAddressLine_2 = address2.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
                self.spaceFullAddress = "\(self.spaceAddressLine_2!), \(self.spaceAddressLine_1!)"
            }
            else {
                self.spaceFullAddress = self.spaceAddressLine_1!
            }
            
            self.spaceImageName = (space["image"] as! String)
            
            if let cPolicy = space["cancellationPolicy"] as? [String : Any] {
                self.spaceCancellationPolicy = MetaData.retrieveCancellationPolicies(with: cPolicy["id"] as! Int)
            }
            
            if let arrAmenities = space["amenities"] as? [[String : Any]] {
                for a in arrAmenities {
                    if let amenity = MetaData.retrieveAmenity(with: a["id"] as! Int) {
                        self.spaceAmenities.append(amenity)
                    }
                }
            }
        }
        
        // Space Extra Amenities
        if let arrExtraAmenities = dic["extraAmenityList"] as? [[String : Any]] {
            for e in arrExtraAmenities {
                let extra = SpaceExtraAmenity()
                extra.initSpaceExtraAmenityForBookings(withDictionary: e)
                
                let bookingExtra = BookingExtraAmenity()
                bookingExtra.initBookingExtraAmenity(withExtraAmenity: extra, type: 1, amount: e["count"] as! Int, totalCharge: 0, index: 0)
                self.spaceBookingExtraAmenities.append(bookingExtra)
            }
        }
        
        // Review and Remarks
        if let reviewed = dic["isReviewed"] as? Bool {
            self.isReviewed = reviewed
        }
        else {
            self.isReviewed = false
        }
        
        if let reviewDetails = dic["review"] as? [String : Any] {
            let r = BookingReview()
            r.initBookingReview(withDictionary: reviewDetails)
            self.review = r
        }
        
        if !(dic["remarks"] is NSNull) {
            //print("Booking initBooking | remarks is NOT NSNull")
        }
        else {
            //print("Booking initBooking | remarks is NSNull")
        }
        
        
        
        // Booking Charge
        if let chargeStr = dic["bookingCharge"] as? String {
            if let chargeF = Float(chargeStr) { // TEMP
                self.bookingCharge = Int(chargeF)
            }
        }
        
        
        // Booking Dates/Times
        if let bookingDates = dic["dates"] as? [[String : Any]] {
            for d in bookingDates {
                let fromDateComp = CalendarUtils.retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr: d["fromDate"] as! String)
                let toDateComp = CalendarUtils.retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr: d["toDate"] as! String)
                let date = ["fromDate" : fromDateComp, "toDate" : toDateComp]
                self.bookingDates.append(date)
            }
            
            if self.bookingDates.count > 0 {
                let first = self.bookingDates[0]
                self.firstBookingDate = first
                let first_FromDateTime = first["fromDate"]!
                let first_ToDateTime = first["toDate"]!
                
                // Check if isUpcoming
                self.isUpcomingBooking = CalendarUtils.isUpcomingDateTime(datetimeStr: "\(first_ToDateTime[0])T\(first_ToDateTime[1])")
                
                // Check if isMultipleDayManualBooking
                self.isMultipleDayManualBooking = first_FromDateTime[0] != first_ToDateTime[0]
            }
            
            self.hasMultipleBlocks = self.bookingDates.count > 1
        }
    }
}
