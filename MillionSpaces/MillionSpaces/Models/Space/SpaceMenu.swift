//
//  SpaceMenu.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceMenu: NSObject {
    
    var id: Int!
    var menuName: String! // Test
    var menuImage: String!
    
    func initSpaceMenu(withDictionary dic: [String: Any]) {
        self.id = dic["id"] as! Int
        //self.menuName = "Menu \(dic["menuId"] as! String)"
        self.menuImage = dic["url"] as! String
        
        if let menuId = dic["menuId"] as? String {
            self.menuName = "Menu \(menuId)"
        }
        else {
            self.menuName = "Menu \(self.id)"
        }
    }
    
    
    
}
