//
//  BookingExtraAmenity.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class BookingExtraAmenity: NSObject {
    
    var index: Int!         // IndexPath Row value for position
    
    var extraAmenity: SpaceExtraAmenity!
    var type: Int!          // Per Hour = 1 | Other = 2
    var amount: Int!
    var totalChargeForExtraAmenity: Int!
    
    
    func initBookingExtraAmenity(withExtraAmenity extra: SpaceExtraAmenity, type: Int, amount: Int, totalCharge: Int, index: Int) {
        self.index = index
        self.extraAmenity = extra
        self.type = type
        self.amount = amount
        self.totalChargeForExtraAmenity = totalCharge
    }
}
