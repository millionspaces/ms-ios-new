//
//  CancelBookingViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class CancelBookingViewController: BaseViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var spaceImageView: UIImageView!
    @IBOutlet weak var spaceNameLabel: UILabel!
    @IBOutlet weak var bookingStatusLabel: UILabel!
    
    @IBOutlet weak var cPolicyNameLabel: UILabel!
    @IBOutlet weak var cPolicyDescriptionLabel: UILabel!
    
    @IBOutlet weak var refundabilityStatusLabel: UILabel!
    @IBOutlet weak var refundableView: UIView!
    @IBOutlet weak var nonRefundableView: UIView!
    @IBOutlet weak var timelineSeparatorView: UIView!
    @IBOutlet weak var bookingMadeDateLabel: UILabel!
    @IBOutlet weak var threshholdDateLabel: UILabel!
    @IBOutlet weak var bookingStartDateLabel: UILabel!
    
    @IBOutlet weak var paidAmountLabel: UILabel!
    @IBOutlet weak var refundAmountLabel: UILabel!
    
    @IBOutlet weak var timeLineParentView: UIView!
    @IBOutlet weak var amountsParentView: UIView!
    
    
    
    // MARK: - Class Variables
    var booking: Booking!
    var isPendingPayment: Bool!
    var refundAmount: Int!
    
    var alertController: UIAlertController?
    
    // R 203
    // G 194
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToCancellationFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToCancellationFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) { // ~ 10s
            alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = "Cancel Booking"
        
        if booking != nil {
            
            self.spaceImageView.sd_setImage(with: URL(string: NetworkConfiguration.IMAGE_BASE_URL + booking.spaceImageName!), completed: nil)
            self.spaceNameLabel.text = booking.spaceName!
            self.bookingStatusLabel.text = booking.bookingReservationStatusDisplayName
            self.bookingStatusLabel.backgroundColor = ColorUtils.BOOKING_RESERVATION_STATUS_COLORS_BY_CODE[booking.bookingReservationStatusId]!
            
            self.cPolicyNameLabel.text = booking.spaceCancellationPolicy.name!
            self.cPolicyDescriptionLabel.text = booking.spaceCancellationPolicy.policyDescription!
            
            self.paidAmountLabel.text = "LKR 0"
            self.refundAmountLabel.text = "LKR 0"
            
            self.amountsParentView.isHidden = true
            self.timeLineParentView.isHidden = true
            
            if self.booking.bookingReservationStatusId != 1 && self.booking.bookingReservationStatusId != 2 {
                self.isPendingPayment = false
                if self.booking.spaceCancellationPolicy!.id != 3 {
                    self.retreiveRefundAmount()
                }
                else {
                    self.refundAmount = 0
                    self.configureTimelineAndAmountLabels()
                }
            }
            else {
                self.isPendingPayment = true
                self.refundAmount = 0
                self.configureTimelineAndAmountLabels()
            }
        }
    }
    
    private func retreiveRefundAmount() {
        self.showActivityIndicator()
        
        WebServiceCall.retrieveRefundAmount(forBookingID: self.booking.id!, successBlock: { (results: Any) in
            print("CancelBookingVc retreiveRefundAmount | success | results : \(results)")
            
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
                
                if let results_ = results as? [String : Any] {
                    if let refund = results_["refund"] as? Int {
                        self.refundAmount = refund
                        self.configureTimelineAndAmountLabels()
                    }
                    else {
                        self.addRefundAmountReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    }
                }
                else {
                    self.addRefundAmountReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        })
        { (errorCode: Int, error: String) in
            print("CancelBookingVc retreiveRefundAmount | failed | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addRefundAmountReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addRefundAmountReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addRefundAmountReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func addRefundAmountReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.SUCCESS_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retreiveRefundAmount()
        }
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    private func configureTimelineAndAmountLabels() {
        
        self.timeLineParentView.isHidden = false
        
        let cPolicy = self.booking.spaceCancellationPolicy!
        
        let eventDate = CalendarUtils.retrieveLocaleDate(fromSimpleDateString: self.booking.bookingDates[0]["fromDate"]![0])
        let eventDateDay = CalendarUtils.retrieveDayFromDate(date: eventDate!)
        let eventDateMonth = CalendarUtils.retrieveMonthShortDisplayNameFromDate(date: eventDate!)
        
        let bookingMadeDate = CalendarUtils.retrieveLocaleDate(fromSimpleDateString: ((CalendarUtils.retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr: self.booking.bookingMadeDate))[0]))
        let bookingMadeDateDay = CalendarUtils.retrieveDayFromDate(date: bookingMadeDate!)
        let bookingMadeDateMonth = CalendarUtils.retrieveMonthShortDisplayNameFromDate(date: bookingMadeDate!)
        
        self.bookingMadeDateLabel.text = "\(bookingMadeDateDay) \(bookingMadeDateMonth)"
        self.bookingStartDateLabel.text = "\(eventDateDay) \(eventDateMonth)"
        
        if self.isPendingPayment {
            self.amountsParentView.isHidden = true
            
            self.refundabilityStatusLabel.text = "Non-refundable"
            self.refundabilityStatusLabel.textAlignment = .center
            
            self.refundableView.backgroundColor = ColorUtils.CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR
            self.nonRefundableView.backgroundColor = ColorUtils.CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR
            
            self.threshholdDateLabel.isHidden = true
            self.timelineSeparatorView.isHidden = true
            
            return
        }
        else {
            self.amountsParentView.isHidden = false
        }
        
        self.paidAmountLabel.text = "LKR \(Utils.formatPriceValues(price: self.booking.bookingCharge))"
        
        let thresholdDate: Date!
        
        if cPolicy.id == 1 { // Flexible | Full refund up to two days prior to event
            thresholdDate = CalendarUtils.retrieveDate(thatComesAfterDays: -2, fromDate: eventDate!)
            
            if bookingMadeDate! < thresholdDate {
                let thresholdDateDay = CalendarUtils.retrieveDayFromDate(date: thresholdDate!)
                let thresholdDateMonth = CalendarUtils.retrieveMonthShortDisplayNameFromDate(date: thresholdDate!)
                
                self.threshholdDateLabel.text = "\(thresholdDateDay) \(thresholdDateMonth)"
                
                if self.refundAmount == 0 {
                    self.refundabilityStatusLabel.text = "Non-refundable"
                    self.refundabilityStatusLabel.textAlignment = .right
                    self.refundAmountLabel.text = "LKR 0"
                }
                else {
                    self.refundabilityStatusLabel.text = "Full refund"
                    self.refundabilityStatusLabel.textAlignment = .left
                    self.refundAmountLabel.text = "LKR \(Utils.formatPriceValues(price: self.refundAmount))"
                }
            }
            else {
                self.refundAmountLabel.text = "LKR 0"
                
                self.refundabilityStatusLabel.text = "Non-refundable"
                self.refundabilityStatusLabel.textAlignment = .center
                
                self.refundableView.backgroundColor = ColorUtils.CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR
                self.nonRefundableView.backgroundColor = ColorUtils.CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR
                
                self.threshholdDateLabel.isHidden = true
                self.timelineSeparatorView.isHidden = true
            }
        }
        else if cPolicy.id == 2 {
            thresholdDate = CalendarUtils.retrieveDate(thatComesAfterDays: -7, fromDate: eventDate!)
            
            if bookingMadeDate! < thresholdDate {
                let thresholdDateDay = CalendarUtils.retrieveDayFromDate(date: thresholdDate!)
                let thresholdDateMonth = CalendarUtils.retrieveMonthShortDisplayNameFromDate(date: thresholdDate!)
                
                self.threshholdDateLabel.text = "\(thresholdDateDay) \(thresholdDateMonth)"
                
                if self.refundAmount == 0 {
                    self.refundabilityStatusLabel.text = "Non-refundable"
                    self.refundabilityStatusLabel.textAlignment = .right
                    self.refundAmountLabel.text = "LKR 0"
                }
                else {
                    self.refundabilityStatusLabel.text = "50% refund"
                    self.refundabilityStatusLabel.textAlignment = .left
                    self.refundAmountLabel.text = "LKR \(Utils.formatPriceValues(price: self.refundAmount))"
                }
            }
            else {
                self.refundAmountLabel.text = "LKR 0"
                
                self.refundabilityStatusLabel.text = "Non-refundable"
                self.refundabilityStatusLabel.textAlignment = .center
                
                self.refundableView.backgroundColor = ColorUtils.CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR
                self.nonRefundableView.backgroundColor = ColorUtils.CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR
                
                self.threshholdDateLabel.isHidden = true
                self.timelineSeparatorView.isHidden = true
            }
            
            
        }
        else if cPolicy.id == 3 {
            
            self.refundAmountLabel.text = "LKR 0"
            
            self.refundabilityStatusLabel.text = "Non-refundable"
            self.refundabilityStatusLabel.textAlignment = .center
            
            self.refundableView.backgroundColor = ColorUtils.CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR
            self.nonRefundableView.backgroundColor = ColorUtils.CANCEL_BOOKING_NON_REFUNDABLE_RED_COLOR
            
            self.threshholdDateLabel.isHidden = true
            self.timelineSeparatorView.isHidden = true
        }
    }
    
    
    
    func cancelBooking() {
        self.showActivityIndicator()
        
        WebServiceCall.cancelGuestMSBooking(forBookingID: self.booking.id!, withRefundableAmount: self.refundAmount!, successBlock: { (result: Any) in
            print("CancelBookingVC cancelBooking | result : \(result)")
            
            if let response = result as? [String : Any], response["new_state"] as? String == "CANCELLED" {
                self.presentResponseAlert(isSuccessful: true)
            }
            else {
                self.presentResponseAlert(isSuccessful: false)
            }
            
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("CancelBookingVC cancelBooking | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func presentConfirmationAlert() {
        alertController = UIAlertController(title: AlertMessages.CONFIRM_TITLE, message: AlertMessages.CANCEL_BOOKING_CONFIRMATION_MESSAGE, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertMessages.YES_ACTION, style: .default) { (alert: UIAlertAction) in
            self.cancelBooking()
        }
        let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
        alertController!.addAction(noAction)
        alertController!.addAction(yesAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func presentResponseAlert(isSuccessful: Bool) {
        if isSuccessful {
            alertController = UIAlertController(title: AlertMessages.SUCCESS_TITLE, message: AlertMessages.CANCEL_BOOKING_SUCCESS_MESSAGE, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
                self.popToRootViewController()
            }
            alertController!.addAction(yesAction)
        }
        else {
            alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.CANCEL_BOOKING_ERROR_MESSAGE, preferredStyle: .alert)
            let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
            let retryAction = UIAlertAction(title: AlertMessages.RETRY_ACTION, style: .default) { (alert: UIAlertAction) in
                self.cancelBooking()
            }
            alertController!.addAction(noAction)
            alertController!.addAction(retryAction)
        }
        
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    // MARK: - IBAction
    @IBAction func viewSpaceProfileButtonPressed(_ sender: UIButton) {
        if booking != nil {
            openSpaceDetailsViewController(forSpaceBySpaceID: booking.spaceId!)
        }
    }
    
    @IBAction func cancelBookingButtonPressed(_ sender: UIButton) {
        //print("CancelBookingVC cancelBookingButtonPressed | refundAmount : \(self.refundAmount!)")
        self.presentConfirmationAlert()
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.popViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
