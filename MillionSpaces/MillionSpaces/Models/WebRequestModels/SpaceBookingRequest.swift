//
//  SpaceBookingRequest.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceBookingRequest: NSObject {
    
    var space: Int!                 // booking space id
    var eventType: Int!             // selected eventType id
    var seatingArrangement: Int!    // selected seatingArrangement id
    var guestCount: Int!            // selected guestCount
    var bookingCharge: Int!         //
    
    var bookingDate: SpaceCalendarDate!
    var bookingTimes: [String] = []
    var dates: [[String : String]] = []
    
    var bookingWithExtraAmenityDtoSet: [[String : Int]] = []
    
    var promoCode: String?
    
    var objectDictionary: [String : Any] = [:]
    
    
    func initSpaceBookingRequestForPerHourBasisSpace(bookingSpaceId: Int, eventTypeId: Int, seatingArrangementId: Int, guestCount: Int, bookingCharge: Int, bookingDate: SpaceCalendarDate, selectedHours: [Int], selectedPerHourExtras: [BookingExtraAmenity], selectedOtherExtras: [BookingExtraAmenity], prCode: String?) {
        
        self.space = bookingSpaceId
        self.eventType = eventTypeId
        self.seatingArrangement = seatingArrangementId
        self.guestCount = guestCount
        self.bookingCharge = bookingCharge
        
        self.bookingDate = bookingDate
        let fromTime = CalendarUtils.convertTo12HourTime(fromTwentyFourTime: "\(selectedHours.first!):00:00", isWithSeconds: true)
        let toTime = CalendarUtils.convertTo12HourTime(fromTwentyFourTime: "\(selectedHours.last!):00:00", isWithSeconds: true)
        self.bookingTimes.append("\(fromTime) - \(toTime)")
        
        
        let perHourSelection = ["fromDate": "\(bookingDate.simpleDate!)T\(selectedHours.first!):00Z", "toDate": "\(bookingDate.simpleDate!)T\(selectedHours.last!):00Z"]
        self.dates.append(perHourSelection)
        
        
        var bookingAmenities: [[String : Int]] = []
        for h in selectedPerHourExtras {
            let perHourExtraDic = ["amenityId" : h.extraAmenity.id!, "number" : h.amount!]
            bookingAmenities.append(perHourExtraDic)
        }
        
        for o in selectedOtherExtras {
            let otherExtraDic = ["amenityId" : o.extraAmenity.id!, "number" : o.amount!]
            bookingAmenities.append(otherExtraDic)
        }
        
        // Dictionary
        self.objectDictionary["space"] = bookingSpaceId
        self.objectDictionary["eventType"] = eventTypeId
        self.objectDictionary["seatingArrangement"] = seatingArrangementId
        self.objectDictionary["guestCount"] = guestCount
        self.objectDictionary["bookingCharge"] = bookingCharge
        self.objectDictionary["dates"] = self.dates
        self.objectDictionary["bookingWithExtraAmenityDtoSet"] = bookingAmenities
        
        if let code = prCode {
            self.promoCode = code
            self.objectDictionary["promoCode"] = code
        }
    }
    
    
    func initSpaceBookingRequestForBlockBasisSpace(bookingSpaceId: Int, eventTypeId: Int, seatingArrangementId: Int, guestCount: Int, bookingCharge: Int, bookingDate: SpaceCalendarDate, selectedBlocks: [SpaceCalendarTimeByBlock], selectedPerHourExtras: [BookingExtraAmenity], selectedOtherExtras: [BookingExtraAmenity], prCode: String?) {
        
        self.space = bookingSpaceId
        self.eventType = eventTypeId
        self.seatingArrangement = seatingArrangementId
        self.guestCount = guestCount
        self.bookingCharge = bookingCharge
        
        self.bookingDate = bookingDate
        
        for block in selectedBlocks {
            let blockSelection = ["fromDate": "\(bookingDate.simpleDate!)T\(block.startTimeHour24H!):00Z", "toDate": "\(bookingDate.simpleDate!)T\(block.endTimeHour24H!):00Z"]
            self.dates.append(blockSelection)
            
            let fromTime = CalendarUtils.convertTo12HourTime(fromTwentyFourTime: "\(block.startTimeHour24H!):00:00", isWithSeconds: true)
            let toTime = CalendarUtils.convertTo12HourTime(fromTwentyFourTime: "\(block.endTimeHour24H!):00:00", isWithSeconds: true)
            self.bookingTimes.append("\(fromTime) - \(toTime)")
        }
        
        var bookingAmenities: [[String : Int]] = []
        for h in selectedPerHourExtras {
            let perHourExtraDic = ["amenityId" : h.extraAmenity.id!, "number" : h.amount!]
            bookingAmenities.append(perHourExtraDic)
        }
        
        for o in selectedOtherExtras {
            let otherExtraDic = ["amenityId" : o.extraAmenity.id!, "number" : o.amount!]
            bookingAmenities.append(otherExtraDic)
        }
        
        
        // Dictionary
        self.objectDictionary["space"] = bookingSpaceId
        self.objectDictionary["eventType"] = eventTypeId
        self.objectDictionary["seatingArrangement"] = seatingArrangementId
        self.objectDictionary["guestCount"] = guestCount
        self.objectDictionary["bookingCharge"] = bookingCharge
        self.objectDictionary["dates"] = self.dates
        self.objectDictionary["bookingWithExtraAmenityDtoSet"] = bookingAmenities
        
        if let code = prCode {
            self.promoCode = code
            self.objectDictionary["promoCode"] = code
        }
    }
}
