//
//  SpaceCalendarTimeByBlock.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceCalendarTimeByBlock: NSObject {
    
    var startTimeHour24H: Int!
    var startTimeIn24H: String!
    var startTimeDisplayName12H: String!
    
    var endTimeHour24H: Int!
    var endTimeIn24H: String!
    var endTimeDisplayName12H: String!
    
    var hoursInBlock: [Int]! // = []
    
    
    var blockPrice: Int!
    
    // if blockChargeType == 0
    var blockMenu: SpaceMenu?
    
    var isBlockInNoticePeriod: Bool!
    
    var isBlockAvailable: Bool!
    
    // Temp bool value to check for overlapping upon selection
    var isBlockAvailableUponSelection: Bool!
    
    
    
    func initSpaceCalendarTimeByBlock(withAvailabilityModuleForBlock block: SpaceAvailabilityModule) {
        let sHour = block.timeFrom!
        let eHour = block.timeTo!
        
        self.startTimeHour24H = CalendarUtils.retrieve24HourIntComponent(fromTimeStr: sHour)
        self.startTimeIn24H = sHour
        self.startTimeDisplayName12H = CalendarUtils.convertTo12HourTime(fromTwentyFourTime: sHour, isWithSeconds: true)
        
        self.endTimeHour24H = CalendarUtils.retrieve24HourIntComponent(fromTimeStr: eHour)
        self.endTimeIn24H = eHour
        self.endTimeDisplayName12H = CalendarUtils.convertTo12HourTime(fromTwentyFourTime: eHour, isWithSeconds: true)
        
        self.hoursInBlock = StaticSpaceFunctions.retrieveHoursInBlock(forAvailabilityModuleBlock: block)
        
        self.blockPrice = block.charge!
        
        if block.blockMenus.count > 0 {
            self.blockMenu = block.blockMenus[0]
        }
        
        // Block in Notice Period is initially set to false initially
        self.isBlockInNoticePeriod = false
        
        // Block availability based on the Future Bookings to blocks
        self.isBlockAvailable = true // NOTE: Initially Set to true
        
        // Block availability based on the selection of overlapping blocks
        self.isBlockAvailableUponSelection = true
    }
}
