//
//  FilterManager.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import GooglePlaces.GMSPlace

struct SpaceFilter {
    
    var eventTypes: [EventType] = []
    var amenities: [Amenity] = []
    var rules: [Rule] = []
    var seatingArrangements: [SeatingArrangement] = []
    var location: GMSPlace?
    var date: Date?
    var timeHour: Int?
    var ampm: String?
    var spaceCapacity: [Int] = []
    var price: [Int] = []
    var sortBy: String?
    var searchKeyword: String?
    var spaceType: [Int] = []
    
    // A property used to manage navigation to/from Spaces List
    var shouldRefreshSpacesListWithFilters = true
}


class FilterManager: NSObject {
    
    static var spaceFilter = SpaceFilter()
    
    static func resetFilters() {
        self.spaceFilter.eventTypes.removeAll()
        self.spaceFilter.amenities.removeAll()
        self.spaceFilter.rules.removeAll()
        self.spaceFilter.seatingArrangements.removeAll()
        self.spaceFilter.location = nil
        self.spaceFilter.date = nil
        self.spaceFilter.timeHour = nil
        self.spaceFilter.ampm = nil
        self.spaceFilter.spaceCapacity.removeAll()
        self.spaceFilter.price.removeAll()
        self.spaceFilter.sortBy = nil
        self.spaceFilter.searchKeyword = nil
        self.spaceFilter.shouldRefreshSpacesListWithFilters = true
        self.spaceFilter.spaceType.removeAll()
    }
    
    static func retrieveFiltersForWebRequest() -> [String : Any] {
        var eventTypes: [Int] = []
        for et in self.spaceFilter.eventTypes {
            eventTypes.append(Int(et.id))
        }
        
        var amenities: [Int] = []
        for a in self.spaceFilter.amenities {
            amenities.append(Int(a.id))
        }
        
        var rules: [Int] = []
        for r in self.spaceFilter.rules {
            rules.append(Int(r.id))
        }
        
        var seatingArrangements: [Int] = []
        for sa in self.spaceFilter.seatingArrangements {
            seatingArrangements.append(Int(sa.id))
        }
        
        var location = [String : String]()
        if let place = self.spaceFilter.location {
            location = self.getLocation(fromPlace: place)
        }
        
        var availability: String? = nil
        if self.spaceFilter.date != nil {
            availability = self.getAvailableDateFilterForRequest()
        }
        
        var price: String? = nil
        if self.spaceFilter.price.count > 0 {
            self.spaceFilter.price.sort()
            if let lowOp = StaticData.FILTER_PRICE_OPTIONS.first(where: { ($0["id"] as! Int) == self.spaceFilter.price.first!}),
                let highOp = StaticData.FILTER_PRICE_OPTIONS.first(where: { ($0["id"] as! Int) == self.spaceFilter.price.last!}) {
                price = "\((lowOp["value"] as! [Int])[0])-\((highOp["value"] as! [Int])[1])"
            }
        }
        
        var spaceCapacity: String? = nil
        if self.spaceFilter.spaceCapacity.count > 0 {
            self.spaceFilter.spaceCapacity.sort()
            if let lowCap = StaticData.FILTER_CAPACITY_OPTIONS.first(where: { ($0["id"] as! Int) == self.spaceFilter.spaceCapacity.first!}),
                let highCap = StaticData.FILTER_CAPACITY_OPTIONS.first(where: { ($0["id"] as! Int) == self.spaceFilter.spaceCapacity.last!}) {
                spaceCapacity = "\((lowCap["value"] as! [Int])[0])-\((highCap["value"] as! [Int])[1])"
            }
        }
        
        var sort: String? = nil
        if let sortBy = self.spaceFilter.sortBy {
            sort = sortBy
        }
        
        var output = [String : Any]()
        output["events"] = eventTypes
        output["amenities"] = amenities
        output["rules"] = rules
        output["seatingArrangements"] = seatingArrangements
        output["available"] = availability
        output["budget"] = price
        output["participation"] = spaceCapacity
        output["sortBy"] = sort
        
        if !location.isEmpty {
            output["location"] = location
        }
        
        output["spaceType"] = self.spaceFilter.spaceType
        
        if let sKeyWord = self.spaceFilter.searchKeyword {
            // NOTE: field for search keyword in advance search API call is called "organizationName" as the feature only supported seaching by organization from 5th Jan 2018 until it was changed on 7th Feb 2018. Now search supports organization name or space name for search. Yet the API's field is still called "organizationName".
            output["organizationName"] = sKeyWord
        }
        
        return output
    }
    
    private static func getLocation(fromPlace place: GMSPlace) -> [String : String] {
        var location = [String : String]()
        location["latitude"] = String(place.coordinate.latitude)
        location["longitude"] = String(place.coordinate.longitude)
        location["address"] = place.name
        return location
    }
    
    private static func getAvailableDateFilterForRequest() -> String {
        let displayDateStr = CalendarUtils.retrieveSimpleDateString(fromDate: self.spaceFilter.date!)
        let timeHour24HValue: Int!
        if self.spaceFilter.ampm! == "AM" {
            timeHour24HValue = (self.spaceFilter.timeHour! == 12) ? 0 : self.spaceFilter.timeHour!
        }
        else {
            timeHour24HValue = (self.spaceFilter.timeHour! == 12) ? 12 : self.spaceFilter.timeHour! + 12
        }
        
        return "\(displayDateStr)T\(timeHour24HValue!):00:00Z"
    }
    
    static func setAvailableDateFilter(date: Date, hour: Int, amPm: Int) {
        self.spaceFilter.date = CalendarUtils.retrieveLocaleDate_2(fromSimpleDate: date)
        self.spaceFilter.timeHour = hour
        self.spaceFilter.ampm = (amPm == 0) ? "AM" : "PM"
    }
    
    static func getAvailableDateFilterDisplayValue() -> String {
        if let date = self.spaceFilter.date {
            let displayDateStr = CalendarUtils.retrieveSimpleDateString_2(fromDate: date)
            return "\(displayDateStr),   \(self.spaceFilter.timeHour!) \(self.spaceFilter.ampm!)"
        }
        else {
            return ""
        }
    }
    
}
