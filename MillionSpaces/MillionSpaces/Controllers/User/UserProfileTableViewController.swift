//
//  UserProfileTableViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class UserProfileTableViewController: BaseTableViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var jobTitleTextField: UITextField!
    @IBOutlet weak var companyPhoneTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: - Class properties / variables
    var didLoadAtViewDidLoad = false
    var isPageEdited = false
    var hasValidDetailsToSave = true
    var alertController: UIAlertController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        configureView()
        retrieveAuthenticatedUserDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToProfileFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToProfileFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) {
            alertController?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }
    
    
    func configureView() {
        self.navigationItem.title = "Profile"
        
        self.saveButton.setTitle("GO BACK", for: .normal)
        
        configureUpdateProfileImageActionSheet()
        
        //self.addKeyBoardDismissOnTap()
        
        self.addNumberPadDoneButton(textField: self.mobileNumberTextField)
        self.addNumberPadDoneButton(textField: self.companyPhoneTextField)
        
        self.mobileNumberTextField.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        self.companyPhoneTextField.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        
        if let profileImageUrl = AuthenticationManager.user?.profileImageUrl  {
            self.profileImageView.contentMode = .scaleAspectFill
            self.profileImageView.sd_setImage(with: profileImageUrl, placeholderImage: #imageLiteral(resourceName: "img-guest-default"))
        }
        else {
            //self.profileImageView.contentMode = .center
            //self.profileImageView.image = #imageLiteral(resourceName: "ico-camera")
            
            // TEMP - Until image upload is implemented
            self.profileImageView.contentMode = .scaleAspectFill
            self.profileImageView.image = #imageLiteral(resourceName: "img-guest-default")
        }
        
        self.nameTextField.text = AuthenticationManager.user!.name
        self.emailTextField.text = AuthenticationManager.user!.email
        
        if let mobile = AuthenticationManager.user?.mobileNumber {
            self.mobileNumberTextField.text = mobile
        }
        
        if let compName = AuthenticationManager.user?.companyName {
            self.companyNameTextField.text = compName
        }
        
        if let address = AuthenticationManager.user?.companyAddress {
            self.addressTextField.text = address
        }
        
        if let job = AuthenticationManager.user?.jobTitle {
            self.jobTitleTextField.text = job
        }
        
        if let compPhone = AuthenticationManager.user?.companyPhone {
            self.companyPhoneTextField.text = compPhone
        }
        
    }
    
    
    func configureUpdateProfileImageActionSheet() {
        
        alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // "Update Profile Image"
        // "Select upload option"
        
        let cancelActionButton = UIAlertAction(title: AlertMessages.CANCEL_ACTION, style: .cancel) { action -> Void in
            print("Cancel")
        }
        alertController!.addAction(cancelActionButton)
        
        let actionButton_1 = UIAlertAction(title: "Select photo from gallery", style: .default) { action -> Void in
            print("Select photo from gallery")
        }
        alertController!.addAction(actionButton_1)
        
        let actionButton_2 = UIAlertAction(title: "Take new photo", style: .default) { action -> Void in
            print("Take new photo")
        }
        
        alertController!.addAction(actionButton_2)
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isPageEdited = true
        self.saveButton.setTitle("SAVE", for: .normal)
        
        if textField == self.nameTextField {
            self.nameTextField.textColor = UIColor.black
            self.nameTextField.setTextFieldPlaceholder(withPlaceholderText: "Name", andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_VALID_PLACEHOLDER_COLOR_GRAY)
        }
        else if textField == self.mobileNumberTextField {
            self.mobileNumberTextField.textColor = UIColor.black
        }
        else if textField == self.companyPhoneTextField {
            self.companyPhoneTextField.textColor = UIColor.black
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text!.trimmingCharacters(in: .whitespaces)
        validateProfileInfo()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if self.nameTextField.isFirstResponder {
            self.mobileNumberTextField.becomeFirstResponder()
        }
        else if self.companyNameTextField.isFirstResponder  {
            self.addressTextField.becomeFirstResponder()
        }
        else if self.addressTextField.isFirstResponder  {
            self.jobTitleTextField.becomeFirstResponder()
        }
        else if self.jobTitleTextField.isFirstResponder  {
            self.companyPhoneTextField.becomeFirstResponder()
        }
        
        return true
    }
    
    @objc func textChanged(_ textField: UITextField) {
        if textField.text!.count > 10 {
            textField.deleteBackward()
        }
    }
    
    func validateProfileInfo() {
        hasValidDetailsToSave = true
        
        if !(self.nameTextField.text!.count > 0 && Utils.containsOnlyLetters(input: self.nameTextField.text!)){
            hasValidDetailsToSave = false
            self.nameTextField.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            self.nameTextField.setTextFieldPlaceholder(withPlaceholderText: "Name", andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED)
        }
        
        if self.mobileNumberTextField.text!.count > 0 {
            if !(self.mobileNumberTextField.text!.count <= 10 && Utils.isStringNumeric(input: self.mobileNumberTextField.text!)) {
                hasValidDetailsToSave = false
                self.mobileNumberTextField.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            }
        }
        
        if self.companyPhoneTextField.text!.count > 0 {
            if !(self.companyPhoneTextField.text!.count <= 10 && Utils.isStringNumeric(input: self.companyPhoneTextField.text!)) {
                hasValidDetailsToSave = false
                self.companyPhoneTextField.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            }
        }
    }
    
    func updateUserProfileDetails() {
        if hasValidDetailsToSave {
            var userProfileDetails: [String : String] = [:]
            
            userProfileDetails["name"] = self.nameTextField.text!
            userProfileDetails["email"] = AuthenticationManager.user!.email
            
            if self.mobileNumberTextField.text!.count == 10 && Utils.isStringNumeric(input: self.mobileNumberTextField.text!) {
                userProfileDetails["mobileNumber"] = self.mobileNumberTextField.text!
            }
            
            if self.companyNameTextField.text!.count > 0 {
                userProfileDetails["companyName"] = self.companyNameTextField.text!
            }
            
            if self.addressTextField.text!.count > 0 {
                userProfileDetails["address"] = self.addressTextField.text!
            }
            
            if self.jobTitleTextField.text!.count > 0 {
                userProfileDetails["jobTitle"] = self.jobTitleTextField.text!
            }
            
            if self.jobTitleTextField.text!.count > 0 {
                userProfileDetails["jobTitle"] = self.jobTitleTextField.text!
            }
            
            if self.companyPhoneTextField.text!.count == 10 && Utils.isStringNumeric(input: self.companyPhoneTextField.text!) {
                userProfileDetails["companyPhone"] = self.companyPhoneTextField.text!
            }
            
            if let profileImageUrl = AuthenticationManager.user?.profileImageUrl {
                userProfileDetails["imageUrl"] = profileImageUrl.absoluteString
            }
            
            
            self.showActivityIndicator()
            
            WebServiceCall.updateUser(userDetails: userProfileDetails, successBlock: { (results: Any) in
                print("UserProfileVC updateUserProfileDetails | success results : \(results)")
                
                if results as! String == "User update success" {
                    self.retrieveAuthenticatedUserDetails()
                }
                else {
                    self.profileUpdateResultAlert(isSuccess: false, message: AlertMessages.EDIT_PROFILE_FAILURE)
                }
                
                DispatchQueue.main.sync(execute: {
                    self.hideActivityIndicator()
                })
                
            },
                                      failureBlock: { (errorCode: Int, error: String) in
                                        print("UserProfileVC updateUserProfileDetails | failure error : \(error)")
                                        DispatchQueue.main.sync(execute: {
                                            self.hideActivityIndicator()
                                            if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                                                self.profileUpdateResultAlert(isSuccess: false, message: AlertMessages.SERVER_ERROR_MESSAGE_2)
                                            }
                                            else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                                                self.profileUpdateResultAlert(isSuccess: false, message: AlertMessages.INTERNET_OFFLINE_MESSAGE_2)
                                            }
                                            else {
                                                self.profileUpdateResultAlert(isSuccess: false, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                                            }
                                        })
            })
        }
    }
    
    // MARK: - Retrieve user details for authenticated user
    func retrieveAuthenticatedUserDetails() {
        self.showActivityIndicator()
        WebServiceCall.retrieveUser(successBlock: { (results: Any) in
            print("UserProfileVC retrieveAuthenticatedUserDetails | user profile : \(results)")
            
            DispatchQueue.main.sync(execute: {
                AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                if self.didLoadAtViewDidLoad == true {
                    self.profileUpdateResultAlert(isSuccess: true, message: AlertMessages.EDIT_PROFILE_SUCCESS)
                    self.saveButton.setTitle("GO BACK", for: .normal)
                    self.isPageEdited = false
                }
                else {
                    self.configureView()
                    self.didLoadAtViewDidLoad = true
                }
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("UserProfileVC retrieveAuthenticatedUserDetails | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                    AuthenticationManager.logOutAuthenticatedUser()
                    self.profileUpdateResultAlert(isSuccess: false, message: AlertMessages.SERVER_ERROR_MESSAGE_2)
                }
                else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.profileUpdateResultAlert(isSuccess: false, message: AlertMessages.SERVER_ERROR_MESSAGE_2)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.profileUpdateResultAlert(isSuccess: false, message: AlertMessages.INTERNET_OFFLINE_MESSAGE_2)
                }
                else {
                    self.profileUpdateResultAlert(isSuccess: false, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func profileUpdateResultAlert(isSuccess: Bool, message: String) {
        let secondAction: UIAlertAction!
        
        if isSuccess{
            alertController = UIAlertController(title: AlertMessages.SUCCESS_TITLE, message: message, preferredStyle: .alert)
            secondAction = UIAlertAction(title: AlertMessages.GO_BACK_ACTION, style: .default) { (alert: UIAlertAction) in
                self.popViewController()
            }
        }
        else {
            alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
            secondAction = UIAlertAction(title: AlertMessages.RETRY_ACTION, style: .default) { (alert: UIAlertAction) in
                self.updateUserProfileDetails()
            }
        }
        
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(secondAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    //MARK: - @IBAction
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func updateProfileImageButtonPressed(_ sender: UIButton) {
        //print("UserProfileVC updateProfileImageButtonPressed")
        //self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        //print("UserProfileVC saveButtonPressed")
        if isPageEdited {
            updateUserProfileDetails()
        }
        else {
            popViewController()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    /*
     override func numberOfSections(in tableView: UITableView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }
     
     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     // #warning Incomplete implementation, return the number of rows
     return 0
     }*/
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
