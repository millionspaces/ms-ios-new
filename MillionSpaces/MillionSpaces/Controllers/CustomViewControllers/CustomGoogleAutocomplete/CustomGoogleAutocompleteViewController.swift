//
//  CustomGoogleAutocompleteViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import GooglePlaces

class CustomGoogleAutocompleteViewController: GMSAutocompleteViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    
    /*
     override func viewWillAppear(_ animated: Bool) {
     NotificationCenter.default.addObserver(self, selector: #selector(self.returnedFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
     }
     
     override func viewWillDisappear(_ animated: Bool) {
     NotificationCenter.default.removeObserver(self)
     }
     
     func returnedFromBackground() {
     print("CustomGoogleAutocompleteViewController returnedFromBackground")
     }*/
    
    
    func configureView() {
        self.navigationController?.navigationBar.barTintColor = UINavigationBar.appearance().barTintColor
        
        //UIColor.init(red: 13.0/255.0, green: 89.0/255.0, blue: 253.0/255.0, alpha: 1.0)
        
        //self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 13.0/255.0, green: 89.0/255.0, blue: 253.0/255.0, alpha: 1.0)
        //self.navigationController?.navigationBar.tintColor = UIColor.white
        //self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        //self.navigationController?.navigationBar.isTranslucent = false
        
        
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 13.0/255.0, green: 89.0/255.0, blue: 143.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //UINavigationBar.appearance().searchBarTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        
        
        
        //UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont(name: "AvenirNextCondensed-Regular", size: 17.0)
        //UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        
        //UISearchBar.appearance().searchBarTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.primaryTextHighlightColor = .black
        self.primaryTextColor = .lightGray
        self.secondaryTextColor = .lightGray
        self.tableCellBackgroundColor = .white
        self.tableCellSeparatorColor = .lightGray
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
