//
//  Space.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class Space: NSObject {
    
    var id: Int!
    var name: String!
    var addressLine2: String? // o
    var participantCount: Int!
    var ratePerHour: Int!
    var thumbnailImage: String!
    // discount
    // preStaringFrom
    
    
    
    var sDescription: String? // !
    var addressLine1: String? // !
    
    var longitude: Double?  // !
    var latitude: Double?  // !

    var size: Int? // !
    var sizeMeasurementUnit: MeasurementUnit? //measurementUnit
    
     // m thumbnailImage
    var images: NSMutableArray = []  // images
    
    var eventTypes: NSMutableArray = [] // eventType  // TODO
    var amenities: NSMutableArray = [] // amenity  // TODO
    var extraAmenities: NSMutableArray = [] //extraAmenity  // TODO
    
      // m ratePerHour
    var availabilityMethod: String? // !    availabilityMethod HOUR_BASE BLOCK_BASE
    var availabilityModules: [SpaceAvailabilityModule] = [] //o  availability  // TODO
    var menus: [SpaceMenu] = []
    
    var blockChargeType: Int = 0 // o      if availabilityMethod == BLOCK_BASE ---> 1 = Space Only Blocks | 2 = Per Guest Based Blocks
    var minimumParticipantCount: Int = 0    // Will only have a value when blockChargeType = 2 aka Per Guest Based Blocks
    
    var noticePeriod: Int?  // o noticePeriod
    var bufferTime: Int?    // ! bufferTime
    var calendarStartDate: String? // ! calendarStart
    var calendarEndDate: String?   // ! calendarEnd
    var calendarEventSize: Int?    // ! calendarEventSize
    
    var cancellationPolicy: CancellationPolicy?  // !cancellationPolicy
    var spaceRules: NSMutableArray = [] // o rules  // ! TODO
    var seatingArrangements: NSMutableArray = [] // ! seatingArrangements  // TODO
    
    var rating: Float?  // ! rating
    var reviewDetails: NSMutableArray = [] // reviewDetails  // TODO
    
    var similarSpaces: NSMutableArray = [] //similarSpaces  // TODO
    
    var securityDeposit: String?    //securityDeposit // TODO To be Assigned
    
    var createdAt: Int? // ! createdAt
    var updatedAt: Int? // ! updatedAt
    
    var hostId: Int?  // ! user
    var hostDetails: NSDictionary? //o spaceOwnerDto
    
    // Custom variables
    var fullAdrress: String?
    var hasReimbursableBlocks: Bool = false // Temp Var
    var spaceType: SpaceType?
    
    
    
    
    
    func initSpace(withDictionary dic: [String : Any]) {
        
        
        // ================= !!!! ==============
        self.id = (dic["id"] as! Int)
        self.name = (dic["name"] as! String).replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
        self.ratePerHour = (dic["ratePerHour"] as! Int)
        self.participantCount = (dic["participantCount"] as! Int)
        self.thumbnailImage = (dic["thumbnailImage"] as! String)
        
        
        
        // ================= ????? ==============
        
        // Description
        if let descrpn = dic["description"] as? String {
            self.sDescription = (descrpn.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil))
        }
        
        // Address 1
        if let address1 = dic["addressLine1"] as? String {
            self.addressLine1 = address1.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
        }
        
        // Address 2
        if let address2 = dic["addressLine2"] as? String {
            self.addressLine2 = address2.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
            
        }
        
        // Full Address
        if self.addressLine1 != nil && self.addressLine2 != nil {
            self.fullAdrress = self.addressLine2! + ", " + self.addressLine1!
        }
        else if self.addressLine1 != nil {
            self.fullAdrress = self.addressLine1!
        }
        else if self.addressLine2 != nil {
            self.fullAdrress = self.addressLine2!
        }
        
        
        // longitude
        if let long = dic["longitude"] as? String {
            self.longitude = Double(long)
        }
        
        // latitude
        if let lat = dic["latitude"] as? String {
            self.latitude = Double(lat)
        }
        
        // size
        if let size = dic["size"] as? Int {
            self.size = size
        }
        
        // Measurement Unit
        if let measurementUnit = dic["measurementUnit"] as? [String : Any] {
            self.sizeMeasurementUnit = MetaData.retrieveMeasurementUnit(with: measurementUnit["id"] as! Int)
        }
        
        // Images
        if let images = dic["images"] as? [String] {
            self.images.addObjects(from: images)
        }

        // Event Types
        if let eventTypeArr = dic["eventType"] as? [Any] {
            for i in eventTypeArr {
                if let et = MetaData.retrieveEventType(with: i as! Int) {
                    self.eventTypes.add(et)
                }
            }
        }
        
        // Amenity
        if let amenityArr = dic["amenity"] as? [Any] {
            for i in amenityArr {
                if let a = MetaData.retrieveAmenity(with: i as! Int) {
                    self.amenities.add(a)
                }
            }
        }
        
        // Extra Amenity
        if let extrAmenityArr = dic["extraAmenity"] as? [Any] {
            for d in extrAmenityArr {
                let ea = SpaceExtraAmenity()
                ea.initSpaceExtraAmenity(withDictionary: d as! [String : Any])
                self.extraAmenities.add(ea)
            }
        }
        
        // Availability Method
        if let avMethod = dic["availabilityMethod"] as? String {
            self.availabilityMethod = avMethod
            
            // blockChargeType when Availability Method = BLOCK_BASE
            if self.availabilityMethod == "BLOCK_BASE" {
                if let blkChargeType = dic["blockChargeType"] as? Int {
                    self.blockChargeType = blkChargeType
                }
                
                if self.blockChargeType == 1 { // Space Only
                    for a in self.availabilityModules {
                        if a.isReimbursable {
                            self.hasReimbursableBlocks = true
                            break
                        }
                    }
                }
                else if self.blockChargeType == 2 { // Per Guest
                    if let minParticipantCount = dic["minParticipantCount"] as? Int {
                        //print("Space initSpace | minParticipantCount - \(minParticipantCount)")
                        self.minimumParticipantCount = minParticipantCount
                    }
                }
            }
        }
        
        // Availability
        if let availabilityArr = dic["availability"] as? [NSDictionary] {
            for d in availabilityArr {
                let am = SpaceAvailabilityModule()
                am.initSpaceAvailabilityModule(withDictionary: d as! [String : Any])
                self.availabilityModules.append(am)
            }
        }
        
        // Menus
        if let menus = dic["menuFiles"] as? [[String : Any]] {
            for i in menus {
                let menu = SpaceMenu()
                menu.initSpaceMenu(withDictionary: i)
                self.menus.append(menu)
            }
            
            if self.menus.count > 0 { self.menus.sort(by: { ($0 as SpaceMenu).menuName < ($1 as SpaceMenu).menuName }) }
        }
        
        // Notice Period
        if let noticePeriod = dic["noticePeriod"] as? Int {
            self.noticePeriod = noticePeriod
        }
        
        // Buffer Time
        if let bufferTime = dic["bufferTime"] as? Int {
            self.bufferTime = bufferTime
        }
        else {
            self.bufferTime = 0
        }
        
        // Calendar Start Date
        if let calStartDate = dic["calendarStart"] as? String {
            self.calendarStartDate = calStartDate
        }
        
        // Calendar End Date
        if let calEndDate = dic["calendarEnd"] as? String {
            self.calendarEndDate = calEndDate
        }
        
        // Calendar Event Size
        if let calEventSize = dic["calendarEventSize"] as? String {
            self.calendarEventSize = Int(calEventSize)
        }
        
        // Cancellation Policy
        if let cancellationPolicyID = dic["cancellationPolicy"] as? Int {
            self.cancellationPolicy = MetaData.retrieveCancellationPolicies(with: cancellationPolicyID)
        }
        
        // Space Rules
        if let rulesArr = dic["rules"] as? [Any] {
            for i in rulesArr {
                if let r = MetaData.retrieveSpaceRule(with: i as! Int) {
                    self.spaceRules.add(r)
                }
            }
        }
        
        // Seating Arrangements
        if let seatingArrangementsArr = dic["seatingArrangements"] as? [Any] {
            for d in seatingArrangementsArr {
                let sa = SpaceSeatingArrangement()
                sa.initSpaceSeatingArrangement(withDictionary: d as! [String : Any])
                self.seatingArrangements.add(sa)
            }
        }
        
        // Space Types
        if let sTypes =  dic["spaceType"] as? [[String : Any]] {
            if sTypes.count > 0 {
                let sType = SpaceType()
                sType.initSpaceType(withDictionary: sTypes[0])
                self.spaceType = sType
            }
        }
        
        // Rating // TODO Clarify
        if let rating = dic["rating"] as? Float {
            self.rating = rating
        }
        
        // ReviewDetails // TODO To be Assigned
        if !(dic["reviewDetails"] is NSNull) {
            //print("Space() reviewDetails NOT NULL")
        }
        
        // Similar Spaces
        if let similarSpacesArr = dic["similarSpaces"] as? [[String : Any]] {
            for d in similarSpacesArr {
                let ss = Space()
                ss.initSpace(withDictionary: d)
                self.similarSpaces.add(ss)
            }
            
        }
        
        // Created Date
        if let cDate = dic["createdAt"] as? Int {
            self.createdAt = cDate
        }
        
        // Updated Date
        if let uDate = dic["updatedAt"] as? Int {
            self.updatedAt = uDate
        }
        
        // Host ID
        if let host_id = dic["user"] as? Int {
            self.hostId = host_id
        }
        
        //securityDeposit // TODO To be Assigned
        if !(dic["spaceOwnerDto"] is NSNull) {
            //print("Space() spaceOwnerDto NOT NULL")
        }
        
        //securityDeposit // TODO To be Assigned
        if !(dic["securityDeposit"] is NSNull) {
            //print("Space() securityDeposit NOT NULL")
        }
    }
    
    
    func addSimilarSpaces(withSimilarSpacesArray arr: [[String : Any]]) {
        for dic in arr {
            let ss = Space()
            ss.initSpace(withDictionary: dic)
            self.similarSpaces.add(ss)
        }
    }
}
