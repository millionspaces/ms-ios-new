//
//  CalendarUtils.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/2/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class CalendarUtils: NSObject {

    static func today() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.string(from: date)
    }
    
    static func currentDateTime() -> [Any] {
        let todayDate = Date()
        //print("CalendarUtils 1) currentDateTimeComponents | todayDate : \(todayDate)")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let todayDateLocalStr = dateFormatter.string(from: todayDate)
        //print("CalendarUtils 2) currentDateTimeComponents | todayDateLocalStr : \(todayDateLocalStr)")
        
        let todayDateDay = retrieveDayFromDate(date: todayDate)
        let todayDateMonth = retrieveMonthFromDate(date: todayDate)
        let todayDateYear = retrieveYearFromDate(date: todayDate)
        
        let todayDateHour24 = retrieveTimeHourIn24Hour(date: todayDate)
        
        let todayDateSimpleStr = "\(todayDateYear)-\(todayDateMonth)-\(todayDateDay)"
        //print("CalendarUtils 3) currentDateTimeComponents | todayDateSimpleStr : \(todayDateSimpleStr)")
        
        //let todayDateSimpleStr_2 = self.retrieveSimpleDateString(fromDate: todayDate)
        //print("CalendarUtils 4) currentDateTimeComponents | todayDateSimpleStr_2 : \(todayDateSimpleStr_2)")
        
        let todayDateFromSimpleDateStr = self.retrieveLocaleDate(fromSimpleDateString: todayDateSimpleStr)!
        //print("CalendarUtils 5) currentDateTimeComponents | todayDateFromSimpleDateStr : \(todayDateFromSimpleDateStr)")
        
        let todayDateSimpleTimeStr24 = "\(todayDateHour24):00:00"
        //print("CalendarUtils 6) currentDateTimeComponents | todayDateSimpleTimeStr24 : \(todayDateSimpleTimeStr24)")
        
        let todayDateSimpleTimeStrNormal = self.convertTo12HourTime(fromTwentyFourTime: todayDateSimpleTimeStr24, isWithSeconds: true)
        //print("CalendarUtils )7 currentDateTimeComponents | todayDateSimpleTimeStrNormal : \(todayDateSimpleTimeStrNormal)")
        
        // TEMP: Return type
        return [todayDateLocalStr, todayDateFromSimpleDateStr, todayDateSimpleTimeStrNormal, todayDateSimpleStr]
    }
    
    
    
    // ===========================================   Date and Time by Device Calendar Settings =====================================
    
    static func currentDeviceDateTime_Test() {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        print("CalendarUtils currentDeviceDateTime_Test :- \(year)-\(month)-\(day)")
        print("CalendarUtils currentDeviceDateTime_Test :- \(hour):\(minute):\(second)")
    }
    
    static func currentDeviceDate() -> Date {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        
        var dateComponents = DateComponents()
        dateComponents.timeZone = TimeZone(secondsFromGMT: 0) // NOTE: This line should be here
        dateComponents.day = day
        dateComponents.month = month
        dateComponents.year = year
        return calendar.date(from: dateComponents)!
    }
    
    static func currentDeviceDateTime() -> Date {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        var dateComponents = DateComponents()
        dateComponents.timeZone = TimeZone(secondsFromGMT: 0) // NOTE: This line should be here
        dateComponents.day = day
        dateComponents.month = month
        dateComponents.year = year
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second
        return calendar.date(from: dateComponents)!
    }
    
    static func currentDeviceDateSimpleString() -> String {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        return "\(year)-\(month)-\(day)"
    }
    
    static func currentDeviceTime() -> String {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        return "\(hour):\(minutes):\(seconds)"
    }
    
    static func currentDeviceTimeShort() -> String {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        return "\(hour):\(minutes)"
    }
    
    static func retrieveNextAvailableHourOfToday() -> Int {
        let currentDateTime = self.currentDeviceDateTime()
        let currentDateTimeForNextHour = self.retrieveDate(thatComesAfterHours: 1, fromDate: currentDateTime)
        return self.retrieveTimeHourIn24Hour(date: currentDateTimeForNextHour)
    }
    
    
    
    // Local date from date string such as "2017-09-15"
    static func retrieveLocaleDate(fromSimpleDateString dateStr: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.date(from: dateStr)
    }
    
    // 2017-12-05 09:54:49 +0000 ---> 2017-12-05 00:00:00 +0000
    static func retrieveLocaleDate_2(fromSimpleDate date: Date) -> Date {
        let simpleDateStr = self.retrieveSimpleDateString(fromDate: date)
        return self.retrieveLocaleDate(fromSimpleDateString: simpleDateStr)!
    }
    
    // Date to a string such as "2017-09-15"
    static func retrieveSimpleDateString(fromDate date: Date) -> String {
        let year = self.retrieveYearFromDate(date: date)
        let month = self.retrieveMonthFromDate(date: date)
        let day = self.retrieveDayFromDate(date: date)
        
        return "\(year)-\(month)-\(day)"
    }
    
    // Date to a string such as "15 Sept 2017"
    static func retrieveSimpleDateString_2(fromDate date: Date) -> String {
        let year = self.retrieveYearFromDate(date: date)
        let month = self.retrieveMonthShortDisplayNameFromDate(date: date)
        let day = self.retrieveDayFromDate(date: date)
        
        return "\(day) \(month) \(year)"
    }
    
    // From String "2017-09-15" to "15 Sept 2017"
    static func retrieveSimpleDateString_2(fromDateStr dateStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let date = dateFormatter.date(from: dateStr)
        
        let year = self.retrieveYearFromDate(date: date!)
        let month = self.retrieveMonthShortDisplayNameFromDate(date: date!)
        let day = self.retrieveDayFromDate(date: date!)
        
        return "\(day) \(month) \(year)"
    }
    
    
    static func retrieveDayFromDate(date: Date) -> Int {
        var cal = Calendar.current
        cal.timeZone = TimeZone(secondsFromGMT: 0)!
        return (cal.dateComponents([.day], from: date)).day!
    }
    
    static func retrieveDayDisplayNameFromDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "eeee"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)!
        return dateFormatter.string(from: date)
    }
    
    static func retrieveDayofWeekFromDate(date: Date) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "e"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)!
        return Int(dateFormatter.string(from: date))!
    }
    
    
    static func retrieveMSDayofWeek(dayDisplayName: String) -> Int {
        switch dayDisplayName {
        case "Sunday":
            return 7
        case "Monday":
            return 1
        case "Tuesday":
            return 2
        case "Wednesday":
            return 3
        case "Thursday":
            return 4
        case "Friday":
            return 5
        case "Saturday":
            return 6
        default :
            return 7
        }
    }
    
    static func retrieveMonthDisplayNameFromDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)!
        return dateFormatter.string(from: date)
    }
    
    static func retrieveMonthShortDisplayNameFromDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLL"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)!
        return (dateFormatter.string(from: date))
    }
    
    static func retrieveMonthFromDate(date: Date) -> Int {
        var cal = Calendar.current
        cal.timeZone = TimeZone(secondsFromGMT: 0)!
        return (cal.dateComponents([.month], from: date)).month!
    }
    
    static func retrieveYearFromDate(date: Date) -> Int {
        var cal = Calendar.current
        cal.timeZone = TimeZone(secondsFromGMT: 0)!
        return (cal.dateComponents([.year], from: date)).year!
    }
    
    static func retrieveFirstDateOfMonth(fromDate date: Date) -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar.date(from: calendar.dateComponents([.month, .year], from: date))!
    }
    
    static func retrieveLastDateOfMonth(fromFirstDateOfMonth date: Date) -> Date {
        var dc = DateComponents()
        dc.month = 1
        dc.day = -1
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar.date(byAdding: dc, to: date)!
    }
    
    
    // Retrieve SpaceCalendarDates for Calendar | calendarType --> 1 = guest | 2 = host
    static func retrieveDates(fromDate: Date, toDate: Date, calendarStartDate: Date, calendarEndDate: Date, spaceCalendarInfo: SpaceCalendarInfo, forGuestCalendar: Bool) -> [SpaceCalendarDate] {
        
        let numOfDates = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day!
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        
        var returnArray: [SpaceCalendarDate] = []
        
        print("CalUtils retrieveDates | numOfDates : \(numOfDates)")
        
        for i in 0...numOfDates {
            let date = calendar.date(byAdding: .day, value: i, to: fromDate)
            let scalDateObj = SpaceCalendarDate()
            
            if forGuestCalendar {
                scalDateObj.initSpaceCalendarDateForGuestCalendar(fromDate: date!, calendarEndingDate: calendarEndDate, spaceCalendarInfo: spaceCalendarInfo)
            }
            else {
                scalDateObj.initSpaceCalendarDateForHostCalendar(fromDate: date!, calendarStartingDate: calendarStartDate, calendarEndingDate: calendarEndDate, spaceCalendarInfo: spaceCalendarInfo)
            }
            
            returnArray.append(scalDateObj)
        }
        
        return returnArray
    }
    
    
    
    static func retrieveDates(fromDate: Date, toDate: Date) -> [Date] {
        var returnArray: [Date] = []
        
        let numOfDates = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day!
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        
        if numOfDates > 0 {
            for i in 0...numOfDates {
                let date = calendar.date(byAdding: .day, value: i, to: fromDate)
                returnArray.append(date!)
            }
        }
        
        return returnArray
    }
    
    // if years = 2 && date = 2017-12-14 18:30 -----> output = 2019-12-16 18:30
    static func retrieveDate(thatComesAfterYears years: Int, fromDate date: Date) -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar.date(byAdding: .year, value: years, to: date)!
    }
    
    // if days = 2 && date = 2017-12-14 18:30 -----> output = 2017-12-16 18:30
    static func retrieveDate(thatComesAfterDays days: Int, fromDate date: Date) -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar.date(byAdding: .day, value: days, to: date)!
    }
    
    // if hours = -10 && date = 2017-12-14 18:30 -----> output = 2017-12-14 08:30
    static func retrieveDate(thatComesAfterHours hours: Int, fromDate date: Date) -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar.date(byAdding: .hour, value: hours, to: date)!
    }
    
    
    // DateTime from String Convert 2017-10-25T15:00 --> 2017-10-25, 15:00
    static func retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr datetime: String) -> [String] {
        return datetime.components(separatedBy: "T")
    }
    
    static func retrieveDateTimeFromComponents(dateStr: String, timeStr: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.date(from: "\(dateStr) \(timeStr)")!
    }
    
    // TODO: Does not work from international time zones
    // Is DateTime from String an upcoming date
    static func isUpcomingDateTime(datetimeStr: String) -> Bool {
        let datetimeComp = self.retrieveSpaceCalendarDateTimeComponents(fromDateTimeStr: datetimeStr)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let datetime = dateFormatter.date(from: "\(datetimeComp[0]) \(datetimeComp[1]):00")!
        
        let todayDateTime = Date().timeIntervalSince1970
        
        if datetime.timeIntervalSince1970 > todayDateTime {
            return true
        }
        return false
    }
    
    
    
    
    
    // MARK: Time Formatting
    
    // Convert 18:00:00 --> 18
    static func retrieve24HourIntComponent(fromTimeStr time: String) -> Int {
        let timeComponentsFromString = time.components(separatedBy: ":")
        return Int(timeComponentsFromString[0])!
    }
    
    // Covert 24 Hour time to 12 Hour time     20:00 / 20:00:00 --> 08 PM
    static func convertTo12HourTime(fromTwentyFourTime: String, isWithSeconds: Bool) -> String {
        //print("CalendarUtils convertTo12HourTime | input fromTwentyFourTime : \(fromTwentyFourTime)")
        
        var twentyFourTimeStr = ""
        var hasSeconds = false
        
        if fromTwentyFourTime == "24:00" || fromTwentyFourTime == "24:00:00" {
            twentyFourTimeStr = "00:00:00"
            hasSeconds = true
        }
        else {
            twentyFourTimeStr = fromTwentyFourTime
            hasSeconds = isWithSeconds
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = (hasSeconds) ? "HH:mm:ss" : "HH:mm"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        if let twentyFourHour = dateFormatter.date(from: twentyFourTimeStr) {
            
            // NOTE - Dependent of Device AM/PM or 24H time setting
            //print("CalendarUtils convertTo12HourTime --> twentyFourHour : \(twentyFourHour)")
            //dateFormatter.dateFormat = "HH A"
            //let twelveHour = dateFormatter.string(from: twentyFourHour)
            //print("CalendarUtils convertTo12HourTime --> twelveHour : \(twelveHour)")
            //return twelveHour
            
            // NOTE - Manually done | Independent of Device AM/PM or 24H time setting
            let hourInt = self.retrieveTimeHourIn24Hour(date: twentyFourHour)
            if hourInt == 0 {
                return "12 AM"
            }
            else if hourInt == 12 {
                return "12 PM"
            }
            else { // hourInt != 12 & != 0
                let returnHourInt = hourInt % 12
                return (hourInt < 12) ? "\(returnHourInt) AM" : "\(returnHourInt) PM"
            }
        }
        return ""
    }
    
    static func retrieveTimeHourIn24Hour(date: Date) -> Int {
        var cal = Calendar.current
        cal.timeZone = TimeZone(secondsFromGMT: 0)!
        return (cal.dateComponents([.hour], from: date)).hour!
    }
    
    
    // MARK: Other
    
    // Set Calendar View font size based on screen width
    static func setMSCalendarFontSize() -> CGFloat {
        if Utils.SCREEN_WIDTH == 320.0 {
            return 13.0
        }
        else if Utils.SCREEN_WIDTH == 375.0 {
            return 15.0
        }
        return 17.0
    }
    
}
