//
//  SpaceExtraAmenity.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceExtraAmenity: NSObject {
    
    var id: Int!
    var extraAmenity: Amenity!
    var rate: Int!
    var extraAmenityUnit: AmenityUnit!
    
    
    func initSpaceExtraAmenity(withDictionary dic: [String : Any]) {
        self.id = dic["amenityId"] as! Int
        self.extraAmenity = MetaData.retrieveAmenity(with: dic["amenityId"] as! Int)
        self.rate = dic["extraRate"] as! Int
        self.extraAmenityUnit = MetaData.retrieveAmenityUnit(with: dic["amenityUnit"] as! Int)
    }
    
    func initSpaceExtraAmenityForBookings(withDictionary dic: [String : Any]) {
        self.id = dic["id"] as! Int
        self.extraAmenity = MetaData.retrieveAmenity(with: dic["id"] as! Int)
        self.rate = dic["rate"] as! Int
        self.extraAmenityUnit = MetaData.retrieveAmenityUnit(with: dic["amenityUnit"] as! Int)
    }
    
}
