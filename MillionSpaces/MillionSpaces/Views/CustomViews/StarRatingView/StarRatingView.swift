//
//  StarRatingView.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class StarRatingView: UIView {
    
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        Bundle.main.loadNibNamed("StarRatingView", owner: self, options: nil)
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.clipsToBounds = true
        //self.view.frame = CGRect.init(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        self.addSubview(self.view)
    }
    
    public func setRatingValue(withInt rating: Int){
        clearStars()
        setStarRatingImages(rating * 10)
    }
    
    public func setRatingValue(withFloat rating: Float){
        clearStars()
        // Logic: 2.6 -> 26 - > (26 + (5 - (26 % 5)) - > 30
        let multipliedRating = Int(rating * 10)
        let remainder = (multipliedRating % 5)
        let newRating = remainder > 2 ? multipliedRating + (5 - remainder) : multipliedRating - remainder
        setStarRatingImages(newRating)
    }
    
    public func clearStars () {
        clearStar(imageView:self.star1)
        clearStar(imageView:self.star2)
        clearStar(imageView:self.star3)
        clearStar(imageView:self.star4)
        clearStar(imageView:self.star5)
    }
    
    private func fillFullStar(imageView:UIImageView){
        imageView.image = #imageLiteral(resourceName: "ico-fill-star")
    }
    private func fillHalfStar(imageView:UIImageView){
        imageView.image = #imageLiteral(resourceName: "ico-half-fill-star")
    }
    private func clearStar(imageView:UIImageView){
        imageView.image = #imageLiteral(resourceName: "ico-no-fill-star")
    }
    
    private func setStarRatingImages (_ rating: Int) {
        if(rating == 5){
            fillHalfStar(imageView: self.star1)
        }
        if( rating > 5 && rating >= 10){
            fillFullStar(imageView: self.star1)
        }
        if(rating == 15 ){
            fillHalfStar(imageView: self.star2)
        }
        if(rating >= 20){
            fillFullStar(imageView: self.star2)
        }
        if(rating == 25 ){
            fillHalfStar(imageView: self.star3)
        }
        if(rating > 25 && rating >= 30){
            fillFullStar(imageView: self.star3)
        }
        if(rating == 35 ){
            fillHalfStar(imageView: self.star4)
        }
        if(rating > 35 && rating >= 40){
            fillFullStar(imageView: self.star4)
        }
        if(rating == 45 ){
            fillHalfStar(imageView: self.star5)
        }
        if(rating == 50){
            fillFullStar(imageView: self.star5)
        }
    }
}
