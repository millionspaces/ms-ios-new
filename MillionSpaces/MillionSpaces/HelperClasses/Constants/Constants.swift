//
//  Constants.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/2/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class Constants: NSObject {

    // MARK: - UserDefaults
    static let USER_DEFAULTS = UserDefaults.standard
    
    
    // MARK: Notification Identifiers
    static let APPLICATION_IN_FOREGROUND = "ApplicationWillEnterForeground"
    static let GOOGLE_ACCESS_TOKEN_RECEIVED = "GOOGLE_ACCESS_TOKEN_RECEIVED"
    static let APPLICATION_DID_ENTER_BACKGROUND_TIMESTAMP = "APPLICATION_DID_ENTER_BACKGROUND_TIMESTAMP"
    static let PAYMEMT_SESSION_IN_PROGRESS = "PAYMEMT_SESSION_IN_PROGRESS"
    static let PAYMEMT_SESSION_IS_UP = "PAYMEMT_SESSION_IS_UP"
    
    
    // MARK: - Authentication Cookie Handling
    static let KEY_AUTH_COOKIE_STORAGE_DATA = "AUTH_COOKIE_STORAGE_DATA"
    static let KEY_AUTH_RESPONSE_COOKIES = "AUTH_RESPONSE_COOKIES"
    static let KEY_AUTH_RESPONSE_URL = "AUTH_RESPONSE_URL"
    
    
    // MARK: - Network status codes/error codes
    static let ERROR_CODE_REQUEST_TIMEOUT = -1001
    static let ERROR_CODE_NETWORK_CONNECTION_LOST = -1005
    static let ERROR_CODE_INTERNET_OFFLINE = -1009
    static let STATUS_CODE_REQUEST_SUCCESS = 200
    static let STATUS_CODE_REQUEST_SUCCESS_RANGE = 200...299
    static let STATUS_CODE_UNAUTHORIZED = 403
    static let STATUS_CODE_SERVER_ERROR_RANGE = 500...599
    static let STATUS_CODE_SERVER_GATEWAY_TIMEOUT = 504
    
    
    // MARK: - MillionSpaces Details
    static let MS_PHONE_NUMBER = "0117811811"
    
    
    // MARK: - Common
    static let MS_TERMS_PRIVACY_POLICY = "https://beta.millionspaces.com/#/terms/privacy"
    
    
    // MARK: - View components
    static let MAIN_STORYBOARD = "Main"
    
    
    // MARK: - View Controller Storyboard IDs
    static let VC_ID_SIGN_IN_SCREEN = "SignInVC"
    static let VC_ID_NOTIFICATION_SCREEN = "NotificationNC"
    static let VC_ID_SPACE_DETAILS_SCREEN = "SpaceDetailsVC"
    static let VC_ID_BOOKING_INITIAL_SCREEN = "BookingInitialVC"
    static let VC_ID_BOOKING_CALENDAR_SCREEN = "BookingCalendarVC"
    
    
    // MARK: - Segue Identifiers
    static let SEGUE_TO_SPACES_SCREEN = "SpacesSegue"
    static let SEGUE_TO_USER_PROFILE_SCREEN = "UserProfileTVCSegue" // UserProfileSegue
    static let SEGUE_TO_MY_SPACES_SCREEN = "MySpacesSegue"
    static let SEGUE_TO_MY_ACTIVITIES_SCREEN = "MyActivitiesSegue"
    static let SEGUE_TO_SIGN_UP_SCREEN = "SignUpSegue"
    static let SEGUE_TO_HOME_SCREEN = "HomeSegue"
    static let SEGUE_TO_FILTER_SCREEN = "FiltersSegue"
    
}
