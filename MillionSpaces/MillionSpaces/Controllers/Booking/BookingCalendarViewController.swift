//
//  BookingCalendarViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class BookingCalendarViewController: BaseViewController, UITextFieldDelegate {
    
    // MARK: - IBActions
    @IBOutlet weak var dateTimeTabButton: UIButton!
    @IBOutlet weak var dateTimeTabView: UIView!
    @IBOutlet weak var selectedDateLabel: UILabel!
    @IBOutlet weak var selectedMonthLabel: UILabel!
    @IBOutlet weak var selectedYearLabel: UILabel!
    @IBOutlet weak var extrasTabButton: UIButton!
    @IBOutlet weak var extrasTabView: UIView!
    @IBOutlet weak var extrasLabel: UILabel!
    @IBOutlet weak var calendarParentView: UIView!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var previousMonthCalendarToggleButton: UIButton!
    @IBOutlet weak var nextMonthCalendarToggleButton: UIButton!
    @IBOutlet weak var timeSelectionView: UIView!
    @IBOutlet weak var availabilityMethodLabel: UILabel!
    @IBOutlet weak var backToCalendarButton: UIButton!
    @IBOutlet weak var timeSelectionTableView: UITableView!
    @IBOutlet weak var extrasSelectionView: UIView!
    @IBOutlet weak var extrasSelectionTableView: UITableView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var constPriceViewBottom: NSLayoutConstraint!
    @IBOutlet weak var spacePriceLabel: UILabel!
    @IBOutlet weak var extrasPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var discountTitleLabel: UILabel!
    @IBOutlet weak var promoDiscountLabel: UILabel!
    @IBOutlet weak var addPromoCodeButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var tempCalendarCV: UICollectionView!
    @IBOutlet weak var constTempCalendarCVHeight: NSLayoutConstraint!
    @IBOutlet weak var blockBasisInfoLabel: UILabel!
    @IBOutlet weak var constBlockBasisInfoLabelHeight: NSLayoutConstraint!
    
    
    // MARK: - Constants
    let CALENDAR_WIDTH_HEIGHT = Utils.SCREEN_WIDTH - 70
    let CALENDAR_DATE_CELL_WIDTH_HEIGHT = (Utils.SCREEN_WIDTH - 70) / 7
    let ADD_REMOVE_PER_HOUR_EXTRA = (Add: "add", Remove: "remove")
    
    
    // MARK: - Class Properties / Variables
    // Data collected from previous screen
    var bookingSpace: Space!
    var selectedEventType: EventType!
    var selectedSeatingArrangement: SeatingArrangement!
    var selectedGuestCount: Int!
    var hasMinimumApplicableGuestCount = false
    //
    // Data to be manipulated in the current screen
    var calendarFontSize: CGFloat?
    var isBookingSpaceAvailableOnHourBasis: Bool!
    var bookingSpaceExtras: NSMutableArray = []
    var spaceCalendarInfo: SpaceCalendarInfo!
    var spaceCalendar: SpaceCalendar!
    var spaceCalendarMonths: [String] = []
    var spaceCalendarDatesDic: NSDictionary!
    var hoursForSelectedDateForPerHourBasis: [SpaceCalendarTimeByHour] = []
    var blocksForSelectedDateForBlockBasis: [SpaceCalendarTimeByBlock] = []
    var currentlyDisplayedMonthIndexPath: IndexPath!
    //
    // Data to be collected from the current screen
    var selectedDate: SpaceCalendarDate?
    var selectedDateVCTag: Int?
    var selectedDateVCItemIndexPath: Int?
    var selectedPerHourCells: [Int] = []    // If bookingSpace.availabilityMethod = "HOUR"
    var selectedBlockCells: [Int] = []      // If bookingSpace.availabilityMethod = "BLOCK"
    var selectedPerHourExtraAmenities: [BookingExtraAmenity] = []
    var selectedOtherExtraAmenities: [BookingExtraAmenity] = []
    var bookingCharge: Int!
    var appliedPromoCode: String?
    var isFlatDiscount: Bool?
    var promoDiscountAmount: Int?
    var alertController: UIAlertController?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView_Pre()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToBookingCalendarFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        self.configureView_Post()
        self.retrieveSpaceCalendarDetails()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToBookingCalendarFromBackground() {
        alertController?.dismiss(animated: false, completion: nil)
        self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
        if Utils.didPassDuration(durationInHours: 0.5) {
            self.popToRootViewController()
        }
        else {
            self.configureView_Post()
            self.retrieveSpaceCalendarDetails()
        }
    }
    
    func configureView_Pre() {
        self.navigationItem.title = Strings.BOOKING_ENGINE
        self.constTempCalendarCVHeight.constant = self.CALENDAR_WIDTH_HEIGHT
        self.dateTimeTabView.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
        self.dateTimeTabView.layer.borderWidth = 1.0
        self.extrasTabView.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
        self.extrasTabView.layer.borderWidth = 1.0
        self.isBookingSpaceAvailableOnHourBasis = (bookingSpace.availabilityMethod == Utils.MAIN_SPACE_CHARGE_TYPES.Hour)
        self.availabilityMethodLabel.text = (bookingSpace.availabilityMethod == Utils.MAIN_SPACE_CHARGE_TYPES.Hour) ? Strings.PER_HOUR : Strings.PER_BLOCK
        
        if self.bookingSpace.extraAmenities.count > 0 {
            self.extrasSelectionTableView.isHidden = false
            prepareExtrasList()
        }
        else {
            self.extrasSelectionTableView.isHidden = true
        }
        
        self.tempCalendarCV.allowsSelection = false
        self.calendarFontSize = CalendarUtils.setMSCalendarFontSize()
    }
    
    
    func configureView_Post() {
        self.proceedButton.isUserInteractionEnabled = false
        self.calendarParentView.isHidden = false
        self.timeSelectionView.isHidden = true
        self.extrasSelectionView.isHidden = true
        
        self.initialSubViewVisibility()
        
        self.spaceCalendarInfo = nil
        self.spaceCalendar = nil
        self.spaceCalendarMonths.removeAll()
        self.spaceCalendarDatesDic = nil
        
        self.selectedDate = nil
        self.selectedDateVCTag = nil
        self.selectedDateVCItemIndexPath = nil
        
        self.hoursForSelectedDateForPerHourBasis.removeAll()
        self.selectedPerHourCells.removeAll()
        
        self.blocksForSelectedDateForBlockBasis.removeAll()
        self.selectedBlockCells.removeAll()
        
        self.selectedPerHourExtraAmenities.removeAll()
        self.selectedOtherExtraAmenities.removeAll()
        
        self.selectedDateLabel.text = Strings.DEFAULT_DATE
        self.selectedMonthLabel.text = Strings.DEFAULT_MONTH
        self.selectedYearLabel.text = Strings.DEFAULT_YEAR
        
        self.appliedPromoCode = nil
        self.isFlatDiscount = nil
        self.promoDiscountAmount = nil
        self.addPromoCodeButton.alpha = 1.0
        self.addPromoCodeButton.setTitleColor(ColorUtils.MS_BLUE_COLOR, for: .normal)
        
        self.tempCalendarCV.reloadData()
        self.timeSelectionTableView.reloadData()
        self.extrasSelectionTableView.reloadData()
    }
    
    
    func retrieveSpaceCalendarDetails() {
        self.showActivityIndicator()
        
        WebServiceCall.retrieveGuestCalendarDetails(withSpaceID: self.bookingSpace.id!, successBlock: { (result: Any) in
            //print("BookingCalendarVC retrieveSpaceCalendarDetails | success result : \(result)")
            if let spaceDetails = result as? [String : Any] {
                self.spaceCalendarInfo = SpaceCalendarInfo()
                self.spaceCalendarInfo.initSpaceCalendarInfo(withSpaceID: self.bookingSpace.id!, andCalendarInfoDic: spaceDetails, forGuestCalendar: true)
                DispatchQueue.main.sync(execute: {
                    self.configureCalendarData()
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    self.addSpaceCalendarDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("BookingCalendarVC retrieveSpaceDetails | failure | errorCode : \(errorCode), error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpaceCalendarDetailsReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpaceCalendarDetailsReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpaceCalendarDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    
    
    
    func configureCalendarData() {
        if self.spaceCalendarInfo.blockChargeType == 2 {
            self.hasMinimumApplicableGuestCount = (self.bookingSpace.minimumParticipantCount <= self.selectedGuestCount!)
            self.blockBasisInfoLabel.text = "Minimum number of guests - \(self.bookingSpace.minimumParticipantCount)\nChanges for menu upgrades to be paid to host directly"
            self.constBlockBasisInfoLabelHeight.constant = Utils.SCREEN_WIDTH == 414.0 ? 28.0 : 42.0
        }
        else {
            self.blockBasisInfoLabel.text = ""
            self.constBlockBasisInfoLabelHeight.constant = 0.0
        }
        
        // Printing Details :
        print("BookingCalendarVC selectedEventType : \(self.selectedEventType.name!)")
        print("BookingCalendarVC selectedSeatingArrangement : \(self.selectedSeatingArrangement.name!)")
        print("BookingCalendarVC selectedGuestCount : \(self.selectedGuestCount!) \n")
        print("BookingCalendarVC BookingSpace : \(self.bookingSpace.id!, self.bookingSpace.name!)")
        print("BookingCalendarVC spaceCalendarInfo calendarStartDate : \(self.spaceCalendarInfo.calendarStartDate!)")
        print("BookingCalendarVC spaceCalendarInfo calendarEndDate : \(self.spaceCalendarInfo.calendarEndDate!)")
        print("BookingCalendarVC spaceCalendarInfo availability method : \(self.spaceCalendarInfo.availabilityMethod!)")
        print("BookingCalendarVC spaceCalendarInfo blockChargeType : \(self.spaceCalendarInfo.blockChargeType)")
        print("BookingCalendarVC spaceCalendarInfo bufferTime : \(self.spaceCalendarInfo.bufferTime)")
        print("BookingCalendarVC spaceCalendarInfo noticePeriod : \(self.spaceCalendarInfo.noticePeriod)")
        print("BookingCalendarVC spaceCalendarInfo futureBookings count : \(self.spaceCalendarInfo.calendarBookings.count)")
        /*
         for fb in self.spaceCalendarInfo.calendarBookings {
         print("BookingCalendarVC spaceCalendarInfo | futureBooking at i | from : \(fb.bookingFromDate!) \(fb.bookingFromTimeHourInt!) | to : \(fb.bookingToDate!) \(fb.bookingToTimeHourInt!)")
         }*/
        
        spaceCalendar = SpaceCalendar()
        spaceCalendar.initSpaceCalendar(withSpaceCalendarInfo: self.spaceCalendarInfo!)
        spaceCalendarMonths = spaceCalendar.calendarDatesDictionary.allKeys as! [String]
        spaceCalendarMonths.sort()
        spaceCalendarDatesDic = spaceCalendar.calendarDatesDictionary
        
        setMonthAndYearLabelTexts(forMonthAndYear: spaceCalendarMonths[0])
        self.selectedDateLabel.text = "\(spaceCalendar.todayDate.day!)"
        
        self.currentlyDisplayedMonthIndexPath = IndexPath.init(item: 0, section: 0)
        self.tempCalendarCV.reloadData()
        
        self.timeSelectionTableView.reloadData()
        
        self.proceedButton.isUserInteractionEnabled = true
    }
    
    func setMonthAndYearLabelTexts(forMonthAndYear my: String) {
        let componentsFromString = my.components(separatedBy: "-")
        self.selectedDateLabel.text = "1"
        self.selectedMonthLabel.text = componentsFromString[1].uppercased()
        self.selectedYearLabel.text = componentsFromString[2]
    }
    
    func prepareExtrasList() {
        // Add non-hourly first
        for i in bookingSpace.extraAmenities {
            if (i as! SpaceExtraAmenity).extraAmenityUnit.name != "per hour" {
                bookingSpaceExtras.add(i)
            }
        }
        
        // Then add hourly
        for i in bookingSpace.extraAmenities {
            if (i as! SpaceExtraAmenity).extraAmenityUnit.name == "per hour" {
                bookingSpaceExtras.add(i)
            }
        }
    }
    
    func backToCalendarView() {
        self.selectedDate = nil
        if self.isBookingSpaceAvailableOnHourBasis {
            self.selectedPerHourCells.removeAll()
            self.hoursForSelectedDateForPerHourBasis.removeAll()
        }
        else {
            self.selectedBlockCells.removeAll()
            validateSelectableBlocks()
            self.blocksForSelectedDateForBlockBasis.removeAll()
            self.selectedPerHourExtraAmenities.removeAll()
        }
        updateMainPriceLabels()
        handleTabSelectionTransition(selectedTab: 1)
    }
    
    func openPaymentSummaryScreen(bookingRequest: SpaceBookingRequest, bookingResponse: [String : Any]) {
        //print("CalendarVC openPaymentSummaryScreen")
        
        PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let paymentSummaryVC = storyboard.instantiateViewController(withIdentifier: "PaymentSummaryVC") as! PaymentViewController
        paymentSummaryVC.bookingSpace = bookingSpace
        paymentSummaryVC.bookingRequest = bookingRequest
        paymentSummaryVC.bookingResponse = bookingResponse
        self.navigationController?.pushViewController(paymentSummaryVC, animated: true)
    }
    
    func openNEWPaymentWebScreen(bookingRequest: SpaceBookingRequest, bookingResponse: [String : Any]) {
        
        PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let paymentWebVC = storyboard.instantiateViewController(withIdentifier: "PaymentWebVC") as! PaymentWebViewController
        paymentWebVC.bookingSpace = bookingSpace
        paymentWebVC.bookingRequest = bookingRequest
        paymentWebVC.bookingResponse = bookingResponse
        self.navigationController?.pushViewController(paymentWebVC, animated: true)
    }
    
    
    //MARK: IBActions
    @IBAction func dateTimeTabSelected(_ sender: UIButton) {
        //print("BookingVC dateTimeTabSelected")
        handleTabSelection(selectedTab: 1)
        handleTabSelectionTransition(selectedTab: 1)
    }
    
    @IBAction func extrasTabSelected(_ sender: UIButton) {
        //print("BookingVC extrasTabSelected")
        handleTabSelection(selectedTab: 2)
        handleTabSelectionTransition(selectedTab: 2)
    }
    
    @IBAction func previousMonthCalendarToggleButtonPressed(_ sender: UIButton) {
        //print("BookingVC previousMonthCalendarToggleButtonPressed")
        if currentlyDisplayedMonthIndexPath.item > 0 {
            self.tempCalendarCV.scrollToItem(at: IndexPath.init(item: currentlyDisplayedMonthIndexPath.item - 1, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    @IBAction func nextMonthCalendarToggleButtonPressed(_ sender: UIButton) {
        //print("BookingVC nextMonthCalendarToggleButtonPressed")
        if currentlyDisplayedMonthIndexPath.item < self.spaceCalendarMonths.count - 1 {
            self.tempCalendarCV.scrollToItem(at: IndexPath.init(item: currentlyDisplayedMonthIndexPath.item + 1, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    @IBAction func backToCalendarButtonPressed(_ sender: UIButton) {
        //print("BookingVC backToCalendarButtonPressed")
        backToCalendarView()
    }
    
    @IBAction func proceedButtonPressed(_ sender: UIButton) {
        //print("BookingVC proceedButtonPressed")
        if self.selectedPerHourCells.count > 0 || self.selectedBlockCells.count > 0 {
            //self.presentConfirmationAlert()
            self.presentUpdateMobileNumberAlert()
        }
        else {
            self.generateAlertToSelectTimeRange()
        }
    }
    
    @IBAction func addPromoCodeButtonPressed(_ sender: UIButton) {
        self.addApplyPromoCodeAlertMessage()
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if !extrasSelectionView.isHidden {
            self.handleTabSelection(selectedTab: 1)
            self.handleTabSelectionTransition(selectedTab: 1)
        }
        else if !timeSelectionView.isHidden {
            backToCalendarView()
        }
        else {
            popViewController()
        }
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        openNotificationViewController()
    }
    
    
    
    
    // ================================ MOBILE NUMBER UPDATE ===========================
    
    
    
    func presentUpdateMobileNumberAlert() {
        self.baseCommonAlertController = UIAlertController(title: "Enter Mobile Number", message: "", preferredStyle: .alert)
        
        self.baseCommonAlertController?.addTextField(configurationHandler: { (textField: UITextField) in
            textField.placeholder = "0777123456"
            textField.text = nil
            textField.keyboardAppearance = .dark
            textField.keyboardType = .phonePad
            textField.addTarget(self, action: #selector(self.mobileNumberAlertViewTextFieldChanged(_:)), for: .editingChanged)
        })
        
    
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        self.baseCommonAlertController!.addAction(dismissAction)
        
        let submitAction = UIAlertAction(title: AlertMessages.SUBMIT_ACTION, style: .default) { (action: UIAlertAction) in
            if let textField = self.baseCommonAlertController?.textFields?[0], textField.text!.count > 0 {
                let phoneNumber = textField.text!
                print("BookingVC | Entered phoneNumber: \(phoneNumber)")
                self.updateMobilePhoneNumber(numberStr: phoneNumber)
            }
        }
        submitAction.isEnabled = false
        self.baseCommonAlertController!.addAction(submitAction)
        self.present(self.baseCommonAlertController!, animated: true, completion: nil)
    }
    
    @objc func mobileNumberAlertViewTextFieldChanged(_ sender: Any) {
        let textField = sender as! UITextField
        var responder : UIResponder! = textField
        while !(responder is UIAlertController) { responder = responder.next }
        let alert = responder as! UIAlertController
        let submitAction = alert.actions[1]
        
        if textField.text!.count > 10 {
            textField.deleteBackward()
        }
        else if textField.text!.count >= 10 {
            submitAction.isEnabled = Utils.isStringNumeric(input: textField.text!)
        }
        else {
            submitAction.isEnabled = false
        }
        
        if textField.text!.count >= 10 {
            
        }
        else {
            submitAction.isEnabled = false
        }
    }
    
    func updateMobilePhoneNumber(numberStr: String) {
        self.showActivityIndicator()
        
        WebServiceCall.updateMobileNumber(withNumberString: numberStr, successBlock: { (results) in
            print("BookingVC updateMobilePhoneNumber | results : \(results)")
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
            })
            
            let response = results as! [String : Any]
            
            if let status = response["status"] as? Int, status == 200 {
                self.proceedToBookSpace()
            }
            else {
                self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
            }
            
            
        }) { (errorCode, error) in
            print("BookingVC updateMobilePhoneNumber | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
                
                if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                    AuthenticationManager.logOutAuthenticatedUser()
                    DispatchQueue.main.sync(execute: {
                        self.hideActivityIndicator()
                    })
                    self.openSignInViewController()
                }
                else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    // =============================================================================================
    
    
    // MARK: - Booking
    func proceedToBookSpace() {
        
        let bookingRequest = SpaceBookingRequest()
        
        if isBookingSpaceAvailableOnHourBasis {
            
            var hours: [Int] = []
            hours.append(hoursForSelectedDateForPerHourBasis[self.selectedPerHourCells.first!].timeHour24H)
            hours.append(hoursForSelectedDateForPerHourBasis[self.selectedPerHourCells.last!].timeHour24H + 1)
            
            bookingRequest.initSpaceBookingRequestForPerHourBasisSpace(bookingSpaceId: bookingSpace.id!, eventTypeId: Int(selectedEventType.id), seatingArrangementId: Int(selectedSeatingArrangement.id), guestCount: selectedGuestCount!, bookingCharge: self.bookingCharge, bookingDate: self.selectedDate!, selectedHours: hours, selectedPerHourExtras: selectedPerHourExtraAmenities, selectedOtherExtras: selectedOtherExtraAmenities, prCode: self.appliedPromoCode)
        }
        else {
            var selectedBlocks: [SpaceCalendarTimeByBlock] = []
            
            for b in selectedBlockCells {
                selectedBlocks.append(blocksForSelectedDateForBlockBasis[b])
            }
            
            bookingRequest.initSpaceBookingRequestForBlockBasisSpace(bookingSpaceId: bookingSpace.id!, eventTypeId: Int(selectedEventType.id), seatingArrangementId: Int(selectedSeatingArrangement.id), guestCount: selectedGuestCount!, bookingCharge: self.bookingCharge, bookingDate: self.selectedDate!, selectedBlocks: selectedBlocks, selectedPerHourExtras: selectedPerHourExtraAmenities, selectedOtherExtras: selectedOtherExtraAmenities, prCode: self.appliedPromoCode)
        }
        
        print("BookingVC proceedButtonPressed | bookingRequest : \(bookingRequest.objectDictionary)")
        
        self.showActivityIndicator()
        
        WebServiceCall.bookSpace(bookingDetails: bookingRequest.objectDictionary as NSDictionary, successBlock: { (results: Any) in
            print("BookingVC proceedButtonPressed | WebServiceCall.bookSpace : results --> \(results)")
            
            DispatchQueue.main.async(execute: {
                let bookSpaceResponse = results as! [String : Any]
                
                if let error = bookSpaceResponse["error"] {
                    print("BookingVC proceedButtonPressed | WebServiceCall.bookSpace : error --> \(error)")
                    // TEMP TODO:
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.GUEST_CALENDAR_BOOKING_FAILS_ERROR_MESSAGE)
                }
                else {
                    
                    // Open New Payment Web View with dynamic payment methods
                    //self.openNEWPaymentWebScreen(bookingRequest: bookingRequest, bookingResponse: bookSpaceResponse)
                    
                    // Open Old Payment Summary screen with static payment methods
                    self.openPaymentSummaryScreen(bookingRequest: bookingRequest, bookingResponse: bookSpaceResponse)
                    
                    // NOT CROSS-CHECKING BookingCharge as Discounts are not substraced in app
                    /*
                    if self.bookingCharge == bookSpaceResponse["bookingCharge"] as! Int {
                        //print("BookingVC proceedButtonPressed | WebServiceCall.bookSpace : success")
                        
                        self.openPaymentSummaryScreen(bookingRequest: bookingRequest, bookingResponse: bookSpaceResponse)
                        
                    }
                    else {
                        // NOTE: For Testing if self.bookingCharge != bookSpaceResponse["bookingCharge"]
                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNAL_ERROR)
                    }*/
                }
                
                self.hideActivityIndicator()
            })
        }, failureBlock: { (errorCode: Int, error: String) in
            print("BookingVC proceedButtonPressed | WebServiceCall.bookSpace failed | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
                
                if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                    AuthenticationManager.logOutAuthenticatedUser()
                    //print("IPaymentGatewayVC handleIpg results : \(results)")
                    DispatchQueue.main.sync(execute: {
                        //self.webView.loadHTMLString(results as! String, baseURL: URL(string: NetworkConfiguration.IPG))
                        self.hideActivityIndicator()
                    })
                    self.openSignInViewController()
                }
                else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpaceBookingReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpaceBookingReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpaceBookingReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        })
    }
    
    //MARK: Verify Promo Code
    func applyPromoCode(code: String) {
        print("BookingCalendarVC applyPromoCode | PromoCode : \(code), bookingSpaceID : \(self.bookingSpace!.id!)")
        
        self.showActivityIndicator()
        
        WebServiceCall.verifyPromoCode(withSpaceID: self.bookingSpace!.id, promoCode: code, successBlock: { (results: Any) in
            print("BookingCalendarVC verifyPromoCode | success | result : \(results)")
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
                
                if let _results = results as? [String : Any], let status = _results["status"] as? Int {
                    if status == 1 {
                        self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.VERIFY_PROMO_CODE_MESSAGE.One)
                        
                        self.appliedPromoCode = code
                        
                        let promo = _results["promoDetails"] as! [String : Any]
                        self.isFlatDiscount = Bool(truncating: (promo["isFlatDiscount"] as! Int) as NSNumber)
                        self.promoDiscountAmount = promo["discount"] as? Int
                        
                        self.addPromoCodeButton.alpha = 0.02
                        self.addPromoCodeButton.setTitleColor(.clear, for: .normal)
                        
                        self.updateMainPriceLabels()
                    }
                    else if status == 3 {
                        self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.VERIFY_PROMO_CODE_MESSAGE.Three)
                    }
                    else {
                        self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.VERIFY_PROMO_CODE_MESSAGE.Two)
                    }
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.VERIFY_PROMO_CODE_MESSAGE.Two)
                }
            })
        })
        { (errorCode: Int, error: String) in
            print("BookingCalendarVC verifyPromoCode | failed | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.async(execute: {
                self.hideActivityIndicator()
                if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                    AuthenticationManager.logOutAuthenticatedUser()
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.USER_SESSION_EXPIRED_ERROR_MESSAGE)
                    self.openSignInViewController()
                }
                else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    // MARK: UIAlertController Alerts
    
    func addSpaceCalendarDetailsReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retrieveSpaceCalendarDetails()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func presentConfirmationAlert() {
        alertController = UIAlertController(title: AlertMessages.CONFIRM_TITLE, message: AlertMessages.CONFIRMATION_TO_PROCEED_MESSAGE, preferredStyle: .alert)
        let noActions = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
        let yesAction = UIAlertAction(title: AlertMessages.YES_ACTION, style: .default) { (alert: UIAlertAction) in
            self.proceedToBookSpace()
        }
        alertController!.addAction(noActions)
        alertController!.addAction(yesAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func addSpaceBookingReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RETRY_ACTION, style: .default) { (alert: UIAlertAction) in
            self.proceedToBookSpace()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func addContactMSForShortNoticeBookingsAlert() {
        
        let alertMessage = AlertMessages.NOTICE_PERIOD_SELECTION_WARNING_MESSAGE + "\n\nNotice Period is \(spaceCalendarInfo.noticePeriod) hour(s)."
        
        alertController = UIAlertController(title: AlertMessages.NOTICE_PERIOD_TITLE, message: alertMessage, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
            let callAction = UIAlertAction(title: AlertMessages.CALL_ACTION, style: .default) { (alert: UIAlertAction) in
                if let phoneNumUrl = URL(string: "tel://\(Constants.MS_PHONE_NUMBER)") {
                    UIApplication.shared.open(phoneNumUrl)
                }
            }
            alertController!.addAction(callAction)
        }
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func addApplyPromoCodeAlertMessage() {
        alertController = UIAlertController(title: AlertMessages.PROMO_CODE_TITLE, message: nil, preferredStyle: .alert)
        
        alertController!.addTextField(configurationHandler: { (textField: UITextField) in
            textField.placeholder = "Promo Code"
            textField.textAlignment = .center
            textField.keyboardAppearance = .dark
        })
        
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        
        let applyAction = UIAlertAction(title: AlertMessages.APPLY_ACTION, style: .default) { (alert: UIAlertAction) in
            self.applyPromoCode(code: self.alertController!.textFields!.first!.text!)
        }
        alertController!.addAction(applyAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



// MARK: - In View changes and transitions
extension BookingCalendarViewController {
    
    func initialSubViewVisibility() {
        self.timeSelectionView.isHidden = true
        self.extrasSelectionView.isHidden = true
        self.constPriceViewBottom.constant = -160.0
        
        handleTabSelection(selectedTab: 1)
    }
    
    func handleTabSelection(selectedTab: Int) {
        if selectedTab == 1 {
            self.dateTimeTabView.layer.backgroundColor = ColorUtils.MS_BLUE_COLOR.cgColor
            self.selectedDateLabel.textColor = UIColor.white
            self.selectedMonthLabel.textColor = UIColor.white
            self.selectedYearLabel.textColor = UIColor.white
            
            self.extrasTabView.layer.backgroundColor = UIColor.white.cgColor
            self.extrasLabel.textColor = UIColor.black
            
            self.dateTimeTabView.layer.shadowColor = UIColor.black.cgColor
            self.dateTimeTabView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.dateTimeTabView.layer.shadowRadius = 3
            self.dateTimeTabView.layer.shadowOpacity = 0.5
            
            self.extrasTabView.layer.shadowColor = UIColor.white.cgColor
            self.extrasTabView.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.extrasTabView.layer.shadowRadius = 0
            self.extrasTabView.layer.shadowOpacity = 0.0
        }
        else {
            self.dateTimeTabView.layer.backgroundColor = UIColor.white.cgColor
            self.selectedDateLabel.textColor = UIColor.black
            self.selectedMonthLabel.textColor = UIColor.black
            self.selectedYearLabel.textColor = UIColor.black
            
            self.extrasTabView.layer.backgroundColor = ColorUtils.MS_BLUE_COLOR.cgColor
            self.extrasLabel.textColor = UIColor.white
            
            self.dateTimeTabView.layer.shadowColor = UIColor.white.cgColor
            self.dateTimeTabView.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.dateTimeTabView.layer.shadowRadius = 0
            self.dateTimeTabView.layer.shadowOpacity = 0.0
            
            self.extrasTabView.layer.shadowColor = UIColor.black.cgColor
            self.extrasTabView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.extrasTabView.layer.shadowRadius = 3
            self.extrasTabView.layer.shadowOpacity = 0.5
        }
    }
    
    func handleTabSelectionTransition(selectedTab: Int) {
        
        self.makeTabButtonSelectionUserAccessibility(isAccessible: false)
        
        if selectedTab == 1 {
            if selectedDate == nil {
                handleExtrasSelectionViewVisibility(isHidden: true, duration: 0.3)
                handleTimeSelectionViewVisibility(isHidden: true, duration: 0.3)
                handlePriceViewVisibility(isVisible: false, duration: 0.3, delay: 0.3)
                delay(0.6, closure: {
                    self.handleCalendarViewVisibility(isHidden: false, duration: 0.3)
                })
            }
            else {
                handleExtrasSelectionViewVisibility(isHidden: true, duration: 0.3)
                handleCalendarViewVisibility(isHidden: true, duration: 0.3)
                handlePriceViewVisibility(isVisible: true, duration: 0.3, delay: 0.3)
                delay(0.6, closure: {
                    self.handleTimeSelectionViewVisibility(isHidden: false, duration: 0.3)
                })
            }
        }
        else {
            if selectedDate == nil {
                handleCalendarViewVisibility(isHidden: true, duration: 0.3)
            }else{
                handleTimeSelectionViewVisibility(isHidden: true, duration: 0.3)
            }
            handlePriceViewVisibility(isVisible: true, duration: 0.3, delay: 0.3)
            delay(0.6, closure:{
                self.handleExtrasSelectionViewVisibility(isHidden: false, duration: 0.3)
            })
        }
        
        self.delay(1.0) {
            self.makeTabButtonSelectionUserAccessibility(isAccessible: true)
        }
    }
    
    func handlePriceViewVisibility(isVisible: Bool, duration: TimeInterval, delay: TimeInterval) {
        self.constPriceViewBottom.constant = (isVisible) ? 0.0 : -160.0
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.allowUserInteraction, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func handleCalendarViewVisibility(isHidden: Bool, duration: TimeInterval) {
        viewVisibility(view: self.calendarParentView, isHidden: isHidden, duration: duration)
        self.tempCalendarCV.reloadData()
    }
    
    func handleTimeSelectionViewVisibility(isHidden: Bool, duration: TimeInterval) {
        viewVisibility(view: self.timeSelectionView, isHidden: isHidden, duration: duration)
        self.timeSelectionTableView.reloadData()
    }
    
    func handleExtrasSelectionViewVisibility(isHidden: Bool, duration: TimeInterval) {
        viewVisibility(view: self.extrasSelectionView, isHidden: isHidden, duration: duration)
        self.extrasSelectionTableView.reloadData()
    }
    
    func viewVisibility(view: UIView, isHidden: Bool, duration: TimeInterval) {
        UIView.transition(with: view, duration: duration, options: .transitionCrossDissolve, animations: {
            view.isHidden = isHidden
        }, completion: nil)
    }
    
    func makeTabButtonSelectionUserAccessibility(isAccessible: Bool) {
        self.dateTimeTabButton.isUserInteractionEnabled = isAccessible
        self.extrasTabButton.isUserInteractionEnabled = isAccessible
    }
    
}


// MARK: - Handle selection and price validations for timeSelectionTableView / extrasSelectionTableView
extension BookingCalendarViewController {
    
    // Validating selectable blocks if availability method == BLOCK
    func validateSelectableBlocks() {
        
        for i in blocksForSelectedDateForBlockBasis {
            i.isBlockAvailableUponSelection = true
        }
        
        for i in selectedBlockCells {
            for j in blocksForSelectedDateForBlockBasis {
                let blockAtI = blocksForSelectedDateForBlockBasis[i]
                let blockAtJ = j
                if blockAtI != blockAtJ {
                    if blockAtJ.hoursInBlock.contains(blockAtI.startTimeHour24H) || blockAtI.hoursInBlock.contains(blockAtJ.startTimeHour24H) {
                        blockAtJ.isBlockAvailableUponSelection = false
                    }
                }
            }
        }
        
        self.timeSelectionTableView.reloadData()
    }
    
    
    @objc func otherExtrasAmountChanged(_ amountText: UITextField) {
        
        let ip = IndexPath.init(row: Int(amountText.accessibilityIdentifier!)!, section: 0)
        let editingOtherExtrasCell = self.extrasSelectionTableView.cellForRow(at: ip)
        let extraAtEditingIndexPath = bookingSpaceExtras[Int(amountText.accessibilityIdentifier!)!] as! SpaceExtraAmenity
        
        if amountText.text!.count > 0 {
            if amountText.text == "0" {
                amountText.text = nil
            }
        }
        
        updateOtherExtraAmenityPriceInCell(otherExtraAmenityCell: editingOtherExtrasCell!, extra: extraAtEditingIndexPath, amountTextField: amountText)
    }
    
    
    @objc func otherExtrasPlusMinusButtonPressed(_ sender: UIButton) {
        
        let ip = IndexPath.init(row: Int(sender.accessibilityIdentifier!)!, section: 0)
        let editingOtherExtrasCell = self.extrasSelectionTableView.cellForRow(at: ip)
        let extraAtEditingIndexPath = bookingSpaceExtras[Int(sender.accessibilityIdentifier!)!] as! SpaceExtraAmenity
        let amountText = editingOtherExtrasCell?.viewWithTag(8) as! UITextField
        
        if sender.tag == 6 {
            if amountText.text!.count > 0 {
                if Int(amountText.text!) == 1 {
                    amountText.text = nil
                }
                else {
                    amountText.text = "\(Int(amountText.text!)! - 1)"
                }
            }
        }
        else if sender.tag == 7 {
            if amountText.text!.count > 0 {
                amountText.text = "\(Int(amountText.text!)! + 1)"
            }
            else {
                amountText.text = "1"
            }
        }
        
        updateOtherExtraAmenityPriceInCell(otherExtraAmenityCell: editingOtherExtrasCell!, extra: extraAtEditingIndexPath, amountTextField: amountText)
    }
    
    
    func updateOtherExtraAmenityPriceInCell(otherExtraAmenityCell: UITableViewCell, extra: SpaceExtraAmenity, amountTextField: UITextField) {
        
        if let cellRow = Int(amountTextField.accessibilityIdentifier!) {
            
            if amountTextField.text!.count > 0 {
                
                let amount = Int(amountTextField.text!)!
                let priceAtCell = extra.rate * amount
                
                (otherExtraAmenityCell.viewWithTag(4) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: priceAtCell))"
                
                if let oea = self.selectedOtherExtraAmenities.first(where: { $0.index == cellRow }) {
                    oea.amount = amount
                    oea.totalChargeForExtraAmenity = priceAtCell
                }
                else {
                    let oea = BookingExtraAmenity()
                    oea.initBookingExtraAmenity(withExtraAmenity: extra, type: 2, amount: amount, totalCharge: priceAtCell, index: cellRow)
                    self.selectedOtherExtraAmenities.append(oea)
                }
            }
            else {
                //print("BookingCalendarVC updateOtherExtraAmenityPriceInCell | 1 cellRow text characters == 0")
                amountTextField.text = nil
                (otherExtraAmenityCell.viewWithTag(4) as! UILabel).text = "LKR 0"
                
                if let index = self.selectedOtherExtraAmenities.index(where: { $0.index == cellRow }) {
                    self.selectedOtherExtraAmenities.remove(at: index)
                }
            }
        }
        else {
            //print("BookingCalendarVC updateOtherExtraAmenityPriceInCell | else")
            amountTextField.text = nil
        }
        
        /*
         for o in self.selectedOtherExtraAmenities {
         print("BookingCalendarVC updateOtherExtraAmenityPriceInCell | selectedOtherExtraAmenities: \(o.extraAmenity.extraAmenity.name!)")
         }*/
        
        //print("")
        
        updateMainPriceLabels()
    }
    
    
    
    func updatePerHourExtraAmenityPriceLabelAndSelectionInCell(perHourExtraAmenityCell: UITableViewCell, cellRow: Int, extra: SpaceExtraAmenity) {
        
        let totalPerHourExtraChargeForCellLabel = perHourExtraAmenityCell.viewWithTag(4) as! UILabel
        
        let ip = IndexPath.init(row: cellRow, section: 0)
        
        if self.selectedPerHourCells.count > 0 || self.selectedBlockCells.count > 0 {
            
            var chargeForPerHourExtra = 0
            
            if self.isBookingSpaceAvailableOnHourBasis {
                chargeForPerHourExtra = extra.rate * self.selectedPerHourCells.count
                totalPerHourExtraChargeForCellLabel.text = "LKR \(Utils.formatPriceValues(price: chargeForPerHourExtra))"
            }
            else {
                for i in self.selectedBlockCells {
                    //print("updatePerHourExtraAmenityPriceLabelInCell | block --> i : \(i)")
                    let blockAtIndex = self.blocksForSelectedDateForBlockBasis[i]
                    let chargeForHoursInBlock = extra.rate * blockAtIndex.hoursInBlock.count
                    chargeForPerHourExtra += chargeForHoursInBlock
                }
                totalPerHourExtraChargeForCellLabel.text = "LKR \(Utils.formatPriceValues(price: chargeForPerHourExtra))"
            }
            
            if self.selectedPerHourExtraAmenities.first(where: { $0.index == cellRow }) != nil {
                //hea.amount = chargeForPerHourExtra
                (perHourExtraAmenityCell.viewWithTag(5) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                self.extrasSelectionTableView.selectRow(at: ip, animated: false, scrollPosition: .none)
            }
            else {
                (perHourExtraAmenityCell.viewWithTag(5) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                self.extrasSelectionTableView.deselectRow(at: ip, animated: false)
            }
        }
        else {
            totalPerHourExtraChargeForCellLabel.text = "LKR 0"
            (perHourExtraAmenityCell.viewWithTag(5) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            self.extrasSelectionTableView.deselectRow(at: ip, animated: false)
            
            // TODO: Check code duplication
            if let index = self.selectedPerHourExtraAmenities.index(where: { $0.index == cellRow }) {
                self.selectedPerHourExtraAmenities.remove(at: index)
            }
        }
        
        updateMainPriceLabels()
    }
    
    
    func addRemovePerHourExtraAmenity(addRemove: String, perHourExtraAmenityCell: UITableViewCell, cellRow: Int, extra: SpaceExtraAmenity) {
        if addRemove == self.ADD_REMOVE_PER_HOUR_EXTRA.Add {
            var totalAmountOfSelectedHours = 0
            var chargeForPerHourExtra = 0
            
            if self.isBookingSpaceAvailableOnHourBasis {
                totalAmountOfSelectedHours = self.selectedPerHourCells.count
                chargeForPerHourExtra = extra.rate * self.selectedPerHourCells.count
            }
            else {
                for i in self.selectedBlockCells {
                    //print("updatePerHourExtraAmenityPriceLabelInCell | block --> i : \(i)")
                    let blockAtIndex = self.blocksForSelectedDateForBlockBasis[i]
                    totalAmountOfSelectedHours += blockAtIndex.hoursInBlock.count
                    //let chargeForHoursInBlock = extra.rate * blockAtIndex.hoursInBlock.count
                    //chargeForPerHourExtra += chargeForHoursInBlock
                }
                
                chargeForPerHourExtra = extra.rate * totalAmountOfSelectedHours
            }
            
            let hea = BookingExtraAmenity()
            hea.initBookingExtraAmenity(withExtraAmenity: extra, type: 1, amount: totalAmountOfSelectedHours, totalCharge: chargeForPerHourExtra, index: cellRow)
            self.selectedPerHourExtraAmenities.append(hea)
        }
        else {
            if let index = self.selectedPerHourExtraAmenities.index(where: { $0.index == cellRow }) {
                self.selectedPerHourExtraAmenities.remove(at: index)
            }
        }
        
        updatePerHourExtraAmenityPriceLabelAndSelectionInCell(perHourExtraAmenityCell: perHourExtraAmenityCell, cellRow: cellRow, extra: extra)
    }
    
    
    func updateExtrasAndSpacePriceOnTimeSelection() {
        for hea in self.selectedPerHourExtraAmenities {
            if self.isBookingSpaceAvailableOnHourBasis {
                hea.amount = self.selectedPerHourCells.count
                hea.totalChargeForExtraAmenity = hea.extraAmenity.rate * self.selectedPerHourCells.count
            }
            else {
                var amountOfHours = 0
                for b in self.selectedBlockCells {
                    let blockAtIndex = self.blocksForSelectedDateForBlockBasis[b]
                    amountOfHours += blockAtIndex.hoursInBlock.count
                }
                hea.amount = amountOfHours
                hea.totalChargeForExtraAmenity = hea.extraAmenity.rate * amountOfHours
            }
        }
        
        updateMainPriceLabels()
    }
    
    
    func updateMainPriceLabels() {
        
        // Space Price
        var spacePrice = 0
        
        if self.selectedPerHourCells.count > 0 || self.selectedBlockCells.count > 0 {
            
            if self.isBookingSpaceAvailableOnHourBasis {
                let hour = self.hoursForSelectedDateForPerHourBasis.first
                spacePrice = hour!.perHourPrice * self.selectedPerHourCells.count
            }
            else {
                for i in self.selectedBlockCells {
                    let blockAtIndex = self.blocksForSelectedDateForBlockBasis[i]
                    if self.spaceCalendarInfo.blockChargeType == 2 {
                        spacePrice += self.hasMinimumApplicableGuestCount ? blockAtIndex.blockPrice! * self.selectedGuestCount : blockAtIndex.blockPrice! * self.bookingSpace.minimumParticipantCount
                    }
                    else {
                        spacePrice += blockAtIndex.blockPrice
                    }
                }
            }
        }
        
        self.spacePriceLabel.text = "LKR \(Utils.formatPriceValues(price: spacePrice))"
        
        
        // Extras Total
        var extrasTotal = 0
        
        // Other Extra Amenities
        for oea in self.selectedOtherExtraAmenities {
            extrasTotal += oea.totalChargeForExtraAmenity!
        }
        
        // Per Hour Extra Amenities
        for hea in self.selectedPerHourExtraAmenities {
            extrasTotal += hea.totalChargeForExtraAmenity
        }
        
        self.extrasPriceLabel.text = "LKR \(Utils.formatPriceValues(price: extrasTotal))"
        
        // Total Price
        if let discount = self.promoDiscountAmount, let flat = self.isFlatDiscount {
            var discountedAmount = 0
            if flat {
                self.discountTitleLabel.text = "Discount"
                discountedAmount = discount > 0 ? discount : 0
            }
            else {
                self.discountTitleLabel.text = "Discount (\(discount)% off)"
                if (spacePrice + extrasTotal) != 0 {
                    let discountWithPercentage = ceil((Double(spacePrice + extrasTotal) * Double(discount)) / 100.0)
                    discountedAmount = discountWithPercentage > 0 ? Int(discountWithPercentage) : 0
                }
            }
            
            self.promoDiscountLabel.text = discountedAmount > 0 ? "- LKR \(Utils.formatPriceValues(price: discountedAmount))" : "LKR 0"
            
            if (spacePrice + extrasTotal) - discountedAmount > 0 {
                self.bookingCharge = (spacePrice + extrasTotal) - discountedAmount
            }
            else {
                self.bookingCharge = 0
            }
            
            self.totalPriceLabel.text = "LKR \(Utils.formatPriceValues(price: bookingCharge))"
        }
        else {
            self.bookingCharge = spacePrice + extrasTotal
            self.totalPriceLabel.text = "LKR \(Utils.formatPriceValues(price: bookingCharge))"
        }
        
        // Priniting prices TEMP
        //print("")
        //print("Booking Calendar updateMainPriceLabels | spacePrice : \(spacePrice)")
        //print("Booking Calendar updateMainPriceLabels | extrasTotal : \(extrasTotal)")
        //print("Booking Calendar updateMainPriceLabels | totalPrice : \(totalPrice)")
        //print("")
    }
    
    func generateAlertToSelectTimeRange() {
        presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.TIME_SELECTION_WARNING_MESSAGE)
    }
}



// MARK: - UITableView DataSource and Delegate
extension BookingCalendarViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.timeSelectionTableView {
            if isBookingSpaceAvailableOnHourBasis {
                return hoursForSelectedDateForPerHourBasis.count
            }
            else {
                return blocksForSelectedDateForBlockBasis.count
            }
        }
        else if tableView == self.extrasSelectionTableView {
            return bookingSpaceExtras.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.timeSelectionTableView {
            if self.isBookingSpaceAvailableOnHourBasis {
                return 35
            }
            else {
                if self.selectedBlockCells.contains(indexPath.row) {
                    return UITableView.automaticDimension
                }
                return 35
            }
        }
        else if tableView == self.extrasSelectionTableView {
            return 80
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.timeSelectionTableView {
            if self.isBookingSpaceAvailableOnHourBasis {
                return 35
            }
            else {
                if self.selectedBlockCells.contains(indexPath.row) {
                    return UITableView.automaticDimension
                }
                return 35
            }
        }
        else if tableView == self.extrasSelectionTableView {
            return 80
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let ip = IndexPath.init(row: indexPath.row, section: indexPath.section)
        
        if tableView == self.timeSelectionTableView {
            
            if self.isBookingSpaceAvailableOnHourBasis {
                let hourAtIndex = self.hoursForSelectedDateForPerHourBasis[indexPath.row]
                
                let hourCell = tableView.dequeueReusableCell(withIdentifier: "HourCell", for: indexPath)
                (hourCell.viewWithTag(1) as! UILabel).text = hourAtIndex.timeDisplayName!
                
                if !hourAtIndex.isHourAvailable {
                    hourCell.isUserInteractionEnabled = false
                    if indexPath.row != self.hoursForSelectedDateForPerHourBasis.count - 1 {
                        hourCell.viewWithTag(2)?.backgroundColor = UIColor.groupTableViewBackground
                    }
                    else {
                        hourCell.viewWithTag(2)?.backgroundColor = UIColor.clear
                    }
                }
                else {
                    hourCell.isUserInteractionEnabled = true
                    
                    if self.selectedPerHourCells.contains(indexPath.row) {
                        tableView.selectRow(at: ip, animated: false, scrollPosition: .none)
                        hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
                    }
                    else {
                        tableView.deselectRow(at: ip, animated: false)
                        hourCell.viewWithTag(2)?.backgroundColor = UIColor.clear
                    }
                }
                
                return hourCell
            }
            else {
                let blockAtIndex = self.blocksForSelectedDateForBlockBasis[indexPath.row]
                
                // BlockCell || BlockCellWithInfo
                let blockCell = tableView.dequeueReusableCell(withIdentifier: "BlockCellWithInfo", for: indexPath)
                (blockCell.viewWithTag(2) as! UILabel).text = "\(blockAtIndex.startTimeDisplayName12H!) - \(blockAtIndex.endTimeDisplayName12H!)" // Block time
                
                let blockRateLabel = blockCell.viewWithTag(3) as! UILabel
                let infoLabel = blockCell.viewWithTag(5) as! UILabel
                
                if self.spaceCalendarInfo.blockChargeType == 2 {
                    // Rate
                    blockRateLabel.text = self.hasMinimumApplicableGuestCount ? "LKR \(Utils.formatPriceValues(price: blockAtIndex.blockPrice! * self.selectedGuestCount))" : "LKR \(Utils.formatPriceValues(price: blockAtIndex.blockPrice! * self.bookingSpace.minimumParticipantCount))"
                    // Info
                    infoLabel.text = "Applicable - \(blockAtIndex.blockMenu!.menuName!) \nApplicable price per guest LKR \(blockAtIndex.blockPrice!)"
                }
                else {
                    // Rate
                    blockRateLabel.text = "LKR \(Utils.formatPriceValues(price: blockAtIndex.blockPrice!))"
                    // Info
                    infoLabel.text = (self.bookingSpace.hasReimbursableBlocks) ? "This amount can be reimbursed against your order bill" : "This is what you will pay to hire the space"
                }
                
                
                if blockAtIndex.isBlockAvailable {
                    if blockAtIndex.isBlockAvailableUponSelection {
                        blockCell.isUserInteractionEnabled = true
                        
                        if self.selectedBlockCells.contains(indexPath.row) {
                            tableView.selectRow(at: ip, animated: false, scrollPosition: .none)
                            blockCell.viewWithTag(1)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
                            (blockCell.viewWithTag(2) as! UILabel).textColor = UIColor.white
                            (blockCell.viewWithTag(3) as! UILabel).textColor = UIColor.white
                            (blockCell.viewWithTag(4) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                            (blockCell.viewWithTag(4) as! UIImageView).alpha = 1.0
                        }
                        else {
                            tableView.deselectRow(at: ip, animated: false)
                            blockCell.viewWithTag(1)?.backgroundColor = UIColor.white
                            (blockCell.viewWithTag(2) as! UILabel).textColor = UIColor.darkGray
                            (blockCell.viewWithTag(3) as! UILabel).textColor = UIColor.darkGray
                            (blockCell.viewWithTag(4) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                            (blockCell.viewWithTag(4) as! UIImageView).alpha = 1.0
                        }
                    }
                    else {
                        blockCell.isUserInteractionEnabled = false
                        blockCell.viewWithTag(1)?.backgroundColor = UIColor.groupTableViewBackground
                        (blockCell.viewWithTag(2) as! UILabel).textColor = UIColor.lightGray
                        (blockCell.viewWithTag(3) as! UILabel).textColor = UIColor.lightGray
                        (blockCell.viewWithTag(4) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-na")
                        (blockCell.viewWithTag(4) as! UIImageView).alpha = 0.3
                        
                    }
                }
                else {
                    blockCell.isUserInteractionEnabled = false
                    blockCell.viewWithTag(1)?.backgroundColor = UIColor.groupTableViewBackground
                    (blockCell.viewWithTag(2) as! UILabel).textColor = UIColor.lightGray
                    (blockCell.viewWithTag(3) as! UILabel).textColor = UIColor.lightGray
                    (blockCell.viewWithTag(4) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-na")
                    (blockCell.viewWithTag(4) as! UIImageView).alpha = 0.3
                }
                
                return blockCell
            }
        }
        else if tableView == self.extrasSelectionTableView {
            
            let extraAtIndex = bookingSpaceExtras[indexPath.row] as! SpaceExtraAmenity
            
            if extraAtIndex.extraAmenityUnit.name != "per hour" {
                
                let otherExtrasCell = tableView.dequeueReusableCell(withIdentifier: "OtherExtrasCell", for: indexPath)
                
                otherExtrasCell.viewWithTag(5)?.layer.borderColor = UIColor.groupTableViewBackground.cgColor
                otherExtrasCell.viewWithTag(5)?.layer.borderWidth = 1.0
                
                otherExtrasCell.viewWithTag(3)?.layer.cornerRadius = 3.0
                otherExtrasCell.viewWithTag(3)?.layer.masksToBounds = true
                
                (otherExtrasCell.viewWithTag(1) as! UIImageView).sd_setImage(with: URL(string: extraAtIndex.extraAmenity.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
                (otherExtrasCell.viewWithTag(2) as! UILabel).text = extraAtIndex.extraAmenity.name
                (otherExtrasCell.viewWithTag(3) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: extraAtIndex.rate!)) \(extraAtIndex.extraAmenityUnit.name!)"
                
                let amountTextField = otherExtrasCell.viewWithTag(8) as! UITextField
                amountTextField.accessibilityIdentifier = "\(indexPath.row)"
                amountTextField.addTarget(self, action: #selector(otherExtrasAmountChanged(_:)), for: .editingChanged)
                self.addNumberPadDoneButton(textField: amountTextField)
                
                
                if let oea = self.selectedOtherExtraAmenities.first(where: { $0.index == indexPath.row }) {
                    amountTextField.text = "\(oea.amount!)"
                    //print("has extra amount : \(oea.amount!)")
                }
                else {
                    amountTextField.text = nil
                    //print("no extra amount")
                }
                
                // Adding target to + / - buttons of other extras cell's amount value
                (otherExtrasCell.viewWithTag(6) as! UIButton).accessibilityIdentifier = "\(indexPath.row)"
                (otherExtrasCell.viewWithTag(6) as! UIButton).addTarget(self, action: #selector(otherExtrasPlusMinusButtonPressed(_:)), for: .touchUpInside)
                
                (otherExtrasCell.viewWithTag(7) as! UIButton).accessibilityIdentifier = "\(indexPath.row)"
                (otherExtrasCell.viewWithTag(7) as! UIButton).addTarget(self, action: #selector(otherExtrasPlusMinusButtonPressed(_:)), for: .touchUpInside)
                
                updateOtherExtraAmenityPriceInCell(otherExtraAmenityCell: otherExtrasCell, extra: extraAtIndex, amountTextField: amountTextField)
                
                return otherExtrasCell
            }
            else {
                let hourlyExtrasCell = tableView.dequeueReusableCell(withIdentifier: "HourlyExtrasCell", for: indexPath)
                
                hourlyExtrasCell.viewWithTag(3)?.layer.cornerRadius = 3.0
                hourlyExtrasCell.viewWithTag(3)?.layer.masksToBounds = true
                
                (hourlyExtrasCell.viewWithTag(1) as! UIImageView).sd_setImage(with: URL(string: extraAtIndex.extraAmenity.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
                (hourlyExtrasCell.viewWithTag(2) as! UILabel).text = extraAtIndex.extraAmenity.name
                (hourlyExtrasCell.viewWithTag(3) as! UILabel).text = "LKR \(Utils.formatPriceValues(price:extraAtIndex.rate!)) \(extraAtIndex.extraAmenityUnit.name!)"
                
                updatePerHourExtraAmenityPriceLabelAndSelectionInCell(perHourExtraAmenityCell: hourlyExtrasCell, cellRow: indexPath.row, extra: extraAtIndex)
                
                return hourlyExtrasCell
            }
        }
        
        // else
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.timeSelectionTableView {
            if self.isBookingSpaceAvailableOnHourBasis {
                
                let hourAtIndex = self.hoursForSelectedDateForPerHourBasis[indexPath.row]
                
                if hourAtIndex.isHourInNoticePeriod {
                    self.addContactMSForShortNoticeBookingsAlert()
                    tableView.deselectRow(at: indexPath, animated: false)
                }
                else {
                    if self.selectedPerHourCells.count > 0 {
                        if (indexPath.row == self.selectedPerHourCells.first! - 1) || (indexPath.row == self.selectedPerHourCells.last! + 1) {
                            //print("timeSelectionTableView didSelectRowAt | correct selection")
                            self.selectedPerHourCells.append(indexPath.row)
                            self.selectedPerHourCells.sort()
                            let hourCell = tableView.cellForRow(at: indexPath)!
                            hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
                        }
                        else {
                            //print("timeSelectionTableView didSelectRowAt | wrong selection")
                            for i in self.selectedPerHourCells {
                                let ip = IndexPath.init(row: i, section: indexPath.section)
                                tableView.deselectRow(at: ip, animated: false)
                                if let hourCellToBeDeselected = tableView.cellForRow(at: ip) {
                                    hourCellToBeDeselected.viewWithTag(2)?.backgroundColor = UIColor.clear
                                }
                            }
                            self.selectedPerHourCells.removeAll()
                            
                            let hourCell = tableView.cellForRow(at: indexPath)!
                            hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
                            self.selectedPerHourCells.append(indexPath.row)
                        }
                    }
                    else {
                        self.selectedPerHourCells.append(indexPath.row)
                        let hourCell = tableView.cellForRow(at: indexPath)!
                        hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
                    }
                }
            }
            else {
                let blockAtIndex = self.blocksForSelectedDateForBlockBasis[indexPath.row]
                
                if blockAtIndex.isBlockInNoticePeriod {
                    self.addContactMSForShortNoticeBookingsAlert()
                    tableView.deselectRow(at: indexPath, animated: false)
                }
                else {
                    
                    if !self.selectedBlockCells.contains(indexPath.row) {
                        self.selectedBlockCells.append(indexPath.row)
                        self.selectedBlockCells.sort()
                        
                        let selectedBlockCell = tableView.cellForRow(at: indexPath)!
                        selectedBlockCell.viewWithTag(1)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
                        (selectedBlockCell.viewWithTag(2) as! UILabel).textColor = UIColor.white
                        (selectedBlockCell.viewWithTag(3) as! UILabel).textColor = UIColor.white
                        (selectedBlockCell.viewWithTag(4) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                        
                        tableView.beginUpdates()
                        tableView.endUpdates()
                        
                        self.delay(0.2, closure: {
                            self.validateSelectableBlocks()
                        })
                    }
                }
            }
            
            self.updateExtrasAndSpacePriceOnTimeSelection()
            
        }
        else if tableView == extrasSelectionTableView {
            //print("timeSelectionTableView didSelectRowAt")
            let selectedExtrasCell = tableView.cellForRow(at: indexPath)!
            if selectedExtrasCell.reuseIdentifier == "HourlyExtrasCell" {
                if self.selectedPerHourCells.count > 0 || self.selectedBlockCells.count > 0 {
                    let perHourExtraAtIndex = bookingSpaceExtras[indexPath.row] as! SpaceExtraAmenity
                    addRemovePerHourExtraAmenity(addRemove: self.ADD_REMOVE_PER_HOUR_EXTRA.Add, perHourExtraAmenityCell: selectedExtrasCell, cellRow: indexPath.row, extra: perHourExtraAtIndex)
                }
                else {
                    self.generateAlertToSelectTimeRange()
                    tableView.deselectRow(at: indexPath, animated: false)
                }
            }
            else {
                tableView.deselectRow(at: indexPath, animated: false)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView == self.timeSelectionTableView {
            if self.isBookingSpaceAvailableOnHourBasis {
                
                if (indexPath.row == self.selectedPerHourCells.first) || (indexPath.row == self.selectedPerHourCells.last) {
                    _ = (indexPath.row == self.selectedPerHourCells.first) ? self.selectedPerHourCells.removeFirst() : self.selectedPerHourCells.removeLast()
                    let hourCell = tableView.cellForRow(at: indexPath)!
                    hourCell.viewWithTag(2)?.backgroundColor = UIColor.clear
                }
                else if self.selectedPerHourCells.contains(indexPath.row) {
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
                else {
                    //print("timeSelectionTableView didDeselectRowAt | wrong deselect")
                }
            }
            else {
                if let indexOfSelectedBlock = self.selectedBlockCells.index(of: indexPath.row) {
                    self.selectedBlockCells.remove(at: indexOfSelectedBlock)
                    
                    let blockCell = tableView.cellForRow(at: indexPath)!
                    blockCell.viewWithTag(1)?.backgroundColor = UIColor.white
                    (blockCell.viewWithTag(2) as! UILabel).textColor = UIColor.darkGray
                    (blockCell.viewWithTag(3) as! UILabel).textColor = UIColor.darkGray
                    (blockCell.viewWithTag(4) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                    
                    
                    tableView.beginUpdates()
                    tableView.endUpdates()
                    
                    self.delay(0.2, closure: {
                        self.validateSelectableBlocks()
                    })
                }
            }
            
            self.updateExtrasAndSpacePriceOnTimeSelection()
            
        }
        else if tableView == extrasSelectionTableView {
            let selectedExtrasCell = tableView.cellForRow(at: indexPath)!
            if selectedExtrasCell.reuseIdentifier == "HourlyExtrasCell" {
                let perHourExtraAtIndex = bookingSpaceExtras[indexPath.row] as! SpaceExtraAmenity
                addRemovePerHourExtraAmenity(addRemove: self.ADD_REMOVE_PER_HOUR_EXTRA.Remove, perHourExtraAmenityCell: selectedExtrasCell, cellRow: indexPath.row, extra: perHourExtraAtIndex)
            }
        }
    }
    
}


extension BookingCalendarViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return self.spaceCalendarMonths.count
        }
        else {
            let datesArrayOfMonthAtIndex = self.spaceCalendarDatesDic[self.spaceCalendarMonths[collectionView.tag-1]] as! NSMutableArray
            let numOfDaysOfWeekBeforeFirstDayOfMonth = (datesArrayOfMonthAtIndex[0] as! SpaceCalendarDate).dayOfWeekMS! - 1
            return datesArrayOfMonthAtIndex.count + numOfDaysOfWeekBeforeFirstDayOfMonth
            //tempDaysAndEmpty[collectionView.tag-1][0] + tempDaysAndEmpty[collectionView.tag-1][1]
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 0 { // Parent Collection View
            collectionView.isPagingEnabled = true
            let cvEmbeddedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewEmbeddedCell", for: indexPath) as! CollectionViewEmbeddedCollectionViewCell
            cvEmbeddedCell.setEmbeddedCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.item+1)
            return cvEmbeddedCell
        }
        else { // Embedded Collection Views in Parent Collection View Cells
            
            let datesArrayOfMonthAtIndex = self.spaceCalendarDatesDic[self.spaceCalendarMonths[collectionView.tag-1]] as! NSMutableArray
            let numOfDaysOfWeekBeforeFirstDayOfMonth = (datesArrayOfMonthAtIndex[0] as! SpaceCalendarDate).dayOfWeekMS! - 1
            
            if indexPath.item < numOfDaysOfWeekBeforeFirstDayOfMonth {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyDateCell", for: indexPath)
                cell.isUserInteractionEnabled = false
                return cell
            }
            else {
                let calendarDateAtItem = datesArrayOfMonthAtIndex[indexPath.item - numOfDaysOfWeekBeforeFirstDayOfMonth] as! SpaceCalendarDate
                
                if calendarDateAtItem.dateStatus == StaticData.SPACE_CALENDAR_DATE_STATUS[1] {
                    // Unavailable Date - Past or fully booked
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UnavailableDateCell", for: indexPath)
                    cell.isUserInteractionEnabled = false
                    (cell.viewWithTag(1) as! UILabel).text = "\(calendarDateAtItem.day!)"
                    (cell.viewWithTag(1) as! UILabel).font = UIFont(name: "Helvetica Neue", size: calendarFontSize!)
                    return cell
                }
                else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvailableDateCell", for: indexPath)
                    
                    (cell.viewWithTag(1) as! UILabel).text = "\(calendarDateAtItem.day!)"
                    (cell.viewWithTag(1) as! UILabel).font = UIFont(name: "Helvetica Neue", size: calendarFontSize!)
                    
                    // Blue-Dot to indicate whether date is partially booked or not booked at all
                    (cell.viewWithTag(2))?.layer.cornerRadius = 2.5
                    (cell.viewWithTag(2))?.layer.masksToBounds = true
                    
                    // condition
                    if calendarDateAtItem.dateStatus == StaticData.SPACE_CALENDAR_DATE_STATUS[3] {
                        cell.viewWithTag(2)?.isHidden = true
                    }
                    else {
                        cell.viewWithTag(2)?.isHidden = false
                    }
                    
                    if selectedDateVCTag != nil, selectedDateVCItemIndexPath != nil {
                        if collectionView.tag == selectedDateVCTag && indexPath.item == selectedDateVCItemIndexPath {
                            cell.backgroundColor = ColorUtils.MS_BLUE_COLOR
                            (cell.viewWithTag(1) as! UILabel).textColor = UIColor.white
                            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .bottom)
                        }
                        else {
                            cell.backgroundColor = UIColor.white
                            (cell.viewWithTag(1) as! UILabel).textColor = UIColor.black
                        }
                    }
                    else {
                        cell.backgroundColor = UIColor.white
                        (cell.viewWithTag(1) as! UILabel).textColor = UIColor.black
                    }
                    
                    return cell
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print("collectionView : \(collectionView.tag) | didSelectItemAt : \(indexPath.item)")
        
        if collectionView.tag != 0 {
            let selectedCell = collectionView.cellForItem(at: indexPath)
            if selectedCell?.reuseIdentifier == "AvailableDateCell" {
                
                selectedCell!.backgroundColor = ColorUtils.MS_BLUE_COLOR
                (selectedCell!.viewWithTag(1) as! UILabel).textColor = UIColor.white
                self.selectedDateLabel.text = (selectedCell?.viewWithTag(1) as! UILabel).text
                
                let datesArrayOfMonthAtIndex = self.spaceCalendarDatesDic[self.spaceCalendarMonths[collectionView.tag-1]] as! NSMutableArray
                let numOfDaysOfWeekBeforeFirstDayOfMonth = (datesArrayOfMonthAtIndex[0] as! SpaceCalendarDate).dayOfWeekMS! - 1
                selectedDate = datesArrayOfMonthAtIndex[indexPath.item-numOfDaysOfWeekBeforeFirstDayOfMonth] as? SpaceCalendarDate
                
                //print("collectionView didSelectItem | selectedDate : \(selectedDate!.date!)")
                if isBookingSpaceAvailableOnHourBasis {
                    self.hoursForSelectedDateForPerHourBasis = selectedDate!.calendarTime!.hoursForCalendarDate
                }
                else {
                    self.blocksForSelectedDateForBlockBasis = selectedDate!.calendarTime!.blocksForCalendarDate
                }
                
                selectedDateVCTag = collectionView.tag
                selectedDateVCItemIndexPath = indexPath.item
                
                handleTabSelectionTransition(selectedTab: 1)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //print("collectionView : \(collectionView.tag) | didDeselectItemAt : \(indexPath.item)")
        
        if collectionView.tag != 0 {
            let delectedCell = collectionView.cellForItem(at: indexPath)
            if delectedCell?.reuseIdentifier == "AvailableDateCell" {
                delectedCell?.backgroundColor = UIColor.white
                (delectedCell?.viewWithTag(1) as! UILabel).textColor = UIColor.black
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            return CGSize(width: self.CALENDAR_WIDTH_HEIGHT, height: self.CALENDAR_WIDTH_HEIGHT)
        }
        else {
            return CGSize(width: self.CALENDAR_DATE_CELL_WIDTH_HEIGHT, height: self.CALENDAR_DATE_CELL_WIDTH_HEIGHT)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}


extension BookingCalendarViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tempCalendarCV {
            let visibleRect = CGRect(origin: self.tempCalendarCV.contentOffset, size: self.tempCalendarCV.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            if let indexPath = self.tempCalendarCV.indexPathForItem(at: visiblePoint) {
                setMonthAndYearLabelTexts(forMonthAndYear: spaceCalendarMonths[indexPath.item])
                currentlyDisplayedMonthIndexPath = indexPath
            }
        }
    }
    
}

