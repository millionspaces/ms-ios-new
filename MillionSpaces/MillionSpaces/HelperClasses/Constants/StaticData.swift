//
//  StaticData.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/2/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class StaticData: NSObject {
    
    static var APP_VERSION: String!
    
    static let HOME_MENU_OPTIONS_ARRAY: NSMutableArray = []
    static let HOME_MENU_OPTIONS_ARRAY_FOR_GUEST: NSMutableArray = []
    
    // Space Filtration and Sorting
    static let FILTER_SORT_OPTIONS = (D: "distance", PLf: "priceLowestFirst", PHf: "priceHighestFirst")
    static var FILTER_PRICE_OPTIONS = [[String : Any]]()
    static var FILTER_CAPACITY_OPTIONS = [[String : Any]]()
    
    static let HOUR_ARRAY = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    static let AMPM_ARRAY = ["AM", "PM"]
    
    static let SPACE_TYPES = [1 : "Indoor", 2 : "Outdoor", 3 : "Indoor & Outdoor"]
    
    static private let MS_DAYS_OF_WEEK_ARRAY = [1 : "Monday", 2 : "Tuesday", 3 : "Wednesday", 4 : "Thursday", 5 : "Friday", 6  : "Saturday", 7 : "Sunday", 8 : "Weekday"]
    
    static let SPACE_CALENDAR_DATE_STATUS = [1 : "UNAVAILABLE", 2 : "PARTIALLY-AVAILABLE", 3 : "AVAILABLE"]
    static let HOST_CALENDAR_DATE_STATUS = [1 : "UNAVAILABLE", 2 : "PARTIALLY-AVAILABLE", 3 : "AVAILABLE", 4 : "OUT-OF-CALENDAR"]
    
    static let RESERVATION_STATUS_TRUE = [1 : "Initiated", 2 : "Pending Payment", 3 : "Payment Done", 4 : "Cancelled", 5 : "Confirmed", 6 : "Expired", 7 : "Discarded"]
    static let RESERVATION_STATUS = [1 : "Pending Payment", 2 : "Pending Payment", 3 : "Confirmed", 4 : "Cancelled", 5 : "Confirmed", 6 : "Expired", 7 : "Expired"]
    
    static let MANUAL_BOOKING_RESERVATION_STATUS = [2 : ["id" : "2", "name" : "PENDING_PAYMENT", "displayName" : "Pending Payment"], 4 : ["id" : "4", "name" : "CANCELLED", "displayName" : "Cancelled"], 5 : ["id" : "5", "name" : "CONFIRMED", "displayName" : "Confirmed"], 6 : ["id" : "6", "name" : "EXPIRED", "displayName" : "Expired"]]
    
    static let MANUAL_BOOKING_RESERVATION_STATUS_2 = [2 : "Pending Payment", 4 : "Cancelled", 5 : "Confirmed", 6 : "Expired"]
    
    static let REVIEWABLE_BOOKING_RESERVATION_STATUS_RANGE = [3, 5]
    static let CANCELABLE_BOOKING_RESERVATION_STATUS_RANGE = [1, 2, 3, 5]
    
    // Cancel---->  1 2 3 5 Cancel
    // Review---->  3 5  + isReviewed == false
    
    public static func setUp() {
        setHomeMenuOptions()
        setHomeMenuOptionsForGuest()
        retrieveAppVersion()
        setStaticFilterOptions()
    }
    
    
    private static func retrieveAppVersion() {
        var versionNum: String!
        
        if NetworkConfiguration.API_ENVIRONMENT_DISPLAY_NAME == "Live" {
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                versionNum = version
            }
            
            APP_VERSION = "MillionSpaces v\(versionNum!)"
        }
        else {
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                versionNum = version
            }
            
            if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                versionNum.append(" - (\(build))")
            }
            
            APP_VERSION = "MillionSpaces v\(versionNum!) - \(NetworkConfiguration.API_ENVIRONMENT_DISPLAY_NAME)"
        }
    }
    
    private static func setHomeMenuOptions() {
        
        let mySpaces: NSDictionary = ["index" : 2, "name" : "My Spaces", "image" : #imageLiteral(resourceName: "ico-myspaces")]
        HOME_MENU_OPTIONS_ARRAY.add(mySpaces)
        
        let myActivities: NSDictionary = ["index" : 3, "name" : "My Activities", "image" : #imageLiteral(resourceName: "ico-availability")]
        HOME_MENU_OPTIONS_ARRAY.add(myActivities)
        
        let logOut: NSDictionary = ["index" : 4, "name" : "Log Out", "image" : #imageLiteral(resourceName: "ico-logout")]
        HOME_MENU_OPTIONS_ARRAY.add(logOut)
    }
    
    private static func setHomeMenuOptionsForGuest() {
        
        let logOut: NSDictionary = ["index" : 4, "name" : "Log Out", "image" : #imageLiteral(resourceName: "ico-logout")]
        HOME_MENU_OPTIONS_ARRAY_FOR_GUEST.add(logOut)
    }
    
    
    private static func setStaticFilterOptions() {
        FILTER_PRICE_OPTIONS.append(["id":0, "displayValue":"Below LKR 5,000", "value":[1,5000]])
        FILTER_PRICE_OPTIONS.append(["id":1, "displayValue":"LKR 5,000 - LKR 10,000", "value":[5000,10000]])
        FILTER_PRICE_OPTIONS.append(["id":2, "displayValue":"LKR 10,000 - LKR 15,000", "value":[10000,15000]])
        FILTER_PRICE_OPTIONS.append(["id":3, "displayValue":"Above LKR 15,000", "value":[15000,100000]])
        
        FILTER_CAPACITY_OPTIONS.append(["id":0, "displayValue":"1 - 10", "value":[1,10]])
        FILTER_CAPACITY_OPTIONS.append(["id":1, "displayValue":"10 - 50", "value":[10,50]])
        FILTER_CAPACITY_OPTIONS.append(["id":2, "displayValue":"50 - 100", "value":[50,100]])
        FILTER_CAPACITY_OPTIONS.append(["id":3, "displayValue":"100 - 250", "value":[100,250]])
        FILTER_CAPACITY_OPTIONS.append(["id":4, "displayValue":"250 - 500", "value":[250,500]])
        FILTER_CAPACITY_OPTIONS.append(["id":5, "displayValue":"Above 500", "value":[500,5000]])
    }
    
    public static func getDayOfWeek(dayIndex: Int) -> String {
        return MS_DAYS_OF_WEEK_ARRAY[dayIndex]!
    }
    
    
    
    
    
    
    
    
    
    /*
     Reservation Statuses
     1    INITIATED    Initiated
     2    PENDING_PAYMENT    Pending Payment
     3    PAYMENT_DONE    Confirmed
     4    CANCELLED    Cancelled
     5    CONFIRMED    Confirmed
     6    EXPIRED    Expired
     7    DISCARDED    Discarded*/
    
}
