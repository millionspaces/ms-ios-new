//
//  PaymentViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class PaymentViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var cardOptionButton: UIButton!
    @IBOutlet weak var onlineTransferOptionButton: UIButton!
    @IBOutlet weak var bankDepositOptionButton: UIButton!
    
    @IBOutlet weak var manualOptionOneLabel: UILabel!
    @IBOutlet weak var manualOptionOneWarningLabel: UILabel!
    @IBOutlet weak var manualOptionTwoLabel: UILabel!
    @IBOutlet weak var manualOptionTwoWarningLabel: UILabel!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    // TEMP
    @IBOutlet weak var cardOptionView: UIView!
    @IBOutlet weak var manualOptionOneView: UIView!
    @IBOutlet weak var manualOptionTwoView: UIView!
    @IBOutlet weak var visaIconImage: UIImageView!
    @IBOutlet weak var mastercardIconImage: UIImageView!
    
    let DARK_GRAY_INACTIVE_COLOR = UIColor.init(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 0.1)
    
    var alertController: UIAlertController?
    
    // MARK: - Class constants/variables
    var bookingSpace: Space!
    var bookingRequest: SpaceBookingRequest!
    var bookingResponse: [String : Any]!
    
    var referenceIdStr: String!
    var spaceNameStr: String!
    var locationStr: String!
    var dateStr: String!
    var reservationTimeStr: String!
    var subTotalStr: String!
    var selectedOption: String! // SegueIDs are assigned
    
    var areManualPaymentOptionsDisabled = false
    var manualPaymentDisabledAlertMessage: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("PaymentViewController viewDidLoad")
        self.view.layoutIfNeeded()
        
        PaymentSessionTimer.sharedTimer.startPaymentSessionTimer()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addPaymentSessionTimerLabelToNavBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePaymentSessionTimerInProgress(_:)), name: NSNotification.Name(rawValue: Constants.PAYMEMT_SESSION_IN_PROGRESS), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePaymentSessionTimerExpiry), name: NSNotification.Name(rawValue: Constants.PAYMEMT_SESSION_IS_UP), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.popToHomeWhenAppGoesForeground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        /*if let timerIsValid = PaymentSessionTimer.sharedTimer.internalTimer?.isValid, !timerIsValid {
         PaymentSessionTimer.sharedTimer.resumePaymentSessionTimer()
         }*/
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.removePaymentSessionTimerLabelFromNavBar()
        //PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func popToHomeWhenAppGoesForeground() {
        alertController?.dismiss(animated: false, completion: nil)
        self.popToRootViewController()
        // TODO: Invalidate Payment Timer
    }
    
    func configureView() {
        
        self.navigationItem.title = "Payment Summary"
        
        // Default selected Payment Option
        self.cardOptionButton.isSelected = true
        selectedOption = "IpgSegue"
        self.manualOptionOneWarningLabel.isHidden = true
        self.manualOptionTwoWarningLabel.isHidden = true
        
        // Show Summary
        self.referenceIdStr = (bookingResponse["orderId"] as! String)
        
        self.spaceNameStr = bookingSpace.name!
        
        self.locationStr = bookingSpace.fullAdrress!
        
        self.dateStr = bookingRequest.bookingDate.simpleDate_2!
        
        self.reservationTimeStr = ""
        if bookingRequest.bookingTimes.count == 1 {
            self.reservationTimeStr = bookingRequest.bookingTimes[0]
        }
        else {
            for t in bookingRequest.bookingTimes {
                self.reservationTimeStr.append("\(t) \n")
            }
            self.reservationTimeStr = String(self.reservationTimeStr.dropLast(1))
        }
        
        self.subTotalStr = "LKR \(Utils.formatPriceValues(price: bookingRequest.bookingCharge!))"
        
        validatedManualPaymentOptionsAvailability()
        
        // TEMP
        //validateForInfinityWarPromo()
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 50.0
        self.tableView.reloadData()
    }
    
    
    func validatedManualPaymentOptionsAvailability() {
        if !(self.bookingResponse["isUserEligibleForManualPayment"] as! Bool) {
            //print("isUserEligibleForManualPayment == 0")
            self.areManualPaymentOptionsDisabled = true
            self.manualPaymentDisabledAlertMessage = AlertMessages.PAYMENT_OPTIONS_MANUAL_OPTIONS_DISABLED_2
        }
        else if !(self.bookingResponse["isBookingTimeEligibleForManualPayment"] as! Bool) {
            //print("isBookingTimeEligibleForManualPayment == 0")
            self.areManualPaymentOptionsDisabled = true
            self.manualPaymentDisabledAlertMessage = AlertMessages.PAYMENT_OPTIONS_MANUAL_OPTIONS_DISABLED_1
        }
    }
    
    @objc func handlePaymentSessionTimerInProgress(_ notification: Notification) {
        if let timerValue = notification.userInfo?["timer"] {
            print("PaymentVC handlePaymentSessionTimerInProgress | timerValue : \(timerValue)")
            self.setPaymentSessionTimerLabelText(text: timerValue as! String)
        }
    }
    
    @objc func handlePaymentSessionTimerExpiry() {
        alertController = UIAlertController(title: AlertMessages.ALERT_TITLE, message: AlertMessages.PAYMENT_SESSION_TIME_UP, preferredStyle: .alert)
        let returnHomeAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
            self.popToRootViewController()
        }
        alertController!.addAction(returnHomeAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
        self.popViewController()
    }
    
    
    @IBAction func paymentOptionPressed(_ sender: UIButton) {
        if sender.tag == 1 {
            print("PaymentVC paymentOptionPressed - 1")
            selectedOption = "IpgSegue"
            self.cardOptionButton.isSelected = true
            self.onlineTransferOptionButton.isSelected = false
            self.bankDepositOptionButton.isSelected = false
            self.manualOptionOneWarningLabel.isHidden = true
            self.manualOptionTwoWarningLabel.isHidden = true
        }
        else if sender.tag == 2 {
            if areManualPaymentOptionsDisabled {
                self.presentDismissableAlertWithMessage(title: AlertMessages.MANUAL_PAYMENT_OPTIONS_DISABLED_TITLE, message: self.manualPaymentDisabledAlertMessage!)
            }
            else {
                print("PaymentVC paymentOptionPressed - 2")
                selectedOption = "ManualPaymentInfoSegue"
                self.cardOptionButton.isSelected = false
                self.onlineTransferOptionButton.isSelected = true
                self.bankDepositOptionButton.isSelected = false
                self.manualOptionOneWarningLabel.isHidden = false
                self.manualOptionTwoWarningLabel.isHidden = true
            }
        }
        else if sender.tag == 3 {
            if areManualPaymentOptionsDisabled {
                self.presentDismissableAlertWithMessage(title: AlertMessages.MANUAL_PAYMENT_OPTIONS_DISABLED_TITLE, message: self.manualPaymentDisabledAlertMessage!)
            }
            else {
                print("PaymentVC paymentOptionPressed - 3")
                selectedOption = "ManualPaymentInfoSegue"
                self.cardOptionButton.isSelected = false
                self.onlineTransferOptionButton.isSelected = false
                self.bankDepositOptionButton.isSelected = true
                self.manualOptionOneWarningLabel.isHidden = true
                self.manualOptionTwoWarningLabel.isHidden = false
            }
        }
            /*
        else if sender.tag == 4 {
            print("PaymentVC paymentOptionPressed - 4")
            selectedOption = "InfinityWarPromoSegue"
            self.cardOptionButton.isSelected = false
            self.onlineTransferOptionButton.isSelected = false
            self.bankDepositOptionButton.isSelected = false
            self.manualOptionOneWarningLabel.isHidden = true
            self.manualOptionTwoWarningLabel.isHidden = true
            self.infinityWarPromoOption.isSelected = true
        }*/
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        print("PaymentVC nextButtonPressed")
        
        performSegue(segueID: selectedOption)
        /*
         if self.selectedOption != "InfinityWarPromoSegue" {
        }
        else {
            confirmInfinityWarPromoEntry()
        }*/
    }
    
    /*
    // MARK: - TEMP Infinity War Promo
    func validateForInfinityWarPromo() {
        if self.bookingRequest.bookingCharge == 0 {
            self.infinityWarPromoOptionView.isHidden = false
            
            self.cardOptionButton.isUserInteractionEnabled = false
            self.cardOptionButton.alpha = 0.3
            self.visaIconImage.alpha = 0.3
            self.mastercardIconImage.alpha = 0.3
            self.onlineTransferOptionButton.isUserInteractionEnabled = false
            self.onlineTransferOptionButton.alpha = 0.3
            self.manualOptionOneLabel.alpha = 0.3
            self.bankDepositOptionButton.isUserInteractionEnabled = false
            self.bankDepositOptionButton.alpha = 0.3
            self.manualOptionTwoLabel.alpha = 0.3
            
            self.selectedOption = "InfinityWarPromoSegue"
            self.infinityWarPromoOption.isSelected = true
            self.cardOptionButton.isSelected = false
        }
        else {
            self.infinityWarPromoOptionView.isHidden = true
        }
    }
    
    func confirmInfinityWarPromoEntry() {
        self.showActivityIndicator()
        
        WebServiceCall.makeTentativeReservation(withBookingID: bookingResponse["id"] as! Int, status: 4, successBlock: { (results: Any) in
            print("PaymentSummaryVC confirmInfinityWarPromoEntry | results : \(results)")
            
            if let responseDic = results as? [String : Any] {
                if !(responseDic["message"] as! String).contains("failure") {
                    DispatchQueue.main.sync(execute: {
                        self.performSegue(segueID: self.selectedOption)
                    })
                }
            }
            
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("PaymentSummaryVC confirmInfinityWarPromoEntry | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }*/
    
    // =======================
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "IpgSegue" {
            if let ipgvc = segue.destination as? IPaymentGatewayViewController {
                print("bookingResponse : \(String(describing: bookingResponse))")
                ipgvc.orderId = (bookingResponse["id"] as! Int)
                ipgvc.bookingId = (bookingResponse["orderId"] as! String) //(bookingResponse["id"] as! Int)
                ipgvc.chargeAmount = bookingRequest.bookingCharge!
            }
        }
        else if segue.identifier == "ManualPaymentInfoSegue" {
            if let manualVC = segue.destination as? ManualPaymentInfoViewController {
                manualVC.chargeAmountStr = self.subTotalStr!
                manualVC.orderId = (bookingResponse["id"] as! Int)
                manualVC.bookingId = self.referenceIdStr!
            }
        }
    }
}


extension PaymentViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.viewWithTag(0)?.backgroundColor = (indexPath.row % 2 == 0) ? UIColor.groupTableViewBackground : UIColor.white
        
        let label = cell.viewWithTag(1) as! UILabel
        let valueLabel = cell.viewWithTag(2) as! UILabel
        
        if indexPath.row == 0 {
            label.text = "Reference ID"
            valueLabel.text = self.referenceIdStr
        }
        else if indexPath.row == 1 {
            label.text = "Space"
            valueLabel.text = self.spaceNameStr
        }
        else if indexPath.row == 2 {
            label.text = "Location"
            valueLabel.text = self.locationStr
        }
        else if indexPath.row == 3 {
            label.text = "Date"
            valueLabel.text = self.dateStr
        }
        else if indexPath.row == 4 {
            label.text = "Reservation Time"
            valueLabel.text = self.reservationTimeStr
        }
        else if indexPath.row == 5 {
            label.text = "Sub Total"
            valueLabel.text = self.subTotalStr
            label.font = UIFont.init(name: "HelveticaNeue-Bold", size: 16.0)
            valueLabel.font = UIFont.init(name: "HelveticaNeue-Medium", size: 16.0)
        }
        
        return cell
    }
}
