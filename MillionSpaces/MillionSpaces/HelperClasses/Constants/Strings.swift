//
//  Strings.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/2/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class Strings: NSObject {

    // NOTE: - The class contains constant properties with values that are intended for displaying elements such as labels, placeholders, etc.
    
    
    // MARK: - Navigation Bar Titles
    static let MS_TITLE = "MillionSpaces"
    static let FILTER_VC_TITLE = "Filter Categories"
    static let BOOKING_ENGINE = "Booking Engine"
    
    
    // MARK: - Label Values
    static let SIGN_IN_UP = "Sign In / Sign Up"
    static let MAIN_EVENT_TYPE_MEETINGS = "MEETINGS"
    static let MAIN_EVENT_TYPE_PARTY = "PARTY"
    static let MAIN_EVENT_TYPE_PHOTOSHOOTS = "PHOTOSHOOTS"
    static let MAIN_EVENT_TYPE_WEDDINGS = "WEDDINGS"
    static let MAIN_EVENT_TYPE_INTERVIEWS = "INTERVIEWS"
    static let EVENT_TYPE = "Event Type"
    static let SPACE_TYPE = "Space Type"
    static let LOCATION = "Location"
    static let DATE_AND_TIME = "Date & Time"
    static let SPACE_CAPACITY = "Space Capacity"
    static let STARTING_PRICE = "Starting Price"
    static let AMENITIES = "Amenities"
    static let SPACE_RULES = "Space Rules"
    static let SEATING_ARRANGEMENTS = "Seating Arrangements"
    static let MENU_OPTIONS = "Menu Options"
    static let COMPLIMENTARY = "Complimentary"
    static let CHARGEABLE = "Chargeable"
    static let SELECT_EVENT_TYPE = "Select Event Type"
    static let SELECT_SEATING_ARRANGEMENTS = "Select Seating Arrangement"
    static let PER_HOUR = "PER HOUR"
    static let PER_BLOCK = "PER BLOCK"
    static let DEFAULT_DATE = "DD"
    static let DEFAULT_MONTH = "MMMM"
    static let DEFAULT_YEAR = "YYYY"
    
    
    // MARK: - Placeholders
    static let EMAIL = "Email"
    static let PASSWORD = "Password"
    static let NAME = "Name"
    static let SEARCH = "Search"
    
    
    // MARK: - MillionSpaces Details
    static let MS_PHONE_NUMBER_LABEL = "011 7 811 811"
    
    // MARK: - MillionSpaces Bank Account Details
    static let MS_BANK_ACCOUNT_HOLDER_NAME = "MillionSpaces (Pvt) Ltd"
    static let MS_BANK_ACCOUNT_NUMBER = "002010566163"
    static let MS_BANK_NAME = "Hatton National Bank"
    static let MS_BANK_BRANCH_NAME = "City Office"
    static let MS_BANK_ACCOUNT_SWIFT_CODE = "HBLILKLX"
    
    
    // MARK: - Space Details Charge Type Description
    static let SPACE_DETAILS_CHARGE_TYPE_HOUR_BASE_DESCRIPTION = "The host charges on a per hour basis."
    static let SPACE_DETAILS_CHARGE_TYPE_BLOCK_BASE_PER_GUEST_DESCRIPTION = "The host charges on a per block basis. You will be paying based on the number of guests attending the event. \n \nMinimum number of guests applicable -"
    static let SPACE_DETAILS_CHARGE_TYPE_BLOCK_BASE_SPACE_ONLY_DESCRIPTION = "The host charges on a per block basis. This is a space hiring charge only."
    static let SPACE_DETAILS_CHARGE_TYPE_BLOCK_BASE_REIMBURSABLE_DESCRIPTION = "The host charges on a per block basis. The below payment can be reimbursed against your food/beverage order on the day of the event."
    
}
