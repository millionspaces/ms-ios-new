//
//  SpaceCalendarTime.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceCalendarTime: NSObject {
    
    var startTime24HourInt: Int!
    var endTime24HoutInt: Int!
    
    // TODO - To Be Set using Future Bookings
    var hasAvailableTime: Bool!
    
    // Hour Basis
    var hoursForCalendarDate: [SpaceCalendarTimeByHour] = []
    
    // Block Basis
    var blocksForCalendarDate: [SpaceCalendarTimeByBlock] = []
    
    
    // Init time for Hour Basis
    func initSpaceCalendarTimeByPerHourBasis(withCalendarDateAvailability availability: [SpaceAvailabilityModule]) {
        let hoursForDate = StaticSpaceFunctions.retrieveHoursForPerHourBasis(forAvailabilityForDate: availability)
        self.startTime24HourInt = hoursForDate.first!.timeHour24H
        self.endTime24HoutInt = hoursForDate.last!.timeHour24H
        self.hoursForCalendarDate = hoursForDate
        
        // Temporarily stating that the date has available time, until validated with future bookings
        self.hasAvailableTime = true
    }
    
    
    // Init time for Block Basis
    func initSpaceCalendarTimeByBlockBasis(withCalendarDateAvailability availability: [SpaceAvailabilityModule]) {
        
        var blocksForDate: [SpaceCalendarTimeByBlock] = []
        
        for i in availability {
            let block = SpaceCalendarTimeByBlock()
            block.initSpaceCalendarTimeByBlock(withAvailabilityModuleForBlock: i)
            blocksForDate.append(block)
        }
        
        blocksForDate.sort(by: {$0.startTimeHour24H < $1.startTimeHour24H})
        
        self.blocksForCalendarDate = blocksForDate
        
        // TEMP
        self.startTime24HourInt = 9
        self.endTime24HoutInt = 20
        self.hasAvailableTime = true
    }
    
    // Init time for Host Calendar - Hour Based
    func initHostCalendarTime() {
        let hoursForDate = StaticSpaceFunctions.retrieveDayHoursForHostCalendar()
        self.startTime24HourInt = hoursForDate.first!.timeHour24H
        self.endTime24HoutInt = hoursForDate.last!.timeHour24H
        self.hoursForCalendarDate = hoursForDate
        
        // Temporarily stating that the date has available time, until validated with future bookings
        self.hasAvailableTime = true
    }
    
}
