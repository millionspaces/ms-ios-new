//
//  SignInViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/7/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import GoogleSignIn

class SignInViewController: BaseViewController, UITextFieldDelegate, GIDSignInUIDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var fbSignInButton: UIButton!
    @IBOutlet weak var googleSignInButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailSignInButton: UIButton!
    
    // MARK: - Class properties / variables
    var isEmailAddressValid: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    
    // TODO: - Handle APPLICATION_IN_FOREGROUND
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handleGoogleSignIn(_:)), name: NSNotification.Name(rawValue: Constants.GOOGLE_ACCESS_TOKEN_RECEIVED), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configureView() {
        //addKeyBoardDismissOnTap()
        self.emailTextField.setTextFieldPlaceholder(withPlaceholderText: Strings.EMAIL, andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_VALID_PLACEHOLDER_COLOR_WHITE)
        self.passwordTextField.setTextFieldPlaceholder(withPlaceholderText: Strings.PASSWORD, andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_VALID_PLACEHOLDER_COLOR_WHITE)
    }
    
    func openTermsOfUseAndPrivacyPolicy() {
        if let url = URL(string: NetworkConfiguration.TERMS_PRIVACY_URL) {
            UIApplication.shared.open(url)
        }
    }
    
    
    // MARK: - IBActions
    
    @IBAction func fbSignInButtonPressed(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.socialAuthType = 1
        handleFacebookLogin()
    }
    
    @IBAction func googleSignInButtonPressed(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.socialAuthType = 2
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func emailSignInButtonPressed(_ sender: UIButton) {
        validateSignInCredentials()
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        performSegue(segueID: Constants.SEGUE_TO_SIGN_UP_SCREEN)
    }
    
    @IBAction func termsOfServiceButtonPressed(_ sender: UIButton) {
        openTermsOfUseAndPrivacyPolicy()
    }
    
    @IBAction func privacyPolicyButtonPressed(_ sender: UIButton) {
        openTermsOfUseAndPrivacyPolicy()
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        dismissViewController()
    }
    
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.textColor = ColorUtils.TEXT_VALIDATION_VALID_FONT_COLOR_WHITE
        if textField == self.emailTextField {
            //self.isValidAccount = false
            textField.setTextFieldPlaceholder(withPlaceholderText: Strings.EMAIL, andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_VALID_PLACEHOLDER_COLOR_WHITE)
        }
        else {
            textField.setTextFieldPlaceholder(withPlaceholderText: Strings.PASSWORD, andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_VALID_PLACEHOLDER_COLOR_WHITE)
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text!.trimmingCharacters(in: .whitespaces)
        textField.text = textField.text!.replacingOccurrences(of: " ", with: "")
        
        if textField == self.emailTextField {
            if textField.text!.count > 0 {
                if Utils.isValidEmailAddress(emailEntry: textField.text!) {
                    self.isEmailAddressValid = true
                    textField.textColor = ColorUtils.TEXT_VALIDATION_VALID_FONT_COLOR_WHITE
                }
                else {
                    self.isEmailAddressValid = false
                    textField.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
                }
            }
            else {
                self.isEmailAddressValid = false
                textField.setTextFieldPlaceholder(withPlaceholderText: Strings.EMAIL, andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_INVALID_PLACEHOLDER_COLOR_RED)
            }
        }
        else {
            if textField.text!.count > 0 {
                textField.textColor = ColorUtils.TEXT_VALIDATION_VALID_FONT_COLOR_WHITE
            }
            else {
                textField.setTextFieldPlaceholder(withPlaceholderText: Strings.PASSWORD, andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_INVALID_PLACEHOLDER_COLOR_RED)
            }
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.emailTextField.isFirstResponder {
            self.passwordTextField.becomeFirstResponder()
        }
        else {
            self.passwordTextField.resignFirstResponder()
            if self.emailTextField.text!.count > 0 && self.passwordTextField.text!.count > 0 {
                validateSignInCredentials()
            }
        }
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension SignInViewController {
    
    func validateSignInCredentials() {
        if isEmailAddressValid && (self.passwordTextField.text?.count)! > 0 {
            let email = self.emailTextField.text!
            let passwordEncrypted = self.passwordTextField.text!.sha256()
            //print("SignInVC validateSignInCredentials | passwordEncrypted : \(passwordEncrypted)")
            signInUser(email: email, password: passwordEncrypted)
        }
        else {
            self.emailTextField.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            self.passwordTextField.textColor = ColorUtils.TEXT_VALIDATION_INVALID_FONT_COLOR_RED
            self.emailTextField.setTextFieldPlaceholder(withPlaceholderText: Strings.EMAIL, andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_INVALID_PLACEHOLDER_COLOR_RED)
            self.passwordTextField.setTextFieldPlaceholder(withPlaceholderText: Strings.PASSWORD, andPlaceholderTextColor: ColorUtils.TEXT_VALIDATION_INVALID_PLACEHOLDER_COLOR_RED)
        }
    }
    
    
    func signInUser(email: String, password: String) {
        self.showActivityIndicator()
        WebServiceCall.authenticateUser(username: email, password: password, successBlock: { (results: Any) in
            print("SignInVC signInUser | Success results : \(results)")
            DispatchQueue.main.sync(execute: {
                AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                self.dismissViewController()
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("SignInVC signInUser | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SIGN_IN_WRONG_CREDENTIALS_ERROR_MESSAGE)
                }
                else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentServerErrorAlert()
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentNetworkUnavailabilityErrorAlert()
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
}



// MARK: - Handle Facebook Login
extension SignInViewController {
    
    func handleFacebookLogin() {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        if((FBSDKAccessToken.current()) != nil) {
                            //print("SignInVC handleFacebookLogin | fb access token : \(FBSDKAccessToken.current().tokenString!)")
                            
                            self.showActivityIndicator()
                            WebServiceCall.authenticateUser(withFacebookAccessToken: "\(FBSDKAccessToken.current().tokenString!)", successBlock: { (results: Any) in
                                DispatchQueue.main.sync(execute: {
                                    AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                                    self.dismissViewController()
                                    self.hideActivityIndicator()
                                })
                            })
                            { (errorCode: Int, error: String) in
                                //print("SignInVC handleFacebookLogin sign in | error : \(error)")
                                DispatchQueue.main.sync(execute: {
                                    self.hideActivityIndicator()
                                    if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SIGN_IN_WRONG_CREDENTIALS_ERROR_MESSAGE)
                                    }
                                    else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                                        self.presentServerErrorAlert()
                                    }
                                    else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                                        self.presentNetworkUnavailabilityErrorAlert()
                                    }
                                    else {
                                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
}

// MARK: - Handle Googlee Login
extension SignInViewController {
    
    @objc func handleGoogleSignIn(_ notification: Notification) {
        if let accessToken = notification.userInfo?["ACCESS_TOKEN"] {
            self.showActivityIndicator()
            WebServiceCall.authenticateUser(withGoogleAccessToken: accessToken as! String, successBlock: { (results: Any) in
                DispatchQueue.main.sync(execute: {
                    //print("SignInVC handleGoogleSignIn | results : \(results)")
                    AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                    self.dismissViewController()
                    self.hideActivityIndicator()
                })
            })
            { (errorCode: Int, error: String) in
                //print("SignInVC handleGoogleSignIn | errorCode : \(errorCode) | error : \(error)")
                DispatchQueue.main.sync(execute: {
                    self.hideActivityIndicator()
                    
                    if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SIGN_IN_WRONG_CREDENTIALS_ERROR_MESSAGE)
                    }
                    else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                        self.presentServerErrorAlert()
                    }
                    else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                        self.presentNetworkUnavailabilityErrorAlert()
                    }
                    else {
                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    }
                })
            }
        }
    }
}

