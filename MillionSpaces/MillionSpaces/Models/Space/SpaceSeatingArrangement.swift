//
//  SpaceSeatingArrangement.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceSeatingArrangement: NSObject {
    
    var id: Int!
    var arrangement: SeatingArrangement!
    var participantCount: Int!
    
    
    func initSpaceSeatingArrangement(withDictionary dic: [String : Any]) {
        self.id = dic["id"] as! Int
        self.arrangement = MetaData.retrieveSeatingArrangement(with: dic["id"] as! Int)
        self.participantCount = dic["participantCount"] as! Int
    }
}
