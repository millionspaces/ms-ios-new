//
//  SpaceDetailsViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import GoogleMaps

struct SpaceDataStructureForTableViewSection {
    var section: Int!
    var sectionSpecialIdentifier: Int!
    var numOfRows: Int!
}

class SpaceDetailsViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var spaceDetailsTableView: UITableView!
    @IBOutlet weak var bookNowButton: UIButton!
    @IBOutlet weak var constBookNowViewBottom: NSLayoutConstraint!
    @IBOutlet weak var fullScreenImageSliderView: UIView!
    @IBOutlet weak var fullScreenImageCollectionView: UICollectionView!
    @IBOutlet weak var fullScreenImageSliderPageController: UIPageControl!
    @IBOutlet weak var menuImageFullScreenView: UIView!
    @IBOutlet weak var scrollViewForMenuImageView: UIScrollView!
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var rightNavBarButton: UIBarButtonItem!
    
    // MARK: - Constants
    let DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS = (COVER_IMAGES: 0, PRIMARY_DETAILS: 10, PRICE: 20, MENUS: 30, AMENITIES: 40, SEATING_ARRANGEMENTS: 50, RULES: 60, CANCELLATION_POLICY: 70, LOCATION: 80, SIMILAR_SPACES: 90)
    
    let COLLECTION_VIEW_SPECIAL_IDENTIFIERS = (COVER_IMAGES_CV: 0, EVENT_TYPE_CV: 10, PRICE_CV: 20, MENUS_CV: 30, AMENITIES_CV: 41, EXTRA_AMENITIES_CV: 42, SEATING_ARRANGEMENTS_CV: 50, SIMILAR_SPACES_CV: 90, FULL_SCREEN_COVER_IMAGES_CV: 99)
    
    let DETAILS_TABLE_VIEW_CELL_IDS = (COVER_IMAGE_CV_EMBEDDED_TABLE_VIEW_CELL_ID: "CoverImageHeaderCell",
                                       TITLE_TABLE_VIEW_CELL_ID: "TitleCell",
                                       OVERVIEW_TABLE_VIEW_CELL_ID: "OverviewCell",
                                       CV_EMBEDDED_TABLE_VIEW_CELL_TYPE_ONE_ID: "CVECellOne",
                                       CV_EMBEDDED_TABLE_VIEW_CELL_TYPE_TWO_ID: "CVECellTwo",
                                       PRICE_HEADER_TABLE_VIEW_CELL_ID: "PriceHeaderCell",
                                       OTHER_HEADER_TABLE_VIEW_CELL_ID: "OtherHeaderCell",
                                       AMENITIES_CV_EMBEDDED_TABLE_VIEW_CELL_ID: "AmenitiesCVECell",
                                       SPACE_RULES_TABLE_VIEW_CELL_ID: "SpaceRuleCell",
                                       CANCELATION_POLICY_TABLE_VIEW_CELL_ID: "cPolicyCell",
                                       LOCATION_TABLE_VIEW_CELL_ID: "LocationCell",
                                       SIMILAR_SPACES_CV_EMBEDDED_TABLE_VIEW_CELL_ID: "SimilarSpacesCVECell")
    
    let COLLECTION_VIEW_CELL_IDS = (COVER_IMAGE_CV_CELL_ID: "CoverImageCell",
                                    EVENT_TYPE_CV_CELL_ID: "EventTypeCell",
                                    PRICE_HOUR_BASIS_CV_CELL_ID: "HourRateCell",
                                    PRICE_BLOCK_BASIS_CV_CELL_ID: "BlockRateCell",
                                    PRICE_BLOCK_BASIS_MENU_CV_CELL_ID: "BlockRateMenuCell",
                                    MENU_OPTIONS_CV_CELL_ID: "MenuOptionsCell",
                                    COMPLIMENTARY_AMENITIES_CV_CELL_ID: "AmenityCompCell",
                                    CHARGEABLE_AMENITIES_CV_CELL_ID: "AmenityChargCell",
                                    SEATING_ARRANGEMENT_CV_CELL_ID: "SeatingStyleCell",
                                    SIMILAR_SPACES_CV_CELL_ID: "SimilarSpaceCVCell",
                                    FULL_SCREEN_COVER_IMAGE_CV_CELL_ID: "FullScreenImageCell")
    
    
    // MARK: - Class properties / variables
    var displayingSpaceId: Int!
    var displayingSpace: Space?
    var displayingSpaceDataStructure: NSMutableDictionary = [:]
    var displayingSpaceAvailabilityModules: [SpaceAvailabilityModule] = []
    var displayingSpaceBlockAvailability: [[String : String]] = []
    var isAMySpace: Bool = false
    var lastLoadedSection: Int = 0
    var areSimilarSpacesLoaded = false
    var similarSpacesSectionHeight: CGFloat!
    var footer: UIView!
    var locationMapView: GMSMapView?
    var locationCameraPosition: GMSCameraPosition?
    var alertController: UIAlertController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preConfigureView()
        retrieveSpaceDetails()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToSpaceDetailsFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        
        if self.menuImageFullScreenView.isHidden {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            UIApplication.shared.isStatusBarHidden = false
        }
        else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.isStatusBarHidden = true
        }
        
        //self.constBookNowViewBottom.constant = -50.0
        
        if self.displayingSpace != nil {
            self.showHideSpaceBookNowButton()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    func preConfigureView() {
        self.fullScreenImageSliderView.isHidden = true
        self.menuImageFullScreenView.isHidden = true
        self.menuImageView.isUserInteractionEnabled = false
        similarSpacesSectionHeight = CGFloat((UIScreen.main.bounds.width / 32) * 39)
        footer = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:10))
        footer.backgroundColor = UIColor.init(red: 235.0/255.0, green: 235.0/255.0, blue: 241.0/255.0, alpha: 1.0)
        self.fullScreenImageCollectionView.tag = COLLECTION_VIEW_SPECIAL_IDENTIFIERS.FULL_SCREEN_COVER_IMAGES_CV
        self.fullScreenImageCollectionView.isPagingEnabled = true
        self.fullScreenImageSliderPageController.isUserInteractionEnabled = false
    }
    
    @objc func returnedToSpaceDetailsFromBackground() {
        if Utils.didPassDuration(durationInHours: 0.5) {
            // (in case if disabled) Enable device orientation restriction and change orientation to portrait
            UIDevice.current.setValue(NSNumber(value: UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
            self.setWindowOrientationRestriction(restriction: true)
            
            // (if shown) hide any alerts
            self.alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            
            // show nav bar and status bar (if hidden while fullscreen image views are open)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            UIApplication.shared.isStatusBarHidden = false
            
            popToRootViewController()
        }
    }
    
    func retrieveSpaceDetails() {
        showActivityIndicator()
        WebServiceCall.retrieveSpace(spaceId: self.displayingSpaceId!, successBlock: { (result: Any) in
            print("SpaceDetailsVC retrieveSpaceDetails | result : \(result)")
            
            if let _results = result as? [String : Any] {
                self.displayingSpace = Space()
                self.displayingSpace!.initSpace(withDictionary: _results)
                DispatchQueue.main.sync(execute: {
                    self.postConfigureView()
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    self.addSpaceDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("SpaceDetailsVC retrieveSpaceDetails | failure errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpaceDetailsReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpaceDetailsReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpaceDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func retrieveSimilarSpacesForSpaceDisplayingSpace() {
        var eventTypeIDs: [Int] = []
        if FilterManager.spaceFilter.eventTypes.count > 0 {
            let displaySpecEventTypesSet: Set = Set.init(self.displayingSpace!.eventTypes as! [EventType])
            let filteredEventTypesSet = Set.init(FilterManager.spaceFilter.eventTypes)
            let eventTypeIntersection: Set = displaySpecEventTypesSet.intersection(filteredEventTypesSet)
            eventTypeIDs = Utils.getEventTypeIDs(eventTypes: [EventType](eventTypeIntersection))
        }
        else {
            eventTypeIDs = Utils.getEventTypeIDs(eventTypes: self.displayingSpace!.eventTypes as! [EventType])
        }
        
        WebServiceCall.retrieveSimilarSpacesForSpace(withSpaceID: self.displayingSpaceId!, eventTypeIDs: eventTypeIDs, successBlock: { (result: Any) in
            if let similarSpaces = result as? [[String : Any]] {
                self.displayingSpace!.addSimilarSpaces(withSimilarSpacesArray: similarSpaces)
            }
            
            DispatchQueue.main.sync(execute: {
                self.configureViewForLoadedSimilarSpaces()
            })
        })
        { (errorCode: Int, error: String) in
            print("SpaceDetailsVC retrieveSimilarSpacesForSpaceDisplayingSpace | failure errorCode : \(errorCode) | error : \(error)")
        }
    }
    
    func addSpaceDetailsReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retrieveSpaceDetails()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func postConfigureView() {
        // Configuring view for the available data of displaying Space
        showHideSpaceBookNowButton()
        var section: Int = 0
        
        // S1 - Cover Images
        var s1 = SpaceDataStructureForTableViewSection()
        s1.section = section
        s1.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.COVER_IMAGES
        s1.numOfRows = 1        // Images
        displayingSpaceDataStructure.setValue(s1, forKey: section.description)
        
        // S2 - Space Primary Details
        section += 1
        var s2 = SpaceDataStructureForTableViewSection()
        s2.section = section
        s2.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.PRIMARY_DETAILS
        s2.numOfRows = 3        // Title + Overview + EventTypes
        displayingSpaceDataStructure.setValue(s2, forKey: section.description)
        
        // S3 - Price
        section += 1
        var s3 = SpaceDataStructureForTableViewSection()
        s3.section = section
        s3.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.PRICE
        s3.numOfRows = 2        // Title + Block/Hour Units
        displayingSpaceDataStructure.setValue(s3, forKey: section.description)
        
        // S4 - Menus
        if displayingSpace!.menus.count > 0 {
            section += 1
            var s4 = SpaceDataStructureForTableViewSection()
            s4.section = section
            s4.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.MENUS
            s4.numOfRows = 2        // Title + Images
            displayingSpaceDataStructure.setValue(s4, forKey: section.description)
        }
        
        // S5 - Amenities
        if displayingSpace!.amenities.count > 0 || displayingSpace!.extraAmenities.count > 0 {
            section += 1
            var s5 = SpaceDataStructureForTableViewSection()
            s5.section = section
            s5.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.AMENITIES
            s5.numOfRows = 1        // Title
            s5.numOfRows = (displayingSpace!.amenities.count > 0) ? s5.numOfRows + 1 : s5.numOfRows + 0 // Complementary
            s5.numOfRows = (displayingSpace!.extraAmenities.count > 0) ? s5.numOfRows + 1 : s5.numOfRows + 0 // Chargable
            displayingSpaceDataStructure.setValue(s5, forKey: section.description)
        }
        
        // S6 - Seating Arrangements
        if displayingSpace!.seatingArrangements.count > 0 {
            section += 1
            var s6 = SpaceDataStructureForTableViewSection()
            s6.section = section
            s6.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.SEATING_ARRANGEMENTS
            s6.numOfRows = 2        // Title + Seating Arrangements
            displayingSpaceDataStructure.setValue(s6, forKey: section.description)
        }
        
        // S7 : Space Rules
        if displayingSpace!.spaceRules.count > 0 {
            section += 1
            var s7 = SpaceDataStructureForTableViewSection()
            s7.section = section
            s7.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.RULES
            s7.numOfRows = displayingSpace!.spaceRules.count + 1   // Title + Rules Count
            displayingSpaceDataStructure.setValue(s7, forKey: section.description)
        }
        
        // S8 : Cancellation Policy
        section += 1
        var s8 = SpaceDataStructureForTableViewSection()
        s8.section = section
        s8.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.CANCELLATION_POLICY
        s8.numOfRows = 1        // Cancellation Policy
        displayingSpaceDataStructure.setValue(s8, forKey: section.description)
        
        // S9 : Location
        section += 1
        var s9 = SpaceDataStructureForTableViewSection()
        s9.section = section
        s9.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.LOCATION
        s9.numOfRows = 2        // Title + Map
        displayingSpaceDataStructure.setValue(s9, forKey: section.description)
        
        lastLoadedSection = section
        
        // Executing other configurations
        configureSpacePricing()
        configureLocationMapView()
        
        self.spaceDetailsTableView.reloadData()
        self.spaceDetailsTableView.delaysContentTouches = true
        
        self.fullScreenImageSliderPageController.numberOfPages = self.displayingSpace!.images.count
        self.fullScreenImageCollectionView.reloadData()
    }
    
    func configureViewForLoadedSimilarSpaces() {
        // S10 : Similar Spaces
        if displayingSpace!.similarSpaces.count > 0 {
            lastLoadedSection += 1
            var s10 = SpaceDataStructureForTableViewSection()
            s10.section = lastLoadedSection
            s10.sectionSpecialIdentifier = DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.SIMILAR_SPACES
            s10.numOfRows = 1
            displayingSpaceDataStructure.setValue(s10, forKey: lastLoadedSection.description)
            
            self.spaceDetailsTableView.reloadData()
        }
    }
    
    func configureSpacePricing() {
        if displayingSpace!.availabilityMethod == Utils.MAIN_SPACE_CHARGE_TYPES.Block {
            displayingSpaceBlockAvailability = StaticSpaceFunctions.prepareBlockPricesForSpaceDetailsScreen(spaceAvailability: displayingSpace!.availabilityModules, blockChargeType: displayingSpace!.blockChargeType)
        }
        else {
            displayingSpaceAvailabilityModules = displayingSpace!.availabilityModules.sorted(by: {($0).day < ($1).day}) //as! NSMutableArray
        }
    }
    
    
    func configureLocationMapView() {
        self.locationCameraPosition = GMSCameraPosition.camera(withLatitude: displayingSpace!.latitude!,
                                                               longitude: displayingSpace!.longitude!,
                                                               zoom: 16)
        let mapViewWidth = UIScreen.main.bounds.width - 20
        let mapViewHeight = (mapViewWidth/30) * 17
        let mapViewSize = CGRect(x: 0, y: 0, width: mapViewWidth, height: mapViewHeight)
        self.locationMapView = GMSMapView.map(withFrame: mapViewSize, camera: self.locationCameraPosition!)
        
        let marker = GMSMarker()
        marker.position = self.locationCameraPosition!.target
        marker.appearAnimation = .pop
        marker.map = self.locationMapView
    }
    
    private func showHideSpaceBookNowButton() {
        if AuthenticationManager.isUserAuthenticated {
            isAMySpace = (AuthenticationManager.user?.id == displayingSpace!.hostId)
            if isAMySpace {
                self.bookNowButton.isUserInteractionEnabled = false
                //self.constBookNowViewBottom.constant = -50.0
            }
            else {
                self.bookNowButton.isUserInteractionEnabled = true
                //self.constBookNowViewBottom.constant = 0.0
            }
        }
        else {
            self.bookNowButton.isUserInteractionEnabled = true
            //self.constBookNowViewBottom.constant = 0.0
        }
        
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.menuImageFullScreenView) else { return }
        if menuImageFullScreenView.frame.contains(location) {
            self.showFullScreenImageViewForMenuImages(show: false, menuImageName: nil)
        }
    }
    
    func showFullScreenImageSliderView(show: Bool) {
        self.navigationController?.setNavigationBarHidden(show, animated: true)
        UIApplication.shared.isStatusBarHidden = show
        self.fullScreenImageSliderView.isHidden = !show
        
        if show {
            self.setWindowOrientationRestriction(restriction: false)
            self.fullScreenImageCollectionView.reloadData()
            //UIDevice.current.setValue(NSNumber(value: UIInterfaceOrientation.landscapeRight.rawValue), forKey: "orientation")
        }
        else {
            UIDevice.current.setValue(NSNumber(value: UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
            self.setWindowOrientationRestriction(restriction: true)
        }
    }
    
    func showFullScreenImageViewForMenuImages(show: Bool, menuImageName: String?) {
        self.navigationController?.setNavigationBarHidden(show, animated: true)
        UIApplication.shared.isStatusBarHidden = show
        self.menuImageFullScreenView.isHidden = !show
        
        UIView.transition(with: self.menuImageFullScreenView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.menuImageFullScreenView.isHidden = !show
        }, completion: nil)
        
        if show {
            self.scrollViewForMenuImageView.setZoomScale(1.0, animated: false)
            self.scrollViewForMenuImageView.minimumZoomScale = self.scrollViewForMenuImageView.frame.size.width / self.menuImageView.frame.size.width
            self.scrollViewForMenuImageView.maximumZoomScale = 5.0
            self.scrollViewForMenuImageView.contentSize = self.menuImageView.frame.size
            
            self.menuImageView.sd_setImage(with: URL(string: (NetworkConfiguration.IMAGE_BASE_URL + menuImageName!)), placeholderImage: #imageLiteral(resourceName: "img-thumbnail"))
            
            let doubleTapToZoom = UITapGestureRecognizer.init(target: self, action: #selector(handleDoubleTapToZoomMenuImage(_:)))
            doubleTapToZoom.numberOfTapsRequired = 2
            self.scrollViewForMenuImageView.addGestureRecognizer(doubleTapToZoom)
        }
        else {
            self.menuImageView.image = nil
        }
    }
    
    
    @objc func handleDoubleTapToZoomMenuImage(_ gestureRecognizer: UITapGestureRecognizer) {
        if self.scrollViewForMenuImageView.zoomScale > self.scrollViewForMenuImageView.minimumZoomScale {
            self.scrollViewForMenuImageView.setZoomScale(1.0, animated: true)
        }
        else {
            let tappedLocation = gestureRecognizer.location(in: gestureRecognizer.view)
            let w = self.scrollViewForMenuImageView.frame.size.width / self.scrollViewForMenuImageView.maximumZoomScale
            let h = self.scrollViewForMenuImageView.frame.size.height / self.scrollViewForMenuImageView.maximumZoomScale
            let x = tappedLocation.x - (w/2.0)
            let y = tappedLocation.y - (h/2.0)
            let rectToZoom = CGRect.init(x: x, y: y, width: w, height: h)
            self.scrollViewForMenuImageView.zoom(to: rectToZoom, animated: true)
        }
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if (size.width / size.height > 1) {
            print("landscape")
        } else {
            print("portrait")
        }
        
        if UIDevice.current.orientation.isLandscape {
            print("SpaceDetailsVC viewWillTransition | isLandscape")
        }
        else {
            print("SpaceDetailsVC viewWillTransition | isPortrait")
        }
        
        if !self.fullScreenImageSliderView.isHidden {
            self.fullScreenImageCollectionView.reloadData()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if let space = displayingSpace {
            var message = "Check out \(space.name!)"
            if let add2 = space.addressLine2 {
                message.append(" at \(add2)!")
            }
            else {
                message.append("!")
            }
            var spaceName = space.name!.trimmingCharacters(in: .whitespaces)
            spaceName = spaceName.replacingOccurrences(of: " ", with: "-")
            let spaceUrl = URL(string: "\(NetworkConfiguration.MAIN_WEBSITE_DOMAIN_FOR_API_ENVIRONMENT)/#/spaces/\(space.id!)/\(spaceName)")
            
            let activityViewController = UIActivityViewController.init(activityItems: [message, spaceUrl!], applicationActivities: nil)
            self.navigationController?.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func bookNowButtonPressed(_ sender: UIButton) {
        if displayingSpace != nil {
            if AuthenticationManager.isUserAuthenticated {
                let storyboard = UIStoryboard.init(name: Constants.MAIN_STORYBOARD, bundle: nil)
                let bookingInitialVC = storyboard.instantiateViewController(withIdentifier: Constants.VC_ID_BOOKING_INITIAL_SCREEN) as! BookingInitialViewController
                bookingInitialVC.bookingSpace = displayingSpace!
                self.navigationController?.pushViewController(bookingInitialVC, animated: true)
            }
            else {
                openSignInViewController()
            }
        }
    }
    
    @IBAction func menuFullScreenViewCloseButtonPressed(_ sender: UIButton) {
        self.showFullScreenImageViewForMenuImages(show: false, menuImageName: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


// MARK: - UIScrollViewDelegate
extension SpaceDetailsViewController {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        if scrollView == self.spaceDetailsTableView {
//            if !isAMySpace {
//                self.constBookNowViewBottom.constant = -50.0
//                UIView.animate(withDuration: 0.3) {
//                    self.view.layoutIfNeeded()
//                }
//            }
//        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if scrollView == self.spaceDetailsTableView {
//            if !isAMySpace {
//                self.constBookNowViewBottom.constant = 0.0
//                UIView.animate(withDuration: 0.3, delay: 0.5, options: UIView.AnimationOptions.allowUserInteraction, animations: {
//                    self.view.layoutIfNeeded()
//                }, completion: nil)
//            }
//        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.COVER_IMAGES_CV {
            if let coverImageCVEmbeddedCell = self.spaceDetailsTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CollectionViewEmbeddedTableViewCell {
                coverImageCVEmbeddedCell.pageController.currentPage = Int((coverImageCVEmbeddedCell.collectionView.contentOffset.x + UIScreen.main.bounds.width / 2) / UIScreen.main.bounds.width)
            }
        }
        else if scrollView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.FULL_SCREEN_COVER_IMAGES_CV {
            print("SpaceDetailsVC scrollViewDidScroll | fullScreenImageCV scrolls")
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.menuImageView
    }
}


// MARK: - UITableView DataSource and Delegate
extension SpaceDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if displayingSpace != nil {
            return displayingSpaceDataStructure.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == displayingSpaceDataStructure.count - 1 {
            return 0.01
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if displayingSpace != nil {
            return numberRowsInTableViewSection(section: section)
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRowAtIndexPath(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRowAtIndexPath(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if displayingSpace != nil {
            return populateTableViewCell(forTableView: tableView, forIndexPath: indexPath)
        }
        else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == displayingSpaceDataStructure.count - 1 && !self.areSimilarSpacesLoaded {
            self.areSimilarSpacesLoaded = true
            self.retrieveSimilarSpacesForSpaceDisplayingSpace()
        }
    }
}



// MARK: - UITableViewDataSource Helper functions
extension SpaceDetailsViewController {
    
    func numberRowsInTableViewSection(section: Int) -> Int {
        let sectionDataStructure = self.displayingSpaceDataStructure[section.description] as! SpaceDataStructureForTableViewSection
        return sectionDataStructure.numOfRows
    }
    
    func heightForRowAtIndexPath(indexPath: IndexPath) -> CGFloat{
        let sectionDataStructure = self.displayingSpaceDataStructure[indexPath.section.description] as! SpaceDataStructureForTableViewSection
        
        switch sectionDataStructure.sectionSpecialIdentifier {
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.COVER_IMAGES:
            // Cover Image
            return CGFloat((UIScreen.main.bounds.width / 32) * 22)
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.PRIMARY_DETAILS:
            // Space Primary Details
            return indexPath.row == 2 ? 85 : UITableView.automaticDimension
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.PRICE:
            // Price
            if displayingSpace!.availabilityMethod == Utils.MAIN_SPACE_CHARGE_TYPES.Block {
                if indexPath.row == 0 {
                    return UITableView.automaticDimension
                }
                else {
                    return displayingSpace!.blockChargeType == 2 ? 150 : 125
                }
            }
            else {
                return indexPath.row == 0 ? UITableView.automaticDimension : 100
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.MENUS:
            // Menus
            return indexPath.row == 0 ? 50 : 130
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.AMENITIES:
            // Amenities
            if indexPath.row == 0 {
                return 50
            }
            else {
                if sectionDataStructure.numOfRows == 3 {
                    if indexPath.row == 1 {
                        return 115 // Complementary
                    }
                    else { // if indexPath.row == 2
                        return 165 // Chargable
                    }
                }
                else {
                    if displayingSpace!.amenities.count > 0 {
                        return 115 // Complementary
                    }
                    else {
                        return 165 // Chargable
                    }
                }
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.SEATING_ARRANGEMENTS:
            // Seating Arrangements
            return indexPath.row == 0 ? 50 : Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: displayingSpace!.seatingArrangements.count, cvCellHeight: 50, itemsPerRow: 2)
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.RULES:
            // Space Rules
            return indexPath.row == 0 ? 50 : 30
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.CANCELLATION_POLICY:
            // Cancellation Policy
            return UITableView.automaticDimension
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.LOCATION:
            // Space Location
            return indexPath.row == 0 ? 50 : UITableView.automaticDimension
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.SIMILAR_SPACES:
            // SimilarSpacesCVECell
            return similarSpacesSectionHeight
            
        default:
            return 0
        }
    }
    
    func populateTableViewCell(forTableView tableView: UITableView, forIndexPath indexPath: IndexPath) -> UITableViewCell {
        let sectionDataStructure = self.displayingSpaceDataStructure[indexPath.section.description] as! SpaceDataStructureForTableViewSection
        
        switch sectionDataStructure.sectionSpecialIdentifier {
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.COVER_IMAGES:
            // Cover Image
            let coverImagesEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.COVER_IMAGE_CV_EMBEDDED_TABLE_VIEW_CELL_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
            coverImagesEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
            return coverImagesEmbeddedCell
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.PRIMARY_DETAILS:
            // Space Primary Details
            switch indexPath.row {
            case 0:
                // Title
                let titleCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.TITLE_TABLE_VIEW_CELL_ID, for: indexPath)
                (titleCell.viewWithTag(1) as! UILabel).text = displayingSpace!.name
                (titleCell.viewWithTag(2) as! UILabel).text = displayingSpace?.fullAdrress!
                
                let ratingView = titleCell.viewWithTag(3) as! StarRatingView
                ratingView.setRatingValue(withFloat: 3.5)
                for c in ratingView.constraints {
                    if c.identifier == "srW" {
                        c.constant = 0
                        // TODO:
                    }
                }
                return titleCell
                
            case 1:
                // Overview
                let overviewCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.OVERVIEW_TABLE_VIEW_CELL_ID, for: indexPath)
                (overviewCell.viewWithTag(1) as! UILabel).text = displayingSpace!.sDescription
                (overviewCell.viewWithTag(2) as! UILabel).text = "\((displayingSpace!.participantCount)!) Guests"
                (overviewCell.viewWithTag(3) as! UILabel).text = "\((displayingSpace!.size)!) \((displayingSpace!.sizeMeasurementUnit!.name)!)"
                
                if let spaceType = self.displayingSpace!.spaceType {
                    (overviewCell.viewWithTag(4) as! UIImageView).sd_setImage(with: URL(string: spaceType.spaceTypeIconUrlStr!), completed: nil)
                    (overviewCell.viewWithTag(5) as! UILabel).text = spaceType.spaceTypeName!
                }
                else {
                    (overviewCell.viewWithTag(4) as! UIImageView).image = nil
                    (overviewCell.viewWithTag(5) as! UILabel).text = ""
                }
                
                return overviewCell
                
            case 2:
                // EventTypes
                let eventTypesEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.CV_EMBEDDED_TABLE_VIEW_CELL_TYPE_ONE_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
                eventTypesEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
                return eventTypesEmbeddedCell
                
            default:
                return UITableViewCell()
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.PRICE:
            // Price
            switch indexPath.row {
            case 0:
                // Price Header
                let priceHeaderCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.PRICE_HEADER_TABLE_VIEW_CELL_ID, for: indexPath)
                let availabilityMethodLabel = priceHeaderCell.viewWithTag(1) as! UILabel
                if displayingSpace!.availabilityMethod == Utils.MAIN_SPACE_CHARGE_TYPES.Block {
                    if displayingSpace!.blockChargeType == 1 {
                        if displayingSpace!.hasReimbursableBlocks {
                            availabilityMethodLabel.text = Strings.SPACE_DETAILS_CHARGE_TYPE_BLOCK_BASE_REIMBURSABLE_DESCRIPTION
                        }
                        else {
                            availabilityMethodLabel.text = Strings.SPACE_DETAILS_CHARGE_TYPE_BLOCK_BASE_SPACE_ONLY_DESCRIPTION
                        }
                    }
                    else if displayingSpace!.blockChargeType == 2 {
                        availabilityMethodLabel.text = "\(Strings.SPACE_DETAILS_CHARGE_TYPE_BLOCK_BASE_PER_GUEST_DESCRIPTION) \(displayingSpace!.minimumParticipantCount)"
                    }
                }
                else {
                    availabilityMethodLabel.text = Strings.SPACE_DETAILS_CHARGE_TYPE_HOUR_BASE_DESCRIPTION
                }
                
                return priceHeaderCell
                
            case 1:
                // Price Units
                let priceUnitsEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.CV_EMBEDDED_TABLE_VIEW_CELL_TYPE_ONE_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
                priceUnitsEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
                return priceUnitsEmbeddedCell
                
            default:
                return UITableViewCell()
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.MENUS:
            // Menus
            switch indexPath.row {
            case 0:
                // Title
                let menuHeaderCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.OTHER_HEADER_TABLE_VIEW_CELL_ID, for: indexPath)
                (menuHeaderCell.viewWithTag(1) as! UILabel).text = Strings.MENU_OPTIONS
                return menuHeaderCell
                
            case 1:
                // Price Units
                let menusEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.CV_EMBEDDED_TABLE_VIEW_CELL_TYPE_ONE_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
                menusEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
                return menusEmbeddedCell
                
            default:
                return UITableViewCell()
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.AMENITIES:
            // Amenities
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.OTHER_HEADER_TABLE_VIEW_CELL_ID, for: indexPath)
                (cell.viewWithTag(1) as! UILabel).text = Strings.AMENITIES
                return cell
            }
            else {
                let amenityEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.AMENITIES_CV_EMBEDDED_TABLE_VIEW_CELL_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
                let amenityTypeLabel = amenityEmbeddedCell.viewWithTag(1) as! UILabel
                
                if sectionDataStructure.numOfRows == 3 {
                    if indexPath.row == 1 {
                        amenityTypeLabel.text = Strings.COMPLIMENTARY
                        amenityEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: (sectionDataStructure.sectionSpecialIdentifier + 1))
                    }
                    else { //if indexPath.row == 2
                        amenityTypeLabel.text = Strings.CHARGEABLE
                        amenityEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: (sectionDataStructure.sectionSpecialIdentifier + 2))
                    }
                }
                else {
                    if displayingSpace!.amenities.count > 0 {
                        amenityTypeLabel.text = Strings.COMPLIMENTARY
                        amenityEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: (sectionDataStructure.sectionSpecialIdentifier + 1))
                    }
                    else {
                        amenityTypeLabel.text = Strings.CHARGEABLE
                        amenityEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: (sectionDataStructure.sectionSpecialIdentifier + 2))
                    }
                }
                
                return amenityEmbeddedCell
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.SEATING_ARRANGEMENTS:
            // Seating Arrangements
            switch indexPath.row {
            case 0:
                // Title
                let seatingArrangementHeaderCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.OTHER_HEADER_TABLE_VIEW_CELL_ID, for: indexPath)
                (seatingArrangementHeaderCell.viewWithTag(1) as! UILabel).text = Strings.SEATING_ARRANGEMENTS
                return seatingArrangementHeaderCell
                
            case 1:
                // Seating Arrangements
                let seatingArrangementsEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.CV_EMBEDDED_TABLE_VIEW_CELL_TYPE_TWO_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
                seatingArrangementsEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
                return seatingArrangementsEmbeddedCell
                
            default:
                return UITableViewCell()
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.RULES:
            // Space Rules
            if indexPath.row == 0 {
                let ruleHeaderCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.OTHER_HEADER_TABLE_VIEW_CELL_ID, for: indexPath)
                (ruleHeaderCell.viewWithTag(1) as! UILabel).text = Strings.SPACE_RULES
                return ruleHeaderCell
            }
            else {
                let ruleCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.SPACE_RULES_TABLE_VIEW_CELL_ID, for: indexPath)
                (ruleCell.viewWithTag(1) as! UILabel).text = (displayingSpace!.spaceRules[indexPath.row-1] as! Rule).name
                return ruleCell
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.CANCELLATION_POLICY:
            // Cancellation Policy
            let cPolicyCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.CANCELATION_POLICY_TABLE_VIEW_CELL_ID, for: indexPath)
            (cPolicyCell.viewWithTag(1) as! UILabel).text = displayingSpace!.cancellationPolicy!.name
            (cPolicyCell.viewWithTag(2) as! UILabel).text = displayingSpace!.cancellationPolicy!.policyDescription
            return cPolicyCell
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.LOCATION:
            // Space Location
            if indexPath.row == 0 {
                let locationHeaderCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.OTHER_HEADER_TABLE_VIEW_CELL_ID, for: indexPath)
                (locationHeaderCell.viewWithTag(1) as! UILabel).text = Strings.LOCATION
                return locationHeaderCell
            }
            else {
                let locationCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.LOCATION_TABLE_VIEW_CELL_ID, for: indexPath)
                let subViewForMap = locationCell.viewWithTag(1)!
                subViewForMap.autoresizesSubviews = true
                self.locationMapView!.animate(to: self.locationCameraPosition!)
                subViewForMap.addSubview(self.locationMapView!)
                return locationCell
            }
            
        case DETAILS_TABLE_VIEW_SECTION_IDENTIFIERS.SIMILAR_SPACES:
            // Similar Spaces
            let similarSpacesEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: DETAILS_TABLE_VIEW_CELL_IDS.SIMILAR_SPACES_CV_EMBEDDED_TABLE_VIEW_CELL_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
            similarSpacesEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
            return similarSpacesEmbeddedCell
            
        default:
            return UITableViewCell()
        }
    }
    
}


// MARK: - UICollectionViewDataSource
extension SpaceDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if displayingSpace != nil {
            return numberOfItemsInCollectionView(collectionView: collectionView)
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if displayingSpace != nil {
            return populateCollectionViewCell(forCollectionView: collectionView, forIndexPath: indexPath)
        }
        else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.COVER_IMAGES_CV {
            self.fullScreenImageCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
        else if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.FULL_SCREEN_COVER_IMAGES_CV {
            self.fullScreenImageSliderPageController.currentPage = indexPath.item
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if displayingSpace != nil {
            if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.SIMILAR_SPACES_CV {
                let similarSpaceAtItem = displayingSpace!.similarSpaces[indexPath.item] as! Space
                openSpaceDetailsViewController(forSpaceBySpaceID: similarSpaceAtItem.id)
            }
            else if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.COVER_IMAGES_CV {
                self.showFullScreenImageSliderView(show: true)
                collectionView.deselectItem(at: indexPath, animated: false)
            }
            else if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.FULL_SCREEN_COVER_IMAGES_CV {
                self.showFullScreenImageSliderView(show: false)
            }
            else if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.MENUS_CV {
                self.showFullScreenImageViewForMenuImages(show: true, menuImageName: (displayingSpace!.menus[indexPath.item]).menuImage!)
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
    }
}


// MARK: - UICollectionViewDelegateFlowLayout
extension SpaceDetailsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if displayingSpace != nil {
            return collectionViewCellLayoutSize(forCollectionView: collectionView)
        }
        else {
            return CGSize()
        }
    }
}



// MARK: - UICollectionViewDataSource and DelegateFlowLayout Helper functions
extension SpaceDetailsViewController {
    
    func numberOfItemsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView.tag {
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.COVER_IMAGES_CV:
            return displayingSpace!.images.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.EVENT_TYPE_CV:
            return displayingSpace!.eventTypes.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.PRICE_CV:
            return (self.displayingSpace!.blockChargeType == 0) ? displayingSpaceAvailabilityModules.count : displayingSpaceBlockAvailability.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.MENUS_CV:
            return displayingSpace!.menus.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.AMENITIES_CV:
            return displayingSpace!.amenities.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.EXTRA_AMENITIES_CV:
            return displayingSpace!.extraAmenities.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.SEATING_ARRANGEMENTS_CV:
            return displayingSpace!.seatingArrangements.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.SIMILAR_SPACES_CV:
            return displayingSpace!.similarSpaces.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.FULL_SCREEN_COVER_IMAGES_CV:
            return displayingSpace!.images.count
            
        default:
            return 0
        }
    }
    
    func populateCollectionViewCell(forCollectionView collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.COVER_IMAGES_CV:
            let coverImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.COVER_IMAGE_CV_CELL_ID, for: indexPath)
            collectionView.isPagingEnabled = true
            coverImageCell.preservesSuperviewLayoutMargins = false
            let imageView = coverImageCell.viewWithTag(1) as! UIImageView
            
            imageView.sd_setImage(with: URL(string: (NetworkConfiguration.IMAGE_BASE_URL + (displayingSpace!.images[indexPath.item] as! String))), completed: nil)
            return coverImageCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.EVENT_TYPE_CV:
            let eventTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.EVENT_TYPE_CV_CELL_ID, for: indexPath)
            let eventTypeAtItem = displayingSpace!.eventTypes[indexPath.item] as! EventType
            
            let imageView = eventTypeCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: eventTypeAtItem.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            (eventTypeCell.viewWithTag(2) as! UILabel).text = eventTypeAtItem.name!
            return eventTypeCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.PRICE_CV:
            if displayingSpace!.availabilityMethod == Utils.MAIN_SPACE_CHARGE_TYPES.Block { // Block Base
                
                let newBlockAtItem = displayingSpaceBlockAvailability[indexPath.item]
                
                let blockRateCell: UICollectionViewCell!
                if displayingSpace!.blockChargeType == 2 {
                    blockRateCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.PRICE_BLOCK_BASIS_MENU_CV_CELL_ID, for: indexPath)
                    (blockRateCell.viewWithTag(4) as! UILabel).text = newBlockAtItem["menu"]!
                }
                else {
                    blockRateCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.PRICE_BLOCK_BASIS_CV_CELL_ID, for: indexPath)
                }
                
                (blockRateCell.viewWithTag(1) as! UILabel).text = newBlockAtItem["time"]!
                (blockRateCell.viewWithTag(2) as! UILabel).text = newBlockAtItem["rate"]!
                (blockRateCell.viewWithTag(3) as! UILabel).text = newBlockAtItem["days"]!
                
                return blockRateCell
            }
            else {
                // Hour Base
                let hourRateCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.PRICE_HOUR_BASIS_CV_CELL_ID, for: indexPath)
                let hourRateAtItem = displayingSpaceAvailabilityModules[indexPath.item] as! SpaceAvailabilityModule
                (hourRateCell.viewWithTag(1) as! UILabel).text = hourRateAtItem.dayDisplayName
                (hourRateCell.viewWithTag(3) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: hourRateAtItem.charge))"
                return hourRateCell
            }
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.MENUS_CV:
            // Menus
            let menuCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.MENU_OPTIONS_CV_CELL_ID, for: indexPath)
            let menuAtIndex = displayingSpace!.menus[indexPath.item]
            (menuCell.viewWithTag(1) as! UILabel).text = menuAtIndex.menuName!
            let imageView = menuCell.viewWithTag(2) as! UIImageView
            imageView.sd_setImage(with: URL(string: (NetworkConfiguration.IMAGE_BASE_URL + menuAtIndex.menuImage!)), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            return menuCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.AMENITIES_CV:
            let amenityCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.COMPLIMENTARY_AMENITIES_CV_CELL_ID, for: indexPath)
            let amenityAtItem = displayingSpace!.amenities[indexPath.item] as! Amenity
            
            let imageView = amenityCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: amenityAtItem.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            (amenityCell.viewWithTag(2) as! UILabel).text = amenityAtItem.name!
            return amenityCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.EXTRA_AMENITIES_CV:
            let extraAmenityCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.CHARGEABLE_AMENITIES_CV_CELL_ID, for: indexPath)
            let extraAmenityAtItem = displayingSpace!.extraAmenities[indexPath.item] as! SpaceExtraAmenity
            
            let imageView = extraAmenityCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: extraAmenityAtItem.extraAmenity.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            
            (extraAmenityCell.viewWithTag(2) as! UILabel).text = extraAmenityAtItem.extraAmenity.name!
            (extraAmenityCell.viewWithTag(4) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: extraAmenityAtItem.rate))"
            (extraAmenityCell.viewWithTag(5) as! UILabel).text = extraAmenityAtItem.extraAmenityUnit.name!
            
            return extraAmenityCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.SEATING_ARRANGEMENTS_CV:
            let seatingArrangementCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.SEATING_ARRANGEMENT_CV_CELL_ID, for: indexPath)
            
            let seatingAtItem = displayingSpace!.seatingArrangements[indexPath.item] as! SpaceSeatingArrangement
            
            let imageView = seatingArrangementCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: seatingAtItem.arrangement.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            (seatingArrangementCell.viewWithTag(2) as! UILabel).text = seatingAtItem.arrangement.name!
            (seatingArrangementCell.viewWithTag(3) as! UILabel).text = "Maximum \(seatingAtItem.participantCount!)"
            
            return seatingArrangementCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.SIMILAR_SPACES_CV:
            let similarSpaceCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.SIMILAR_SPACES_CV_CELL_ID, for: indexPath)
            
            let similarSpaceAtItem = displayingSpace!.similarSpaces[indexPath.item] as! Space
            let imageView = similarSpaceCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: (NetworkConfiguration.IMAGE_BASE_URL + similarSpaceAtItem.thumbnailImage!)), placeholderImage: #imageLiteral(resourceName: "img-thumbnail"))
            
            (similarSpaceCell.viewWithTag(2) as! UILabel).text = similarSpaceAtItem.name
            (similarSpaceCell.viewWithTag(3) as! UILabel).text = (similarSpaceAtItem.addressLine2 != nil) ? similarSpaceAtItem.addressLine2! : similarSpaceAtItem.name
            
            (similarSpaceCell.viewWithTag(4) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: similarSpaceAtItem.ratePerHour!))"
            
            // TEMP: / TODO
            (similarSpaceCell.viewWithTag(5) as! StarRatingView).isHidden = true
            //(similarSpaceCell.viewWithTag(4) as! StarRatingView).setRatingValue(withFloat: 3.5)
            
            return similarSpaceCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.FULL_SCREEN_COVER_IMAGES_CV:
            let fullScreenImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTION_VIEW_CELL_IDS.FULL_SCREEN_COVER_IMAGE_CV_CELL_ID, for: indexPath)
            let imageView = fullScreenImageCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: (NetworkConfiguration.IMAGE_BASE_URL + (displayingSpace!.images[indexPath.item] as! String))), placeholderImage: #imageLiteral(resourceName: "img-thumbnail"))
            return fullScreenImageCell
            
        default:
            return UICollectionViewCell()
        }
    }
    
    
    func collectionViewCellLayoutSize(forCollectionView collectionView: UICollectionView) -> CGSize {
        switch collectionView.tag {
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.COVER_IMAGES_CV:
            let cellWidth = UIScreen.main.bounds.width
            let cellHeight = (cellWidth/32) * 22
            return CGSize(width: cellWidth, height: cellHeight)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.EVENT_TYPE_CV:
            return CGSize(width: 85, height: 85)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.PRICE_CV:
            if displayingSpace!.availabilityMethod == Utils.MAIN_SPACE_CHARGE_TYPES.Block {
                if displayingSpace!.blockChargeType == 2 {
                    return CGSize(width: 120, height: 150)
                }
                else {
                    return CGSize(width: 120, height: 125)
                }
            }
            else {
                return CGSize(width: 120, height: 100)
            }
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.MENUS_CV:
            return CGSize(width: 70, height: 130)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.AMENITIES_CV:
            return CGSize(width: 100, height: 85)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.EXTRA_AMENITIES_CV:
            return CGSize(width: 120, height: 135)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.SEATING_ARRANGEMENTS_CV:
            let cellWidth = (UIScreen.main.bounds.width - 50) / 2
            return CGSize(width: cellWidth, height: 50)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.SIMILAR_SPACES_CV:
            let cellWidth = UIScreen.main.bounds.width - 70.0
            return CGSize(width: cellWidth, height: similarSpacesSectionHeight - 40)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.FULL_SCREEN_COVER_IMAGES_CV:
            return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
        default:
            return CGSize()
        }
    }
}

