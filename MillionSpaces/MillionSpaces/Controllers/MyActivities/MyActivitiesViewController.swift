//
//  MyActivitiesViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class MyActivitiesViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var pastSegmentedButton: UIButton!
    @IBOutlet weak var upcomingSegmentedButton: UIButton!
    @IBOutlet weak var myActivitiesTableView: UITableView!
    @IBOutlet weak var noBookingsMessageLabel: UILabel!
    
    //MARK: - Class variables
    var myactivitiesUpcoming: [Booking] = []
    var myactivitiesPast: [Booking] = []
    let noUpcomingBookingsMessage = "You currently do not have any upcoming bookings."
    let noPastBookingsMessage = "You currently do not have any past bookings."
    var selectedSegment = 2  // 1 - Past / 2 - Upcoming
    
    var selectedBooking: Booking?
    
    var alertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToMyActivitiesFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToMyActivitiesFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) { // ~ 10s
            alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = "Bookings"
        self.noBookingsMessageLabel.isHidden = true
        self.configureSelectedUpcomingSegmentedButton()
        self.retrieveMyActivities()
    }
    
    func showPastBookings() {
        configureSelectedPastSegmentedButton()
        self.selectedSegment = 1
        self.noBookingsMessageLabel.text = noPastBookingsMessage
        if self.myactivitiesPast.count > 0 {
            self.myActivitiesTableView.isHidden = false
            self.noBookingsMessageLabel.isHidden = true
            self.myActivitiesTableView.reloadData()
            self.myActivitiesTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
        else {
            self.myActivitiesTableView.isHidden = true
            self.noBookingsMessageLabel.isHidden = false
        }
    }
    
    func showUpcomingBookings() {
        configureSelectedUpcomingSegmentedButton()
        self.selectedSegment = 2
        self.noBookingsMessageLabel.text = noUpcomingBookingsMessage
        if self.myactivitiesUpcoming.count > 0 {
            self.myActivitiesTableView.isHidden = false
            self.noBookingsMessageLabel.isHidden = true
            self.myActivitiesTableView.reloadData()
            self.myActivitiesTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
        else {
            self.myActivitiesTableView.isHidden = true
            self.noBookingsMessageLabel.isHidden = false
        }
    }
    
    func configureSelectedPastSegmentedButton() {
        self.pastSegmentedButton.isSelected = true
        self.pastSegmentedButton.backgroundColor = ColorUtils.MS_BLUE_COLOR
        self.upcomingSegmentedButton.isSelected = false
        self.upcomingSegmentedButton.backgroundColor = UIColor.white
    }
    
    func configureSelectedUpcomingSegmentedButton() {
        self.pastSegmentedButton.isSelected = false
        self.pastSegmentedButton.backgroundColor = UIColor.white
        self.upcomingSegmentedButton.isSelected = true
        self.upcomingSegmentedButton.backgroundColor = ColorUtils.MS_BLUE_COLOR
    }
    
    func retrieveMyActivities() {
        self.showActivityIndicator()
        
        self.myactivitiesUpcoming.removeAll()
        self.myactivitiesPast.removeAll()
        
        WebServiceCall.retrieveMyActivities(successBlock: { (results: Any) in
            //print("MyActivitiesVC retrieveMyActivities | succeeded | results : \(results)")
            
            if let arrMyBookings = results as? [[String : Any]] {
                
                //for t in arrMyBookings {
                // print("MyActivitiesVC retrieveMyActivities | booking at i | id : \(t["id"]), bookedDate : \(t["bookedDate"])")
                //}
                
                for b in arrMyBookings {
                    let booking = Booking()
                    booking.initBooking(withDictionary: b)
                    
                    if booking.isUpcomingBooking {
                        self.myactivitiesUpcoming.append(booking)
                    }
                    else {
                        self.myactivitiesPast.append(booking)
                    }
                }
                
                DispatchQueue.main.sync(execute: {
                    
                    // TODO: Sort by Booking Dates | Dates to be sorted at the Booking Object creation
                    self.myactivitiesPast.sort(by: { $0.id > $1.id })
                    self.myactivitiesUpcoming.sort(by: { $0.id > $1.id })
                    
                    self.showUpcomingBookings()
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    self.myActivitiesTableView.isHidden = true
                    self.noBookingsMessageLabel.isHidden = false
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("MyActivitiesVC retrieveMyActivities | failure | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.myActivitiesTableView.isHidden = true
                self.noBookingsMessageLabel.isHidden = false
                self.myActivitiesTableView.reloadData()
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpacesReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpacesReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpacesReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func addSpacesReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retrieveMyActivities()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    @objc func goToReviewScreen(_ sender: UIButton) {
        self.selectedBooking = self.myactivitiesPast[Int(sender.accessibilityIdentifier!)!]
        self.performSegue(segueID: "ReviewBookingSegue")
    }
    
    // NOTE: Not in use
    func goToCancelBookingScreen(_ sender: UIButton) {
        self.selectedBooking = self.myactivitiesUpcoming[Int(sender.accessibilityIdentifier!)!]
        self.performSegue(segueID: "CancelBookingSegue")
    }
    
    @objc func handleBookingCancelButtonAction(_ sender: UIButton) {
        let cancellingBookingRow = Int(sender.accessibilityIdentifier!)!
        self.selectedBooking = self.myactivitiesUpcoming[cancellingBookingRow]
        if self.selectedBooking!.bookingReservationStatusId != 1 && self.selectedBooking!.bookingReservationStatusId != 2 {
            self.performSegue(segueID: "CancelBookingSegue")
        }
        else {
            self.presentConfirmationAlertForCancellation(cancellingBookingRow: cancellingBookingRow)
        }
    }
    
    
    // MARK: - IBAction
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func pastSegmentedButtonPressed(_ sender: UIButton) {
        //print("MyActivitiesVC pastSegmentedButtonPressed")
        showPastBookings()
    }
    
    @IBAction func upcomingSegmentedButtonPressed(_ sender: UIButton) {
        //print("MyActivitiesVC upcomingSegmentedButtonPressed")
        showUpcomingBookings()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookingDetailsSegue" {
            let bookingDetailsVC = segue.destination as! BookingDetailsViewController
            bookingDetailsVC.bookingId = self.selectedBooking!.id!
            bookingDetailsVC.isManualBooking = false
            bookingDetailsVC.isViewedAsGuest = true
            bookingDetailsVC.shouldReturnToPreviousScreen = true
        }
        else if segue.identifier == "CancelBookingSegue" {
            let cancelBookingVC = segue.destination as! CancelBookingViewController
            cancelBookingVC.booking = self.selectedBooking!
        }
        else if segue.identifier == "ReviewBookingSegue" {
            let reviewBookingVC = segue.destination as! ReviewViewController
            reviewBookingVC.booking = self.selectedBooking!
        }
    }
}

/*
 parent view 1
 image 2
 space name 3
 address 4
 date 5
 time 6
 b price 7
 status - 8
 button 9
 button H - 10
 */


extension MyActivitiesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedSegment == 1 {
            return self.myactivitiesPast.count
        }
        return self.myactivitiesUpcoming.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let activityCell = tableView.dequeueReusableCell(withIdentifier: "MyActivityCell", for: indexPath)
        
        if let parentView = activityCell.viewWithTag(1) {
            parentView.layer.shadowColor = UIColor.black.cgColor
            parentView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
            parentView.layer.shadowRadius = 1
            parentView.layer.shadowOpacity = 0.5
        }
        
        let bookingAtIndex = (selectedSegment == 1) ? self.myactivitiesPast[indexPath.row] : self.myactivitiesUpcoming[indexPath.row]
        
        (activityCell.viewWithTag(2) as! UIImageView).sd_setImage(with: URL(string: NetworkConfiguration.IMAGE_BASE_URL + bookingAtIndex.spaceImageName!), completed: nil)
        (activityCell.viewWithTag(3) as! UILabel).text = bookingAtIndex.spaceName!
        (activityCell.viewWithTag(4) as! UILabel).text = (bookingAtIndex.spaceAddressLine_2 != nil) ? bookingAtIndex.spaceAddressLine_2! : bookingAtIndex.spaceFullAddress!
        
        (activityCell.viewWithTag(7) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: bookingAtIndex.bookingCharge))"
        
        let statusLabel = activityCell.viewWithTag(8) as! UILabel
        statusLabel.text = bookingAtIndex.bookingReservationStatusDisplayName
        statusLabel.backgroundColor = ColorUtils.BOOKING_RESERVATION_STATUS_COLORS_BY_CODE[bookingAtIndex.bookingReservationStatusId]!
        
        let button = activityCell.viewWithTag(9) as! UIButton
        button.accessibilityIdentifier = "\(indexPath.row)"
        
        if selectedSegment == 1 {
            button.removeTarget(nil, action: nil, for: .allEvents)
            button.setTitle("Review", for: .normal)
            button.setTitleColor(ColorUtils.MS_BLUE_COLOR, for: .normal)
            button.borderColor = ColorUtils.MS_BLUE_COLOR
            button.addTarget(self, action: #selector(goToReviewScreen(_:)), for: .touchUpInside)
            
            for c in button.constraints {
                if c.identifier == "10" {
                    if !bookingAtIndex.isReviewed && StaticData.REVIEWABLE_BOOKING_RESERVATION_STATUS_RANGE.contains(bookingAtIndex.bookingReservationStatusId) {
                        c.constant = 30.0
                    }
                    else {
                        c.constant = 0.0
                    }
                }
            }
        }
        else {
            button.removeTarget(nil, action: nil, for: .allEvents)
            button.setTitle("Cancel", for: .normal)
            button.setTitleColor(ColorUtils.BOOKING_RESERVATION_STATUS_COLORS.CANCELLED, for: .normal)
            button.borderColor = ColorUtils.BOOKING_RESERVATION_STATUS_COLORS.CANCELLED
            
            button.addTarget(self, action: #selector(handleBookingCancelButtonAction(_:)), for: .touchUpInside)
            
            for c in button.constraints {
                if c.identifier == "10" {
                    if StaticData.CANCELABLE_BOOKING_RESERVATION_STATUS_RANGE.contains(bookingAtIndex.bookingReservationStatusId) {
                        c.constant = 30.0
                    }
                    else {
                        c.constant = 0.0
                    }
                }
            }
        }
        
        let dateLabel = activityCell.viewWithTag(5) as! UILabel
        let timeLabel = activityCell.viewWithTag(6) as! UILabel
        
        if bookingAtIndex.bookingDates.count > 0 {
            dateLabel.text = CalendarUtils.retrieveSimpleDateString_2(fromDateStr: bookingAtIndex.bookingDates[0]["fromDate"]![0])
            timeLabel.text = "\(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: bookingAtIndex.bookingDates[0]["fromDate"]![1], isWithSeconds: false)) - \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: bookingAtIndex.bookingDates[0]["toDate"]![1], isWithSeconds: false))"
            if bookingAtIndex.hasMultipleBlocks { timeLabel.text?.append(" ✚") }
        }
        else {
            dateLabel.text = "N/A"
            timeLabel.text = "N/A"
        }
        
        return activityCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedBooking = (selectedSegment == 1) ? self.myactivitiesPast[indexPath.row] : self.myactivitiesUpcoming[indexPath.row]
        self.performSegue(segueID: "BookingDetailsSegue")
    }
}



// NOTE / TODO : Code repetition / To be removed
extension MyActivitiesViewController {
    
    func cancelBooking(cancellingBookingRow row: Int) {
        self.showActivityIndicator()
        
        WebServiceCall.cancelGuestMSBooking(forBookingID: self.selectedBooking!.id!, withRefundableAmount: 0, successBlock: { (result: Any) in
            print("CancelBookingVC cancelBooking | result : \(result)")
            
            if let response = result as? [String : Any], response["new_state"] as? String == "CANCELLED" {
                self.presentResponseAlert(isSuccessful: true, cancellingBookingRow: row)
            }
            else {
                self.presentResponseAlert(isSuccessful: false, cancellingBookingRow: nil)
            }
            
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("CancelBookingVC cancelBooking | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func presentConfirmationAlertForCancellation(cancellingBookingRow row: Int) {
        alertController = UIAlertController(title: AlertMessages.CONFIRM_TITLE, message: AlertMessages.CANCEL_BOOKING_CONFIRMATION_MESSAGE, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertMessages.YES_ACTION, style: .default) { (alert: UIAlertAction) in
            self.cancelBooking(cancellingBookingRow: row)
        }
        let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
        alertController!.addAction(noAction)
        alertController!.addAction(yesAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func presentResponseAlert(isSuccessful: Bool, cancellingBookingRow row: Int?) {
        if isSuccessful {
            alertController = UIAlertController(title: AlertMessages.SUCCESS_TITLE, message: AlertMessages.CANCEL_BOOKING_SUCCESS_MESSAGE, preferredStyle: .alert)
            let dissmissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default) { (alert: UIAlertAction) in
                self.selectedBooking!.bookingReservationStatusId = 4
                self.selectedBooking!.bookingReservationStatusDisplayName = StaticData.RESERVATION_STATUS[4]!
                self.myActivitiesTableView.reloadRows(at: [IndexPath.init(row: row!, section: 0)], with: .middle)
            }
            alertController!.addAction(dissmissAction)
        }
        else {
            alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.CANCEL_BOOKING_ERROR_MESSAGE, preferredStyle: .alert)
            let dissmissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
            alertController!.addAction(dissmissAction)
        }
        self.present(alertController!, animated: true, completion: nil)
    }
}


