//
//  SpaceAvailabilityModule.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceAvailabilityModule: NSObject {
    
    // Applicable for HOUR_BASE and BLOCK_BASE Availability Modules
    
    var id: Int!
    var spaceId: Int!
    var day: Int!
    var dayDisplayName: String!
    var timeFrom: String!
    var timeTo: String!
    var charge: Int!
    
    var isReimbursable: Bool = false
    var blockMenus: [SpaceMenu] = []
    
    
    func initSpaceAvailabilityModule(withDictionary dic: [String : Any]) {
        self.id = dic["id"] as! Int
        self.spaceId = dic["space"] as! Int
        self.day = dic["day"] as! Int
        self.dayDisplayName = StaticData.getDayOfWeek(dayIndex: dic["day"] as! Int)
        self.timeFrom = dic["from"] as! String
        self.timeTo = dic["to"] as! String
        self.charge = dic["charge"] as! Int
        
        if let r = dic["isReimbursable"] as? Int {
            self.isReimbursable = Bool(r as NSNumber)
        }
        
        if let menus = dic["menuFiles"] as? [[String : Any]] {
            for i in menus {
                let menu = SpaceMenu()
                menu.initSpaceMenu(withDictionary: i)
                self.blockMenus.append(menu)
            }
        }
    }
}
