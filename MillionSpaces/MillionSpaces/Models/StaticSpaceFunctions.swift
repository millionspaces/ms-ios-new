//
//  StaticSpaceFunctions.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class StaticSpaceFunctions: NSObject {
    
    static func prepareBlockPricesForSpaceDetailsScreen(spaceAvailability: [SpaceAvailabilityModule], blockChargeType: Int) -> [[String : String]] {
        
        let availability = spaceAvailability.sorted(by: { $0.day < $1.day })
        
        var returnDictionary: [String : [String : String]] = [:]
        
        if blockChargeType == 2 { // ----> Per Guest (w/ Menu)
            
            for a in availability {
                
                let key = "\(a.timeFrom!)-\(a.timeTo!)-\(a.charge!)-\(a.blockMenus[0].menuName!)"
                
                if let valueForKey = returnDictionary[key] {
                    let daysValue = valueForKey["days"]!
                    returnDictionary[key]!["days"] = "\(daysValue), \(a.dayDisplayName!)"
                }
                else {
                    let newEntry = ["time" : "\(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: a.timeFrom!, isWithSeconds: true)) - \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: a.timeTo!, isWithSeconds: true))", "rate" : "LKR \(Utils.formatPriceValues(price: a.charge!))", "days" : a.dayDisplayName!, "menu" : (a.blockMenus[0]).menuName!]
                    
                    returnDictionary[key] = newEntry
                }
            }
        }
        else { // blockChargeType == 1 ---> Space Only
            
            for a in availability {
                
                let key = "\(a.timeFrom!)-\(a.timeTo!)-\(a.charge!)"
                
                if let valueForKey = returnDictionary[key] {
                    let daysValue = valueForKey["days"]!
                    returnDictionary[key]!["days"] = "\(daysValue), \(a.dayDisplayName!)"
                }
                else {
                    let newEntry = ["time" : "\(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: a.timeFrom!, isWithSeconds: true)) - \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: a.timeTo!, isWithSeconds: true))", "rate" : "LKR \(Utils.formatPriceValues(price: a.charge!))", "days" : a.dayDisplayName!]
                    
                    returnDictionary[key] = newEntry
                }
            }
        }
        
        var returnAvailabilityArray: [[String : String]] = []
        
        for k in returnDictionary.keys {
            returnAvailabilityArray.append(returnDictionary[k]!)
        }
        
        return returnAvailabilityArray
    }
    
    
    static func retrieveSpaceAvailabilityModules(forSpaceCalendarDateMSDayOfWeek dayOfWeekMS: Int, availabilityMethod: Int, availability: [SpaceAvailabilityModule]) -> [SpaceAvailabilityModule] {
        
        var returnArray: [SpaceAvailabilityModule] = []
        
        if availabilityMethod == 1 { // Hour Basis
            if let i = availability.index(where: { $0.day == dayOfWeekMS }) {
                returnArray.append(availability[i])
            }
            return returnArray
        }
        else { // Block Basis
            for i in availability {
                if i.day == dayOfWeekMS {
                    returnArray.append(i)
                }
                else if i.day == 8 && (1 ... 5).contains(dayOfWeekMS) {
                    returnArray.append(i)
                }
            }
            return returnArray
        }
    }
    
    
    static func retrieveHoursForPerHourBasis(forAvailabilityForDate availability: [SpaceAvailabilityModule]) -> [SpaceCalendarTimeByHour] {
        let perhourAvailabilityForDate = availability[0]
        let startTimeHourInt = CalendarUtils.retrieve24HourIntComponent(fromTimeStr: perhourAvailabilityForDate.timeFrom)
        var endTimeHourInt = CalendarUtils.retrieve24HourIntComponent(fromTimeStr: perhourAvailabilityForDate.timeTo)
        if endTimeHourInt == 0 { endTimeHourInt = 24 }
        
        var returnArray: [SpaceCalendarTimeByHour] = []
        
        for i in startTimeHourInt...endTimeHourInt {
            let time = SpaceCalendarTimeByHour()
            time.initSpaceCalendarTimeByHour(fromHour24Value: i, withHourlyPrice: perhourAvailabilityForDate.charge)
            if i == endTimeHourInt { time.isHourAvailable = false }
            returnArray.append(time)
        }
        
        return returnArray
    }
    
    
    static func retrieveDayHoursForHostCalendar() -> [SpaceCalendarTimeByHour] {
        let startTimeHourInt = 0
        let endTimeHourInt = 24
        
        var returnArray: [SpaceCalendarTimeByHour] = []
        
        for i in startTimeHourInt...endTimeHourInt {
            let time = SpaceCalendarTimeByHour()
            time.initSpaceCalendarTimeByHour(fromHour24Value: i, withHourlyPrice: 0)
            if i == endTimeHourInt { time.isHourAvailable = false }
            returnArray.append(time)
        }
        
        return returnArray
    }
    
    static func retrieveHoursInBlock(forAvailabilityModuleBlock block: SpaceAvailabilityModule) -> [Int] {
        
        let startTimeHourInt = CalendarUtils.retrieve24HourIntComponent(fromTimeStr: block.timeFrom)
        var endTimeHourInt = CalendarUtils.retrieve24HourIntComponent(fromTimeStr: block.timeTo)
        if endTimeHourInt == 0 { endTimeHourInt = 24 }
        
        var returnIntArray: [Int] = []
        for i in startTimeHourInt..<endTimeHourInt {
            returnIntArray.append(i)
        }
        
        return returnIntArray
    }
    
    
    static func updateOverlappingBlocksForTheBlock(block: SpaceCalendarTimeByBlock, blocksForDate: [SpaceCalendarTimeByBlock]) -> [Int] {
        
        var overlappingBlockIndexes: [Int] = []
        
        for i in 0..<blocksForDate.count {
            let blockAtI = blocksForDate[i]
            if block != blockAtI {
                if blockAtI.hoursInBlock.contains(block.startTimeHour24H) || block.hoursInBlock.contains(blockAtI.startTimeHour24H) {
                    overlappingBlockIndexes.append(i)
                }
            }
        }
        
        return overlappingBlockIndexes
    }
}
