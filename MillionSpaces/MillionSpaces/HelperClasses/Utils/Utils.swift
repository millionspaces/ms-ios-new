//
//  Utils.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/2/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import CryptoSwift

class Utils: NSObject {

    // MARK: - Constants
    
    static let SCREEN_WIDTH: CGFloat = UIScreen.main.bounds.width
    static let SCREEN_HEIGHT: CGFloat = UIScreen.main.bounds.height
    
    static let MAIN_SPACE_CHARGE_TYPES = (Hour: "HOUR_BASE", Block: "BLOCK_BASE")
    
    
    
    
    
    
    
    
    static func setLabelText(forCollectionViewCell cell: UICollectionViewCell, withTag tag:Int, andText text: Any){
        if let label = cell.viewWithTag(tag) {
            (label as! UILabel).text = text as? String;
        }
    }
    
    static func setLabelText(forTableViewCell cell: UITableViewCell, withTag tag:Int, andText text: Any){
        if let label = cell.viewWithTag(tag) {
            (label as! UILabel).text = text as? String;
        }
    }
    
    static func setImageViewImage(forCollectionViewCell cell: UICollectionViewCell, withTag tag:Int, andImageUrl imageUrlString: Any) {
        if let imagePathString = imageUrlString as? String {
            (cell.viewWithTag(tag) as! UIImageView).imageFromServerURL(urlString: imagePathString)
        }
    }
    
    static func setImageViewImage(forTableViewCell cell: UITableViewCell,  withTag imageViewTag:Int, andImageUrl imageUrlString: Any) {
        if let imagePathString = imageUrlString as? String {
            (cell.viewWithTag(imageViewTag) as! UIImageView).imageFromServerURL(urlString: imagePathString)
        }
    }
    
    static func setImageViewImage(forImageView: UIImageView, withImageUrl: String) {
        (forImageView).imageFromServerURL(urlString: withImageUrl)
    }
    
    
    
    static func areTwoArraysEqual(filteredData: [NSDictionary], originalData: [NSDictionary]) -> Bool {
        return false
    }
    
    static func getDictionaryValueForKey(_ data: Any?, _ forKey: String?) -> String {
        if let dictionary = data as? NSDictionary, let key = forKey {
            if let value = dictionary[key] as? String{
                return value
            }
        }
        return "[Not Available]"
    }
    
    static func getDictionaryIntValueForKey(_ data: Any?, _ forKey: String?) -> Int {
        if let dictionary = data as? NSDictionary, let key = forKey {
            if let value = dictionary[key] as? Int{
                return value
            }
        }
        return 0
    }
    
    // Price values formatter   2000 --> 2,000
    static func formatPriceValues(price: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value: price))!
    }
    
    // Price values formatter   2000.0 --> 2,000.00
    static func formatFloatPriceValues(price: Float) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        numberFormatter.currencySymbol = ""
        return numberFormatter.string(from: NSNumber(value: price))!
    }
    
    // Vertical CollectionView Embedded TableViewCell Height - For Two CollectionView Cells in a single row
    static func calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: Int, cvCellHeight: Int, itemsPerRow: Int) -> CGFloat {
        return dataCount % itemsPerRow == 0 ? CGFloat((dataCount / itemsPerRow) * cvCellHeight) : CGFloat(((dataCount / itemsPerRow) + 1) * cvCellHeight)
    }
    
    
    // IPG Signature
    static func prepareIpgSignature(input: String) -> String {
        let input_sha1 = input.sha1() as NSString
        var i = 0
        let limit = input_sha1.length
        var output = ""
        
        while i < limit {
            let range = NSMakeRange(i, 2)
            let str = input_sha1.substring(with: range)
            if let num = Int(str, radix: 16) {
                output.append(Character(UnicodeScalar(num)!))
            }
            i += 2
        }
        
        let finalOutput = output.StringToBase64Encorded()!
        return finalOutput
    }
    
    // IPG Amount Validation
    static func validatePurchaceAmount(amount: Double) -> String! {
        let roundedAmount = String(format: "%.2f", amount)
        let numberStr = roundedAmount.replacingOccurrences(of: ".", with: "")
        let length = numberStr.count
        
        if length <= 12 {
            var output: String = ""
            for _ in 0..<(12-length) {
                output += "0"
            }
            output += numberStr
            return output
        }
        else{
            return "Invalid"
        }
    }
    
    // Check if input contains only letters and [space], [.] & [-]
    static func containsOnlyLetters(input: String) -> Bool {
        var characterset = CharacterSet.letters
        characterset.insert(charactersIn: " .-")
        if input.rangeOfCharacter(from: characterset.inverted) != nil {
            return false
        }
        return true
    }
    
    // Check for a valid email
    static func isValidEmailAddress(emailEntry: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailEntry)
    }
    
    // Check if String is numeric
    static func isStringNumeric(input: String) -> Bool {
        return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: input))
    }
    
    static func getEventTypeIDs(eventTypes: [EventType]) -> [Int] {
        var _eventTypes: [Int] = []
        for et in eventTypes {
            _eventTypes.append(Int(et.id))
        }
        return _eventTypes
    }
    
    static func didPassDuration(durationInHours hours: Double) -> Bool {
        if let backgroundEnteredTimestamp = Constants.USER_DEFAULTS.object(forKey: Constants.APPLICATION_DID_ENTER_BACKGROUND_TIMESTAMP) as? TimeInterval {
            let durationInSeconds = 3600.0 * hours
            let currentTimestamp = Date().timeIntervalSince1970
            if currentTimestamp - backgroundEnteredTimestamp >= durationInSeconds {
                return true
            }
            else {
                return false
            }
        }
        return false
    }

}



extension String {
    func StringToBase64Encorded() -> String? {
        if let data = self.data(using: .isoLatin1) {
            return data.base64EncodedString()
        }
        return nil
    }
}

extension UITextField {
    
    public func setTextFieldPlaceholder(withPlaceholderText text: String, andPlaceholderTextColor color: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
}

extension UIImageView {
    public func imageFromServerURL(urlString: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "error: default value")
                return
            }
            DispatchQueue.main.sync(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
        }).resume()
    }
}

