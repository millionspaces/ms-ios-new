//
//  SpaceCalendarTimeByHour.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/6/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class SpaceCalendarTimeByHour: NSObject {
    
    var timeHour24H: Int!
    var time: String!
    var timeDisplayName: String!
    var perHourPrice: Int!
    
    var isHourInNoticePeriod: Bool!
    var isHourAvailable: Bool!
    
    
    
    func initSpaceCalendarTimeByHour(fromHour24Value hour: Int, withHourlyPrice price: Int) { //, isAvailable: Bool
        // From a int such as 9 or 18
        self.timeHour24H = hour
        let time24Str = "\(hour):00:00"
        self.time = time24Str
        self.timeDisplayName = CalendarUtils.convertTo12HourTime(fromTwentyFourTime: time24Str, isWithSeconds: true)
        
        self.perHourPrice = price
        
        
        // Hour in Notice Period is initially set to false initially
        self.isHourInNoticePeriod = false
        
        self.isHourAvailable = true // NOTE: Initially Set to true
    }
    
}
