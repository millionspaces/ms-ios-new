//
//  NetworkConfiguration.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/2/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class NetworkConfiguration: NSObject {
    
    // ================================= BASE URLs ===========================================
    
    static let AVAILABLE_ENVIRONMENTS = (QA: "qapi" , Live: "api")
    //static let OLD_AVAILABLE_ENVIRONMENTS = (Dev: "dapi", QA: "qapi", Training: "tapi", Beta: "bapi", Live: "api")
    
    static let IMAGE_BASE_URL_OPTIONS = (QA: "https://res.cloudinary.com/dz4u73trs/image/upload/c_scale,w_600,q_75/millionspaces/", Live: "https://res.cloudinary.com/dgcojyezg/image/upload/c_scale,w_600,q_75/millionspaces/")
    //static let OLD_IMAGE_BASE_URL_OPTIONS = (Dev: "https://res.cloudinary.com/dgcojyezg/image/upload/development/", Other: "https://res.cloudinary.com/dgcojyezg/image/upload/millionspaces/")
    
    static let AVAILABLE_MAIN_WEBSITE_DOMAINS = (QA: "https://qae.millionspaces.com", Live: "https://www.millionspaces.com")
    
    
    
    // NOTE: The API environment the app is currently running on and the corresponding web url
    // Change accordingly when deployed for different environments
    // ==========================================================
    static let API_ENVIRONMENT = AVAILABLE_ENVIRONMENTS.Live
    // ==========================================================
    static let MAIN_WEBSITE_DOMAIN_FOR_API_ENVIRONMENT = AVAILABLE_MAIN_WEBSITE_DOMAINS.Live
    // ==========================================================
    
    
    
    static let API_ENVIRONMENT_DISPLAY_NAME = getAPIEnvironmentDisplayName()
    
    // NOTE: The Base and Image URLs set based on currently used API environment
    static let BASE_URL = "https://" + API_ENVIRONMENT + ".millionspaces.com"
    static let IMAGE_BASE_URL = (API_ENVIRONMENT == AVAILABLE_ENVIRONMENTS.QA) ? IMAGE_BASE_URL_OPTIONS.QA : IMAGE_BASE_URL_OPTIONS.Live
    
    
    private static func getAPIEnvironmentDisplayName() -> String {
        switch API_ENVIRONMENT {
        //case AVAILABLE_ENVIRONMENTS.Dev: return "Development"
        case AVAILABLE_ENVIRONMENTS.QA: return "QA"
            //case AVAILABLE_ENVIRONMENTS.Training: return "Training"
        //case AVAILABLE_ENVIRONMENTS.Beta: return "Beta"
        case AVAILABLE_ENVIRONMENTS.Live: return "Live"
        default: return ""
        }
    }
    
    
    // =============================== Authenticate ================================
    
    // POST - Authenticate
    static let MS_AUTHENTICATE = BASE_URL + "/api/grant_eventspace_security"
    
    
    
    // =============================== Authenticate ================================
    
    // POST - Create User | PUT - Update User | GET - User Details
    static let USER = BASE_URL + "/api/user"
    
    // POST - Check Email for existing user
    static let CHECK_EMAIL = BASE_URL + "/api/user/emailcheck"
    
    // POST - Verify Email for sign up
    static let VERIFY_EMAIL = BASE_URL + "/api/user/verify"
    
    
    
    // ================================== Spaces ====================================
    
    // GET - Spaces
    static let SPACES = BASE_URL + "/api/space"
    
    // GET - Similar Spaces
    static let SIMILAR_SPACES = BASE_URL + "/api/similarSpaces/space/"
    
    // POST - Spaces advance search with pagination
    static let ADVANCE_SEARCH_SPACES = BASE_URL + "/api/space/asearch/page/"
    
    // GET - User Spaces
    static let MY_SPACES = BASE_URL + "/api/user/spaces"
    
    // GET - Space Calendar Details
    static let GUEST_CALENDAR_DETAILS = BASE_URL + "/api/calendarDetails/space/"
    
    // GET - Space Host Calendar Details
    static let HOST_CALENDAR_DETAILS = BASE_URL + "/api/calendarDetails/host/space/"
    
    
    
    // =================================== Booking ===================================
    
    // POST Send Phone Number to Backend
    static let BOOKING_MOBILE_NUMBER = BASE_URL + "/api/user/mobile/v1"
    
    // POST Verify Promo Code
    static let VERIFY_PROMO_CODE = BASE_URL + "/api/space/promo"
    
    // POST - Book Space as Guest | PUT - Manual Payment / Tentative Reservation | PUT - Cancel Payment as Guest
    static let BOOK_SPACE = BASE_URL + "/api/book/space"
    
    // GET - My Bookings (Guest)
    static let BOOKINGS_MY_ACTIVITIES = BASE_URL + "/api/book/spaces/guest"
    
    // GET - Bookings of My Space by Space ID (Host)
    static let BOOKINGS_MY_SPACE = BASE_URL + "/api/book/spaces/space/"
    
    // POST - Booking by ID - Guest or Manual - For Booking Details Screen
    static let BOOKING_BY_ID = BASE_URL + "/api/book/spaces"
    
    // POST - Create Manual Booking | PUT - Update Manual Booking
    // DELETE - Delete Manual Booking | GET - Manual Booking Details for Manual Booking Screen ====> /{id}
    
    static let MANUAL_BOOKING = BASE_URL + "/api/book/manual"
    
    /*
     {
     "space" : 21,
     "fromDate": "2015-09-25T12:00Z",
     "toDate"   : "2015-09-25T13:00Z",
     "dateBookingMade": "2015-09-25T13:00",
     "cost": "100.00",
     "note": "note",
     "guestContactNumber": "111111",
     "guestEmail": "striemailng",
     "guestName": "name",
     "noOfGuests": "100",
     "eventTitle": "title",
     "eventTypeId": 1,
     "extrasRequested": "extra",
     "seatingArrangementId": "1",
     }
     */
    
    // POST - Add Review to Booking/Space
    static let ADD_BOOKING_REVIEW = BASE_URL + "/api/book/addReview"
    
    // PUT - Add Remark for Booking as Host | For Manual or Guest Bookings
    static let ADD_BOOKING_REMARK = BASE_URL + "/api/book/addRemark"
    
    // GET - Refund amount for Booking Cancellation
    static let REFUND_FOR_BOOKING_CANCELLATION = BASE_URL + "/api/book/refund/"
    
    
    // =================================== Commons ====================================
    
    // GET All Amenities
    static let ALL_AMENITIES = BASE_URL + "/api/common/amenities"
    
    // GET All Amenity Units
    static let AMENITY_UNITS = BASE_URL + "/api/common/amenityUnits"
    
    // GET All Event Types
    static let ALL_EVENT_TYPES = BASE_URL + "/api/common/eventTypes"
    
    // GET All Sub Event Types
    static let ALL_SUB_EVENT_TYPES = BASE_URL + "/api/common/eventSubTypes"
    
    // GET Cancellation Policies
    static let CANCELLATION_POLICIES = BASE_URL + "/api/common/cancellationPolicies"
    
    // GET Space Rules
    static let SPACE_RULES = BASE_URL + "/api/common/rules"
    
    // GET Seating Arrangements
    static let SEATING_ARRANGEMENTS = BASE_URL + "/api/common/seatingArrangement"
    
    // GET Measurement Units
    static let MEASUREMENT_UNITS = BASE_URL + "/api/common/measurementUnit"
    
    
    
    // =============================== Payment / IPG =====================================
    
    // POST IPG
    static let IPG = "https://www.hnbpg.hnb.lk/SENTRY/PaymentGateway/Application/ReDirectLink.aspx"
    
    static let IPG_CALLBACK =  BASE_URL + "/api/payment/mobileCallback"
    // web --> callback | mobile --> mobileCallback
    
    static let Version = "1.0.0"
    static let PASSWORD = "j9vA4Z05"
    static let MerID = "14481900"
    static let AcqID = "415738"
    static let PurchaseCurrency = "144"
    static let PurchaseCurrencyExponent = "2"
    static let SENTRYORD = "SENTRYORD"
    static let SignatureMethod = "SHA1"
    static let CaptureFlag = (API_ENVIRONMENT == AVAILABLE_ENVIRONMENTS.Live) ? "A" : "M"
    //
    static let MerRespURL = "MerRespURL" //
    static let Signature = "Signature" //
    static let PurchaseAmt = "PurchaseAmt" //
    
    
    // =============================== Payment NEW =====================================
    
    static let PAYMENT_WEB_URL = "https://qpayment.millionspaces.com/api/payment"
    
    
    
    // =========================== Terms / Privacy Policy URL ==================================
    
    static let TERMS_PRIVACY_URL = "https://www.millionspaces.com/#/terms/privacy"
    
    
}
