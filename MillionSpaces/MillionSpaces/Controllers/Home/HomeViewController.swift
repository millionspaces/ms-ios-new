//
//  HomeViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import GooglePlaces

class HomeViewController: BaseViewController, UIGestureRecognizerDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var dimmedBackgroundLayerView: UIView!
    @IBOutlet weak var eventTypeTableView: UITableView!
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var constMenuViewLeading: NSLayoutConstraint!
    @IBOutlet weak var constMenuViewWidth: NSLayoutConstraint!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileArrowImageView: UIImageView!
    
    
    // MARK: - Constants
    
    let HOME_TABLE_VIEW_CELLS = (meetings: 0, partyAndPhotoshoots: 1, weddings: 2, interviews: 3)
    let HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS = (image: 1, label: 2, labelBackground: 3)
    let HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS = (imageOne: 1, labelOne: 2, labelOneBackground: 3, imageTwo: 4, labelTwo: 5, labelTwoBackground: 6)
    let HOME_TABLE_VIEW_CELL_TYPE_TWO_BUTTON_TAGS = (party: 7, photoshoots: 8)
    let HOME_TABLE_VIEW_HEADER_VIEW_TAGS = (titleView: 1, searchField: 2, searchButton: 3, searchView: 4,  eventTypeFieldButton: 5, locationFieldButton: 6, eventTypeTextField: 7, locationTextField: 8)
    let HOME_TABLE_VIEW_HEADER_HEIGHTS = (shrinked: 168, expanded: 268)
    let HOME_SCREEN_MAIN_EVENT_TYPE_IDS = (meetings: 4, party: 1, photoshoot: 7, wedding: 2, interviews: 5)
    let EVENT_TYPE_FILTER_TABLE_VIEW_CELL_TAGS = (imageView: 1, label: 2)
    
    let HOME_TABLE_VIEW_HEADER_IMAGE_CELL_ID = "HeaderImageCell"
    let HOME_TABLE_VIEW_HEADER_CELL_ID = "SearchHeaderCell"
    let HOME_TABLE_VIEW_CELL_TYPE_ONE_ID = "MainEventTypeOne"
    let HOME_TABLE_VIEW_CELL_TYPE_TWO_ID = "MainEventTypeTwo"
    let EVENT_TYPE_FILTER_TABLE_VIEW_CELL_ID = "EventTypeCell"
    let MENU_TABLE_VIEW_CELL_ID = "MenuOptionCell"
    
    
    // MARK: - Class Properties / Variables
    
    var leftSwipeForMenu: UISwipeGestureRecognizer!
    var rightSwipeForMenu: UISwipeGestureRecognizer!
    
    var isMenuVisible: Bool!
    var menuOptions: NSMutableArray = []
    var eventTypes = [EventType]()
    var selectedEventType: EventType?
    var selectedLocation: GMSPlace?
    var isLocationSelectorOpen: Bool = false
    var searchHeaderHeight: Int!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = Strings.MS_TITLE
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToHomeFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        if !self.isLocationSelectorOpen {
            configureView()
        }
        else {
            self.homeTableView.reloadData()
            addMenuSwipeGestures()
            self.isLocationSelectorOpen = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.dimmedBackgroundLayerView.isHidden = true
        handleEventTypeTableViewVisibility(isHidden: true)
        removeMenuSwipeGestures()
        handleMenuViewVisibility(isVisible: false)
    }
    
    @objc func returnedToHomeFromBackground() {
        configureView()
    }
    
    
    // MARK: - The following is needed for the swipe from Left gesture to go to previous screen in Navigaion Controller stack
    /*
     func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
     return true
     }*/
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            return false
        }
        return true
    }
    
    private func configureView() {
        self.dimmedBackgroundLayerView.isHidden = true
        self.eventTypeTableView.isHidden = true
        self.constMenuViewLeading.constant = -(UIScreen.main.bounds.width)
        self.isMenuVisible = false
        self.searchHeaderHeight = self.HOME_TABLE_VIEW_HEADER_HEIGHTS.shrinked
        self.homeTableView.isExclusiveTouch = false
        self.selectedEventType = nil
        self.selectedLocation = nil
        self.isLocationSelectorOpen = false
        self.eventTypes = MetaData.retrieveEventTypes() as! [EventType]
        
        FilterManager.resetFilters()
        
        configureHomeMenu()
        
        self.homeTableView.reloadData()
        self.homeTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: true)
        self.eventTypeTableView.reloadData()
        
        self.appVersionLabel.text = StaticData.APP_VERSION
    }
    
    
    @objc func tableViewHeaderButtonPressed(_ button: UIButton) {
        if button.tag == self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.titleView {
            self.searchHeaderHeight = self.HOME_TABLE_VIEW_HEADER_HEIGHTS.shrinked
            self.homeTableView.reloadData()
        }
        else if button.tag == self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.searchField {
            self.searchHeaderHeight = self.HOME_TABLE_VIEW_HEADER_HEIGHTS.expanded
            self.homeTableView.reloadData()
        }
        else if button.tag == self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.searchButton {
            if let et = self.selectedEventType {
                FilterManager.spaceFilter.eventTypes.append(et)
            }
            if let location = self.selectedLocation {
                FilterManager.spaceFilter.location = location
            }
            performSegue(segueID: Constants.SEGUE_TO_SPACES_SCREEN)
        }
        else if button.tag == self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.eventTypeFieldButton {
            handleEventTypeTableViewVisibility(isHidden: false)
        }
        else if button.tag == self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.locationFieldButton {
            let autocompleteController = CustomGoogleAutocompleteViewController()
            autocompleteController.delegate = self
            let autocompleteFilter = GMSAutocompleteFilter()
            autocompleteFilter.country = "LK"
            autocompleteController.autocompleteFilter = autocompleteFilter
            present(autocompleteController, animated: true, completion: nil)
            self.isLocationSelectorOpen = true
        }
    }
    
    @objc func tapHandlerForEventType(_ tap: UITapGestureRecognizer) {
        if tap.view?.tag == self.HOME_TABLE_VIEW_CELL_TYPE_TWO_BUTTON_TAGS.party {
            if let et = MetaData.retrieveEventType(with: self.HOME_SCREEN_MAIN_EVENT_TYPE_IDS.party) {
                FilterManager.spaceFilter.eventTypes.append(et)
            }
        }
        else if tap.view?.tag == self.HOME_TABLE_VIEW_CELL_TYPE_TWO_BUTTON_TAGS.photoshoots {
            if let et = MetaData.retrieveEventType(with: self.HOME_SCREEN_MAIN_EVENT_TYPE_IDS.photoshoot) {
                FilterManager.spaceFilter.eventTypes.append(et)
            }
        }
        
        performSegue(segueID: Constants.SEGUE_TO_SPACES_SCREEN)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if dimmedBackgroundLayerView.frame.contains(location) {
            if self.isMenuVisible {
                handleMenuViewVisibility(isVisible: false)
            }
            else {
                handleEventTypeTableViewVisibility(isHidden: true)
            }
        }
    }
    
    func handleEventTypeTableViewVisibility(isHidden: Bool) {
        if isHidden {
            addMenuSwipeGestures()
            self.homeTableView.reloadData()
        }
        else {
            removeMenuSwipeGestures()
        }
        
        UIView.transition(with: self.dimmedBackgroundLayerView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.dimmedBackgroundLayerView.isHidden = isHidden
            self.eventTypeTableView.isHidden = isHidden
        }, completion: nil)
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.eventTypeTableView.isHidden = true
        if self.isMenuVisible {
            handleMenuViewVisibility(isVisible: false)
        }
        else {
            handleMenuViewVisibility(isVisible: true)
        }
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        openNotificationViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



// MARK: - Handle Home Menu
extension HomeViewController {
    
    func configureHomeMenu() {
        
        self.constMenuViewWidth.constant = UIScreen.main.bounds.width - 50.0
        
        // Adding Swipe Gesture to Menu
        leftSwipeForMenu = UISwipeGestureRecognizer.init(target: self, action: #selector(handleSwipeGesture(_:)))
        rightSwipeForMenu = UISwipeGestureRecognizer.init(target: self, action: #selector(handleSwipeGesture(_:)))
        leftSwipeForMenu.direction = .left
        rightSwipeForMenu.direction = .right
        addMenuSwipeGestures()
        
        // Setting Menu Options
        menuOptions.removeAllObjects()
        
        if !AuthenticationManager.isUserAuthenticated {
            self.userProfileImageView.image = #imageLiteral(resourceName: "img-app-logo-small-tb")
            self.userNameLabel.text = Strings.SIGN_IN_UP
            self.profileArrowImageView.isHidden = false
        }
        else {
            self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.bounds.width/2
            self.userProfileImageView.layer.masksToBounds = true
            
            if let user = AuthenticationManager.user {
                if let userRole = user.role {
                    if userRole == "USER" {
                        self.userNameLabel.text = user.name
                        if let imgUrl = user.profileImageUrl {
                            self.userProfileImageView.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "img-guest-default"))
                        }
                        else {
                            self.userProfileImageView.image = #imageLiteral(resourceName: "img-guest-default")
                        }
                        
                        menuOptions.addObjects(from: StaticData.HOME_MENU_OPTIONS_ARRAY as! [Any])
                        self.profileArrowImageView.isHidden = false
                    }
                    else {
                        self.userNameLabel.text = user.email
                        menuOptions.addObjects(from: StaticData.HOME_MENU_OPTIONS_ARRAY_FOR_GUEST as! [Any])
                        self.profileArrowImageView.isHidden = true
                    }
                }
            }
        }
        
        self.menuTableView.reloadData()
    }
    
    func addMenuSwipeGestures() {
        self.view.addGestureRecognizer(leftSwipeForMenu)
        self.view.addGestureRecognizer(rightSwipeForMenu)
    }
    
    func removeMenuSwipeGestures() {
        self.view.removeGestureRecognizer(leftSwipeForMenu)
        self.view.removeGestureRecognizer(rightSwipeForMenu)
    }
    
    @objc func handleSwipeGesture(_ swipe: UISwipeGestureRecognizer) {
        if swipe.direction == .left {
            handleMenuViewVisibility(isVisible: false)
        }
        else if swipe.direction == .right {
            handleMenuViewVisibility(isVisible: true)
        }
    }
    
    func handleMenuViewVisibility(isVisible: Bool) {
        self.isMenuVisible = isVisible
        
        if isVisible {
            self.constMenuViewLeading.constant = 0
        }
        else {
            self.constMenuViewLeading.constant = -(UIScreen.main.bounds.width)
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        handleDimmedBackgroundLayerViewVisibilityForMenu(isHidden: !isVisible)
    }
    
    func handleDimmedBackgroundLayerViewVisibilityForMenu(isHidden: Bool) {
        UIView.transition(with: self.dimmedBackgroundLayerView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.dimmedBackgroundLayerView.isHidden = isHidden
        }, completion: nil)
    }
    
    
    // MARK: - Menu Options
    @IBAction func userProfileButtonPressed(_ sender: UIButton) {
        if !AuthenticationManager.isUserAuthenticated {
            self.openSignInViewController()
        }
        else {
            if let user = AuthenticationManager.user {
                if let userRole = user.role {
                    if userRole == "USER" {
                        self.performSegue(segueID: Constants.SEGUE_TO_USER_PROFILE_SCREEN)
                    }
                }
            }
        }
    }
    
    @IBAction func listMySpaceButtonPressed(_ sender: UIButton) {
        self.handleMenuViewVisibility(isVisible: false)
        self.presentDismissableAlertWithMessage(title: AlertMessages.HI_TITLE, message: AlertMessages.HOME_LIST_MYSPACE_ALERT_MESSAGE)
    }
}




extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == homeTableView {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == homeTableView {
            if section == 1 {
                let header = tableView.dequeueReusableCell(withIdentifier: HOME_TABLE_VIEW_HEADER_CELL_ID)
                
                (header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.titleView) as! UIButton).addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                (header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.searchField) as! UIButton).addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                
                let btnSearch = header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.searchButton) as! UIButton
                btnSearch.layer.cornerRadius = 5
                btnSearch.clipsToBounds = true
                btnSearch.addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                
                (header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.eventTypeFieldButton) as! UIButton).addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                (header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.locationFieldButton) as! UIButton).addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                
                
                if searchHeaderHeight == self.HOME_TABLE_VIEW_HEADER_HEIGHTS.expanded {
                    (header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.searchView))?.isHidden = false
                    
                    (header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.eventTypeTextField) as! UITextField).text = (selectedEventType != nil) ? selectedEventType?.name! : nil
                    (header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.locationTextField) as! UITextField).text = (selectedLocation != nil) ? selectedLocation?.name : nil
                }
                else {
                    header?.viewWithTag(self.HOME_TABLE_VIEW_HEADER_VIEW_TAGS.searchView)?.isHidden = true
                }
                
                return header
            }
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == homeTableView {
            if section == 1 {
                return CGFloat(searchHeaderHeight)
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == homeTableView {
            if section == 1 {
                return 4
            }
            else {
                return 1
            }
        }
        else if tableView == eventTypeTableView {
            return eventTypes.count
        }
        else {
            return menuOptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == homeTableView {
            return UITableView.automaticDimension
        }
        else if tableView == eventTypeTableView {
            return 50
        }
        else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == homeTableView {
            return UITableView.automaticDimension
        }
        else if tableView == eventTypeTableView {
            return 50
        }
        else {
            return 60
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == homeTableView {
            if indexPath.section == 1 {
                if indexPath.row == self.HOME_TABLE_VIEW_CELLS.partyAndPhotoshoots {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: HOME_TABLE_VIEW_CELL_TYPE_TWO_ID, for: indexPath)
                    
                    let tapLeft = UITapGestureRecognizer.init(target: self, action: #selector(tapHandlerForEventType(_:)))
                    cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_BUTTON_TAGS.party)?.addGestureRecognizer(tapLeft)
                    let tapRight = UITapGestureRecognizer.init(target: self, action: #selector(tapHandlerForEventType(_:)))
                    cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_BUTTON_TAGS.photoshoots)?.addGestureRecognizer(tapRight)
                    
                    // PARTY
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.imageOne) as! UIImageView).image = #imageLiteral(resourceName: "img-event-party")
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.labelOne) as! UILabel).text = Strings.MAIN_EVENT_TYPE_PARTY
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.labelOneBackground) as! UIImageView).image = #imageLiteral(resourceName: "img-cs-2")
                    
                    // PHOTOSHOOTS
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.imageTwo) as! UIImageView).image = #imageLiteral(resourceName: "img-event-photoshoot")
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.labelTwo) as! UILabel).text = Strings.MAIN_EVENT_TYPE_PHOTOSHOOTS
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.labelTwoBackground) as! UIImageView).image = #imageLiteral(resourceName: "img-cs-3")
                    
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: HOME_TABLE_VIEW_CELL_TYPE_ONE_ID, for: indexPath)
                    
                    let imageView = cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS.image) as! UIImageView
                    let label = cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS.label) as! UILabel
                    let labelBackground = cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS.labelBackground) as! UIImageView
                    
                    if indexPath.row == self.HOME_TABLE_VIEW_CELLS.meetings {   // MEETINGS
                        imageView.image = #imageLiteral(resourceName: "img-event-meeting")
                        label.text = Strings.MAIN_EVENT_TYPE_MEETINGS
                        labelBackground.image = #imageLiteral(resourceName: "img-cs-1")
                    }
                    else if indexPath.row == self.HOME_TABLE_VIEW_CELLS.weddings {   // WEDDINGS
                        imageView.image = #imageLiteral(resourceName: "img-event-wedding")
                        label.text = Strings.MAIN_EVENT_TYPE_WEDDINGS
                        labelBackground.image = #imageLiteral(resourceName: "img-cs-4")
                    }
                    else if indexPath.row == self.HOME_TABLE_VIEW_CELLS.interviews {   // INTERVIEWS
                        imageView.image = #imageLiteral(resourceName: "img-event-interview")
                        label.text = Strings.MAIN_EVENT_TYPE_INTERVIEWS
                        labelBackground.image = #imageLiteral(resourceName: "img-cs-1")
                    }
                    
                    return cell
                }
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: HOME_TABLE_VIEW_HEADER_IMAGE_CELL_ID, for: indexPath)
                cell.isUserInteractionEnabled = false
                return cell
            }
        }
        else if tableView == eventTypeTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: EVENT_TYPE_FILTER_TABLE_VIEW_CELL_ID, for: indexPath)
            let imageView = cell.viewWithTag(self.EVENT_TYPE_FILTER_TABLE_VIEW_CELL_TAGS.imageView) as? UIImageView
            imageView?.imageFromServerURL(urlString: eventTypes[indexPath.row].iconUrl!)
            (cell.viewWithTag(self.EVENT_TYPE_FILTER_TABLE_VIEW_CELL_TAGS.label) as? UILabel)?.text  = eventTypes[indexPath.row].name!
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: MENU_TABLE_VIEW_CELL_ID, for: indexPath)
            let optionAtIndex = menuOptions[indexPath.row] as! NSDictionary
            (cell.viewWithTag(1) as! UIImageView).image = optionAtIndex.value(forKey: "image") as? UIImage
            (cell.viewWithTag(2) as! UILabel).text = optionAtIndex.value(forKey: "name") as? String
            return cell
        }
    }
    
    
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == homeTableView {
            if !(indexPath.section == 0 && indexPath.row == 0) {
                
                if indexPath.row == 0 {
                    if let et = MetaData.retrieveEventType(with: self.HOME_SCREEN_MAIN_EVENT_TYPE_IDS.meetings) {
                        FilterManager.spaceFilter.eventTypes.append(et)
                    }
                }
                else if indexPath.row == 1 {
                    // NOTE: - Catched by the tap gesture action for party or photo selection
                }
                else if indexPath.row == 2 {
                    if let et = MetaData.retrieveEventType(with: self.HOME_SCREEN_MAIN_EVENT_TYPE_IDS.wedding) {
                        FilterManager.spaceFilter.eventTypes.append(et)
                    }
                }
                else if indexPath.row == 3 {
                    if let et = MetaData.retrieveEventType(with: self.HOME_SCREEN_MAIN_EVENT_TYPE_IDS.interviews) {
                        FilterManager.spaceFilter.eventTypes.append(et)
                    }
                }
                
                performSegue(segueID: Constants.SEGUE_TO_SPACES_SCREEN)
            }
            
        }
        else if tableView == eventTypeTableView {
            self.selectedEventType = eventTypes[indexPath.row]
            self.handleEventTypeTableViewVisibility(isHidden: true)
        }
        else if tableView == menuTableView {
            
            if indexPath.row == 0 {
                if let user = AuthenticationManager.user {
                    
                    if let userRole = user.role {
                        if userRole == "USER" {
                            performSegue(segueID: Constants.SEGUE_TO_MY_SPACES_SCREEN)
                        }
                        else {
                            // Log Out
                            self.handleMenuViewVisibility(isVisible: false)
                            AuthenticationManager.logOutAuthenticatedUser()
                            self.configureHomeMenu()
                        }
                    }
                }
            }
            else if indexPath.row == 1 {
                performSegue(segueID: Constants.SEGUE_TO_MY_ACTIVITIES_SCREEN)
            }
            else {
                // Log Out
                self.handleMenuViewVisibility(isVisible: false)
                AuthenticationManager.logOutAuthenticatedUser()
                self.configureHomeMenu()
            }
        }
    }
}


// MARK: - Google Autocomplete View Controller Delegate
extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.selectedLocation = place
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
