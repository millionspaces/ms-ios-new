//
//  HostCalendarViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class HostCalendarViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var selectedDateLabel: UILabel!
    @IBOutlet weak var selectedYearLabel: UILabel!
    @IBOutlet weak var selectedMonthLabel: UILabel!
    @IBOutlet weak var backToCalendarButton: UIButton!
    @IBOutlet weak var continueToManualBookingScreenButton: UIButton!
    
    @IBOutlet weak var calendarParentView: UIView!
    @IBOutlet weak var previousMonthCalendarToggleButton: UIButton!
    @IBOutlet weak var nextMonthCalendarToggleButton: UIButton!
    
    @IBOutlet weak var tempHostCalendarCollectionView: UICollectionView!
    @IBOutlet weak var constTempHostCalendarCVHeight: NSLayoutConstraint!
    
    @IBOutlet weak var timeSelectionView: UIView!
    @IBOutlet weak var timeSelectionTableView: UITableView!
    
    //MARK: - Class variables
    var mySpaceID: Int!
    
    var calendarFontSize: CGFloat?
    let calendarWidthHeight = Utils.SCREEN_WIDTH - 70
    let calendarCellHeightWidth = (Utils.SCREEN_WIDTH - 70) / 7
    
    //var mySpace: Space!
    var spaceCalendarInfo: SpaceCalendarInfo!
    var spaceCalendar: SpaceCalendar!
    var spaceCalendarMonths: [String] = []
    var spaceCalendarDatesDic: NSDictionary!
    
    var currentlyDisplayedMonthIndexPath: IndexPath!
    var selectedDate: SpaceCalendarDate?
    var selectedDateVCTag: Int?
    var selectedDateVCItemIndexPath: Int?
    
    var hoursForSelectedDate: [SpaceCalendarTimeByHour] = []
    var selectedHourCells: [Int] = []
    
    var alertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("HostCalendarVC viewDidLoad")
        
        self.navigationItem.title = "Host Calendar"
        
        print("HostCalendarVC | Calendar Width / Height : \(calendarWidthHeight)")
        print("HostCalendarVC | Calendar Cell Height & Width : \(calendarCellHeightWidth)")
        
        self.constTempHostCalendarCVHeight.constant = calendarWidthHeight
        
        self.tempHostCalendarCollectionView.allowsSelection = false
        calendarFontSize = CalendarUtils.setMSCalendarFontSize()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(returnedToHostCalendarFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        
        self.startUpGuestCalendar(reload: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToHostCalendarFromBackground() {
        alertController?.dismiss(animated: false, completion: nil)
        if Utils.didPassDuration(durationInHours: 0.5) { // ~ 10s
            self.popToRootViewController()
        }
        else {
            self.startUpGuestCalendar(reload: true)
        }
    }
    
    func startUpGuestCalendar(reload: Bool) {
        if reload {
            self.configureView()
            self.retrieveSpaceCalendarDetails()
        }
    }
    
    func configureView() {
        self.continueToManualBookingScreenButton.isUserInteractionEnabled = false
        
        self.timeSelectionView.isHidden = true
        self.calendarParentView.isHidden = false
        
        self.spaceCalendarInfo = nil
        self.spaceCalendar = nil
        self.spaceCalendarMonths.removeAll()
        self.spaceCalendarDatesDic = nil
        
        self.selectedDate = nil
        self.selectedDateVCTag = nil
        self.selectedDateVCItemIndexPath = nil
        self.hoursForSelectedDate.removeAll()
        self.selectedHourCells.removeAll()
        
        self.selectedDateLabel.text = "DD"
        self.selectedMonthLabel.text = "MMMM"
        self.selectedYearLabel.text = "YYYY"
        
        self.backToCalendarButton.isHidden = true
        
        self.tempHostCalendarCollectionView.reloadData()
        self.timeSelectionTableView.reloadData()
    }
    
    func retrieveSpaceCalendarDetails() {
        self.showActivityIndicator()
        
        WebServiceCall.retrieveHostCalendarDetails(withSpaceID: self.mySpaceID!, successBlock: { (result: Any) in
            //print("HostCalendarVC retrieveSpaceCalendarDetails | success result : \(result)")
            
            if let spaceDetails = result as? [String : Any] {
                
                self.spaceCalendarInfo = SpaceCalendarInfo()
                self.spaceCalendarInfo.initSpaceCalendarInfo(withSpaceID: self.mySpaceID!, andCalendarInfoDic: spaceDetails, forGuestCalendar: false)
                
                DispatchQueue.main.sync(execute: {
                    self.configureCalendarData()
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    self.addSpaceCalendarDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("HostCalendarVC retrieveSpaceDetails | failure error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpaceCalendarDetailsReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpaceCalendarDetailsReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpaceCalendarDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func addSpaceCalendarDetailsReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retrieveSpaceCalendarDetails()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func configureCalendarData() {
        print("\nHostCalendarVC calendarInfo spaceID : \(self.spaceCalendarInfo.spaceID!)")
        print("HostCalendarVC calendarInfo calendarStartDate : \(self.spaceCalendarInfo.calendarStartDate!)")
        print("HostCalendarVC calendarInfo calendarEndDate : \(self.spaceCalendarInfo.calendarEndDate!)")
        print("HostCalendarVC calendarInfo availability method : \(self.spaceCalendarInfo.availabilityMethod!)")
        print("HostCalendarVC calendarInfo bufferTime : \(self.spaceCalendarInfo.bufferTime)")
        print("HostCalendarVC calendarInfo noticePeriod : \(self.spaceCalendarInfo.noticePeriod)")
        print("HostCalendarVC calendarInfo calendarBookings count : \(self.spaceCalendarInfo.calendarBookings.count)\n")
        
        /*
         for fb in self.spaceCalendarInfo.calendarBookings {
         print("HostCalendarVC calendarInfo | calendarBooking at i | from : \(fb.bookingFromDate!) \(fb.bookingFromTimeHourInt!) | to : \(fb.bookingToDate!) \(fb.bookingToTimeHourInt!)")
         }*/
        
        spaceCalendar = SpaceCalendar()
        spaceCalendar.initHostCalendar(withSpaceCalendarInfo: self.spaceCalendarInfo)
        spaceCalendarMonths = spaceCalendar.calendarDatesDictionary.allKeys as! [String]
        spaceCalendarMonths.sort()
        spaceCalendarDatesDic = spaceCalendar.calendarDatesDictionary
        
        self.currentlyDisplayedMonthIndexPath = IndexPath.init(item: 0, section: 0)
        self.tempHostCalendarCollectionView.reloadData()
        self.timeSelectionTableView.reloadData()
        
        if let currentMonthIndex = self.spaceCalendarMonths.index(of: spaceCalendar.currentMonthKey) {
            self.currentlyDisplayedMonthIndexPath = IndexPath.init(item: currentMonthIndex, section: 0)
            self.tempHostCalendarCollectionView.scrollToItem(at: self.currentlyDisplayedMonthIndexPath, at: .centeredHorizontally, animated: false)
            self.selectedDateLabel.text = "\(spaceCalendar.todayDate.day!)"
            self.selectedMonthLabel.text = "\(spaceCalendar.todayDate.monthDisplayName!)"
            self.selectedYearLabel.text = "\(spaceCalendar.todayDate.year!)"
        }
        
        self.continueToManualBookingScreenButton.isUserInteractionEnabled = true
    }
    
    func setMonthAndYearLabelTexts(forMonthAndYear my: String) {
        let componentsFromString = my.components(separatedBy: "-")
        self.selectedDateLabel.text = "1"
        self.selectedMonthLabel.text = componentsFromString[1].uppercased()
        self.selectedYearLabel.text = componentsFromString[2]
    }
    
    func backToCalendarView() {
        self.backToCalendarButton.isHidden = true
        
        // TODO : Transition Animation
        self.timeSelectionView.isHidden = true
        self.calendarParentView.isHidden = false
        self.selectedHourCells.removeAll()
        self.timeSelectionTableView.reloadData()
    }
    
    // IBActions
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if self.timeSelectionView.isHidden {
            popViewController()
        }
        else {
            backToCalendarView()
        }
    }
    
    @IBAction func continueToManualBookingScreenButtonPressed(_ sender: UIButton) {
        if self.spaceCalendarInfo != nil {
            self.performSegue(segueID: "ManualBookingSegue")
            self.continueToManualBookingScreenButton.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func backToCalendarButtonPressed(_ sender: UIButton) {
        backToCalendarView()
    }
    
    @IBAction func previousMonthCalendarToggleButtonPressed(_ sender: UIButton) {
        if currentlyDisplayedMonthIndexPath.item > 0 {
            self.tempHostCalendarCollectionView.scrollToItem(at: IndexPath.init(item: currentlyDisplayedMonthIndexPath.item - 1, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    @IBAction func nextMonthCalendarToggleButtonPressed(_ sender: UIButton) {
        if currentlyDisplayedMonthIndexPath.item < self.spaceCalendarMonths.count - 1 {
            self.tempHostCalendarCollectionView.scrollToItem(at: IndexPath.init(item: currentlyDisplayedMonthIndexPath.item + 1, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ManualBookingSegue" {
            if let manualVC = segue.destination as? ManualBookingViewController {
                manualVC.mySpaceID = self.mySpaceID!
                manualVC.spaceCalendarEndDate = self.spaceCalendar.spaceCalendarEndingDate!
                
                // TODO: Check if Manual Booking Is Being Updated or Newly added
                
                manualVC.manualBookingID = nil // Set TEMP ^
                
                if let setDate = self.selectedDate, self.selectedHourCells.count > 0 {
                    manualVC.calendarSetDate = setDate
                    manualVC.calendarSetStartingTimeHourInt = hoursForSelectedDate[self.selectedHourCells.first!].timeHour24H
                    manualVC.calendarSetEndingTimeHourInt = hoursForSelectedDate[self.selectedHourCells.last!].timeHour24H + 1
                }
            }
        }
    }
    
    
}



extension HostCalendarViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return self.spaceCalendarMonths.count
        }
        else {
            let datesArrayOfMonthAtIndex = self.spaceCalendarDatesDic[self.spaceCalendarMonths[collectionView.tag-1]] as! NSMutableArray
            let numOfDaysOfWeekBeforeFirstDayOfMonth = (datesArrayOfMonthAtIndex[0] as! SpaceCalendarDate).dayOfWeekMS! - 1
            return datesArrayOfMonthAtIndex.count + numOfDaysOfWeekBeforeFirstDayOfMonth
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 0 { // Parent Collection View
            collectionView.isPagingEnabled = true
            let cvEmbeddedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewEmbeddedCell", for: indexPath) as! CollectionViewEmbeddedCollectionViewCell
            cvEmbeddedCell.setEmbeddedCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.item+1)
            return cvEmbeddedCell
        }
        else { // Embedded Collection Views in Parent Collection View Cells
            
            let datesArrayOfMonthAtIndex = self.spaceCalendarDatesDic[self.spaceCalendarMonths[collectionView.tag-1]] as! NSMutableArray
            let numOfDaysOfWeekBeforeFirstDayOfMonth = (datesArrayOfMonthAtIndex[0] as! SpaceCalendarDate).dayOfWeekMS! - 1
            
            if indexPath.item < numOfDaysOfWeekBeforeFirstDayOfMonth {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyDateCell", for: indexPath)
                cell.isUserInteractionEnabled = false
                return cell
            }
            else {
                let calendarDateAtItem = datesArrayOfMonthAtIndex[indexPath.item - numOfDaysOfWeekBeforeFirstDayOfMonth] as! SpaceCalendarDate
                
                if calendarDateAtItem.dateStatus == StaticData.HOST_CALENDAR_DATE_STATUS[1] || calendarDateAtItem.dateStatus == StaticData.HOST_CALENDAR_DATE_STATUS[4] {
                    
                    // 1 --> Unavailable Date - Fully booked
                    // 4 --> Out of calendar availability
                    
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UnavailableDateCell", for: indexPath)
                    
                    cell.isUserInteractionEnabled = (calendarDateAtItem.dateStatus == StaticData.SPACE_CALENDAR_DATE_STATUS[1]) ? true : false
                    
                    (cell.viewWithTag(1) as! UILabel).text = "\(calendarDateAtItem.day!)"
                    (cell.viewWithTag(1) as! UILabel).font = UIFont(name: "Helvetica Neue", size: calendarFontSize!)
                    
                    if selectedDateVCTag != nil, selectedDateVCItemIndexPath != nil {
                        if collectionView.tag == selectedDateVCTag && indexPath.item == selectedDateVCItemIndexPath {
                            cell.backgroundColor = ColorUtils.MS_BLUE_COLOR
                            (cell.viewWithTag(1) as! UILabel).textColor = UIColor.white
                            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .bottom)
                        }
                        else {
                            cell.backgroundColor = UIColor.white
                            (cell.viewWithTag(1) as! UILabel).textColor = UIColor.lightGray
                        }
                    }
                    else {
                        cell.backgroundColor = UIColor.white
                        (cell.viewWithTag(1) as! UILabel).textColor = UIColor.lightGray
                    }
                    
                    return cell
                }
                else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvailableDateCell", for: indexPath)
                    cell.isUserInteractionEnabled = true
                    
                    (cell.viewWithTag(1) as! UILabel).text = "\(calendarDateAtItem.day!)"
                    (cell.viewWithTag(1) as! UILabel).font = UIFont(name: "Helvetica Neue", size: calendarFontSize!)
                    
                    // Blue-Dot to indicate whether date is partially booked or not booked at all
                    (cell.viewWithTag(2))?.layer.cornerRadius = 2.5
                    (cell.viewWithTag(2))?.layer.masksToBounds = true
                    
                    // condition
                    if calendarDateAtItem.dateStatus == StaticData.HOST_CALENDAR_DATE_STATUS[3] {
                        cell.viewWithTag(2)?.isHidden = true
                    }
                    else {
                        cell.viewWithTag(2)?.isHidden = false
                    }
                    
                    
                    if selectedDateVCTag != nil, selectedDateVCItemIndexPath != nil {
                        if collectionView.tag == selectedDateVCTag && indexPath.item == selectedDateVCItemIndexPath {
                            cell.backgroundColor = ColorUtils.MS_BLUE_COLOR
                            (cell.viewWithTag(1) as! UILabel).textColor = UIColor.white
                            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .bottom)
                        }
                        else {
                            cell.backgroundColor = UIColor.white
                            (cell.viewWithTag(1) as! UILabel).textColor = UIColor.black
                        }
                    }
                    else {
                        cell.backgroundColor = UIColor.white
                        (cell.viewWithTag(1) as! UILabel).textColor = UIColor.black
                    }
                    
                    return cell
                }
                
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("collectionView : \(collectionView.tag) | didSelectItemAt : \(indexPath.item)")
        
        if collectionView.tag != 0 {
            let selectedCell = collectionView.cellForItem(at: indexPath)
            if selectedCell?.reuseIdentifier == "AvailableDateCell" || selectedCell?.reuseIdentifier == "UnavailableDateCell" {
                
                selectedCell!.backgroundColor = ColorUtils.MS_BLUE_COLOR
                (selectedCell!.viewWithTag(1) as! UILabel).textColor = UIColor.white
                self.selectedDateLabel.text = (selectedCell?.viewWithTag(1) as! UILabel).text
                
                let datesArrayOfMonthAtIndex = self.spaceCalendarDatesDic[self.spaceCalendarMonths[collectionView.tag-1]] as! NSMutableArray
                let numOfDaysOfWeekBeforeFirstDayOfMonth = (datesArrayOfMonthAtIndex[0] as! SpaceCalendarDate).dayOfWeekMS! - 1
                selectedDate = datesArrayOfMonthAtIndex[indexPath.item-numOfDaysOfWeekBeforeFirstDayOfMonth] as? SpaceCalendarDate
                
                selectedDateVCTag = collectionView.tag
                selectedDateVCItemIndexPath = indexPath.item
                
                //print("collectionView didSelectItem | selectedDate : \(selectedDate!.date!)")
                self.hoursForSelectedDate = selectedDate!.calendarTime!.hoursForCalendarDate
                self.timeSelectionView.isHidden = false
                self.calendarParentView.isHidden = true
                self.backToCalendarButton.isHidden = false
                self.timeSelectionTableView.reloadData()
                self.timeSelectionTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("collectionView : \(collectionView.tag) | didDeselectItemAt : \(indexPath.item)")
        
        if collectionView.tag != 0 {
            let delectedCell = collectionView.cellForItem(at: indexPath)
            if delectedCell?.reuseIdentifier == "AvailableDateCell" || delectedCell?.reuseIdentifier == "UnavailableDateCell" {
                delectedCell?.backgroundColor = UIColor.white
                (delectedCell?.viewWithTag(1) as! UILabel).textColor = (delectedCell?.reuseIdentifier == "AvailableDateCell") ? UIColor.black : UIColor.lightGray
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            return CGSize(width: calendarWidthHeight, height: calendarWidthHeight)
        }
        else {
            return CGSize(width: calendarCellHeightWidth, height: calendarCellHeightWidth)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}



// MARK: - UITableView DataSource and Delegate
extension HostCalendarViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.spaceCalendarInfo != nil {
            return self.hoursForSelectedDate.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.spaceCalendarInfo != nil {
            let ip = IndexPath.init(row: indexPath.row, section: indexPath.section)
            
            let hourAtIndex = self.hoursForSelectedDate[indexPath.row]
            
            let hourCell = tableView.dequeueReusableCell(withIdentifier: "HourCell", for: indexPath)
            (hourCell.viewWithTag(1) as! UILabel).text = hourAtIndex.timeDisplayName!
            
            if !hourAtIndex.isHourAvailable {
                hourCell.isUserInteractionEnabled = false
                if indexPath.row != self.hoursForSelectedDate.count - 1 {
                    hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_DARK_BLUE_COLOR
                }
                else {
                    hourCell.viewWithTag(2)?.backgroundColor = UIColor.clear
                }
            }
            else {
                hourCell.isUserInteractionEnabled = true
                
                if self.selectedHourCells.contains(indexPath.row) {
                    tableView.selectRow(at: ip, animated: false, scrollPosition: .none)
                    hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
                }
                else {
                    tableView.deselectRow(at: ip, animated: false)
                    hourCell.viewWithTag(2)?.backgroundColor = UIColor.clear
                }
            }
            
            return hourCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.selectedHourCells.count > 0 {
            if (indexPath.row == self.selectedHourCells.first! - 1) || (indexPath.row == self.selectedHourCells.last! + 1) {
                //print("timeSelectionTableView didSelectRowAt | correct selection")
                self.selectedHourCells.append(indexPath.row)
                self.selectedHourCells.sort()
                let hourCell = tableView.cellForRow(at: indexPath)!
                hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
            }
            else {
                //print("timeSelectionTableView didSelectRowAt | wrong selection")
                for i in self.selectedHourCells {
                    let ip = IndexPath.init(row: i, section: indexPath.section)
                    tableView.deselectRow(at: ip, animated: false)
                    if let hourCellToBeDeselected = tableView.cellForRow(at: ip) {
                        hourCellToBeDeselected.viewWithTag(2)?.backgroundColor = UIColor.clear
                    }
                }
                self.selectedHourCells.removeAll()
                
                let hourCell = tableView.cellForRow(at: indexPath)!
                hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
                self.selectedHourCells.append(indexPath.row)
            }
        }
        else {
            self.selectedHourCells.append(indexPath.row)
            let hourCell = tableView.cellForRow(at: indexPath)!
            hourCell.viewWithTag(2)?.backgroundColor = ColorUtils.MS_BLUE_COLOR
        }
        
        //self.configureContinueToManualBookingScreenButton()
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == self.selectedHourCells.first) || (indexPath.row == self.selectedHourCells.last) {
            _ = (indexPath.row == self.selectedHourCells.first) ? self.selectedHourCells.removeFirst() : self.selectedHourCells.removeLast()
            let hourCell = tableView.cellForRow(at: indexPath)!
            hourCell.viewWithTag(2)?.backgroundColor = UIColor.clear
        }
        else if self.selectedHourCells.contains(indexPath.row) {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        else {
            //print("timeSelectionTableView didDeselectRowAt | wrong deselect")
        }
        
        //self.configureContinueToManualBookingScreenButton()
    }
}



extension HostCalendarViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tempHostCalendarCollectionView {
            let visibleRect = CGRect(origin: self.tempHostCalendarCollectionView.contentOffset, size: self.tempHostCalendarCollectionView.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            if let indexPath = self.tempHostCalendarCollectionView.indexPathForItem(at: visiblePoint) {
                setMonthAndYearLabelTexts(forMonthAndYear: spaceCalendarMonths[indexPath.item])
                currentlyDisplayedMonthIndexPath = indexPath
            }
        }
    }
}


