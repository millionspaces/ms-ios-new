//
//  FilterViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit
import GooglePlaces

class FilterViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var filtersTableView: UITableView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var viewForDateTimePickers: UIView!
    @IBOutlet weak var pickersViewCancelButton: UIButton!
    @IBOutlet weak var pickersViewSetButton: UIButton!
    @IBOutlet weak var constViewForDateTimePickersSubviewBottom: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var hourPicker: UIPickerView!
    @IBOutlet weak var amPmPicker: UIPickerView!
    
    // MARK: - Constants
    let FILTER_TABLE_VIEW_SECTIONS = (EventType: 0, SpaceType: 1, Location: 2, DateTime: 3, SpaceCapacity: 4, StartingPrice: 5, Amenities: 6, SpaceRules: 7, SeatingArrangements: 8)
    let COLLECTION_VIEW_EMBEDDED_TABLE_VIEW_CELL_ID = "CollectionViewEmbeddedCell"
    let TEXT_FIELD_TABLE_VIEW_CELL_ID = "TextFieldCell"
    let DATE_PICKER_TABLE_VIEW_CELL_ID = "DatePickerCell"
    let SINGLE_CHECK_BOX_TABLE_VIEW_CELL_ID = "SingleCheckBoxCell"
    let SINGLE_CHECK_BOX_OTHER_TABLE_VIEW_CELL_ID = "SingleCheckBoxCell-Other"
    
    // MARK: - Class properties / variables
    var eventTypes = [EventType]()
    var amenities = [Amenity]()
    var rules = [Rule]()
    var seatingArrangements = [SeatingArrangement]()
    var spaceCapacityOptions = [[String : Any]]()
    var priceOptions = [[String : Any]]()
    var expandedSections: NSMutableArray = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFilterData()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToFiltersFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToFiltersFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) {
            FilterManager.resetFilters()
            dismissViewController()
        }
    }
    
    func loadFilterData() {
        self.eventTypes = MetaData.retrieveEventTypes() as! [EventType]
        self.amenities = MetaData.retrieveAmenities() as! [Amenity]
        self.rules = MetaData.retrieveSpaceRules() as! [Rule]
        self.seatingArrangements = MetaData.retrieveSeatingArrangements() as! [SeatingArrangement]
        self.spaceCapacityOptions = StaticData.FILTER_CAPACITY_OPTIONS
        self.priceOptions = StaticData.FILTER_PRICE_OPTIONS
    }
    
    func configureView() {
        self.navigationItem.title = Strings.FILTER_VC_TITLE
        self.filtersTableView.allowsMultipleSelection = true
        self.viewForDateTimePickers.isHidden = true
        self.constViewForDateTimePickersSubviewBottom.constant = -250.0
        self.datePicker.minimumDate = Date()
        self.hourPicker.delegate = self
        self.amPmPicker.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if self.viewForDateTimePickers.frame.contains(location) && !self.viewForDateTimePickers.viewWithTag(3)!.frame.contains(location) {
            hideViewForDateTimePickers()
        }
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        FilterManager.spaceFilter.shouldRefreshSpacesListWithFilters = false
        dismissViewController()
    }
    
    @IBAction func resetButtonPressed(_ sender: UIButton) {
        FilterManager.resetFilters()
        dismissViewController()
    }
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        FilterManager.spaceFilter.shouldRefreshSpacesListWithFilters = true
        dismissViewController()
    }
    
    @IBAction func pickersViewCancelButtonPressed(_ sender: UIButton) {
        hideViewForDateTimePickers()
    }
    
    @IBAction func pickersViewSetButtonPressed(_ sender: UIButton) {
        let setDate = self.datePicker.date
        let setTimeHour = self.hourPicker.selectedRow(inComponent: 0) + 1
        let setAmPm = self.amPmPicker.selectedRow(inComponent: 0)
        FilterManager.setAvailableDateFilter(date: setDate, hour: setTimeHour, amPm: setAmPm)
        hideViewForDateTimePickers()
        self.filtersTableView.reloadData()
    }
    
    @objc func dateTimeCellButtonPressed(_ sender: UIButton) {
        showViewForDateTimePickers()
    }
    
    @objc func locationCellButtonPressed(_ sender: UIButton) {
        let autocompleteController = CustomGoogleAutocompleteViewController()
        autocompleteController.delegate = self
        let autocompleteFilter = GMSAutocompleteFilter()
        autocompleteFilter.country = "LK"
        autocompleteController.autocompleteFilter = autocompleteFilter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func showViewForDateTimePickers() {
        self.viewForDateTimePickers.isHidden = false
        self.constViewForDateTimePickersSubviewBottom.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideViewForDateTimePickers() {
        self.constViewForDateTimePickersSubviewBottom.constant = -250.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        delay(0.5) {
            self.viewForDateTimePickers.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


// MARK: - UIPickerView DataSource and Delegate
extension FilterViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == hourPicker {
            return StaticData.HOUR_ARRAY.count
        }
        return StaticData.AMPM_ARRAY.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == hourPicker {
            return StaticData.HOUR_ARRAY[row]
        }
        return StaticData.AMPM_ARRAY[row]
    }
}


// MARK: - Configure Table View
extension FilterViewController {
    
    func configureSectionHeader(headerView: UIView, section: Int) -> UIView {
        let headerLabel = headerView.viewWithTag(2) as! UILabel
        
        switch section {
        case FILTER_TABLE_VIEW_SECTIONS.EventType:
            headerLabel.text = Strings.EVENT_TYPE
            break
        case FILTER_TABLE_VIEW_SECTIONS.SpaceType:
            headerLabel.text = Strings.SPACE_TYPE
            break
        case FILTER_TABLE_VIEW_SECTIONS.Location:
            headerLabel.text = Strings.LOCATION
            break
        case FILTER_TABLE_VIEW_SECTIONS.DateTime:
            headerLabel.text = Strings.DATE_AND_TIME
            break
        case FILTER_TABLE_VIEW_SECTIONS.SpaceCapacity:
            headerLabel.text = Strings.SPACE_CAPACITY
            break
        case FILTER_TABLE_VIEW_SECTIONS.StartingPrice:
            headerLabel.text = Strings.STARTING_PRICE
            break
        case FILTER_TABLE_VIEW_SECTIONS.Amenities:
            headerLabel.text = Strings.AMENITIES
            break
        case FILTER_TABLE_VIEW_SECTIONS.SpaceRules:
            headerLabel.text = Strings.SPACE_RULES
            break
        case FILTER_TABLE_VIEW_SECTIONS.SeatingArrangements:
            headerLabel.text = Strings.SEATING_ARRANGEMENTS
            break
        default:
            break
        }
        
        headerView.tag = section
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleSectionSelection(_:)))
        headerView.addGestureRecognizer(tap)
        return headerView
    }
    
    
    func calculateNumberOfRowsForSection(section: Int) -> Int {
        var rowsCount: Int!
        
        switch section {
        case FILTER_TABLE_VIEW_SECTIONS.EventType:
            rowsCount = self.expandedSections.contains(section) ? 1 : 0
            break
        case FILTER_TABLE_VIEW_SECTIONS.SpaceType:
            rowsCount = self.expandedSections.contains(section) ? 1 : 0
            break
        case FILTER_TABLE_VIEW_SECTIONS.Location:
            rowsCount = self.expandedSections.contains(section) ? 1 : 0
            break
        case FILTER_TABLE_VIEW_SECTIONS.DateTime:
            rowsCount = self.expandedSections.contains(section) ? 1 : 0
            break
        case FILTER_TABLE_VIEW_SECTIONS.SpaceCapacity:
            rowsCount = self.expandedSections.contains(section) ? 1 : 0
            break
        case FILTER_TABLE_VIEW_SECTIONS.StartingPrice:
            rowsCount = self.expandedSections.contains(section) ? self.priceOptions.count : 0
            break
        case FILTER_TABLE_VIEW_SECTIONS.Amenities:
            rowsCount = self.expandedSections.contains(section) ? 1 : 0
            break
        case FILTER_TABLE_VIEW_SECTIONS.SpaceRules:
            rowsCount = self.expandedSections.contains(section) ? self.rules.count : 0
            break
        case FILTER_TABLE_VIEW_SECTIONS.SeatingArrangements:
            rowsCount = self.expandedSections.contains(section) ? self.seatingArrangements.count : 0
            break
        default:
            break
        }
        
        return rowsCount
    }
    
    func calculateHeightForRowInSection(section: Int) -> CGFloat {
        switch section {
        case FILTER_TABLE_VIEW_SECTIONS.EventType:
            return Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.eventTypes.count, cvCellHeight: 40, itemsPerRow: 2)
            
        case FILTER_TABLE_VIEW_SECTIONS.SpaceType:
            return 80
            
        case FILTER_TABLE_VIEW_SECTIONS.Location:
            return 60
            
        case FILTER_TABLE_VIEW_SECTIONS.DateTime:
            return 60
            
        case FILTER_TABLE_VIEW_SECTIONS.SpaceCapacity:
            return Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.spaceCapacityOptions.count, cvCellHeight: 40, itemsPerRow: 2)
            
        case FILTER_TABLE_VIEW_SECTIONS.Amenities:
            return Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.amenities.count, cvCellHeight: 40, itemsPerRow: 2)
            
        case FILTER_TABLE_VIEW_SECTIONS.SeatingArrangements:
            return 50
            
        default:
            return 40
        }
    }
}


// MARK: - Handle section header tap
extension FilterViewController {
    
    @objc func handleSectionSelection(_ tap: UIGestureRecognizer){
        let section = tap.view!.tag
        if self.expandedSections.contains(section) {
            self.expandedSections.remove(section)
            self.filtersTableView.reloadData()
        }
        else {
            self.expandedSections.add(section)
            self.filtersTableView.reloadData()
            self.filtersTableView.scrollToRow(at: IndexPath.init(row: 0, section: section), at: UITableView.ScrollPosition.top, animated: true)
        }
    }
}


// MARK: - UITableViewDataSource
extension FilterViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell")
        return configureSectionHeader(headerView: header!, section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calculateNumberOfRowsForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return calculateHeightForRowInSection(section: indexPath.section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case FILTER_TABLE_VIEW_SECTIONS.EventType:
            let cell = tableView.dequeueReusableCell(withIdentifier: COLLECTION_VIEW_EMBEDDED_TABLE_VIEW_CELL_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
            cell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.section)
            return cell
            
        case FILTER_TABLE_VIEW_SECTIONS.SpaceType:
            let cell = tableView.dequeueReusableCell(withIdentifier: COLLECTION_VIEW_EMBEDDED_TABLE_VIEW_CELL_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
            cell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.section)
            return cell
            
        case FILTER_TABLE_VIEW_SECTIONS.Location:
            let cell = tableView.dequeueReusableCell(withIdentifier: TEXT_FIELD_TABLE_VIEW_CELL_ID, for: indexPath)
            if let filterLocation =  FilterManager.spaceFilter.location {
                (cell.viewWithTag(1) as! UITextField).text = filterLocation.name
            }
            (cell.viewWithTag(2) as! UIButton).addTarget(self, action: #selector(locationCellButtonPressed(_:)), for: .touchUpInside)
            return cell
            
        case FILTER_TABLE_VIEW_SECTIONS.DateTime:
            let cell = tableView.dequeueReusableCell(withIdentifier: DATE_PICKER_TABLE_VIEW_CELL_ID, for: indexPath)
            (cell.viewWithTag(1) as! UITextField).text = FilterManager.getAvailableDateFilterDisplayValue()
            (cell.viewWithTag(2) as! UIButton).addTarget(self, action: #selector(dateTimeCellButtonPressed(_:)), for: .touchUpInside)
            return cell
            
        case FILTER_TABLE_VIEW_SECTIONS.SpaceCapacity:
            let cell = tableView.dequeueReusableCell(withIdentifier: COLLECTION_VIEW_EMBEDDED_TABLE_VIEW_CELL_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
            cell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.section)
            return cell
            
        case FILTER_TABLE_VIEW_SECTIONS.StartingPrice:
            let cell = tableView.dequeueReusableCell(withIdentifier: SINGLE_CHECK_BOX_TABLE_VIEW_CELL_ID, for: indexPath)
            let priceOptionAtRow = self.priceOptions[indexPath.row]
            (cell.viewWithTag(2) as! UILabel).text = priceOptionAtRow["displayValue"] as? String
            if FilterManager.spaceFilter.price.contains(priceOptionAtRow["id"] as! Int) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                tableView.deselectRow(at: indexPath, animated: true)
            }
            return cell
            
        case FILTER_TABLE_VIEW_SECTIONS.Amenities:
            let cell = tableView.dequeueReusableCell(withIdentifier: COLLECTION_VIEW_EMBEDDED_TABLE_VIEW_CELL_ID, for: indexPath) as! CollectionViewEmbeddedTableViewCell
            cell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.section)
            return cell
            
        case FILTER_TABLE_VIEW_SECTIONS.SpaceRules:
            let cell = tableView.dequeueReusableCell(withIdentifier: SINGLE_CHECK_BOX_TABLE_VIEW_CELL_ID, for: indexPath)
            let ruleAtRow = self.rules[indexPath.row]
            (cell.viewWithTag(2) as! UILabel).text = ruleAtRow.displayName!
            if FilterManager.spaceFilter.rules.contains(ruleAtRow) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                tableView.deselectRow(at: indexPath, animated: true)
            }
            return cell
            
        case FILTER_TABLE_VIEW_SECTIONS.SeatingArrangements:
            let cell = tableView.dequeueReusableCell(withIdentifier: SINGLE_CHECK_BOX_OTHER_TABLE_VIEW_CELL_ID, for: indexPath)
            (cell.viewWithTag(3) as! UIImageView).image = #imageLiteral(resourceName: "img-thumbnail-small")
            let seatingAtRow = self.seatingArrangements[indexPath.row]
            (cell.viewWithTag(2) as! UILabel).text = seatingAtRow.name
            
            // ImageView
            let imageView = cell.viewWithTag(3) as! UIImageView
            imageView.sd_setImage(with: URL(string: seatingAtRow.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            if FilterManager.spaceFilter.seatingArrangements.contains(seatingAtRow) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                tableView.deselectRow(at: indexPath, animated: true)
            }
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}


// MARK: - UITableViewDelegate
extension FilterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == FILTER_TABLE_VIEW_SECTIONS.StartingPrice { // Price
            if !FilterManager.spaceFilter.price.contains(indexPath.row) {
                FilterManager.spaceFilter.price.append(indexPath.row)
                let selectedPriceOptionCell = tableView.cellForRow(at: indexPath)
                (selectedPriceOptionCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if indexPath.section == FILTER_TABLE_VIEW_SECTIONS.SpaceRules { // Rules
            let ruleAtRow = self.rules[indexPath.row]
            if !FilterManager.spaceFilter.rules.contains(ruleAtRow) {
                FilterManager.spaceFilter.rules.append(ruleAtRow)
                let selectedRuleCell = tableView.cellForRow(at: indexPath)
                (selectedRuleCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if indexPath.section == FILTER_TABLE_VIEW_SECTIONS.SeatingArrangements { // Seating
            let seatingAtRow = self.seatingArrangements[indexPath.row]
            if !FilterManager.spaceFilter.seatingArrangements.contains(seatingAtRow) {
                FilterManager.spaceFilter.seatingArrangements.append(seatingAtRow)
                let selectedSeatingCell = tableView.cellForRow(at: indexPath)
                (selectedSeatingCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == FILTER_TABLE_VIEW_SECTIONS.StartingPrice { // Price
            if let index = FilterManager.spaceFilter.price.index(of: indexPath.row) {
                FilterManager.spaceFilter.price.remove(at: index)
                let deselectedPriceOptionCell = tableView.cellForRow(at: indexPath)
                (deselectedPriceOptionCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if indexPath.section == FILTER_TABLE_VIEW_SECTIONS.SpaceRules { // Rules
            let ruleAtRow = self.rules[indexPath.row]
            if let index = FilterManager.spaceFilter.rules.index(of: ruleAtRow) {
                FilterManager.spaceFilter.rules.remove(at: index)
                let deselectedRuleCell = tableView.cellForRow(at: indexPath)
                (deselectedRuleCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if indexPath.section == FILTER_TABLE_VIEW_SECTIONS.SeatingArrangements { // Seating
            let seatingAtRow = self.seatingArrangements[indexPath.row]
            if let index = FilterManager.spaceFilter.seatingArrangements.index(of: seatingAtRow) {
                FilterManager.spaceFilter.seatingArrangements.remove(at: index)
                let deselectedSeatingCell = tableView.cellForRow(at: indexPath)
                (deselectedSeatingCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
    }
}


// MARK: - UICollectionViewDataSource
extension FilterViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.EventType { // et
            return self.eventTypes.count
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.SpaceType { // space type
            return StaticData.SPACE_TYPES.count
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.SpaceCapacity { // capacity
            return self.spaceCapacityOptions.count
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.Amenities { // amenities
            return self.amenities.count
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let imageView = cell.viewWithTag(1) as! UIImageView
        let label = cell.viewWithTag(2) as! UILabel
        
        if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.EventType { // Event Type
            let eventTypeAtItem = self.eventTypes[indexPath.item]
            label.text = eventTypeAtItem.name
            
            if FilterManager.spaceFilter.eventTypes.contains(eventTypeAtItem) {
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-filled")
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
                cell.isSelected = true
            }
            else {
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-empty")
                collectionView.deselectItem(at: indexPath, animated: true)
                cell.isSelected = false
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.SpaceType { // Space Type
            label.text = StaticData.SPACE_TYPES[indexPath.item + 1]
            
            if FilterManager.spaceFilter.spaceType.contains(indexPath.item + 1) {
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-filled")
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
                cell.isSelected = true
            }
            else {
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-empty")
                collectionView.deselectItem(at: indexPath, animated: true)
                cell.isSelected = false
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.SpaceCapacity { // Guest Count
            let guestCountAtItem = self.spaceCapacityOptions[indexPath.item]
            label.text = guestCountAtItem["displayValue"] as? String
            
            if FilterManager.spaceFilter.spaceCapacity.contains(indexPath.item) {
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-filled")
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
                cell.isSelected = true
            }
            else {
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-empty")
                collectionView.deselectItem(at: indexPath, animated: true)
                cell.isSelected = false
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.Amenities { // Amenities
            let amenityAtItem = self.amenities[indexPath.item]
            label.text = amenityAtItem.name
            
            if FilterManager.spaceFilter.amenities.contains(amenityAtItem) {
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-filled")
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
                cell.isSelected = true
            }
            else {
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-empty")
                collectionView.deselectItem(at: indexPath, animated: true)
                cell.isSelected = false
            }
        }
        return cell
    }
}


extension FilterViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (UIScreen.main.bounds.width - 20) / 2
        return CGSize(width: cellWidth, height: 40)
    }
}


extension FilterViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageView = (collectionView.cellForItem(at: indexPath))!.viewWithTag(1) as! UIImageView
        
        if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.EventType { // EventTypes
            let eventTypeAtItem = self.eventTypes[indexPath.item]
            if !FilterManager.spaceFilter.eventTypes.contains(eventTypeAtItem) {
                FilterManager.spaceFilter.eventTypes.append(eventTypeAtItem)
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.SpaceType { // Space Type
            if !FilterManager.spaceFilter.spaceType.contains(indexPath.item + 1) {
                FilterManager.spaceFilter.spaceType.append(indexPath.item + 1)
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.SpaceCapacity { // Space Capacity
            if !FilterManager.spaceFilter.spaceCapacity.contains(indexPath.item) {
                FilterManager.spaceFilter.spaceCapacity.append(indexPath.item)
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.Amenities { // Amenities
            let amenityAtItem = self.amenities[indexPath.item]
            if !FilterManager.spaceFilter.amenities.contains(amenityAtItem) {
                FilterManager.spaceFilter.amenities.append(amenityAtItem)
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let imageView = (collectionView.cellForItem(at: indexPath))!.viewWithTag(1) as! UIImageView
        
        if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.EventType { // EventTypes
            let eventTypeAtItem = self.eventTypes[indexPath.item]
            if let index = FilterManager.spaceFilter.eventTypes.index(of: eventTypeAtItem) {
                FilterManager.spaceFilter.eventTypes.remove(at: index)
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.SpaceType { // Space Type
            if let index = FilterManager.spaceFilter.spaceType.index(of: indexPath.item + 1) {
                FilterManager.spaceFilter.spaceType.remove(at: index)
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.SpaceCapacity { // Space Capacity
            if let index = FilterManager.spaceFilter.spaceCapacity.index(of: indexPath.item) {
                FilterManager.spaceFilter.spaceCapacity.remove(at: index)
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if collectionView.tag == FILTER_TABLE_VIEW_SECTIONS.Amenities { // Amenities
            let amenityAtItem = self.amenities[indexPath.item]
            if let index = FilterManager.spaceFilter.amenities.index(of: amenityAtItem) {
                FilterManager.spaceFilter.amenities.remove(at: index)
                imageView.image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
    }
}


extension FilterViewController : GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        FilterManager.spaceFilter.location = place
        self.filtersTableView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
