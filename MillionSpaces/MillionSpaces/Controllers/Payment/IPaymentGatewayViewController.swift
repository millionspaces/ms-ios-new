//
//  IPaymentGatewayViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class IPaymentGatewayViewController: BaseViewController {
    
    @IBOutlet weak var timerBarButton: UIBarButtonItem!
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var paymentResultView: UIView!
    @IBOutlet weak var paymentResultImageView: UIImageView!
    @IBOutlet weak var paymentResultLabel: UILabel!
    @IBOutlet weak var paymentResultMessageLabel: UILabel!
    @IBOutlet weak var paymentResultButton: UIButton!
    
    var chargeAmount: Int!
    var orderId: Int!
    var bookingId: String!
    
    var isPaymentSuccessful = false
    
    var alertController: UIAlertController?
    var timerAlertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Payment"
        
        self.webView.delegate = self
        
        self.handleIpg(amount: Double(self.chargeAmount))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addPaymentSessionTimerLabelToNavBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePaymentSessionTimerInProgress(_:)), name: NSNotification.Name(rawValue: Constants.PAYMEMT_SESSION_IN_PROGRESS), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePaymentSessionTimerExpiry), name: NSNotification.Name(rawValue: Constants.PAYMEMT_SESSION_IS_UP), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(popToHomeWhenAppGoesForeground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        
        self.paymentResultView.isHidden = true
        
        print("IPGVC orderId : \(orderId!)")
        print("IPGVC bookingId : \(bookingId!)")
        print("IPGVC chargeAmount : \(chargeAmount!)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.removePaymentSessionTimerLabelFromNavBar()
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func popToHomeWhenAppGoesForeground() {
        alertController?.dismiss(animated: false, completion: nil)
        timerAlertController?.dismiss(animated: false, completion: nil)
        self.popToRootViewController()
    }
    
    @objc func handlePaymentSessionTimerInProgress(_ notification: Notification) {
        if let timerValue = notification.userInfo?["timer"] {
            print("IPGVC handlePaymentSessionTimerInProgress | timerValue : \(timerValue)")
            self.setPaymentSessionTimerLabelText(text: timerValue as! String)
        }
    }
    
    @objc func handlePaymentSessionTimerExpiry() {
        alertController?.dismiss(animated: false, completion: nil)
        
        timerAlertController = UIAlertController(title: AlertMessages.ALERT_TITLE, message: AlertMessages.PAYMENT_SESSION_TIME_UP_IPG, preferredStyle: .alert)
        let returnHomeAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
            self.popToRootViewController()
        }
        timerAlertController!.addAction(returnHomeAction)
        self.present(timerAlertController!, animated: true, completion: nil)
    }
    
    func viewBookingDetails() {
        print("IPGVC viewBooking")
        self.performSegue(segueID: "BookingDetailsSegue")
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.popViewController()
    }
    
    @IBAction func paymentResultViewProceedButtonPressed(_ sender: UIButton) {
        if isPaymentSuccessful {
            self.popToRootViewController()
        }
        else {
            self.paymentResultView.isHidden = true
            self.handleIpg(amount: Double(self.chargeAmount))
        }
    }
    
    
    func handleIpg(amount: Double) {
        guard let purchaseAmount = Utils.validatePurchaceAmount(amount: amount), purchaseAmount != "Invalid" else {
            // Error amount alert
            print("IPaymentGatewayVC handleIpg | Invalid amount")
            self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INVALID_AMOUNT_ERROR_MESSAGE)
            return
        }
        
        self.showActivityIndicator()
        
        let currentTimestamp = Int64(Date().timeIntervalSince1970 * 1000)
        let bookingIDWithTimestamp = "\(self.bookingId!)-\(currentTimestamp)"
        print("IPaymentGatewayVC handleIpg | bookingIDWithTimestamp : \(bookingIDWithTimestamp)")
        
        WebServiceCall.ipg(bookingId: bookingIDWithTimestamp, amount: purchaseAmount, successBlock: { (results: Any) in
            print("IPaymentGatewayVC handleIpg results : \(results)")
            DispatchQueue.main.sync(execute: {
                self.webView.loadHTMLString(results as! String, baseURL: URL(string: NetworkConfiguration.IPG))
                self.hideActivityIndicator()
            })
        }) { (errorCode: Int, error: String) in
            print("IPaymentGatewayVC handleIpg | failed | errorCode : \(errorCode), error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.prepareAlertForIPGFailure(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.prepareAlertForIPGFailure(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.prepareAlertForIPGFailure(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func prepareAlertForIPGFailure(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.handleIpg(amount: Double(self.chargeAmount))
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func prepareAlertForIPGCancellation() {
        alertController = UIAlertController(title: AlertMessages.ALERT_TITLE, message: AlertMessages.CANCEL_IPG_WARNING_MESSAGE, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertMessages.YES_ACTION, style: .default) { (alert: UIAlertAction) in
            self.popViewController()
        }
        let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default) { (alert: UIAlertAction) in
            self.handleIpg(amount: Double(self.chargeAmount))
        }
        alertController!.addAction(noAction)
        alertController!.addAction(yesAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    func preparePaymentResultViewForPaymentResponse_2(withResponseCode code: Int) {
        
        if code == 1 {
            PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
            self.setPaymentSessionTimerLabelText(text: "00:00")
            timerAlertController?.dismiss(animated: false, completion: nil)
            
            alertController = UIAlertController(title: "Payment Success", message: "Transaction is completed.", preferredStyle: .alert)
            let returnToHomeAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
                self.popToRootViewController()
            }
            
            let viewHomeAction = UIAlertAction(title: "View Booking", style: .default) { (alert: UIAlertAction) in
                self.viewBookingDetails()
            }
            
            alertController!.addAction(returnToHomeAction)
            alertController!.addAction(viewHomeAction)
        }
        else {
            alertController = UIAlertController(title: "Payment Failed", message: "Transaction did not complete.", preferredStyle: .alert)
            let returnToHomeAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
                self.popToRootViewController()
            }
            alertController!.addAction(returnToHomeAction)
        }
        
        self.present(alertController!, animated: true, completion: nil)
    }
    
    /*
     func preparePaymentResultViewForPaymentResponse(withResponseCode code: Int) {
     self.paymentResultView.isHidden = false
     
     if code == 1 {
     self.isPaymentSuccessful = true
     self.paymentResultImageView.image = #imageLiteral(resourceName: "ico-payment-success")
     self.paymentResultLabel.text = "PAYMENT SUCCESS"
     self.paymentResultLabel.textColor = UIColor.init(red: 0.0, green: 150.0/255.0, blue: 0.0, alpha: 1.0)
     self.paymentResultMessageLabel.text = "Transaction is approved!"
     self.paymentResultButton.setTitle("RETURN TO HOME", for: .normal)
     }
     else {
     self.isPaymentSuccessful = false
     self.paymentResultImageView.image = #imageLiteral(resourceName: "ico-payment-failed")
     self.paymentResultLabel.text = "PAYMENT FAILED"
     self.paymentResultLabel.textColor = UIColor.init(red: 200.0, green: 0.0, blue: 0.0, alpha: 1.0)
     self.paymentResultMessageLabel.text = "Transaction did not complete!"
     self.paymentResultButton.setTitle("DISMISS", for: .normal)
     }
     }*/
    
    /*
     Test Card Details
     // OLD
     
     visa
     4157385074550003
     12/17
     765
     
     master
     5488060111546002
     01/20
     087
     
     // NEW
     Type :VISA
     Card No - 4157 3851 0235 4006
     Exp - 08/20
     CVV – 341
     
     Type - MasterCard Card No - 5488060112235001
     Exp - 08/20
     CVV - 071
     */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookingDetailsSegue" {
            let bookingDetailsVC = segue.destination as! BookingDetailsViewController
            bookingDetailsVC.bookingId = self.orderId!
            bookingDetailsVC.isManualBooking = false
            bookingDetailsVC.isViewedAsGuest = true
            bookingDetailsVC.shouldReturnToPreviousScreen = false
        }
    }
}


// MARK: - UIWebViewDelegate
extension IPaymentGatewayViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.showActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hideActivityIndicator()
        
        let url = String(describing: webView.request?.mainDocumentURL)
        print("IPaymentGatewayVC UIWebView webViewDidFinishLoad | url : \(url)")
        
        if url.range(of: NetworkConfiguration.BASE_URL + "/#/payments/response?code=1") != nil {
            print("IPaymentGatewayVC UIWebView webViewDidFinishLoad | PAYMENT RESPONSE RECEIVED - Transaction is approved")
            
            self.preparePaymentResultViewForPaymentResponse_2(withResponseCode: 1)
        }
        else if url.range(of: NetworkConfiguration.BASE_URL + "/#/payments/response?code=3&reasonCode=401&orderId=\(self.orderId!)&status=Cycle+interrupted+by+the+user+or+client%2Fbrowser+connection+not+available") != nil {
            print("IPaymentGatewayVC UIWebView webViewDidFinishLoad | Common browser error in IPG")
            self.handleIpg(amount: Double(self.chargeAmount))
        }
        else if url.range(of: NetworkConfiguration.BASE_URL + "/#/payments/response?code=3&reasonCode=36&orderId=\(self.orderId!)&status=Credit+Card+holder+canceled+the+request") != nil {
            self.prepareAlertForIPGCancellation()
        }
        else if url.range(of: NetworkConfiguration.BASE_URL + "/#/payments/response?code=") != nil {
            self.preparePaymentResultViewForPaymentResponse_2(withResponseCode: 0)
        }
    }
    
}



