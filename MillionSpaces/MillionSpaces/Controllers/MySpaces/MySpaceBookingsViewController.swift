//
//  MySpaceBookingsViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class MySpaceBookingsViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var pastSegmentedButton: UIButton!
    @IBOutlet weak var upcomingSegmentedButton: UIButton!
    @IBOutlet weak var mySpaceBookingsTableView: UITableView!
    @IBOutlet weak var noBookingsMessageLabel: UILabel!
    
    //MARK: - Class variables
    var mySpaceID: Int!
    var mySpaceBookingsUpcoming: [Booking] = []
    var mySpaceBookingsPast: [Booking] = []
    let noUpcomingBookingsMessage = "Currently there's no upcoming bookings for this space."
    let noPastBookingsMessage = "Currently there's no past bookings for this space."
    var selectedSegment: Int = 2 // 1 - Past / 2 - Upcoming
    
    var selectedBooking: Booking?
    
    var alertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToMySpaceBookingsFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToMySpaceBookingsFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) { // ~ 10s
            alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = "Bookings"
        configureSelectedUpcomingSegmentedButton()
        retrieveMySpaceBookings()
    }
    
    func showPastBookings() {
        configureSelectedPastSegmentedButton()
        self.selectedSegment = 1
        self.noBookingsMessageLabel.text = noPastBookingsMessage
        if self.mySpaceBookingsPast.count > 0 {
            self.mySpaceBookingsTableView.isHidden = false
            self.noBookingsMessageLabel.isHidden = true
            self.mySpaceBookingsTableView.reloadData()
            self.mySpaceBookingsTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
        else {
            self.mySpaceBookingsTableView.isHidden = true
            self.noBookingsMessageLabel.isHidden = false
        }
    }
    
    func showUpcomingBookings() {
        configureSelectedUpcomingSegmentedButton()
        self.selectedSegment = 2
        self.noBookingsMessageLabel.text = noUpcomingBookingsMessage
        if self.mySpaceBookingsUpcoming.count > 0 {
            self.mySpaceBookingsTableView.isHidden = false
            self.noBookingsMessageLabel.isHidden = true
            self.mySpaceBookingsTableView.reloadData()
            self.mySpaceBookingsTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
        else {
            self.mySpaceBookingsTableView.isHidden = true
            self.noBookingsMessageLabel.isHidden = false
        }
    }
    
    func configureSelectedPastSegmentedButton() {
        self.pastSegmentedButton.isSelected = true
        self.pastSegmentedButton.backgroundColor = ColorUtils.MS_BLUE_COLOR
        self.upcomingSegmentedButton.isSelected = false
        self.upcomingSegmentedButton.backgroundColor = UIColor.white
    }
    
    func configureSelectedUpcomingSegmentedButton() {
        self.pastSegmentedButton.isSelected = false
        self.pastSegmentedButton.backgroundColor = UIColor.white
        self.upcomingSegmentedButton.isSelected = true
        self.upcomingSegmentedButton.backgroundColor = ColorUtils.MS_BLUE_COLOR
    }
    
    func retrieveMySpaceBookings() {
        self.showActivityIndicator()
        
        self.mySpaceBookingsUpcoming.removeAll()
        self.mySpaceBookingsPast.removeAll()
        
        WebServiceCall.retrieveMySpaceBookings(forMySpace: self.mySpaceID!, successBlock: { (results: Any) in
            //print("MySpaceBookingsViewController retrieveMySpaceBookings | success results : \(results)")
            
            if let arrMySpaceBookings = results as? [[String : Any]] {
                
                for b in arrMySpaceBookings {
                    let booking = Booking()
                    booking.initBooking(withDictionary: b)
                    
                    if booking.isUpcomingBooking {
                        self.mySpaceBookingsUpcoming.append(booking)
                    }
                    else {
                        self.mySpaceBookingsPast.append(booking)
                    }
                }
                
                DispatchQueue.main.sync(execute: {
                    self.showUpcomingBookings()
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    //self.addSpacesReloadAlert(message: AlertMessages.INTERNAL_ERROR)
                    self.mySpaceBookingsTableView.isHidden = true
                    self.noBookingsMessageLabel.isHidden = false
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("MySpaceBookingsViewController retrieveMySpaceBookings | failure | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.mySpaceBookingsTableView.reloadData()
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpacesReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpacesReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpacesReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func addSpacesReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retrieveMySpaceBookings()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    
    //MARK: - IBActions
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func pastSegmentedButtonPressed(_ sender: UIButton) {
        //print("MySpaceBookingsVC pastSegmentedButtonPressed")
        showPastBookings()
    }
    
    @IBAction func upcomingSegmentedButtonPressed(_ sender: UIButton) {
        //print("MySpaceBookingsVC upcomingSegmentedButtonPressed")
        showUpcomingBookings()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookingDetailsSegue" {
            let bookingDetailsVC = segue.destination as! BookingDetailsViewController
            bookingDetailsVC.bookingId = self.selectedBooking!.id!
            bookingDetailsVC.isManualBooking = self.selectedBooking!.isManual!
            bookingDetailsVC.isViewedAsGuest = false
            bookingDetailsVC.shouldReturnToPreviousScreen = true
        }
    }
    
}


/*
 main view - 1
 ref id - 2
 client name - 3
 from date - 4
 to date - 5
 amount - 6
 status - 7
 m logo - 8
 */


extension MySpaceBookingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedSegment == 1 {
            return self.mySpaceBookingsPast.count
        }
        return self.mySpaceBookingsUpcoming.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let mySpacesCell = tableView.dequeueReusableCell(withIdentifier: "MySpaceBookingCell", for: indexPath)
        
        if let parentView = mySpacesCell.viewWithTag(1) {
            parentView.layer.shadowColor = UIColor.black.cgColor
            parentView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
            parentView.layer.shadowRadius = 1
            parentView.layer.shadowOpacity = 0.5
        }
        
        let bookingAtIndex = (selectedSegment == 1) ? self.mySpaceBookingsPast[indexPath.row] : self.mySpaceBookingsUpcoming[indexPath.row]
        
        (mySpacesCell.viewWithTag(2) as! UILabel).text = "Reference ID: \(bookingAtIndex.orderId!)"
        (mySpacesCell.viewWithTag(3) as! UILabel).text = bookingAtIndex.bookingByUserName
        
        (mySpacesCell.viewWithTag(8) as! UIImageView).isHidden = bookingAtIndex.isManual
        
        (mySpacesCell.viewWithTag(6) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: bookingAtIndex.bookingCharge))"
        
        let statusLabel = mySpacesCell.viewWithTag(7) as! UILabel
        statusLabel.text = bookingAtIndex.bookingReservationStatusDisplayName
        statusLabel.backgroundColor = ColorUtils.BOOKING_RESERVATION_STATUS_COLORS_BY_CODE[bookingAtIndex.bookingReservationStatusId]!
        
        let fromLabel = mySpacesCell.viewWithTag(4) as! UILabel
        let toLabel = mySpacesCell.viewWithTag(5) as! UILabel
        
        if bookingAtIndex.bookingDates.count > 0 {
            fromLabel.text = "From   \(CalendarUtils.retrieveSimpleDateString_2(fromDateStr: bookingAtIndex.bookingDates[0]["fromDate"]![0]))   \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: bookingAtIndex.bookingDates[0]["fromDate"]![1], isWithSeconds: false))"
            toLabel.text = "To        \(CalendarUtils.retrieveSimpleDateString_2(fromDateStr: bookingAtIndex.bookingDates[0]["toDate"]![0]))   \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: bookingAtIndex.bookingDates[0]["toDate"]![1], isWithSeconds: false))"
            if bookingAtIndex.hasMultipleBlocks { toLabel.text?.append(" ✚") }
        }
        else {
            fromLabel.text = "N/A"
            toLabel.text = "N/A"
        }
        
        return mySpacesCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedBooking = (selectedSegment == 1) ? self.mySpaceBookingsPast[indexPath.row] : self.mySpaceBookingsUpcoming[indexPath.row]
        self.performSegue(segueID: "BookingDetailsSegue")
    }
}

