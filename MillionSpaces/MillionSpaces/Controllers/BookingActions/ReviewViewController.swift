//
//  ReviewViewController.swift
//  MillionSpaces
//
//  Created by Ishan Sirimanne on 11/8/18.
//  Copyright © 2018 MillionSpaces. All rights reserved.
//

import UIKit

class ReviewViewController: BaseViewController, UITextViewDelegate, UITextFieldDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var constContainerViewTop: NSLayoutConstraint! //  Initially - 10.0
    @IBOutlet weak var reviewTItleText: UITextField!
    @IBOutlet weak var reviewDescriptionTextView: UITextView!
    
    @IBOutlet weak var submitButton: UIButton!
    
    // MARK: - Class variables
    var booking: Booking!
    
    var reviewTitle: String!
    var reviewDescription: String!
    var reviewScoreService = 0
    var reviewScoreCleanliness = 0
    var reviewScoreValue = 0
    
    var hasValidReviewDetailsToSubmit = false
    
    var alertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ReviewBookingVC viewDidLoad")
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToReviewFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func returnedToReviewFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) { // ~ 10s
            alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = "Review"
        
        //self.addKeyBoardDismissOnTap()
        
        validateReviewDetails()
    }
    
    func validateReviewDetails () {
        
        reviewTitle = (self.reviewTItleText.text!.count > 0 && self.reviewTItleText.hasText) ? self.reviewTItleText.text : nil
        reviewDescription = (self.reviewDescriptionTextView.text!.count > 0 && self.reviewDescriptionTextView.hasText) ? self.reviewDescriptionTextView.text : nil
        
        //print("ReviewBookingVC validateReviewDetails | reviewScoreService = \(reviewScoreService)")
        //print("ReviewBookingVC validateReviewDetails | reviewScoreCleanliness = \(reviewScoreCleanliness)")
        //print("ReviewBookingVC validateReviewDetails | reviewScoreValue = \(reviewScoreValue) \n")
        
        if reviewTitle != nil && reviewDescription != nil && reviewScoreService > 0 && reviewScoreCleanliness > 0 && reviewScoreValue > 0 {
            self.submitButton.backgroundColor = ColorUtils.MS_BLUE_COLOR
            hasValidReviewDetailsToSubmit = true
        }
        else {
            self.submitButton.backgroundColor = UIColor.lightGray
            hasValidReviewDetailsToSubmit = false
        }
    }
    
    func reviewBooking(withRatingStr rating: String) {
        self.showActivityIndicator()
        
        WebServiceCall.addBookingReview(withBookingID: booking.id!, title: reviewTitle!, rate: rating, reviewDescription: reviewDescription!, successBlock: { (results: Any) in
            print("ReviewBookingVC reviewBooking | results : \(results)")
            
            if let response = results as? String, response.contains("review successfully added") {
                self.presentResponseAlert(isSuccessful: true)
            }
            else {
                self.presentResponseAlert(isSuccessful: false)
            }
            
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("ReviewBookingVC reviewBooking | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func presentResponseAlert(isSuccessful: Bool) {
        //var alertController: UIAlertController!
        
        if isSuccessful {
            alertController = UIAlertController(title: AlertMessages.SUCCESS_TITLE, message: AlertMessages.REVIEW_BOOKING_SUCCESS_MESSAGE, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
                self.popToRootViewController()
            }
            alertController!.addAction(yesAction)
        }
        else {
            alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.REVIEW_BOOKING_ERROR_MESSAGE, preferredStyle: .alert)
            let noAction = UIAlertAction(title: AlertMessages.NO_ACTION, style: .default, handler: nil)
            let retryAction = UIAlertAction(title: AlertMessages.RETRY_ACTION, style: .default) { (alert: UIAlertAction) in
                self.popToRootViewController()
            }
            alertController!.addAction(noAction)
            alertController!.addAction(retryAction)
        }
        
        self.present(alertController!, animated: true, completion: nil)
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text!.trimmingCharacters(in: .whitespaces)
        validateReviewDetails()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.reviewDescriptionTextView.becomeFirstResponder()
        return true
    }
    
    // MARK: - UITextViewDelegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.constContainerViewTop.constant =  -150.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = textView.text.replacingOccurrences(of: "\n", with: " ")
        textView.text = textView.text!.trimmingCharacters(in: .whitespaces)
        self.constContainerViewTop.constant =  10.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        validateReviewDetails()
    }
    
    
    
    // MARK: - UITextFieldDelegate
    
    @IBAction func serviceRatingButtonPressed(_ sender: UIButton) {
        for i in 1...5 {
            if i <= sender.tag {
                (self.view.viewWithTag(i) as! UIButton).isSelected = true
            }
            else {
                (self.view.viewWithTag(i) as! UIButton).isSelected = false
            }
        }
        
        reviewScoreService = sender.tag
        validateReviewDetails()
    }
    
    
    @IBAction func cleanlinessRatingButtonPressed(_ sender: UIButton) {
        for i in 6...10 {
            if i <= sender.tag {
                (self.view.viewWithTag(i) as! UIButton).isSelected = true
            }
            else {
                (self.view.viewWithTag(i) as! UIButton).isSelected = false
            }
        }
        
        reviewScoreCleanliness = sender.tag - 5
        validateReviewDetails()
    }
    
    @IBAction func valueForMoneyRatingButtonPressed(_ sender: UIButton) {
        for i in 11...15 {
            if i <= sender.tag {
                (self.view.viewWithTag(i) as! UIButton).isSelected = true
            }
            else {
                (self.view.viewWithTag(i) as! UIButton).isSelected = false
            }
        }
        
        reviewScoreValue = sender.tag - 10
        validateReviewDetails()
    }
    
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        if hasValidReviewDetailsToSubmit {
            let reviewScoreOverall = (reviewScoreService + reviewScoreCleanliness + reviewScoreValue) / 3
            let ratingStr = "\(reviewScoreOverall)|\(reviewScoreService)|\(reviewScoreCleanliness)|\(reviewScoreValue)"
            self.reviewBooking(withRatingStr: ratingStr)
            //print("ReviewVC submitButtonPressed | reviewScoreOverall = \(reviewScoreOverall)")
            //print("ReviewVC submitButtonPressed | ratingStr = \(ratingStr)")
        }
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
